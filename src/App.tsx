import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import history from "./history";
import { Provider } from "react-redux";
import { initializeStore } from "./store";
import PostLoginRouting from "./components/PostLoginRouting";
import LandingPage from "./components/LandingPage";
import azureB2C from "react-azure-adb2c";
import TeamContainer from "./components/Team";
import CheckpointsList from "./components/CheckpointsList/CheckpointsList";
import QrCodeForm from "./components/QrCodeForm/QrCodeForm";
import QrCode from "./components/QrCodeForm/QrCode";
import AddQuest from "./components/AddQuest/QuestFormContainer";
import TeamGameContainer from "./components/TeamGame";
import GameDefinitionFormContainer from "./components/GameDefinitionFormContainer";
import GameMasterNavigationWrapper from "./components/GameMasterNavigationWrapper/";
import CheckpointCreate from "./components/CheckpointCreate/CheckpointCreate";
import { routes } from "./static/routesUrl";
import StartGameScreen from "./components/StartGameScreen";
import StartGameQrScanner from "./components/StartGameScreen/StartGameQrScanner";
import GameStateRouting from "./components/GameStateRouting";
import NextCheckpoint from "./components/NextCheckpoint";
import NextCheckpointQrScanner from "./components/NextCheckpoint/NextCheckpointQrScanner";
import InvalidQrCodeScreen from "./components/QrCodeScanner/InvalidQrCodeScreen";
import RouteEditor from "./components/RouteEditor/RouteEditor";
import GameDefinitionsList from "./components/GameDefinitionsList/GameDefinitionsList";
import TeamsList from "./components/TeamsList";
import TermsAndConditions from "./components/TermsAndConditions";
import AboutGameContainer from "./components/TeamGame/AboutGame";
import ConnectionStatus from "./shared/ConnectionStatus";
import StartQuest from "./components/StartQuest/StartQuest";
import CheckpointQuest from "./components/CheckpointQuest";

const styles = createStyles({});

interface Props extends WithStyles<typeof styles> {
  setLanguage: (lang: string) => {};
}
const store = initializeStore();
const App: React.FC<Props> = ({ setLanguage }) => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={LandingPage} />

          <Route exact path={routes.authenticated} component={azureB2C.required(PostLoginRouting)} />
          <Route exact path={[routes.createTeam, routes.team]} component={azureB2C.required(TeamContainer)} />
          <Route exact path={routes.game} component={azureB2C.required(TeamGameContainer)} />
          <Route exact path={routes.startGame} component={azureB2C.required(StartGameScreen)} />
          <Route exact path={routes.startGameQrScanner} component={azureB2C.required(StartGameQrScanner)} />
          <Route exact path={routes.gameStateRouting} component={azureB2C.required(GameStateRouting)} />
          <Route exact path={routes.nextCheckpoint} component={azureB2C.required(NextCheckpoint)} />
          <Route exact path={routes.nextCheckpointQrScanner} component={azureB2C.required(NextCheckpointQrScanner)} />
          <Route exact path={routes.invalidQrCode} component={azureB2C.required(InvalidQrCodeScreen)} />
          <Route exact path={routes.termsAndConditions} component={azureB2C.required(TermsAndConditions)} />
          <Route exact path={routes.aboutGame} component={azureB2C.required(AboutGameContainer)} />
          <Route exact path={routes.startQuest} component={azureB2C.required(StartQuest)} />
          <Route exact path={routes.questInProgress} component={azureB2C.required(CheckpointQuest)} />

          <GameMasterNavigationWrapper setLanguage={lang => setLanguage(lang)}>
            <Route exact path={routes.createGame} component={azureB2C.required(GameDefinitionFormContainer)} />
            <Route exact path={routes.checkpointCreate} component={azureB2C.required(CheckpointCreate)} />
            <Route exact path={routes.checkpointEdit(":guid")} component={azureB2C.required(CheckpointCreate)} />
            <Route exact path={routes.checkpointList} component={azureB2C.required(CheckpointsList)} />
            <Route exact path={routes.createRoute} component={azureB2C.required(RouteEditor)} />
            <Route exact path={routes.createQuest} component={azureB2C.required(AddQuest)} />
            <Route exact path={routes.checkpointQrCodeForm(":guid")} component={azureB2C.required(QrCodeForm)} />
            <Route exact path={routes.checkpointQrCode(":guid")} component={azureB2C.required(QrCode)} />
            <Route exact path={routes.gameDefinitionsList} component={azureB2C.required(GameDefinitionsList)} />
            <Route exact path={routes.gameDefinitionQrCodeForm(":guid")} component={azureB2C.required(QrCodeForm)} />
            <Route exact path={routes.gameDefinitionQrCode(":guid")} component={azureB2C.required(QrCode)} />
            <Route exact path={routes.teamsList} component={azureB2C.required(TeamsList)} />
          </GameMasterNavigationWrapper>
        </Switch>
      </Router>
      <ConnectionStatus />
    </Provider>
  );
};

export default withStyles(styles)(App);
