import React from "react";
import { shallow } from "enzyme";
import App from "./App";

it("renders without crashing", () => {
  const sut = shallow(<App />);
  expect(sut.isEmptyRender()).toBe(false);
});
