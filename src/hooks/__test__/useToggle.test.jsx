import React from "react";
import { shallow } from "enzyme";
import useToggle from "../useToggle";

describe("useToggle", () => {
  let wrapper;

  function ToggleWrapper() {
    return <div hook={useToggle()} />;
  }

  beforeEach(() => {
    wrapper = shallow(<ToggleWrapper />);
  });

  it("should set a default value as false", () => {
    const [toggle] = wrapper.prop("hook");
    expect(toggle).toBe(false);
  });

  it("should toggle value after invoking a function", () => {
    const [toggle, setToggle] = wrapper.prop("hook");

    setToggle();

    const [updatedToggle] = wrapper.prop("hook");
    expect(updatedToggle).toBe(true);
  });
});
