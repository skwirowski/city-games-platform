import { useState } from "react";

function useToggle(initialVal: boolean = false): [boolean, () => void] {
  const [toggle, setToggle] = useState(initialVal);

  const toggleValue = () => {
    setToggle(!toggle);
  };
  return [toggle, toggleValue];
}

export default useToggle;
