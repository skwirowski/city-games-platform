import React from "react";
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from "enzyme";
import LoadingScreen from "../LoadingScreen";

describe("LoadingScreen component", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedLoadingScreen = unwrap(LoadingScreen);
const createSut = props =>
  shallow(<UnwrappedLoadingScreen classes={{}} {...props} />);
