import React from "react";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import bg from "../../assets/images/bg_blur.jpg";
import * as colors from "../../shared/colors";

const styles = createStyles({
  container: {
    width: "100%",
    height: "100%",
    background: `url(${bg}) no-repeat top fixed`,
    backgroundSize: "cover",
    position: "relative"
  },
  loader: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    margin: "auto",
    color: colors.secondary
  }
});

interface Props extends WithStyles<typeof styles> {}

const LoadingScreen: React.SFC<Props> = ({ classes }) => {
  return (
    <div className={classes.container}>
      <CircularProgress className={classes.loader} />
    </div>
  );
};

export default withStyles(styles)(LoadingScreen);
