import React, { useEffect, useState } from "react";
import { Redirect } from "react-router";
import { getUserData } from "../../services/auth";
import { routes } from "../../static/routesUrl";
import { checkWhetherIsGameMaster } from "../../services/checkWhetherIsGameMaster";
import { checkWhetherTeamIsRegistered } from "../../services/checkWhetherTeamIsRegistered";
import LoadingScreen from "./LoadingScreen";

const getTeamsPath = async (uuid: string) => {
  const isTeamRegistered = await checkWhetherTeamIsRegistered(uuid);
  if (isTeamRegistered) return routes.game;
  return routes.team;
};

const getPath = async () => {
  const { uuid } = getUserData();
  const isGameMaster = await checkWhetherIsGameMaster(uuid);
  if (isGameMaster) return routes.createGame;
  return getTeamsPath(uuid);
};

interface Props {}

const PostLoginRouting: React.FC<Props> = () => {
  const [path, setPath] = useState("");
  useEffect(() => {
    getPath().then(path => {
      setPath(path);
    });
  }, []);
  return path ? <Redirect to={path} /> : <LoadingScreen />;
};

export default PostLoginRouting;
