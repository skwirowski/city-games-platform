import React, { useState } from "react";
import { WithStyles, withStyles, TextField, Button, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import Intl from "../../utils/Intl";
import httpGameDefinition from "../../services/gameDefinitionApi";
import httpRoute from "../../services/routeServiceApi";
import { encryptString } from "./utils/hashingFunctions";
import { qrCodeHashKey } from "./utils/constants";
import { styles } from "./QrCodeFormStyles";

interface QrCodeFormProps extends WithStyles<typeof styles> {
  history: {
    push: (arg1: string, arg2: {}) => {};
  };
  match: {
    params: {
      guid: string;
    };
  };
  location: {
    state: {
      route: string;
      description: string;
      label: string;
      helperText: string;
    };
  };
}

const QrCodeForm: React.FC<QrCodeFormProps> = ({ classes, history, match, location }) => {
  const [qrCodeIdentifier, setQrCodeIdentifier] = useState<string>("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQrCodeIdentifier(event.target.value);
  };

  const { route, description, label, helperText } = location.state;

  const onGenerateButtonClick = () => {
    const hashedMessage = encryptString(qrCodeIdentifier, qrCodeHashKey);
    const guid = match.params.guid;

    const postGameDefinitionData = async () => {
      httpGameDefinition
        .post(`/api/GameDefinition/${guid}/qrcode`, {
          gameDefinitionGuid: guid,
          qrCodeContent: hashedMessage
        })
        .then(response => response.status);
    };

    const postRouteData = async () => {
      httpRoute
        .put(`/api/Checkpoints/${guid}/qrcode`, {
          checkpointGuid: guid,
          qrCodeContent: hashedMessage
        })
        .then(response => response.status);
    };

    route === "/game-master/checkpoint" ? postRouteData() : postGameDefinitionData();

    history.push(`${route}/${guid}/qr-code`, { value: hashedMessage });
  };

  return (
    <div className={classes.container}>
      <Typography variant="h2">
        <FormattedMessage id="QrCodeForm.pageHeading" />
      </Typography>

      <div className={classes.wrapper}>
        <Typography variant="subtitle1">
          <FormattedMessage id={description} />
        </Typography>
        <form className="qr-code-form-input">
          <TextField
            className={classes.textfield}
            label={Intl.messages[label]}
            placeholder={Intl.messages["QrCodeForm.identifierInputPlaceholder"]}
            helperText={Intl.messages[helperText]}
            value={qrCodeIdentifier}
            onChange={handleChange}
            variant="outlined"
            margin="normal"
          />
          <Button className={classes.button} size="large" onClick={() => onGenerateButtonClick()} variant="outlined">
            <FormattedMessage id="QrCodeForm.generateButtonText" />
          </Button>
        </form>
      </div>
    </div>
  );
};

export default withStyles(styles)(QrCodeForm);
