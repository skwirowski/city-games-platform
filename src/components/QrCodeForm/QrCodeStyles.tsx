import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  container: {
    display: "flex",
    height: "100vh",
    width: "100vw",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  }
});
