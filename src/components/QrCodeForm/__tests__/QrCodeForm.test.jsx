import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import QrCodeForm from '../QrCodeForm';

describe('QrCodeForm', () => {
    let wrapper;
    beforeEach(() => {
        const QrCodeFormUnwrapped = unwrap(QrCodeForm);
        wrapper = shallow(<QrCodeFormUnwrapped classes={{textfield: 'textfield'}} />)
    });

    xit('should render without crash', () => {
        expect(wrapper).toBeTruthy();
    });
    xit('should call mock function on change event', () => {
        const inputElement = wrapper.find('.textfield');
        inputElement.props().onChange( {
            target: { value: 'example input value'}
        });
        expect(wrapper.find('.textfield').props().value).toEqual('example input value');
    });
});