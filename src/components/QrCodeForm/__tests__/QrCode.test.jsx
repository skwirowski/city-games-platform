import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import QrCode from '../QrCodeForm';

describe('QrCode', () => {
    let wrapper;
    beforeEach(() => {
        const QrCodeUnwrapped = unwrap(QrCode);
        wrapper = shallow(<QrCodeUnwrapped classes={{}} />)
    });

    xit('should render without crash', () => {
        expect(wrapper);
    });
});