import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  container: {
    display: "block",
    width: "90%",
    margin: "30px auto"
  },
  wrapper: {
    borderBottom: "2px solid #a7a7a7",
    paddingBottom: "30px",
    marginTop: "30px",
    "&:last-child": {
      borderBottom: "none"
    }
  },
  textfield: {
    width: "100%"
  },
  button: {
    display: "block",
    marginTop: "30px",
    marginLeft: "auto",
    marginRight: "auto"
  }
});
