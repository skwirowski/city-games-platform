import React from "react";
import { WithStyles, withStyles } from "@material-ui/core";
import QRCode from "qrcode.react";
import { styles } from "./QrCodeStyles";

interface QrCodeProps extends WithStyles<typeof styles> {
  location: {
    state: {
      value: string;
    };
  };
}

const QrCodeGenerator: React.FC<QrCodeProps> = ({ classes, location }) => {
  return (
    <div className={classes.container}>
      <QRCode value={location.state.value} size={256} fgColor="#000000" bgColor="#FFFFFF" level="L" renderAs="svg" />
    </div>
  );
};

export default withStyles(styles)(QrCodeGenerator);
