import CryptoJS from 'crypto-js';

export const encryptString: (textToHash: string, hashKey: string) => string =
  (stringToHash, hashKey) => {
  return CryptoJS.AES.encrypt(stringToHash, hashKey).toString();
}

export const decryptString: (textToDecrypt: string, hashKey: string) => string =
  (stringToDecrypt, hashKey) => {
    const decryptedBytes = CryptoJS.AES.decrypt(stringToDecrypt, hashKey);

    return decryptedBytes.toString(CryptoJS.enc.Utf8);
  }