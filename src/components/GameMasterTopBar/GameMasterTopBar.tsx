import React from "react";
import { WithStyles, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { styles } from "./GameMasterTopBarStyles";

interface IProps extends WithStyles<typeof styles> {
  setLanguage: (language: string) => void;
}

const CheckpointsList: React.FC<IProps> = ({ classes, setLanguage }) => {
  return (
    <div className={classes.container}>
      <div className={classes.welcomeMessage}>
        <FormattedMessage id="GameMasterTopBar.welcomeMessage" />
      </div>
      <div className={classes.languages}>
        <img src="/images/pl.svg" onClick={() => setLanguage("pl")} className={`${classes.flag} plFlag`} />
        <img src="/images/gb.svg" onClick={() => setLanguage("en")} className={`${classes.flag} enFlag`} />
      </div>
    </div>
  );
};
export default withStyles(styles)(CheckpointsList);
