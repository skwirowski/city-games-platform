import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils'
import GameMasterTopBar from '../GameMasterTopBar';
// import {mockedCheckpoints} from '../../mockedData';

describe('ErrorMessage', () => {
    let props = {
        setLanguage: jest.fn(),
        classes: {}
    }
    let UnwrappedGameMasterTopBar;
    let wrapper;

      beforeEach(() => {
        UnwrappedGameMasterTopBar = unwrap(GameMasterTopBar);
        wrapper = shallow(<UnwrappedGameMasterTopBar {...props}/>)
      })

    it('should render without crash', () => {
        expect(wrapper).toBeTruthy();
    });

    it('should invoke setLanguage function after click plFlag', () => {
        const plFlag = wrapper.find('.plFlag');
        plFlag.simulate('click');
        expect(props.setLanguage).toBeCalled();
    });

    it('should invoke setLanguage function after click enFlag', () => {
        const enFlag = wrapper.find('.enFlag');
        enFlag.simulate('click');
        expect(props.setLanguage).toBeCalled();
    });
})