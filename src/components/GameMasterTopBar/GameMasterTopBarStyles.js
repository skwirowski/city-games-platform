export const styles = {
    container: {
        "display": "flex",
        "justifyContent": "space-between",
        "width": "100%",
        "backgroundColor": "#EEF9FF",
        "borderBottom": "1px solid #008FD5"
    },
    welcomeMessage: {
        "display": "flex",
        "padding": "0.5rem 1rem",

    },
    languages: {
        "display": "flex",
        "padding": "0.5rem 1rem",
    },
    flag: {
        width: "2rem",
        marginLeft: "0.1rem",
        padding: "0",
        height: "1rem",
        cursor: "pointer"
    }
}