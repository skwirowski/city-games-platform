import React from "react";
import {
  WithStyles,
  withStyles,
  Grid,
  Typography,
  createStyles
} from "@material-ui/core";
import ArrowBack from "../../shared/ArrowBack";
import Intl from "../../utils/Intl";

const styles = createStyles({
  termsContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    background: "rgb(238, 249, 255)",
    margin: "0 auto",
    padding: "25px 30px 40px",
    minHeight: "80vh"
  },
  typographyPrimary: {
    color: "rgb(0,143, 213)",
    marginBottom: "10px",
    paddingLeft: "25px"
  },
  header: {
    marginTop: "20px",
    marginBottom: "10px"
  }
});

interface Props extends WithStyles<typeof styles> {
  history: any;
}

const TermsAndConditions: React.FC<Props> = ({ classes, history }) => {
  const handleGoBack = (e: any) => {
    e.preventDefault();
    history.goBack();
  };

  return (
    <Grid>
      <Grid>
        <Grid
          container
          direction="row"
          alignItems="center"
          justify="center"
          className={classes.header}
        >
          <Grid item xs={3}>
            <Grid onClick={e => handleGoBack(e)}>
              <ArrowBack />
            </Grid>
          </Grid>
          <Grid item xs={9}>
            <Typography variant="h4" className={classes.typographyPrimary}>
              {Intl.messages["TermsAndConditions.termsHeader"]}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid className={classes.termsContainer}>
        <Typography variant="subtitle1">
          {Intl.messages["TermsAndConditions.terms"]}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(TermsAndConditions);
