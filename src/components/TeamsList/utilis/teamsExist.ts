const teamsExist = (teams: any) => {
  return teams && teams.length > 0;
};

export default teamsExist;
