import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography,
  Button
} from "@material-ui/core";
import Intl from "../../../utils/Intl";

const styles = createStyles({
  listItemContainer: {
    background: "white",
    borderBottom: "1px solid black",
    padding: "5px",
    height: "auto"
  },
  teamListItem: {
    paddingLeft: "10px"
  },
  teamButton: {
    marginRight: "5px"
  }
});

interface Props extends WithStyles<typeof styles> {
  team: any;
  deleteTeam: (guid: string) => {};
  setIsModalOpen: (open: boolean) => void;
  setSelectedTeam: (team: any) => void;
}

const TeamsListItem: React.FC<Props> = ({ classes, team, deleteTeam, setIsModalOpen, setSelectedTeam }) => {
  const handleAttachRoute = (team:any) => {
    setSelectedTeam(team);
    setIsModalOpen(true);
  }
  return (
    <Grid container direction="row" className={classes.listItemContainer}>
      <Grid item xs={1} className={classes.teamListItem}>
        <Typography>{team.teamId}</Typography>
      </Grid>
      <Grid item xs={3} className={classes.teamListItem}>
        <Typography variant="subtitle2">{team.teamName}</Typography>
      </Grid>
      <Grid item xs={2} className={classes.teamListItem}>
        <Typography variant="subtitle2">{team.teamMembers.length}</Typography>
      </Grid>
      <Grid item xs={3} className={classes.teamListItem}>
        <Typography variant="subtitle2">{team.attachedRoute ? team.attachedRouteName : Intl.messages["TeamsList.noRoute"]}</Typography>
      </Grid>
      <Grid item xs={3}>
        <Grid container direction="row" justify="center">
          <Button
            variant="outlined"
            color="secondary"
            size="small"
            className={classes.teamButton}
            onClick={() => deleteTeam(team.teamUuid)}
          >
            {Intl.messages["TeamsList.delete"]}
          </Button>
            <Button
            variant="outlined"
            color="primary"
            size="small"
            className={classes.teamButton}
            onClick={() => handleAttachRoute(team)}
            >
            {
              team.attachedRoute ? <>{Intl.messages["TeamsList.changeRoute"]}</> :  <>{Intl.messages["TeamsList.assignRoute"]}</>
            }
            </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};
export default withStyles(styles)(TeamsListItem);
