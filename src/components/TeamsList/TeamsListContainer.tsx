import React, { useState, useEffect } from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import TeamsList from "./TeamsList";
import deleteTeamByAdmin from "../../services/deleteTeamByAdmin";
import fetchTeamsInGame from "../../services/fetchTeamsInGame";
import fetchRoutesForTeamInGames from "../../services/fetchRoutesForTeamInGames";
import fetchRoutes from "../../services/fetchRoutes";
import actualGameUuid from "../../static/gameDefinitionUuid";

const styles = createStyles({
  container: {
    width: "100%",
    flexDirection: "row"
  }
});
interface Props extends WithStyles<typeof styles> {}

export interface Teams extends Array<Team> {}

export interface Team {
  guid: string;
  teamUuid: string;
  attachedRoute: string;
  teamName: string;
  teamId: number;
}

interface RouteForTeamsInGames {
  teamGuid: string;
  gameDefinitionGuid: string;
}

export interface Route {
  checkpoints: Array<string>;
  guid: string;
  routeName: string;
}

const TeamsListContainer: React.FC<Props> = ({ classes }) => {
  const [teams, setTeams] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        await setTeamList();
      } catch (error) {
        setError(error.message);
      }
    };
    fetchData();
  }, []);

  const setTeamList = async () => {
    const teamsInGame = await fetchTeamsInGame(actualGameUuid);
    const routesForTeamsInGames = await fetchRoutesForTeamInGames();

    const teamsWithRoutes = routesForTeamsInGames.map(
      (routeForTeamsInGames: RouteForTeamsInGames) =>
        routeForTeamsInGames.teamGuid
    );

    const res = await fetchRoutes();
    const routes = res.data;

    const teamsWithInfoAboutRoute = teamsInGame.map((team: Team) => {
      if (teamsWithRoutes.includes(team.teamUuid)) {
        const attachedRoute = routesForTeamsInGames.filter(
          (routeForTeamsInGames: RouteForTeamsInGames) => {
            return (
              routeForTeamsInGames.teamGuid === team.teamUuid &&
              routeForTeamsInGames.gameDefinitionGuid === actualGameUuid
            );
          }
        );
        const attachedRouteName = routes.filter((route: Route) => {
          return route.guid === attachedRoute[0].routeGuid;
        });
        return {
          ...team,
          attachedRoute: attachedRoute[0],
          attachedRouteName: attachedRouteName[0]
            ? attachedRouteName[0].routeName
            : null
        };
      } else {
        return { ...team, attachedRoute: null };
      }
    });

    await setTeams(teamsWithInfoAboutRoute);
  };

  const handleDeleteTeam = async (teamUuid: string) => {
    try {
      await deleteTeamByAdmin(teamUuid);
      const teamsInGame = await fetchTeamsInGame(actualGameUuid);
      setTeams(teamsInGame);
    } catch (error) {
      setError(error.message);
    }
  };
  return (
    <Grid className={classes.container}>
      <Grid>
        <Typography>{error ? `Error: ${error}` : ""}</Typography>
      </Grid>
      <TeamsList
        teams={teams}
        deleteTeam={handleDeleteTeam}
        setTeamList={setTeamList}
      />
    </Grid>
  );
};

export default withStyles(styles)(TeamsListContainer);
