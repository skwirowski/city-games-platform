import React, { useState } from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography,
  Dialog
} from "@material-ui/core";
import TeamsListItem from "./TeamsListItem/TeamsListItem";
import { FormattedMessage } from "react-intl";
import Intl from "../../utils/Intl";
import teamsExist from "./utilis/teamsExist";
import AttachRouteModal from "./AttachRouteModal/AttachRouteModal";
import { Teams, Team } from "./TeamsListContainer";
import createRouteForTeamInGame from "../../services/createRouteForTeamInGame";
import updateRouteForTeamInGame from "../../services/updateRouteForTeamInGame";
import actualGameUuid from "../../static/gameDefinitionUuid";
import { Route } from "./TeamsListContainer";

const styles = createStyles({
  container: {
    width: "100%",
    padding: "30px 100px 30px 30px",
    flexDirection: "column",
    alignItems: "center"
  },
  listHeader: {
    background: "lightgray",
    boxShadow: "2px 2px 5px black",
    padding: "10px"
  },
  listContainer: {
    marginTop: "20px",
    boxShadow: "2px 2px 5px black",
    Height: "100px",
    overflow: "auto"
  }
});

interface Props extends WithStyles<typeof styles> {
  teams: Teams;
  deleteTeam: (guid: string) => {};
  setTeamList: () => void;
}

export interface FormValues {
  startDate: any;
}

const TeamsList: React.FC<Props> = ({
  classes,
  teams,
  deleteTeam,
  setTeamList
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedTeam, setSelectedTeam] = useState({
    guid: "",
    teamUuid: "",
    attachedRoute: "",
    teamName: "",
    teamId: 0
  });
  const [selectedRoute, setSelectedRoute] = useState<Route>({
    guid: "",
    checkpoints: [""],
    routeName: ""
  });

  const handleSave = async (values: FormValues) => {
    const createRouteForTeamInGameValues = {
      teamGuid: selectedTeam.teamUuid,
      routeGuid: selectedRoute.guid,
      gameStartTimeForTeam: new Date(values.startDate),
      gameDefinitionGuid: actualGameUuid
    };
    try {
      if (selectedTeam.attachedRoute) {
        await updateRouteForTeamInGame(createRouteForTeamInGameValues);
      } else {
        await createRouteForTeamInGame(createRouteForTeamInGameValues);
      }
    } catch {}
    setTeamList();
    setIsModalOpen(false);

    return null;
  };
  return (
    <>
      <Grid className={classes.container}>
        <Grid container direction="row" className={classes.listHeader}>
          <Grid item xs={1}>
            <Typography variant="subtitle2">
              {Intl.messages["TeamsList.id"]}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography variant="subtitle2">
              {Intl.messages["TeamsList.teamName"]}
            </Typography>
          </Grid>
          <Grid item xs={2}>
            <Typography variant="subtitle2">
              {Intl.messages["TeamsList.numberTeamMembers"]}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography variant="subtitle2">
              {Intl.messages["TeamsList.attachedRoute"]}
            </Typography>
          </Grid>
          <Grid item xs={3} />
        </Grid>
        <Grid className={classes.listContainer}>
          {teamsExist(teams) ? (
            teams.map((team: Team) => {
              return (
                <TeamsListItem
                  team={team}
                  key={team.teamId}
                  deleteTeam={deleteTeam}
                  setIsModalOpen={setIsModalOpen}
                  setSelectedTeam={setSelectedTeam}
                />
              );
            })
          ) : (
            <FormattedMessage id="shared.loading" />
          )}
        </Grid>
        <Grid container direction="row" className={classes.listHeader}>
          <Grid>
            <Typography variant="subtitle2">
              {Intl.messages["TeamsList.numberAllTeams"]}
              {teamsExist(teams) ? (
                ` ${teams.length}`
              ) : (
                <FormattedMessage id="shared.loading" />
              )}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Dialog
        open={isModalOpen}
        disableBackdropClick={true}
        fullWidth
        maxWidth="lg"
      >
        <AttachRouteModal
          team={selectedTeam}
          setIsModalOpen={setIsModalOpen}
          setTeamList={setTeamList}
          submit={handleSave}
          setSelectedRoute={setSelectedRoute}
          selectedRoute={selectedRoute}
        />
      </Dialog>
    </>
  );
};
export default withStyles(styles)(TeamsList);
