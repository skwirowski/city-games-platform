import React, {useEffect, useState} from "react";
import { withFormik, Form, Field, FormikErrors, FieldProps } from "formik";
import * as Yup from "yup";
import fetchRoutes from "../../../services/fetchRoutes";
import fetchCheckpoints from "../../../services/fetchCheckpoints";
import {
    WithStyles,
    withStyles,
    Button,
    List,
    ListItem
  } from "@material-ui/core";
import classnames from "classnames";
import {styles} from "./AttachRouteModalStyles";
import Intl from "../../../utils/Intl";
import FDateTimePicker from "../../../shared/FormikComponents/FDateTimePicker";
import {FormValues} from "../TeamsList";
import {Route} from "../TeamsListContainer";
import SubmitButton from "../../../shared/SubmitButton";

interface Props extends WithStyles<typeof styles> {
    team: {
        guid: string;
        teamUuid: string;
        attachedRoute: string;
        teamName: string;
    };
    setIsModalOpen: (open: boolean) => void;
    setTeamList: () => void;
    setSelectedRoute: (route:Route) => void;
    selectedRoute: Route;
    submit: (values: FormValues) => Promise<FormikErrors<FormValues> | null>;
}

interface Checkpoint {
    guid: string;
}

const AttachRouteModal:React.FC<Props> = ({team, setIsModalOpen, classes, setTeamList, setSelectedRoute, selectedRoute, submit}) => {
    const [routes, setRoutes] = useState([{guid: "", checkpoints: [], routeName: ""}]);
    const [checkpointsFromSelectedRoute, setCheckpointsFromSelectedRoute] = useState([{name: "", guid: ""}]);

    useEffect(() => {
        const fetchData = async () => {
            const res = await fetchRoutes();
            setRoutes(res.data);
            handleSelectRoute(res.data[0]);
        }
        fetchData();
    }, [])

    const handleSelectRoute = async (route:Route) => {
        setSelectedRoute(route);
        let checkpoints = await fetchCheckpoints();
        checkpoints = checkpoints.filter(((checkpoint:Checkpoint) => {
            return route.checkpoints.includes(checkpoint.guid)
        }))
        setCheckpointsFromSelectedRoute(checkpoints);
    };


    return (
        <>
            <div className={classes.modalHeader}>
                {Intl.messages["AttachRouteModal.ModalHeader"]} {team.teamName}
            </div>
            <div className={classes.container}>
                <div>
                    <div className={classes.listHeader}>
                        {Intl.messages["AttachRouteModal.AvailableRoutes"]}
                    </div>
                    <List className={classes.list}>
                        {routes.map((route) => {
                            return <ListItem 
                                        className={route.guid === selectedRoute.guid ? 
                                                    classnames(classes.listElement, classes.routeListElement, classes.selected) : 
                                                    classnames(classes.listElement, classes.routeListElement)} 
                                        key={route.guid} 
                                        onClick={() => handleSelectRoute(route)}>
                                            {route.routeName}
                                    </ListItem>
                        })}
                    </List>
                    </div>
                <div>
                    <div className={classes.listHeader}>
                        {Intl.messages["AttachRouteModal.CheckpointsOnRoute"]}
                    </div>
                    <List className={classes.list}>
                        {checkpointsFromSelectedRoute.map((checkpoint) => {
                            return <ListItem key={checkpoint.guid} className={classes.listElement}>{checkpoint.name}</ListItem>
                        })}
                    </List>
                </div>
            </div>
            <div className={classes.modalFooter}> 
                <Form className={classes.modalFooter}>
                    <Field name="startDate">
                        {(props: FieldProps) => (
                            <FDateTimePicker
                                fieldProps={props}
                                label={Intl.messages["GameDefinitionFormFields.startDate"] }
                                className={classes.formInput}
                            />
                        )}
                    </Field>
                    <SubmitButton disabled={false} type="submit" className={classes.saveButton}>{Intl.messages["shared.saveButton"]}</SubmitButton>
                    <Button onClick={() => setIsModalOpen(false)}>{Intl.messages["shared.cancelButton"]}</Button>
                </Form>
            </div>
        </>
    )
}

export default withStyles(styles)(
    withFormik<Props, FormValues>({
      enableReinitialize: true,
      mapPropsToValues: (props) => {
          return {
              startDate: Date.now()
          }
      },
      validationSchema: () =>
      Yup.object().shape({ 
          startDate: Yup.date()
      }),
      handleSubmit: async (values, { props, setErrors }) => {
        const errors = await props.submit(values);
        if (errors) {
          setErrors(errors);
        }
      }
    })(AttachRouteModal));
