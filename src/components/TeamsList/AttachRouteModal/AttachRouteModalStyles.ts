import { createStyles } from "@material-ui/core";

export const styles = createStyles({
    container: {
        display: "grid",
        width: "100%",
        gridTemplateColumns: "1fr 1fr"
    },
    modalHeader: {
        width: "100%",
        textAlign: "center",
        fontSize: "2rem",
        fontWeight: "bold",
    },
    list: {
        margin: "1rem",
        overflowY: "auto",
        maxHeight: "70vh",
        padding: "1rem"
    },
    listHeader: {
        textAlign: "center",
        fontWeight: "bold"
    },
    listElement: {
        display: "flex",
        alignItems: "center",
        padding: "5px",
        border: "1px solid transparent",
        borderRadius: "5px",
        backgroundColor: "#fff",
        minHeight: "40px",
        margin: "0 0 5px 5px ",
        fontSize: "0.9rem",
        outline: "none",
        boxShadow: " 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)",
      },
      selected: {
          backgroundColor: "rgba(0,143,211, 0.7)"
      },
      routeListElement: {
        cursor: "pointer",
        "&:hover": {
          border: "1px solid #008fd3"
        }
      },
      modalFooter: {
          display: "flex",
          justifyContent: "flex-end",
          margin: "1rem"
      },
      formInput: {
        margin: "0 1rem 0 0 "
      },
      saveButton: {
          width: "fit-content",
          margin: "0 0.5rem"
      }
  });