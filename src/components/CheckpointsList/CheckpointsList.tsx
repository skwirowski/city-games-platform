import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Paper, List, WithStyles, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { styles } from "./CheckpointsListStyles";
import CheckpointsListElement from "./CheckpointsListElement/CheckpointsListElement";
import http from "../../services/routeServiceApi";
import { routes } from "../../static/routesUrl";
import { setCheckpoint, SetCheckpoint } from "../../store/actions";
import notifications from "../../static/notifications";

export interface Checkpoint {
  id: number;
  guid: string;
  name: string;
  descriptionAfterArrival: string;
  descriptionOnTheWay: string;
  address: string;
  latitude: number;
  longitude: number;
  isQrCodeConfirmationSelected: boolean;
  isQrCodeGenerated: boolean;
  objectName: string;
}

interface CheckpointsListProps extends WithStyles<typeof styles> {
  history: {
    push: (arg1: string, arg2?: {}) => {};
  };
  setCheckpoint: SetCheckpoint;
}

const CheckpointsList: React.FC<CheckpointsListProps> = ({ classes, history, setCheckpoint }) => {
  const [checkpoints, setCheckpoints] = useState<Array<Checkpoint>>([]);

  useEffect(() => {
    const fetchData = async () => {
      const res = await http.get("/api/Checkpoints");
      setCheckpoints(res.data);
    };
    setCheckpoint(null);

    fetchData();
    // eslint-disable-next-line
  }, []);

  const deleteCheckpoint = async (guid: string) => {
    try {
      await http.delete(`/api/Checkpoints/${guid}`);
      notifications.checkpoint.checkpointDeletedSuccess();
      const res = await http.get("/api/Checkpoints");
      setCheckpoints(res.data);
    } catch (error) {
      notifications.checkpoint.checkpointUpdatedError();
      console.log("TCL: deleteCheckpoint -> error", error);
    }
  };

  const onGenerateQrCodeClick = (guid: string) => {
    return history.push(routes.checkpointQrCodeForm(guid), {
      route: "/game-master/checkpoint",
      description: "CheckpointsList.createIdentifierDescription",
      label: "CheckpointsList.identifierInputLabel",
      helperText: "CheckpointsList.identifierInputHelperText"
    });
  };

  const onPrintQrCodeClick = (guid: string) => {
    const fetchQrCodeIdentifier = async () => {
      await http
        .get(`/api/Checkpoints/${guid}/qrcode`)
        .then(response => {
          return response.data.qrCodeContent;
        })
        .then(data => {
          history.push(routes.checkpointQrCode(guid), { value: data });
        });
    };

    fetchQrCodeIdentifier();
  };

  return (
    <div className={classes.container}>
      <Paper className={classes.listHeader}>
        <p>
          <FormattedMessage id="CheckpointsList.nameHeader" />
        </p>
        <p>
          <FormattedMessage id="CheckpointsList.latitudeHeader" />
        </p>
        <p>
          <FormattedMessage id="CheckpointsList.longitudeHeader" />
        </p>
        <p>
          <FormattedMessage id="CheckpointsList.descriptionOnTheWayHeader" />
        </p>
        <p>
          <FormattedMessage id="CheckpointsList.descriptionAfterArrivalHeader" />
        </p>
        <p>
          <FormattedMessage id="CheckpointsList.addressHeader" />
        </p>
        <p>
          <FormattedMessage id="CheckpointsList.objectNameHeader" />
        </p>
      </Paper>
      <List>
        {checkpoints && checkpoints.length > 0 ? (
          checkpoints.map(checkpoint => {
            return (
              <CheckpointsListElement
                checkpoint={checkpoint}
                deleteCheckpoint={deleteCheckpoint}
                key={checkpoint.guid}
                generateQrCode={onGenerateQrCodeClick}
                printQrCode={onPrintQrCodeClick}
              />
            );
          })
        ) : (
          <FormattedMessage id="shared.loading" />
        )}
      </List>
    </div>
  );
};
export default withStyles(styles)(
  connect(
    null,
    { setCheckpoint }
  )(CheckpointsList)
);
