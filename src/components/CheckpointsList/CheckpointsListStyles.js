export const styles = {
    container: {
        width: '85%',
        margin: '2rem auto'
    },
    listHeader: {
        display:'grid',
        gridTemplateColumns: '1fr 1fr 1fr 2fr 2fr 2fr 2fr 1fr',
        gridGap: '1rem',
        padding: '0 1rem',
        backgroundColor: '#ccc',
    },
}