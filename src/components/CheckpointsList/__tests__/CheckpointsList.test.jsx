import React from "react";
import { Provider } from "react-redux";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import CheckpointsList from "../CheckpointsList";
import { initializeStore } from "../../../store";
const store = initializeStore();
describe("ErrorMessage", () => {
  it("should render without crash", () => {
    const UnwrappedCheckpointsList = unwrap(CheckpointsList);
    const wrapper = shallow(
      <Provider store={store}>
        <UnwrappedCheckpointsList classes={{}} />
      </Provider>
    );
    expect(wrapper).toBeTruthy();
  });

  it("should match snapshot", () => {
    const UnwrappedCheckpointsList = unwrap(CheckpointsList);
    const wrapper = shallow(
      <Provider store={store}>
        <UnwrappedCheckpointsList classes={{}} />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
