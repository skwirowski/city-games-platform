import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import CheckpointsListElement from "../CheckpointsListElement";
import { mockedCheckpoints } from "../../mockedData";

describe("ErrorMessage", () => {
  let props = {
    deleteCheckpoint: jest.fn(),
    printQrCode: jest.fn(),
    generateQrCode: jest.fn(),
    checkpoint: mockedCheckpoints[0],
    classes: {}
  };
  let UnwrappedCheckpointsListElement;
  let wrapper;

  beforeEach(() => {
    UnwrappedCheckpointsListElement = unwrap(CheckpointsListElement);
    wrapper = shallow(<UnwrappedCheckpointsListElement {...props} />);
  });

  it("should render without crash", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should render correct name", () => {
    const name = wrapper.find(".name");
    expect(name.text()).toBe("ut");
  });

  it("should render correct latitude", () => {
    const latitude = wrapper.find(".latitude");
    expect(latitude.text()).toBe(`45°34'59.801"S`);
  });

  it("should render correct longitute", () => {
    const longitute = wrapper.find(".longitute");
    expect(longitute.text()).toBe(`179°48'54.720"E`);
  });

  it("should render correct descriptionOnTheWay", () => {
    const descriptionOnTheWay = wrapper.find(".descriptionOnTheWay");
    expect(descriptionOnTheWay.text()).toBe(
      "Nostrud laborum ea laborum ea laborum nisi et magna veniam. Et mollit enim Lorem laborum aliquip exercitation quis. Nostrud veniam mollit fugiat cupidatat do enim. Eiusmod aliquip nostrud eiusmod nulla minim commodo veniam aliquip ipsum dolor et ullamco. Id quis exercitation et mollit reprehenderit sint pariatur aliqua culpa."
    );
  });

  it("should render correct descriptionAfterArrival", () => {
    const descriptionAfterArrival = wrapper.find(".descriptionAfterArrival");
    expect(descriptionAfterArrival.text()).toBe(
      "Laboris magna ut minim eu proident eu labore irure minim magna. Et cillum adipisicing laboris dolore ipsum pariatur ullamco proident in esse aute labore tempor. Do Lorem qui proident quis. Cillum irure cillum ex amet. Ea ullamco sunt proident minim est tempor do occaecat."
    );
  });

  it("should render correct address", () => {
    const address = wrapper.find(".address");
    expect(address.text()).toBe("753 Albany Avenue, Bancroft, Ohio, 6785");
  });

  it("should invoke delete function after click", () => {
    const deleteButton = wrapper.find(".deleteButton");
    deleteButton.simulate("click");
    expect(props.deleteCheckpoint).toBeCalled();
  });

  it("should render NO buttons when isQrCodeConfirmationSelected = false", () => {
    const container = shallow(
      <UnwrappedCheckpointsListElement checkpoint={mockedCheckpoints[7]} classes={props.classes} />
    );
    const generateButton = container.find(".qrCodeGenerateButton");
    const printButton = container.find(".qrCodePrintButton");
    expect(generateButton).toHaveLength(0) && expect(printButton).toHaveLength(0);
  });

  it("should render qrCodeGenerateButton when isQrCodeGenerated = false", () => {
    const container = shallow(
      <UnwrappedCheckpointsListElement checkpoint={mockedCheckpoints[1]} classes={props.classes} />
    );
    const generateButton = container.find(".qrCodeGenerateButton");
    expect(generateButton).toHaveLength(1);
  });

  it("should render printButton when isQrCodeGenerated = true", () => {
    const container = shallow(
      <UnwrappedCheckpointsListElement checkpoint={mockedCheckpoints[0]} classes={props.classes} />
    );
    const printButton = container.find(".qrCodePrintButton");
    expect(printButton).toHaveLength(1);
  });

  // it('should invoke GENERATE function after click', () => {
  //     const container = shallow(
  //         <UnwrappedCheckpointsListElement
  //             checkpoint={mockedCheckpoints[1]}
  //             classes={props.classes}
  //         />
  //     );
  //     const generateButton = container.find('.qrCodeGenerateButton');
  //     generateButton.simulate('click');
  //     expect(props.generateQrCode).toBeCalled();
  //     });

  // it('should invoke PRINT function after click', () => {
  //     const container = shallow(
  //         <UnwrappedCheckpointsListElement
  //             checkpoint={mockedCheckpoints[0]}
  //             classes={props.classes}
  //         />
  //     );
  //     const printButton = container.find('.qrCodePrintButton');
  //     printButton.simulate('click');
  //     expect(props.printQrCode).toBeCalled();
  // });
});
