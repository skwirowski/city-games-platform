import {
    createStyles
} from '@material-ui/core';

export const styles = createStyles({
    listElement: {
        display: 'grid',
        padding: '0 1rem',
        gridTemplateColumns: '1fr 1fr 1fr 2fr 2fr 2fr 2fr 1fr',
        gridGap: '1rem',
    },
    listElementButtons: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        whiteSpace: 'nowrap'
    },
    listElementName: {
        fontWeight: 700
    }
})