import React from "react";
import { FormattedMessage } from "react-intl";
import { Paper, Button, WithStyles, withStyles } from "@material-ui/core";
import { styles } from "./CheckpointsListElementStyles";
import { Checkpoint } from "../CheckpointsList";
import { convertDMS } from "../../../utils/coordinates";
import Link from "../../../shared/LinkWithoutDefaultStyles";

interface CheckpointListElementProps extends WithStyles<typeof styles> {
  checkpoint: Checkpoint;
  deleteCheckpoint: (guid: string) => {};
  generateQrCode: (guid: string) => {};
  printQrCode: (guid: string) => void;
}

const CheckpointsListElement: React.FC<CheckpointListElementProps> = ({
  classes,
  checkpoint,
  deleteCheckpoint,
  generateQrCode,
  printQrCode
}) => {
  const coordsDMS = convertDMS(checkpoint.latitude, checkpoint.longitude);

  return (
    <Paper className={classes.listElement}>
      <p className={`${classes.listElementName} name`}>{checkpoint.name}</p>
      <p className="latitude">{coordsDMS.lat}</p>
      <p className="longitute">{coordsDMS.lng}</p>
      <p className="descriptionOnTheWay">{checkpoint.descriptionOnTheWay}</p>
      <p className="descriptionAfterArrival">
        {checkpoint.descriptionAfterArrival}
      </p>
      <p className="address">{checkpoint.address}</p>
      <p className="objectName">{checkpoint.objectName}</p>
      <div className={classes.listElementButtons}>
        <Button
          className="deleteButton"
          color="secondary"
          onClick={() => deleteCheckpoint(checkpoint.guid)}
        >
          <FormattedMessage id="shared.deleteButton" />
        </Button>
        <Link to={`/game-master/checkpoint/edit/${checkpoint.guid}`}>
          <Button color="primary" onClick={() => {}}>
            <FormattedMessage id="CheckpointsList.editButton" />
          </Button>
        </Link>
        {checkpoint.isQrCodeConfirmationSelected ? (
          checkpoint.isQrCodeGenerated ? (
            <Button
              className="qrCodePrintButton"
              variant="outlined"
              onClick={() => printQrCode(checkpoint.guid)}
            >
              <FormattedMessage id="CheckpointsList.qrCodePrintButton" />
            </Button>
          ) : (
            <Button
              className="qrCodeGenerateButton"
              variant="outlined"
              onClick={() => generateQrCode(checkpoint.guid)}
            >
              <FormattedMessage id="CheckpointsList.qrCodeGenerateButton" />
            </Button>
          )
        ) : null}
      </div>
    </Paper>
  );
};

export default withStyles(styles)(CheckpointsListElement);
