import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import CheckPointForm from "../CheckpointForm";

describe("CheckpointForm", () => {
  let wrapper;
  const handleSubmit = jest.fn();
  const formValues = {
    name: "aaaaa",
    descriptionOnTheWay: "aaaaa",
    descriptionAfterArrival: "aaaaa",
    address: "aaaaa",
    objectName: "aaaaa",
    latitude: `41°24'12.2"N`,
    longitude: `2°10'26.5"E`,
    qrCode: true,
    geolocalization: false,
    isLocationVisible: true,
    isAddressVisible: false,
    isObjectNameVisible: false,
    isDescriptionOnTheWayVisible: false,
    isDescriptionAfterArrivalVisible: false
  };

  beforeEach(() => {
    const Unwrapped = unwrap(<CheckPointForm submit={handleSubmit} initialValues={formValues} />);
    wrapper = shallow(Unwrapped);
  });
  it("should render without crash", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should match snapshot", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
