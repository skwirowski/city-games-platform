import React from "react";
import { WithStyles, withStyles } from "@material-ui/core";
import { withFormik, Form, Field, FormikErrors, FormikProps, FieldProps } from "formik";
import * as Yup from "yup";
import { injectIntl, InjectedIntlProps, FormattedMessage } from "react-intl";
import { styles } from "./CheckpointsFormStyles";
import FInput from "../../shared/FormikComponents/FInput";
import ErrorMessage from "../../shared/FormikComponents/ErrorMessage";
import FChekcbox from "../../shared/FormikComponents/FCheckbox";
import Intl from "../../utils/Intl";
import SubmitButton from "../../shared/SubmitButton";
import classnames from "classnames";

interface Props extends WithStyles<typeof styles> {
  submit: (values: FormValues) => Promise<FormikErrors<FormValues> | null>;
  loading: boolean;
  checkpoint: FormValues | null;
  match: {
    params: {
      guid: string;
    };
  };
}

export interface FormValues {
  name: string;
  descriptionOnTheWay: string;
  descriptionAfterArrival: string;
  address: string;
  objectName: string;
  latitude: string;
  longitude: string;
  isQrCodeConfirmationSelected: boolean;
  isGeolocationConfirmationSelected: boolean;
  isLocationVisible: boolean;
  isAddressVisible: boolean;
  isObjectNameVisible: boolean;
  isDescriptionOnTheWayVisible: boolean;
  isDescriptionAfterArrivalVisible: boolean;
}

const CheckpointForm: React.FC<FormikProps<FormValues> & Props & InjectedIntlProps> = ({
  touched,
  errors,
  classes,
  values,
  intl,
  loading,
  match
}) => {
  const confirmationOfArrivalTouched = [
    touched.isQrCodeConfirmationSelected,
    touched.isGeolocationConfirmationSelected
  ];

  const isVisibleTouched = [
    touched.isAddressVisible,
    touched.isDescriptionAfterArrivalVisible,
    touched.isDescriptionOnTheWayVisible,
    touched.isLocationVisible,
    touched.isObjectNameVisible
  ];

  return (
    <Form className={classnames(classes.container, classes.styledScroll)}>
      <div className={classes.textField}>
        <Field
          name="name"
          render={(props: FieldProps) => (
            <FInput
              fieldProps={props}
              placeholder={intl.formatMessage({ id: "CheckpointForm.namePlaceholder" })}
              className={classes.formInput}
            />
          )}
        />
        <ErrorMessage error={errors.name} touched={touched.name} aria-label="nameErrorMessage" />
      </div>
      <div className={classes.coords}>
        <div className={classes.textField}>
          <Field
            name="latitude"
            render={(props: FieldProps) => (
              <FInput
                name="latitude"
                fieldProps={props}
                placeholder={intl.formatMessage({ id: "CheckpointForm.latitudePlaceholder" })}
                className={classes.formInput}
              />
            )}
          />
          <ErrorMessage error={errors.latitude} touched={touched.latitude} />
        </div>
        <div className={classes.textField}>
          <Field
            name="longitude"
            render={(props: FieldProps) => (
              <FInput
                name="longitude"
                fieldProps={props}
                placeholder={intl.formatMessage({ id: "CheckpointForm.longitudePlaceholder" })}
                className={classes.formInput}
              />
            )}
          />
          <ErrorMessage error={errors.longitude} touched={touched.longitude} />
        </div>
      </div>
      <div className={classes.textField}>
        <Field
          name="descriptionOnTheWay"
          render={(props: FieldProps) => (
            <FInput
              fieldProps={props}
              placeholder={intl.formatMessage({ id: "CheckpointForm.descriptionOnTheWayPlaceholder" })}
              multiline
              rows={3}
              className={classes.formInput}
            />
          )}
        />
        <ErrorMessage error={errors.descriptionOnTheWay} touched={touched.descriptionOnTheWay} />
      </div>
      <div className={classes.textField}>
        <Field
          name="descriptionAfterArrival"
          render={(props: FieldProps) => (
            <FInput
              fieldProps={props}
              placeholder={intl.formatMessage({ id: "CheckpointForm.descriptionAfterArrivalPlaceholder" })}
              multiline
              rows={3}
              className={classes.formInput}
            />
          )}
        />
        <ErrorMessage error={errors.descriptionAfterArrival} touched={touched.descriptionAfterArrival} />
      </div>
      <div className={classes.textField}>
        <Field
          name="address"
          render={(props: FieldProps) => (
            <FInput
              fieldProps={props}
              placeholder={intl.formatMessage({ id: "CheckpointForm.addressPlaceholder" })}
              multiline
              rows={3}
              className={classes.formInput}
            />
          )}
        />
        <ErrorMessage error={errors.address} touched={touched.address} />
      </div>
      <div className={classes.textField}>
        <Field
          name="objectName"
          render={(props: FieldProps) => (
            <FInput
              fieldProps={props}
              placeholder={intl.formatMessage({ id: "CheckpointForm.objectNamePlaceholder" })}
              multiline
              rows={3}
              className={classes.formInput}
            />
          )}
        />
        <ErrorMessage error={errors.objectName} touched={touched.objectName} />
      </div>
      <div>
        <div className={classes.checkboxGroupLabel}>
          <FormattedMessage id="CheckpointForm.confirmationOfArrivalLabel" />
        </div>
        <div>
          <Field
            name="isQrCodeConfirmationSelected"
            render={(props: FieldProps) => (
              <FChekcbox fieldProps={props} label={intl.formatMessage({ id: "CheckpointForm.qrCodeLabel" })} />
            )}
          />
          <Field
            name="isGeolocationConfirmationSelected"
            render={(props: FieldProps) => (
              <FChekcbox
                fieldProps={props}
                label={intl.formatMessage({ id: "CheckpointForm.geolocalizationLabel" })}
                disabled={true}
              />
            )}
          />
        </div>
        <ErrorMessage
          error={errors.isQrCodeConfirmationSelected}
          touched={confirmationOfArrivalTouched.includes(true)}
        />
      </div>
      <div>
        <div className={classes.checkboxGroupLabel}>
          <FormattedMessage id="CheckpointForm.fieldsToShowToUsersLabel" />
        </div>
        <div>
          <Field
            name="isLocationVisible"
            render={(props: FieldProps) => (
              <FChekcbox
                fieldProps={props}
                label={intl.formatMessage({ id: "CheckpointForm.isLocationVisibleLabel" })}
              />
            )}
          />
          <Field
            name="isAddressVisible"
            render={(props: FieldProps) => (
              <FChekcbox
                fieldProps={props}
                label={intl.formatMessage({ id: "CheckpointForm.isAddressVisibleLabel" })}
              />
            )}
          />
          <Field
            name="isObjectNameVisible"
            render={(props: FieldProps) => (
              <FChekcbox
                fieldProps={props}
                label={intl.formatMessage({ id: "CheckpointForm.isObjectNameVisibleLabel" })}
              />
            )}
          />
          <Field
            name="isDescriptionOnTheWayVisible"
            render={(props: FieldProps) => (
              <FChekcbox
                fieldProps={props}
                label={intl.formatMessage({ id: "CheckpointForm.isDescriptionOnTheWayVisibleLabel" })}
              />
            )}
          />
          <Field
            name="isDescriptionAfterArrivalVisible"
            render={(props: FieldProps) => (
              <FChekcbox
                fieldProps={props}
                label={intl.formatMessage({ id: "CheckpointForm.isDescriptionAfterArrivalVisibleLabel" })}
              />
            )}
          />
        </div>
        <ErrorMessage error={errors.isLocationVisible} touched={isVisibleTouched.includes(true)} />
      </div>
      <SubmitButton type="submit" disabled={loading} className={classes.addButton} aria-label="addCheckpointButton">
        <FormattedMessage
          id={`${
            match.params && match.params.guid ? "CheckpointForm.submitButtonSaved" : "CheckpointForm.submitButtonLabel"
          }`}
        />
      </SubmitButton>
    </Form>
  );
};

const latitudeRegex = /^[+-]?(([1-8]?\d)\D+([1-5]?\d|60)\D+([1-5]?\d|60)(\.\d+)?|90\D+0\D+0)\D+[NSns]?$/;
const longitudeRegex = /^[+-]?([1-7]?\d{1,2}\D+([1-5]?\d|60)\D+([1-5]?\d|60)(\.\d+)?|180\D+0\D+0)\D+[EWew]?$/;

export default withStyles(styles)(
  withFormik<Props, FormValues>({
    enableReinitialize: true,
    mapPropsToValues: props =>
      props.checkpoint || {
        name: "",
        descriptionOnTheWay: "",
        descriptionAfterArrival: "",
        address: "",
        objectName: "",
        latitude: "",
        longitude: "",
        isQrCodeConfirmationSelected: false,
        isGeolocationConfirmationSelected: false,
        isLocationVisible: true,
        isAddressVisible: false,
        isObjectNameVisible: false,
        isDescriptionOnTheWayVisible: false,
        isDescriptionAfterArrivalVisible: false
      },
    validationSchema: () =>
      Yup.object().shape({
        name: Yup.string()
          .min(3, Intl.messages["CheckpointForm.nameMinErrorMessage"])
          .max(255, Intl.messages["CheckpointForm.nameMaxErrorMessage"])
          .required(Intl.messages["CheckpointForm.nameRequiredErrorMessage"]),
        descriptionOnTheWay: Yup.string()
          .min(3, Intl.messages["CheckpointForm.descriptionOnTheWayMinErrorMessage"])
          .max(255, Intl.messages["CheckpointForm.descriptionOnTheWayMaxErrorMessage"]),
        descriptionAfterArrival: Yup.string()
          .required(Intl.messages["CheckpointForm.descriptionAfterArrivalRequiredErrorMessage"])
          .min(3, Intl.messages["CheckpointForm.descriptionAfterArrivalMinErrorMessage"])
          .max(255, Intl.messages["CheckpointForm.descriptionAfterArrivalMaxErrorMessage"]),
        address: Yup.string()
          .min(3, Intl.messages["CheckpointForm.addresMinErrorMessage"])
          .max(255, Intl.messages["CheckpointForm.addressMaxErrorMessage"]),
        objectName: Yup.string()
          .min(3, Intl.messages["CheckpointForm.objectNameMinErrorMessage"])
          .max(255, Intl.messages["CheckpointForm.objectNameMaxErrorMessage"]),
        latitude: Yup.string()
          .matches(latitudeRegex, Intl.messages["CheckpointForm.latitudeMatchesErrorMessage"])
          .required(Intl.messages["CheckpointForm.latitudeRequiredErrorMessage"]),
        longitude: Yup.string()
          .matches(longitudeRegex, Intl.messages["CheckpointForm.longitudeMatchesErrorMessage"])
          .required(Intl.messages["CheckpointForm.longitudeRequiredErrorMessage"]),
        isQrCodeConfirmationSelected: Yup.boolean().when("geolocalization", {
          is: isGeolocationConfirmationSelected => !isGeolocationConfirmationSelected,
          then: Yup.boolean().oneOf([true], Intl.messages["CheckpointForm.confirmationOfArrivalRequiredError"])
        }),
        isLocationVisible: Yup.boolean().when(['isAddressVisible', 'isObjectNameVisible'],{
          is: false,
          then: Yup.boolean().oneOf([true],Intl.messages["CheckpointForm.isVisibleRequiredError"])
        }),
      }),
    handleSubmit: async (values, { props, setErrors }) => {
      const errors = await props.submit(values);
      if (errors) {
        setErrors(errors);
      }
    }
  })(injectIntl(CheckpointForm))
);
