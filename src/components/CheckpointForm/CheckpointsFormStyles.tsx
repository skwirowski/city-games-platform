import { createStyles } from "@material-ui/core";
import styledScroll from "../styles/styledScroll";

export const styles = createStyles({
  container: {
    padding: "1rem 1rem 5rem 1rem",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    overflowY: "scroll"
  },
  form: {
    display: "flex",
    flexDirection: "column"
  },
  formInput: {
    width: "100%",
    margin: "0.2rem"
  },
  textField: {
    width: "100%",
    margin: "0.2rem"
  },
  coords: {
    display: "flex"
  },
  errorMessage: {
    fontSize: "0.8rem",
    color: "red"
  },
  addButton: {
    alignSelf: "center",
    marginTop: "1rem",
    height: "2.5rem",
    backgroundColor: "#1890ff",
    "&:hover": {
      backgroundColor: "#40a9ff"
    }
  },
  checkboxGroupLabel: {
    marginTop: "1rem"
  },
  styledScroll: styledScroll
});
