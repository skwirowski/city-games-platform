import React, { useState, useEffect } from "react";
import QrReader from "react-qr-reader";
import {
  WithStyles,
  withStyles,
  createStyles,
  Typography,
  Grid
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import history from "../../history";
import { FormattedMessage } from "react-intl";
import { secondary } from "../../shared/colors";
import ErrorSnackbar from "./ErrorSnackbar";

const styles = createStyles({
  navbar: {
    margin: 0,
    backgroundColor: secondary,
    color: "white",
    padding: "0.5em",
    fontSize: "1em"
  },
  text: {
    textAlign: "center",
    margin: "1em"
  },
  qrReader: {
    width: "100%",
    maxWidth: "400px",
    margin: "0 auto"
  }
});

interface Props extends WithStyles<typeof styles> {
  onScan(data: string): void;
}

const QrCodeScanner: React.SFC<Props> = ({ classes, onScan }) => {
  const scanInterval = 300;
  const [qrValue, setQrValue] = useState("");
  const [openSnackbar, setOpenSnackbar] = useState(false);

  useEffect(() => {
    if (qrValue) onScan(qrValue);
  }, [qrValue]);

  const handleScan = (data: string | null) => {
    if (data) setQrValue(data);
  };
  const handleError = (error: any) => {
    setOpenSnackbar(true);
  };
  const handleCloseSnackbar = () => {
    setOpenSnackbar(false);
  };

  return (
    <Grid container>
      <Grid item xs={12} className={classes.navbar}>
        <ArrowBackIcon onClick={history.goBack} />
      </Grid>
      <Grid item xs={12}>
        <QrReader
          delay={scanInterval}
          onError={handleError}
          onScan={handleScan}
          className={classes.qrReader}
        />
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h5" className={classes.text}>
          <FormattedMessage id="QrCodeScanner.bottomText" />
        </Typography>
      </Grid>
      <ErrorSnackbar open={openSnackbar} onClose={handleCloseSnackbar} />
    </Grid>
  );
};

export default withStyles(styles)(QrCodeScanner);
