import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Typography,
  Grid
} from "@material-ui/core";
import history from "../../history";
import { FormattedMessage } from "react-intl";
import Button from "../../shared/Button";

const styles = createStyles({
  container: {
    padding: "2em",
    textAlign: "center"
  },
  text: {
    margin: "1em"
  }
});

interface Props extends WithStyles<typeof styles> {
  onScan(data: string): void;
}

const InvalidQrCodeScreen: React.SFC<Props> = ({ classes }) => {
  return (
    <Grid className={classes.container}>
      <Typography variant="h5" className={classes.text}>
        <FormattedMessage id="InvalidQrCodeScreen.message" />
      </Typography>
      <Button variant="outlined" color="secondary" onClick={history.goBack}>
        <FormattedMessage id="InvalidQrCodeScreen.buttonText" />
      </Button>
    </Grid>
  );
};

export default withStyles(styles)(InvalidQrCodeScreen);
