import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Snackbar,
  SnackbarContent,
  Omit
} from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/Error";
import { SnackbarProps } from "@material-ui/core/Snackbar";
import { FormattedMessage } from "react-intl";
import { AUTO_HIDE_DURATION } from "../../static/snackbar";

const styles = createStyles({
  message: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    opacity: 0.9,
    marginRight: "5px"
  }
});

interface Props
  extends WithStyles<typeof styles>,
    Omit<SnackbarProps, "classes"> {}

const ErrorSnackbar: React.SFC<Props> = ({ classes, ...props }) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center"
      }}
      autoHideDuration={AUTO_HIDE_DURATION}
      {...props}
    >
      <SnackbarContent
        message={
          <span className={classes.message}>
            <ErrorIcon className={classes.icon} />
            <FormattedMessage id="QrCodeScanner.errorMessage" />
          </span>
        }
      />
    </Snackbar>
  );
};

export default withStyles(styles)(ErrorSnackbar);
