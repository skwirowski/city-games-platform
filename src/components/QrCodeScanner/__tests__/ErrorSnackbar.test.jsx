import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import ErrorSnackbar from "../ErrorSnackbar";

describe("ErrorSnackbar", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedErrorSnackbar = unwrap(ErrorSnackbar);
const createSut = props =>
  shallow(<UnwrappedErrorSnackbar classes={{}} {...props} />);
