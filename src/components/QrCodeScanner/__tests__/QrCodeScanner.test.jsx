import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import QrCodeScanner from "../QrCodeScanner";

describe("QrCodeScanner", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedQrCodeScanner = unwrap(QrCodeScanner);
const createSut = props =>
  shallow(<UnwrappedQrCodeScanner classes={{}} {...props} />);
