import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  routeEditorContainer: {
    display: "flex",
    width: "100%",
    height: "calc(100vh - 64px)",
    position: "relative"
  },

  openEditorButton: {
    position: "absolute",
    top: 20,
    left: 33,
    zIndex: 1200,
    backgroundColor: "var(--main-gm-blue)",
    "&:hover": {
      backgroundColor: "var(--main-gm-blue--hover)"
    }
  },
  iconFont: {
    fontSize: 30
  }
});
