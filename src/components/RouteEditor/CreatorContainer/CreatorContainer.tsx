import React from "react";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./CreatorContainerStyles";
import { CSSTransition } from "react-transition-group";
import "../../styles/dialog.css";

interface Props extends WithStyles<typeof styles> {
  windowOpen: boolean;
  children: Array<React.ReactNode> | React.ReactNode;
}
const CreatorContainer: React.FC<Props> = ({ classes, windowOpen, children }) => {
  const { creatorContainer, content, hideCreator } = classes;

  return (
    <CSSTransition in={windowOpen} timeout={400} classNames="dialog" unmountOnExit appear>
      <div className={`${!windowOpen ? hideCreator : creatorContainer}`}>
        <div className={content}>{children}</div>
      </div>
    </CSSTransition>
  );
};

export default withStyles(styles)(CreatorContainer);
