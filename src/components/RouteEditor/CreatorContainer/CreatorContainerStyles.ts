import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  creatorContainer: {
    margin: "10px",
    borderRadius: "4px",
    backgroundColor: "#fff",
    zIndex: 1300,
    boxShadow: "0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)",
    padding: "10px",

    minWidth: "530px",
    width: "35vw",
    height: "calc(100% - 84px)",
    position: "fixed"
  },
  content: {
    display: "grid",
    gridTemplateRows: "1fr 32px",
    gridTemplateColumns: "1fr 1fr",
    height: "100%"
  },

  hideCreator: {
    display: "none"
  }
});
