import React from "react";
import { withStyles, WithStyles } from "@material-ui/core";
import moment from "moment";
import { TableRow, TableCell, IconButton, Typography } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from "@material-ui/icons/Edit";
import Assignment from "@material-ui/icons/Assignment";
import DoneIcon from "@material-ui/icons/Done";
import ClearIcon from "@material-ui/icons/Clear";
import Tooltip from "@material-ui/core/Tooltip";
import classnames from "classnames";
import { styles } from "./RouteElementStyles";
import notifications from "../../../../static/notifications";

interface Props extends WithStyles<typeof styles> {
  route: any;
  deleteRoute: any;
  selectedRow: any;
  loading: boolean;
  setLoading: (isLoading: boolean) => void;
  onEditRouteClickHandle: (guid: string) => Promise<void>;
  onEditQuestClickHandle: (guid: string) => Promise<void>;
  setRowProps: (guid: string, isFocus: boolean, isDelete: boolean) => void;
}

const RouteElement: React.SFC<Props> = ({
  classes,
  route,
  deleteRoute,
  selectedRow,
  setRowProps,
  setLoading,
  loading,
  onEditRouteClickHandle,
  onEditQuestClickHandle
}) => {
  const { tableElement, tableCell, bluredElement, root, textWrap, toolIcon } = classes;
  const { guid, creationDate, checkpoints, routeName } = route;
  const { guid: guidRow, isDelete } = selectedRow;

  const onDeleteHandle = async (guid: string) => {
    if (isDelete) {
      try {
        setLoading(true);
        await deleteRoute(guid);
        notifications.route.routeDeletedSuccess();
        setLoading(false);
        setRowProps("", false, false);
      } catch (error) {
        notifications.route.routeDeletedError();
        setLoading(false);
        setRowProps("", false, false);
      }
    }
  };
  const onFocusHandle = () => {
    isDelete && setRowProps(guid, true, true);
  };

  const isDeleteMode = () => {
    return guid === guidRow && isDelete;
  };

  return (
    <TableRow
      className={guid !== guidRow && isDelete ? classnames(bluredElement, tableElement) : classnames(tableElement, tableCell)}
      key={guid}
      tabIndex={-1}
      onClick={onFocusHandle}
    >
      {isDeleteMode() ? (
        <>
          <TableCell classes={{ root: root }} colSpan={3}>
            <Typography className={tableCell}>Are you sure you want delete this?</Typography>
          </TableCell>
          <TableCell classes={{ root: root }} className={tableCell}>
            <Tooltip title="Save">
              <IconButton disabled={loading} onClick={() => onDeleteHandle(guid)} className={classnames(tableCell, toolIcon)}>
                <DoneIcon />
              </IconButton>
            </Tooltip>

            <Tooltip title="Cancel">
              <IconButton
                disabled={loading}
                className={tableCell}
                onClick={e => {
                  e.stopPropagation();
                  setRowProps(guid, false, false);
                }}
              >
                <ClearIcon />
              </IconButton>
            </Tooltip>
          </TableCell>
        </>
      ) : (
        <>
          <TableCell classes={{ root: root }} className={tableCell}>
            <span className={textWrap}>{routeName}</span>
          </TableCell>
          <TableCell classes={{ root: root }} className={tableCell}>
            <span className={textWrap}>{moment(creationDate).format("L")}</span>
          </TableCell>
          <TableCell classes={{ root: root }} className={tableCell}>
            {checkpoints.length}
          </TableCell>
          <TableCell classes={{ root: root }}>
            <Tooltip title="Attach quests">
              <IconButton className={toolIcon} onClick={() => onEditQuestClickHandle(guid)}>
                <Assignment />
              </IconButton>
            </Tooltip>
            <Tooltip className={toolIcon} title="Edit">
              <IconButton onClick={() => onEditRouteClickHandle(guid)}>
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip className={toolIcon} title="Delete">
              <IconButton
                onClick={(e: any) => {
                  e.stopPropagation();
                  setRowProps(guid, true, true);
                }}
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </TableCell>
        </>
      )}
    </TableRow>
  );
};

export default withStyles(styles)(RouteElement);
