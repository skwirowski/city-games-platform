import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  tableElement: {
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#dbdbdb"
    },
    "&:focus": {
      backgroundColor: "#dbdbdb",
      color: "#fff"
    },
    wordWrap: "break-word",
    "&:first-child": {
      maxWidth: "200px"
    }
  },
  focusRow: {
    backgroundColor: "#1890ff"
  },
  "@keyframes fadeAnimation": {
    from: { opacity: 0.2 },
    to: { opacity: 1 }
  },
  "@keyframes blockAnimation": {
    from: { opacity: 1 },
    to: { opacity: 0.2 }
  },
  tableCell: {
    color: "#757575",
    animation: ".2s fadeAnimation ease-in-out forwards"
  },
  root: {
    //padding: "4px 10px 4px  35px",
    padding: "5px 2px 5px  35px",
    height: 59
    //padding: "4px 5px 4px 15px"
  },
  cellBackground: {
    backgroundColor: "#fff"
  },
  bluredElement: {
    animation: "0.2s blockAnimation ease-in-out forwards",
    pointerEvents: "none"
  },
  textWrap: {
    display: "inline-block",
    width: "100px"
  },
  toolIcon: {
    padding: 10
  }
});
