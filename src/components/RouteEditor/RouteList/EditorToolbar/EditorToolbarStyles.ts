import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  toolbarContainer: {
    position: "sticky",
    top: 0,
    margin: 0,
    padding: 0,
    zIndex: 2,
    borderBottom: "1px solid #e0e0e0",
    display: "flex",
    justifyContent: "space-between",
    alignContent: "center"
  },
  toolbarContent: {
    width: "100%",
    height: "100%",
    display: "grid",
    gridTemplateColumns: "100px 1fr 100px",
    justifyItems: "center",
    alignContent: "center",
    zIndex: 10,
    background: "#fff"
  },
  toolbarText: {
    alignSelf: "center",
    fontSize: "24px",
    color: "#757575"
  },
  addRouteButton: {
    backgroundColor: "#1890ff",
    "&:hover": {
      backgroundColor: "#40a9ff"
    }
  },
  toolbarClose: {
    color: "#757575",
    backgroundColor: "#ebebeb",
    "&:hover": {
      backgroundColor: "#d8d8d8"
    }
  },
  addIcon: {
    fontSize: 30
  }
});
