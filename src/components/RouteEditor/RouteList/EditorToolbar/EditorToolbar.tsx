import React from "react";
import { Toolbar, Typography, Tooltip } from "@material-ui/core";
import { withStyles, WithStyles, Fab } from "@material-ui/core";
import { styles } from "./EditorToolbarStyles";
import AddIcon from "@material-ui/icons/Add";
import Close from "@material-ui/icons/Close";
import { CLOSE_ALL, OPEN_CREATOR } from "../../WindowOpenReducer/constants";

interface Props extends WithStyles<typeof styles> {
  dispatchWindowOpen: (action: { type: string }) => void;
}

const EditorToolbar: React.FC<Props> = ({ classes, dispatchWindowOpen }) => {
  const { addRouteButton, toolbarContainer, toolbarContent, toolbarText, toolbarClose, addIcon } = classes;

  const onAddRouteHandle = () => {
    dispatchWindowOpen({ type: OPEN_CREATOR });
  };

  return (
    <Toolbar className={toolbarContainer}>
      <div className={toolbarContent}>
        <Tooltip title={"Close editor"}>
          <Fab size="medium" className={toolbarClose} onClick={() => dispatchWindowOpen({ type: CLOSE_ALL })}>
            <Close />
          </Fab>
        </Tooltip>

        <Typography variant="headline" className={toolbarText}>
          Route Editor
        </Typography>

        <Tooltip title={"Add route"}>
          <Fab color="primary" onClick={onAddRouteHandle} aria-label="Add" size="medium" className={addRouteButton}>
            <AddIcon className={addIcon} />
          </Fab>
        </Tooltip>
      </div>
    </Toolbar>
  );
};

export default withStyles(styles)(EditorToolbar);
