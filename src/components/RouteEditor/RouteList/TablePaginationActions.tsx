import React from "react";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";

const TablePaginationActions = (props: any) => {
  const { count, page, rowsPerPage, onChangePage } = props;
  const pageOffset = 1;

  function handleFirstPageButtonClick(event: any) {
    onChangePage(event, 0);
  }

  function handleBackButtonClick(event: any) {
    onChangePage(event, page - pageOffset);
  }

  function handleNextButtonClick(event: any) {
    onChangePage(event, page + pageOffset);
  }

  function handleLastPageButtonClick(event: any) {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - pageOffset));
  }

  function localizationText() {
    return `${page * rowsPerPage + pageOffset}-${Math.min((page + pageOffset) * rowsPerPage, count)} of ${count}`;
  }

  return (
    <div style={{ flexShrink: 0 }}>
      <IconButton onClick={handleFirstPageButtonClick} disabled={page === 0} aria-label="First Page">
        <FirstPageIcon />
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="Previous Page">
        <KeyboardArrowLeft />
      </IconButton>
      <span>{localizationText()}</span>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - pageOffset}
        aria-label="Next Page"
      >
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - pageOffset}
        aria-label="Last Page"
      >
        <LastPageIcon />
      </IconButton>
    </div>
  );
};

export default TablePaginationActions;
