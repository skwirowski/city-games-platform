import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  routeListContainer: {
    width: "35vw",
    minWidth: "580px",
    height: "calc(100% - 84px)",
    backgroundColor: "#fff",
    margin: "10px 10px 0 10px",
    position: "fixed",
    borderRadius: "4px",
    zIndex: 1200,
    display: "flex",
    flexDirection: "column",
    outline: "none",
    overflow: "hidden",
    boxShadow: " 0px 1px 3px 0px rgba(0,0,0,0.3), 0px 1px 1px 0px rgba(0,0,0,0.19), 0px 2px 1px -1px rgba(0,0,0,0.15)"
  },
  tableContainer: {
    overflowY: "auto",
    backgroundColor: "#fff",
    borderRadius: "4px"
  },

  selectedRowsNumber: {
    marginRight: "5px"
  },
  tableElement: {
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#dbdbdb"
    },
    "&:focus": {
      backgroundColor: "#dbdbdb",
      color: "#fff"
    }
  },

  tableHeader: {
    backgroundColor: "var(--main-gm-blue)"
  },
  tableHeaderCell: {
    color: "#fff"
  },
  emptyRender: {
    textAlign: "center",
    paddingTop: 0,
    paddingBottom: 0
  },
  containerLoader: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 55,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: "100px"
  },
  loaderWrapper: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,

    backgroundColor: "white",
    opacity: 0.6
  },
  loader: {
    color: "var(--main-gm-blue)"
  },
  addRouteButton: {
    backgroundColor: "var(--main-gm-blue)",
    "&:hover": {
      backgroundColor: "var(--main-gm-blue--hover)"
    }
  },
  "@keyframes hideAnimation": {
    "0%": {
      display: "inlineFlex",
      opacity: 1,
      transform: "scale(1)"
    },
    "99%": {
      display: "inlineFlex",
      opacity: 0,
      transform: "scale(0)"
    },
    "100%": {
      display: "none",
      opacity: 0,
      transform: "scale(0)"
    }
  },

  hideEditor: {
    display: "none"
  },
  root: {
    //padding: "4px 10px 4px 35px",
    // padding: "4px 5px 4px 15px",
    padding: "5px 2px 5px  35px",
    wordWrap: "break-word",
    height: 59
  }
});
