import React, { useState, ReactNode } from "react";
import { connect } from "react-redux";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles, WithStyles } from "@material-ui/core";
import RouteElement from "./RouteElement/RouteElement";
import TablePaginationActions from "./TablePaginationActions";
import PerfectScrollbar from "react-perfect-scrollbar";
import EditorToolbar from "./EditorToolbar/EditorToolbar";
import "react-perfect-scrollbar/dist/css/styles.css";
import { CSSTransition } from "react-transition-group";
import { styles } from "./RouteListStyles";
import "../../styles/dialog.css";
import { deleteRoute } from "../../../store/actions";

interface Props extends WithStyles<typeof styles> {
  routes: any;
  deleteRoute: any;
  loading: boolean;
  windowOpenState: any;
  setLoading: (isLoading: boolean) => void;
  dispatchWindowOpen: (action: { type: string }) => void;
  onEditRouteClickHandle: (guid: string) => Promise<void>;
  onEditQuestClickHandle: (guid: string) => Promise<void>;
}

const selectedRowInit = {
  guid: "",
  isFocus: false,
  isDelete: false
};

const RouteList: React.SFC<Props> = ({
  routes,
  classes,
  loading,
  setLoading,
  deleteRoute,
  windowOpenState,
  dispatchWindowOpen,
  onEditRouteClickHandle,
  onEditQuestClickHandle
}) => {
  const [page, setPage] = useState(0);
  const [selectedRow, setSelectedRow] = useState(selectedRowInit);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);
  const { editorOpen, creatorOpen } = windowOpenState;
  const {
    selectedRowsNumber,
    routeListContainer,
    tableHeader,
    tableHeaderCell,
    emptyRender,
    loaderWrapper,
    containerLoader,
    loader,
    hideEditor,
    root
  } = classes;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, routes.length - page * rowsPerPage);

  const handleChangePage = (event: any, newPage: any) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const setRowProps = (guid: string, isFocus: boolean, isDelete: boolean) => {
    const selected = {
      guid,
      isFocus,
      isDelete
    };
    setSelectedRow(selected);
  };

  const renderRoutes = () => {
    if (!routes.length) return;
    return routes
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
      .map((route: any) => (
        <RouteElement
          key={route.guid}
          deleteRoute={deleteRoute}
          selectedRow={selectedRow}
          setRowProps={setRowProps}
          setLoading={setLoading}
          route={route}
          loading={loading}
          onEditRouteClickHandle={onEditRouteClickHandle}
          onEditQuestClickHandle={onEditQuestClickHandle}
        />
      ));
  };

  const renderFreeSpace = () => {
    // Render free space if there is not full page of routes
    const elementHeight = 56;
    const heightFreeSpace = elementHeight * emptyRows;
    return (
      emptyRows > 0 && (
        <TableRow key="empty-key" style={{ height: heightFreeSpace }}>
          <TableCell colSpan={4} />
        </TableRow>
      )
    );
  };

  const renderEmpty = () => {
    const elementHeight = 56;
    const emptyRows = 10;
    const heightFreeSpace = elementHeight * emptyRows;
    return (
      <TableRow key="empty-key" style={{ height: heightFreeSpace }}>
        <TableCell colSpan={4} className={emptyRender}>
          No records to display
        </TableCell>
      </TableRow>
    );
  };

  const renderRowsNumber = (rowsNumb: any): ReactNode => {
    return <div className={selectedRowsNumber}>{`${rowsNumb} rows `}</div>;
  };

  const renderData = () => {
    return routes.length ? [renderRoutes(), renderFreeSpace()] : renderEmpty();
  };

  const renderLoader = () => {
    if (loading || !routes.length) {
      return (
        <div className={containerLoader}>
          <div className={loaderWrapper} />
          <CircularProgress className={loader} />
        </div>
      );
    }
  };

  return (
    <CSSTransition in={editorOpen} timeout={400} classNames="dialog" unmountOnExit appear>
      <PerfectScrollbar className={`${creatorOpen ? hideEditor : routeListContainer}`}>
        <span>{renderLoader()}</span>
        <EditorToolbar dispatchWindowOpen={dispatchWindowOpen} />
        <Table>
          <TableHead>
            <TableRow className={tableHeader}>
              <TableCell classes={{ root: root }} className={tableHeaderCell}>
                Route Name
              </TableCell>
              <TableCell classes={{ root: root }} className={tableHeaderCell}>
                Creation Date
              </TableCell>
              <TableCell classes={{ root: root }} className={tableHeaderCell}>
                Checkpoints
              </TableCell>
              <TableCell classes={{ root: root }} className={tableHeaderCell} />
            </TableRow>
          </TableHead>
          <TableBody>{renderData()}</TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[10, 15]}
                colSpan={4}
                count={routes.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  renderValue: renderRowsNumber
                }}
                labelDisplayedRows={() => undefined}
                labelRowsPerPage=""
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={(props: any) => <TablePaginationActions {...props} />}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </PerfectScrollbar>
    </CSSTransition>
  );
};

const mapStateToProps = (state: any) => {
  return {
    routes: state.routesGM.routes
  };
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    { deleteRoute }
  )(RouteList)
);
