import { CLOSE_ALL, OPEN_EDITOR, OPEN_CREATOR, OPEN_ROUTE_EDITOR, OPEN_ATTACH_QUEST_EDITOR } from "./constants";

interface WindowAction {
  type: string;
}

interface WindowOpen {
  closeAll: boolean;
  editorOpen: boolean;
  creatorOpen: boolean;
  routeEditorOpen: boolean;
  attachQuestEditorOpen: boolean;
}

export const initialState = {
  closeAll: false,
  editorOpen: true,
  creatorOpen: false,
  routeEditorOpen: false,
  attachQuestEditorOpen: false
};

export default (state: WindowOpen, action: WindowAction) => {
  switch (action.type) {
    case CLOSE_ALL:
      return {
        closeAll: true,
        editorOpen: false,
        creatorOpen: false,
        routeEditorOpen: false,
        attachQuestEditorOpen: false
      };
    case OPEN_EDITOR:
      return {
        closeAll: false,
        editorOpen: true,
        creatorOpen: false,
        routeEditorOpen: false,
        attachQuestEditorOpen: false
      };
    case OPEN_CREATOR:
      return {
        closeAll: false,
        editorOpen: false,
        creatorOpen: true,
        routeEditorOpen: false,
        attachQuestEditorOpen: false
      };
    case OPEN_ROUTE_EDITOR:
      return {
        closeAll: false,
        editorOpen: false,
        creatorOpen: true,
        routeEditorOpen: true,
        attachQuestEditorOpen: false
      };
    case OPEN_ATTACH_QUEST_EDITOR:
      return {
        closeAll: false,
        editorOpen: false,
        creatorOpen: false,
        routeEditorOpen: false,
        attachQuestEditorOpen: true
      };
    default:
      return state;
  }
};
