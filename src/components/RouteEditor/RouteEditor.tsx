import React, { useState, useEffect, useReducer } from "react";
import { connect } from "react-redux";
import { withStyles, WithStyles, Fab } from "@material-ui/core";
import { styles } from "./RouteEditorStyles";
import RouteList from "./RouteList/RouteList";
import RouteCreator from "./RouteCreator/RouteCreator";
import AttachQuestEditor from "./AttachQuestEditor/AttachQuestEditor";
import Map from "../Map/Map";
import windowOpen, { initialState } from "./WindowOpenReducer/windowOpen";
import {
  getRoutes,
  getQuests,
  deleteRoute,
  updateRoute,
  setCheckpoint,
  fetchCheckpoints,
  setRouteCheckpoints,
  deleteRouteCheckpoint,
  fetchAndSetRouteCheckpoints,
  fetchAndSetRouteCheckpointsAndQuest
} from "../../store/actions";
import Settings from "@material-ui/icons/Settings";
import { OPEN_EDITOR, OPEN_ROUTE_EDITOR, OPEN_ATTACH_QUEST_EDITOR } from "./WindowOpenReducer/constants";

interface Props extends WithStyles<typeof styles> {
  route: any;
  routes: any;
  allQuests: any;
  getRoutes: any;
  checkpoints: any;
  deleteRoute: any;
  routeCheckpoints: any;
  setRouteCheckpoints: any;
  deleteRouteCheckpoint: (guid: string) => Promise<void>;
  updateRoute: (guid: string, updatedRoute: any) => Promise<void>;
  setCheckpoint: (checkpoint: any) => Promise<void>;
  selectedCheckpoint: any;
  getQuests: () => void;
  fetchAndSetRouteCheckpoints: (guid: string, checkpoints: any) => Promise<void>;
  fetchAndSetRouteCheckpointsAndQuest: (guid: string, checkpoints: any, quests: any) => Promise<void>;
  fetchCheckpoints: () => Promise<void>;
}

const RouteEditor: React.SFC<Props> = ({
  route,
  routes,
  classes,
  allQuests,
  getRoutes,
  getQuests,
  updateRoute,
  checkpoints,
  deleteRoute,
  setCheckpoint,
  fetchCheckpoints,
  routeCheckpoints,
  selectedCheckpoint,
  setRouteCheckpoints,
  deleteRouteCheckpoint,
  fetchAndSetRouteCheckpoints,
  fetchAndSetRouteCheckpointsAndQuest
}) => {
  const { routeEditorContainer, iconFont } = classes;
  const [loading, setLoading] = useState(false);
  const { openEditorButton } = classes;
  const [windowOpenState, dispatchWindowOpen] = useReducer(windowOpen, initialState);

  useEffect(() => {
    const fetchData = async () => {
      getQuests();
      fetchCheckpoints();
      setLoading(true);
      await getRoutes();
      setLoading(false);
    };
    setRouteCheckpoints([]);
    fetchData();

    return () => {
      setRouteCheckpoints([]);
    };

    // eslint-disable-next-line
  }, []);

  const onEditRouteClickHandle = async (guid: string) => {
    try {
      setLoading(true);
      if (checkpoints.length) {
        await fetchAndSetRouteCheckpoints(guid, checkpoints);
        setLoading(false);
      } else {
        await fetchAndSetRouteCheckpoints(guid, []);
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
    }
    dispatchWindowOpen({ type: OPEN_ROUTE_EDITOR });
  };

  const onRouteQuestsEditClick = async (guid: string) => {
    try {
      setLoading(true);
      const checkpointsData = checkpoints.length ? checkpoints : [];
      const questsData = allQuests.length ? allQuests : [];
      await fetchAndSetRouteCheckpointsAndQuest(guid, checkpointsData, questsData);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
    dispatchWindowOpen({ type: OPEN_ATTACH_QUEST_EDITOR });
  };

  return (
    <div className={routeEditorContainer}>
      <Fab className={openEditorButton} color="primary" onClick={() => dispatchWindowOpen({ type: OPEN_EDITOR })} aria-label="Add" size="medium">
        <Settings className={iconFont} />
      </Fab>
      <RouteCreator dispatchWindowOpen={dispatchWindowOpen} windowOpenState={windowOpenState} />
      <RouteList
        onEditRouteClickHandle={onEditRouteClickHandle}
        onEditQuestClickHandle={onRouteQuestsEditClick}
        windowOpenState={windowOpenState}
        loading={loading}
        setLoading={setLoading}
        dispatchWindowOpen={dispatchWindowOpen}
      />
      <AttachQuestEditor dispatchWindowOpen={dispatchWindowOpen} windowOpenState={windowOpenState} selectedRouteGuid={route ? route.guid : ""} />
      <Map singleCheckpoint={false} match={{ params: { guid: "" } }} toggle={false} blocked={true} />
    </div>
  );
};
const mapStateToProps = (state: any) => {
  return {
    route: state.routesGM.route,
    routes: state.routesGM.routes,
    routeCheckpoints: state.routeCheckpoints,
    selectedCheckpoint: state.checkpoint,
    checkpoints: state.checkpoints,
    allQuests: state.questsGM.allQuests
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    {
      fetchAndSetRouteCheckpointsAndQuest,
      fetchAndSetRouteCheckpoints,
      deleteRouteCheckpoint,
      setRouteCheckpoints,
      fetchCheckpoints,
      setCheckpoint,
      deleteRoute,
      updateRoute,
      getRoutes,
      getQuests
    }
  )(RouteEditor)
);
