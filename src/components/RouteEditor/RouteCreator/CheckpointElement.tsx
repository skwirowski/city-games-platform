import React from "react";
import { Draggable } from "react-beautiful-dnd";
import DeleteIcon from "@material-ui/icons/Delete";
import Add from "@material-ui/icons/Add";
import Location from "@material-ui/icons/MyLocation";
import { Checkpoint } from "./RouteCreatorStyles";
import { Button, withStyles, WithStyles } from "@material-ui/core";
import InfoPopper from "../InfoPopper/InfoPopper";
import { styles } from "./RouteCreatorStyles";
import { ICheckpoint } from "./types";

interface Props extends WithStyles<typeof styles> {
  checkpoint: any;
  haveDelete: boolean;
  index: number;
  onElementDelete?: (elementId: string) => void | undefined;
  onElementAdd?: (guid: string) => void;
  onCenterClick?: (checkpoint: ICheckpoint) => void;
  selectedCheckpoint: any;
}

const CheckpointElement: React.FC<Props> = ({
  checkpoint,
  index,
  classes,
  haveDelete,
  onElementDelete,
  onElementAdd,
  onCenterClick,
  selectedCheckpoint
}): JSX.Element => {
  const { checkpoint_id, name, guid } = checkpoint;

  const { elementDragging, toolButton, checkpointFocused } = classes;

  const isFocused = () => {
    if (!selectedCheckpoint) return false;
    return checkpoint_id === selectedCheckpoint.checkpoint_id;
  };

  const getStyles = (isDragging: boolean) => {
    return `${isFocused() ? checkpointFocused : ""} ${isDragging ? elementDragging : ""}`;
  };

  return (
    <Draggable key={checkpoint_id} draggableId={checkpoint_id} index={index}>
      {(provided, snapshot) => (
        <Checkpoint ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} className={getStyles(snapshot.isDragging)}>
          <div>{name}</div>
          {haveDelete ? (
            <span style={{ marginLeft: "auto" }}>
              <InfoPopper checkpoint={checkpoint} className={toolButton} />
              <Button className={toolButton} onClick={() => onCenterClick && onCenterClick(checkpoint)}>
                <Location fontSize="inherit" aria-label="Delete" color="inherit" />
              </Button>
              <Button className={toolButton} onClick={() => onElementDelete && onElementDelete(checkpoint_id)}>
                <DeleteIcon fontSize="inherit" aria-label="Delete" color="inherit" />
              </Button>
            </span>
          ) : (
            // <InfoPopper checkpoint={checkpoint} className={toolButton} />
            <span style={{ marginLeft: "auto" }}>
              <Button className={toolButton} onClick={() => onElementAdd && onElementAdd(guid)}>
                <Add fontSize="inherit" aria-label="Add" color="inherit" />
              </Button>
            </span>
          )}
        </Checkpoint>
      )}
    </Draggable>
  );
};

export default withStyles(styles)(CheckpointElement);
