import { ICheckpoints, ICheckpoint } from "../types";
export const getCheckpointsGuid = (checkopoints: ICheckpoints) => {
  const guidList = checkopoints.map((checkpoint: ICheckpoint) => checkpoint.guid);
  return guidList;
};
