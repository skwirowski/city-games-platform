import uuidv4 from "uuid/v4";
import { ICheckpoints } from "../types";

interface IState {
  checkpoints: ICheckpoints;
  routeCheckpoints: ICheckpoints;
  [key: string]: ICheckpoints;
}
interface ITransfer {
  droppableId: string;
  index: number;
  [propName: string]: string | number | undefined;
}
export const move = (state: IState, source: ITransfer, destination: ITransfer): IState => {
  const scrListClone: ICheckpoints = [...state[source.droppableId]];

  const destListClone: ICheckpoints = source.droppableId === destination.droppableId ? scrListClone : [...state[destination.droppableId]];

  if (source.droppableId === "checkpoints" && destination.droppableId === "routeCheckpoints") {
    const movedElement = { ...scrListClone[source.index] };
    movedElement.checkpoint_id = uuidv4();
    destListClone.splice(destination.index, 0, movedElement);
  } else if (
    (source.droppableId === "checkpoints" && destination.droppableId === "checkpoints") ||
    (source.droppableId === "routeCheckpoints" && destination.droppableId === "routeCheckpoints")
  ) {
    const [movedElement] = scrListClone.splice(source.index, 1);
    destListClone.splice(destination.index, 0, movedElement);
  }

  return {
    [source.droppableId]: scrListClone,
    ...(source.droppableId === destination.droppableId
      ? {}
      : {
          [destination.droppableId]: destListClone
        })
  } as IState;
};
