import {
    move
} from '../index'
import initialData from '../../initialData';

const argumentsData = [{
        state: {
            checkpoints: initialData,
            routeCheckpoints: [],
        },
        source: {
            droppableId: "checkpoints",
            index: 0
        },
        destination: {
            droppableId: "routeCheckpoints",
            index: 0
        }
    },

    {
        state: {
            checkpoints: initialData,
            routeCheckpoints: [],
        },
        source: {
            droppableId: "checkpoints",
            index: 0
        },
        destination: {
            droppableId: "checkpoints",
            index: 1
        }
    },

    {
        state: {
            checkpoints: initialData,
            routeCheckpoints: [initialData[0]],
        },
        source: {
            droppableId: "routeCheckpoints",
            index: 0
        },
        destination: {
            droppableId: "checkpoints",
            index: 0
        }
    }
]

describe('Move function.', () => {
    it("should move an element from checkpoints into routeCheckpoints.", () => {
        const {
            state,
            source,
            destination
        } = argumentsData[0];
        const updatedState = move(state, source, destination);
        expect(updatedState.routeCheckpoints.length).toBe(1);

    })

    it("should move an element inside a specific column.", () => {
        const {
            state,
            source,
            destination
        } = argumentsData[1];

        const elementGuid = state.checkpoints[0].guid;
        const updatedState = move(state, source, destination);
        const movedElementGuid = updatedState.checkpoints[1].guid;

        expect(elementGuid).toEqual(movedElementGuid);

    })

    it("shouldn't move an element back to a checkpoints column.", () => {
        const {
            state,
            source,
            destination
        } = argumentsData[2];

        const checkpointsLength = state.checkpoints.length;
        const updatedState = move(state, source, destination);
        const updatedLength = updatedState.checkpoints.length;

        expect(checkpointsLength).toEqual(updatedLength);

    })
})