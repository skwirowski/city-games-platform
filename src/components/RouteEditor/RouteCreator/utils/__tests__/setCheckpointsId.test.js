import {
    setCheckpointsId
} from '../index';
import initialData from '../../initialData';

describe('setCheckpointsId function.', () => {
    it('sholud add a checkpoint_id property to all checkpoint elements.', () => {
        const updatedCheckpoints = setCheckpointsId(initialData);
        updatedCheckpoints.forEach(checkpoint => {
            expect(checkpoint.checkpoint_id).toBeTruthy()
        })

    })
})