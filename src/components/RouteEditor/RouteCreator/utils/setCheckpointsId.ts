import uuidv4 from "uuid/v4";
import { ICheckpoints, ICheckpoint } from "../types";

export const setCheckpointsId = (checkpointData: ICheckpoints) => {
  if (Array.isArray(checkpointData) && checkpointData.length) {
    return checkpointData.map(checkpoint => {
      checkpoint.checkpoint_id = uuidv4();
      return checkpoint;
    });
  }
};
export const setCheckpointId = (checkpoint: ICheckpoint) => {
  if (!checkpoint) return;
  const newCheckpoint = { ...checkpoint };
  newCheckpoint.checkpoint_id = uuidv4();

  return newCheckpoint;
};
