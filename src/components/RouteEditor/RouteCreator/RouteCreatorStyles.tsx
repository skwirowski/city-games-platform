import { createStyles } from "@material-ui/core";
import styled from "styled-components";

export const styles = createStyles({
  creatorContainer: {
    margin: "10px",
    borderRadius: "4px",
    backgroundColor: "#fff",
    zIndex: 1300,
    boxShadow: "0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)",
    padding: "10px",
    minWidth: "530px",
    width: "35vw",
    height: "calc(100% - 84px)",
    position: "fixed"
  },
  content: {
    display: "grid",
    gridTemplateRows: "1fr 32px",
    gridTemplateColumns: "1fr 1fr",
    height: "100%"
  },
  modalButtonsContainer: {
    gridArea: "2/2/2/2",
    display: "flex",
    justifyContent: "flex-end"
  },
  columnsContainer: {
    display: "flex"
  },

  cancelButton: {
    marginLeft: "5px",
    marginRight: "5px"
  },
  columnContainer: {
    borderRadius: "4px",
    margin: "5px 5px 10px 5px",
    boxShadow: " 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)",
    overflow: "hidden"
  },
  checkpointList: {
    paddingTop: "10px",
    textAlign: "justify",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "0 0 4px 4px",
    height: "78vh"
  },
  srollbarContainer: {
    paddingBottom: "90px"
  },
  checkpoint: {
    color: "#757575",
    display: "flex",
    alignItems: "center",
    padding: "5px",
    border: "1px solid transparent",
    borderRadius: "4px",
    backgroundColor: "#fff",
    minHeight: "40px",
    margin: "0 12px 10px 12px",
    fontSize: "0.8125rem",
    outline: "none",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.3), 0px 1px 1px 0px rgba(0,0,0,0.19), 0px 2px 1px -1px rgba(0,0,0,0.15)",
    "&:hover": {
      border: "1px solid var(--main-gm-blue);"
    },
    "&:first-child": {
      marginTop: 1
    }
  },
  columnHeader: {
    backgroundColor: "var(--main-gm-blue)",
    fontSize: "1.1rem",
    color: "#fff",
    width: "60%",
    padding: "10px 0px 10px 0px"
  },
  columnHeaderContainer: {
    backgroundColor: "var(--main-gm-blue)",
    display: "flex",
    padding: "5px 16px 5px 16px"
  },
  elementDragging: {
    backgroundColor: "#EAF4FF",

    border: "1px solid var(--main-gm-blue)"
  },
  columnOver: {
    backgroundColor: "#f2f2f2"
  },
  draggingNotOver: {
    background: "#ff8282"
  },
  toolButton: {
    position: "relative",
    right: 0,
    top: 0,
    padding: 2,
    minWidth: "30px",
    height: "30px",
    fontSize: "1.4rem",
    color: "#757575",
    "&:first-child": {
      marginLeft: "auto"
    }
  },
  hideCreator: {
    display: "none"
  },

  checkpointFocused: {
    backgroundColor: "#EAF4FF",
    border: "1px solid var(--main-gm-blue)"
  },
  headerInput: {
    color: "white",
    "&::before": {
      borderColor: "white",
      bottom: "10px"
    },
    "&::after": {
      borderColor: "white",
      bottom: "10px"
    }
  },
  overridesUnderline: {
    "&:hover:not(.disabled):not(.error):not(.focused):before": {
      borderBottomColor: "#fff"
    }
  }
});

export const Checkpoint = styled.div`
  color: #757575;
  display: flex;
  align-items: center;
  padding: 5px;
  border: 1px solid transparent;
  border-radius: 4px;
  background-color: #fff;
  min-height: 45px;
  margin: 0 12px 10px 12px;
  font-size: 0.8125rem;
  outline: none;
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.3), 0px 1px 1px 0px rgba(0, 0, 0, 0.19), 0px 2px 1px -1px rgba(0, 0, 0, 0.15);
  :hover {
    border: 1px solid var(--main-gm-blue);
  }
  ,
  :first-child {
    margin-top: 1px;
  }
`;
