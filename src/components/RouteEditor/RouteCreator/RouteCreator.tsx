import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { withStyles, WithStyles } from "@material-ui/core";
import { move, setCheckpointsId, getCheckpointsGuid, setCheckpointId } from "./utils";
import { styles } from "./RouteCreatorStyles";
import { DropResult, ICheckpoints } from "./types";
import fetchCheckpoints from "../../../services/fetchCheckpoints";
import notifications from "../../../static/notifications";
import messages from "../../../static/messages";
import { Button } from "antd";
import { DragDropContext } from "react-beautiful-dnd";
import { FormattedMessage } from "react-intl";
import DroppableColumn from "./DroppableColumn";
import CreatorContainer from "../CreatorContainer/CreatorContainer";
import Intl from "../../../utils/Intl";
import { updateRoute, setRouteCheckpoints, createRoute, setCheckpoint, setMapCenter } from "../../../store/actions";
import { OPEN_EDITOR } from "../WindowOpenReducer/constants";
import "../../styles/dialog.css";

interface Props extends WithStyles<typeof styles> {
  route: any;
  createRoute: (checkpoins: any, routeName: string) => Promise<void>;
  routeCheckpoints: any;
  setRouteCheckpoints: any;
  updateRoute: (guid: string, updatedRoute: any) => Promise<void>;
  setCheckpoint: (checkpoint: any) => void;
  setMapCenter: (cords: any, lngOffset: boolean, offsetValue: number, routeEditor: boolean) => Promise<void>;
  mapMode: any;
  selectedCheckpoint: any;
  windowOpenState: any;
  dispatchWindowOpen: (action: { type: string }) => void;
}

const RouteCreator: React.SFC<Props> = ({
  classes,
  createRoute,
  routeCheckpoints,
  setRouteCheckpoints,
  route,
  updateRoute,
  setCheckpoint,
  setMapCenter,
  selectedCheckpoint,
  mapMode,
  windowOpenState,
  dispatchWindowOpen
}): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [routeName, setRouteName] = useState("");
  const [checkpoints, setCheckpoints] = useState<ICheckpoints>([]);
  const { modalButtonsContainer, cancelButton } = classes;
  const { creatorOpen, routeEditorOpen } = windowOpenState;

  useEffect(() => {
    const setData = async () => {
      try {
        const data = await fetchCheckpoints();
        setCheckpointsId(data);
        setCheckpoints(data);
      } catch (e) {
        console.log("TCL: setData -> e", JSON.stringify(e, null, 2));
      }
    };
    setData();
  }, []);

  useEffect(() => {
    if (route && route.routeName && route.routeName !== routeName) {
      setRouteName(route.routeName);
    }
    // eslint-disable-next-line
  }, [route]);

  const onDragEnd = (result: DropResult) => {
    const { source, destination } = result;

    if (!destination) {
      return;
    }
    const updatedState = move({ checkpoints, routeCheckpoints }, source, destination);

    if (Object.keys(updatedState).length === 1) {
      return updatedState["checkpoints"] ? setCheckpoints(updatedState["checkpoints"]) : setRouteCheckpoints(updatedState["routeCheckpoints"]);
    } else if (Object.keys(updatedState).length === 2) {
      setCheckpoints(updatedState["checkpoints"]);
      setRouteCheckpoints(updatedState["routeCheckpoints"]);
    }
  };
  const onElementDelete = (elementId: string) => {
    if (!routeCheckpoints.length) return;
    let newRouteCheckpoints = [...routeCheckpoints];

    newRouteCheckpoints = newRouteCheckpoints.filter(elelemnt => elelemnt.checkpoint_id !== elementId);
    setRouteCheckpoints(newRouteCheckpoints);
  };

  const onElementAdd = (guid: string) => {
    let newCheckpoint = checkpoints.find(checkpoint => checkpoint.guid === guid);
    if (!newCheckpoint) return;

    newCheckpoint = setCheckpointId(newCheckpoint);

    if (!newCheckpoint) return;
    const newCheckpoints = [...routeCheckpoints];
    newCheckpoints.push(newCheckpoint);
    setRouteCheckpoints(newCheckpoints);
  };

  const onCancel = () => {
    setRouteCheckpoints([]);
    setRouteName("");
    dispatchWindowOpen({ type: OPEN_EDITOR });
  };

  const onRouteUpdate = async () => {
    if (!route && !route.guid) return notifications.route.routeUpdatedError();
    if (!routeCheckpoints.length) return messages.route.routeCreatedLengthError();
    if (!routeName.length) return messages.route.routeCreatedNameEmptyError();
    if (routeName.length < 3) return messages.route.routeCreatedNameLengthError();
    try {
      const guidList = getCheckpointsGuid(routeCheckpoints);
      const newRoute = { ...route, checkpoints: guidList, routeName: routeName };
      setLoading(true);
      await updateRoute(route.guid, newRoute);

      notifications.route.routeUpdatedSuccess();
      setLoading(false);
    } catch (error) {
      notifications.route.routeUpdatedError();
    }
  };

  const onRouteCreate = async () => {
    if (!routeCheckpoints.length) return messages.route.routeCreatedLengthError();
    if (!routeName.length) return messages.route.routeCreatedNameEmptyError();
    if (routeName.length < 3) return messages.route.routeCreatedNameLengthError();
    const guidList = getCheckpointsGuid(routeCheckpoints);
    try {
      setLoading(true);
      await createRoute(guidList, routeName);
      notifications.route.routeCreatedSuccess();
      setLoading(false);
    } catch (error) {
      setLoading(false);
      notifications.route.routeCreatedError();
    }
  };
  const onCenterClick = (checkpoint: any) => {
    if (!checkpoint) return;
    const { longitude, latitude } = checkpoint;
    setMapCenter([latitude, longitude], true, mapMode.lngOffset, true);
    setCheckpoint(checkpoint);
  };

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRouteName(e.target.value);
  };
  return (
    <CreatorContainer windowOpen={creatorOpen}>
      <DragDropContext onDragEnd={onDragEnd}>
        <DroppableColumn
          column_id="checkpoints"
          classes={classes}
          header={Intl.messages["RouteCreator.allCheckpoints"]}
          checkpoints={checkpoints}
          onElementAdd={onElementAdd}
        />
        <DroppableColumn
          column_id="routeCheckpoints"
          classes={classes}
          header={Intl.messages["RouteCreator.newRoute"]}
          checkpoints={routeCheckpoints}
          onElementDelete={onElementDelete}
          onCenterClick={onCenterClick}
          selectedCheckpoint={selectedCheckpoint}
          onInputChange={onInputChange}
          routeName={routeName}
          selectedRoute={route}
        />
      </DragDropContext>

      <div className={modalButtonsContainer}>
        {routeEditorOpen ? (
          <Button type="primary" loading={loading} onClick={onRouteUpdate}>
            <FormattedMessage id="RouteCreator.saveButton" />
          </Button>
        ) : (
          <Button type="primary" loading={loading} onClick={onRouteCreate}>
            <FormattedMessage id="RouteCreator.createButton" />
          </Button>
        )}

        <Button onClick={onCancel} className={cancelButton}>
          <FormattedMessage id="shared.cancelButton" />
        </Button>
      </div>
    </CreatorContainer>
  );
};

const mapStateToProps = (state: any) => {
  return {
    route: state.routesGM.route,
    routeCheckpoints: state.routeCheckpoints,
    selectedCheckpoint: state.checkpoint,
    mapMode: state.mapMode
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    {
      setRouteCheckpoints,
      updateRoute,
      createRoute,
      setCheckpoint,
      setMapCenter
    }
  )(RouteCreator)
);
