import React from "react";
import { shallow } from "enzyme";
import RouteCreator from "../RouteCreator";
import { unwrap } from "@material-ui/core/test-utils";
import classes from "../RouteCreatorStyles";
import { initializeStore } from "../../../../store/index";
const store = initializeStore();
describe("<RouteCreator/>", () => {
  let wrapper;

  beforeEach(() => {
    const UnwrappedRouteCreator = unwrap(RouteCreator);
    wrapper = shallow(<UnwrappedRouteCreator store={store} classes={{ classes }} />);
  });

  it("renders without crashing", () => {
    expect(wrapper.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
