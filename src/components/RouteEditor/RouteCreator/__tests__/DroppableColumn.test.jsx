import React from "react";
import { shallow } from "enzyme";
import DroppableColumn from "../DroppableColumn";
import { unwrap } from "@material-ui/core/test-utils";
import classes from "../RouteCreatorStyles";
import initialData from "../initialData";
import { setCheckpointsId } from "../utils";

describe("DroppableColumn", () => {
  const checkpoints = setCheckpointsId(initialData);
  let UnwrappedRouteCreator;
  beforeEach(() => {
    UnwrappedRouteCreator = unwrap(DroppableColumn);
  });

  it("renders without crashing.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator classes={{ classes }} checkpoints={[]} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));
    expect(renderChildWrapper.isEmptyRender()).toBe(false);
  });

  it("shouldn't render any <CheckpointElement/> if empty array of checkpoints passed.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator classes={{ classes }} checkpoints={[]} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));

    expect(renderChildWrapper.find("CheckpointElement").isEmptyRender()).toBe(true);
  });
  it("shouldn't render any <CheckpointElement/> if undefined passed.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator classes={{ classes }} checkpoints={undefined} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));

    expect(renderChildWrapper.find("CheckpointElement").isEmptyRender()).toBe(true);
  });
  xit("renders <CheckpointElement/> if valid checkpoints passed.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator classes={{ classes }} checkpoints={checkpoints} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));
    expect(renderChildWrapper.find("CheckpointElement").isEmptyRender()).toBe(false);
  });
  it("should match snapshot.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator classes={{ classes }} checkpoints={checkpoints} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));
    expect(renderChildWrapper.debug()).toMatchSnapshot();
  });
});
