import React from "react";
import { shallow } from "enzyme";
import CheckpointElement from "../CheckpointElement";
import { unwrap } from "@material-ui/core/test-utils";
import initialData from "../initialData";
import { setCheckpointsId } from "../utils";

describe("DroppableColumn", () => {
  const checkpoint = setCheckpointsId(initialData)[0];
  let UnwrappedRouteCreator;
  beforeEach(() => {
    UnwrappedRouteCreator = unwrap(CheckpointElement);
  });

  it("renders without crashing", () => {
    const wrapper = shallow(<UnwrappedRouteCreator checkpoint={checkpoint} classes={{ checkpoint: "class" }} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));
    expect(renderChildWrapper.isEmptyRender()).toBe(false);
  });

  it("shouldn't render <IconButton/> if a prop haveDelete equals to false.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator checkpoint={checkpoint} haveDelete={false} classes={{ checkpoint: "class" }} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));

    expect(renderChildWrapper.find('[aria-label="Delete"]').isEmptyRender()).toBe(true);
  });

  it("renders <IconButton/> if a prop haveDelete equals to true.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator checkpoint={checkpoint} haveDelete={true} classes={{ checkpoint: "class" }} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));
    expect(renderChildWrapper.find('[aria-label="Delete"]').isEmptyRender()).toBe(false);
  });

  it("should match snapshot.", () => {
    const wrapper = shallow(<UnwrappedRouteCreator checkpoint={checkpoint} classes={{ checkpoint: "class" }} />);
    const renderChildWrapper = shallow(wrapper.prop("children")({}, {}));
    expect(renderChildWrapper.debug()).toMatchSnapshot();
  });
});
