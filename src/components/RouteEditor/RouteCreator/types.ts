import { DraggableId, TypeId, DroppableId } from "react-beautiful-dnd";

type DraggableLocation = {
  droppableId: DroppableId;
  index: number;
};
export type DropResult = {
  draggableId: DraggableId;
  type: TypeId;
  source: DraggableLocation;
  destination?: DraggableLocation;
};

export interface IClasses {
  checkpoint: string;
  checkpointList: string;
  columnContainer: string;
  columnHeader: string;
  columnOver: string;
  columnsContainer: string;
  draggingNotOver: string;
  elementDragging: string;
  srollbarContainer: string;
  modalButtonsContainer: string;
  cancelButton: string;
}

export interface ICheckpoint {
  guid: string;
  name: string;
  latitude: number;
  longitude: number;
  address: string;
  descriptionOnTheWay: string;
  descriptionAfterArrival: string;
  timeStamp: string;
  creationDate: string;
  isDescriptionOnTheWayVisible: boolean;
  isDescriptionAfterArrivalVisible: boolean;
  isLocationVisible: boolean;
  isAddressVisible: boolean;
  creatorGuid: string;
  checkpoint_id?: string;
  cancelButton: string;
}

export interface ICheckpoints extends Array<ICheckpoint> {}
