import React, { ReactNode } from "react";
import { IClasses } from "./types";
import { DroppableProvided, DroppableStateSnapshot } from "react-beautiful-dnd";

type InnerRef = (instance: HTMLDivElement | null) => void;

interface CheckpointListProps {
  children: ReactNode;
  classes: IClasses;
  innerRef: InnerRef;
  provided: DroppableProvided;
  snapshot: DroppableStateSnapshot;
}

export default function CheckpointList(props: CheckpointListProps): JSX.Element {
  const {
    provided,
    innerRef,
    children,
    classes: { checkpointList, columnOver },
    snapshot
  } = props;
  return (
    <div {...provided.droppableProps} ref={innerRef} className={`${checkpointList} ${snapshot.isDraggingOver ? columnOver : ""}`}>
      {children}
    </div>
  );
}
