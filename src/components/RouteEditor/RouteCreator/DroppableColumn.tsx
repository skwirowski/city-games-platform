import React from "react";
import { Typography, Input, WithStyles } from "@material-ui/core";
import { Droppable } from "react-beautiful-dnd";
import CheckpointList from "./CheckpointList";
import CheckpointElement from "./CheckpointElement";
import { ICheckpoints, ICheckpoint } from "./types";
import PerfectScrollbar from "react-perfect-scrollbar";
import { styles } from "./RouteCreatorStyles";
import classnames from "classnames";

interface Props extends WithStyles<typeof styles> {
  checkpoints: ICheckpoints;
  header: string;
  column_id: string;
  onElementDelete?: (elementId: string) => void;
  onElementAdd?: (guid: string) => void;
  onCenterClick?: (checkpoint: any) => void;
  selectedCheckpoint?: any;
  onInputChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  routeName?: string;
  selectedRoute?: any;
}

const DroppableColumn: React.FC<Props> = ({
  classes,
  checkpoints,
  header,
  column_id,
  onElementDelete,
  onElementAdd,
  onCenterClick,
  selectedCheckpoint,
  onInputChange,
  routeName,
  selectedRoute
}): JSX.Element => {
  const { columnContainer, headerInput, columnHeaderContainer, columnHeader, srollbarContainer, overridesUnderline } = classes;

  return (
    <Droppable droppableId={column_id}>
      {(provided, snapshot) => (
        <div className={columnContainer}>
          <div className={columnHeaderContainer}>
            {column_id === "routeCheckpoints" ? (
              <>
                <Typography className={columnHeader}>{header}</Typography>
                <Input value={routeName} placeholder="Route name" onChange={onInputChange} className={classnames(headerInput, overridesUnderline)} />
              </>
            ) : (
              <Typography className={columnHeader}>{header}</Typography>
            )}
          </div>

          <CheckpointList classes={classes} snapshot={snapshot} provided={provided} innerRef={provided.innerRef}>
            <PerfectScrollbar className={srollbarContainer}>
              {checkpoints
                ? checkpoints.map((checkpoint: ICheckpoint, index: number) => (
                    <CheckpointElement
                      key={checkpoint.checkpoint_id}
                      checkpoint={checkpoint}
                      index={index}
                      onElementDelete={onElementDelete}
                      onElementAdd={onElementAdd}
                      haveDelete={column_id === "routeCheckpoints"}
                      onCenterClick={onCenterClick}
                      selectedCheckpoint={selectedCheckpoint}
                    />
                  ))
                : null}

              {provided.placeholder}
            </PerfectScrollbar>
          </CheckpointList>
        </div>
      )}
    </Droppable>
  );
};

export default DroppableColumn;
