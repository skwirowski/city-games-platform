import React, { useState } from "react";
import { connect } from "react-redux";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./AttachQuestEditorStyles";
import notifications from "../../../static/notifications";
import { FormattedMessage } from "react-intl";
import { DragDropContext } from "react-beautiful-dnd";
import Intl from "../../../utils/Intl";
import { attachQuestToRouteCheckpoint, setRouteCheckpoints, attachQuestsToCheckpoints, setQuestsForCheckpoints } from "../../../store/actions";
import CreatorContainer from "../CreatorContainer/CreatorContainer";
import QuestEditorColumn from "./QuestEditorColumn/QuestEditorColumn";
import { Button } from "antd";
import { OPEN_EDITOR } from "../WindowOpenReducer/constants";
import { mapToKeyValue } from "../../../utils/mapGuidsToRouteCheckpoints";

interface Props extends WithStyles<typeof styles> {
  quests: any;
  windowOpenState: any;
  routeCheckpoints: any;
  questsForCheckpoints: any;
  selectedRouteGuid?: string;
  setRouteCheckpoints: (checkpoints: any) => void;
  dispatchWindowOpen: (action: { type: string }) => void;
  setQuestsForCheckpoints: (routeGuid: string) => Promise<void>;
  attachQuestToRouteCheckpoint: (quest: any, checkpoint_id: string) => void;
  attachQuestsToCheckpoints: (questToAdd: any, questToUpdate: any, questToDelete: any) => Promise<void>;
}

export const questColumnType = "QUEST_COLUMN";
export const checkopintColumnType = "CHECKPOINT_COLUMN";
const nullGuid = "00000000-0000-0000-0000-000000000000";
const AttachQuestEditor: React.FC<Props> = ({
  quests,
  classes,
  windowOpenState,
  routeCheckpoints,
  selectedRouteGuid,
  dispatchWindowOpen,
  setRouteCheckpoints,
  questsForCheckpoints,
  setQuestsForCheckpoints,
  attachQuestsToCheckpoints,
  attachQuestToRouteCheckpoint
}) => {
  const { attachQuestEditorOpen } = windowOpenState;
  const [loading, setLoading] = useState(false);
  const { cancelButton } = classes;

  const onDragEndHandle = (dropResult: any) => {
    if ((!dropResult.destination && !dropResult.source) || (!dropResult.combine && !dropResult.source)) return;
    const { draggableId: sourceId } = dropResult;
    const questToAttach = quests.find((quest: any) => quest.uuid === sourceId);
    if (!questToAttach) return;
    if (dropResult.destination) {
      const { droppableId: destinationId } = dropResult.destination;
      attachQuestToRouteCheckpoint(questToAttach, destinationId);
    } else if (dropResult.combine) {
      const { droppableId: destinationId } = dropResult.combine;
      attachQuestToRouteCheckpoint(questToAttach, destinationId);
    }
  };

  const onCancelClick = () => {
    setRouteCheckpoints([]);
    dispatchWindowOpen({ type: OPEN_EDITOR });
  };

  const onSaveClick = async () => {
    if (!selectedRouteGuid && !setRouteCheckpoints.length) return;

    const mappedQuestForCheckpints = questsForCheckpoints.length ? mapToKeyValue(questsForCheckpoints, "checkpointGuid") : [];
    const questsToAdd: any = [];
    const questsToUpdate: any = [];
    const questsToDelete: any = [];

    routeCheckpoints.map((checkpoint: any) => {
      const { attachedQuest } = checkpoint;
      const mappedQuest = mappedQuestForCheckpints[checkpoint.guid];
      if (questsForCheckpoints.length && mappedQuest) {
        if (attachedQuest && mappedQuest.questGuid !== nullGuid && mappedQuest.questGuid !== attachedQuest.uuid) {
          const newQuest = {
            checkpointGuid: checkpoint.guid,
            questGuid: checkpoint.attachedQuest.uuid,
            routeGuid: selectedRouteGuid
          };
          return questsToUpdate.push(newQuest);
        } else if (attachedQuest && mappedQuest.questGuid === nullGuid) {
          const newQuest = {
            checkpointGuid: checkpoint.guid,
            questGuid: checkpoint.attachedQuest.uuid,
            routeGuid: selectedRouteGuid
          };
          return questsToAdd.push(newQuest);
        } else if (!attachedQuest && mappedQuest && mappedQuest.questGuid !== nullGuid) {
          const newQuest = {
            checkpointGuid: checkpoint.guid,
            routeGuid: selectedRouteGuid
          };
          return questsToDelete.push(newQuest);
        }
      }
      return {
        checkpointGuid: checkpoint.guid,
        questGuid: checkpoint.attachedQuest ? checkpoint.attachedQuest.uuid : "",
        routeGuid: selectedRouteGuid
      };
    });

    const questCheckpoints = routeCheckpoints.map((checkpoint: any) => {
      return {
        checkpointGuid: checkpoint.guid,
        questGuid: checkpoint.attachedQuest ? checkpoint.attachedQuest.uuid : nullGuid,
        routeGuid: selectedRouteGuid
      };
    });

    try {
      if (!questsToAdd.length && !questsToUpdate.length && !questsToDelete.length) return;
      setLoading(true);
      await attachQuestsToCheckpoints(questsToAdd, questsToUpdate, questsToDelete);
      if (selectedRouteGuid) {
        setQuestsForCheckpoints(questCheckpoints);
      }
      notifications.route.routeUpdatedSuccess();

      setLoading(false);
    } catch (error) {
      notifications.route.routeUpdatedError();
      setLoading(false);
    }
  };

  return (
    <CreatorContainer windowOpen={attachQuestEditorOpen}>
      <DragDropContext onDragEnd={onDragEndHandle}>
        <QuestEditorColumn columnType={questColumnType} header={Intl.messages["RouteEditor.availableQuests"]} />
        <QuestEditorColumn columnType={checkopintColumnType} header={Intl.messages["RouteEditor.routeCheckpoints"]} />
      </DragDropContext>
      <div className={classes.buttonsContainer}>
        <Button loading={loading} type="primary" onClick={onSaveClick}>
          <FormattedMessage id="RouteCreator.saveButton" />
        </Button>
        <Button onClick={onCancelClick} className={cancelButton}>
          <FormattedMessage id="shared.cancelButton" />
        </Button>
      </div>
    </CreatorContainer>
  );
};

const mapStateToProps = (state: any) => {
  return {
    routeCheckpoints: state.routeCheckpoints,
    quests: state.questsGM.storeQuests,
    questsForCheckpoints: state.questsGM.questsForCheckpoints
  };
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    { attachQuestToRouteCheckpoint, setRouteCheckpoints, attachQuestsToCheckpoints, setQuestsForCheckpoints }
  )(AttachQuestEditor)
);
