import React from "react";
import { Typography, withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./QuestEditorColumnContainer";
import { questColumnType } from "../AttachQuestEditor";
import QuestList from "./QuestsList/QuestList";
import CheckpointList from "./CheckpointList/CheckpointList";

interface Props extends WithStyles<typeof styles> {
  header: string;
  columnType: string;
}

const QuestEditorColumnContainer: React.FC<Props> = ({ classes, columnType, header }): JSX.Element => {
  const { columnContainer, columnHeaderContainer, columnHeader } = classes;
  return (
    <div className={columnContainer}>
      <div className={columnHeaderContainer}>
        <Typography className={columnHeader}>{header}</Typography>
      </div>
      {columnType === questColumnType ? <QuestList /> : <CheckpointList />}
    </div>
  );
};

export default withStyles(styles)(QuestEditorColumnContainer);
