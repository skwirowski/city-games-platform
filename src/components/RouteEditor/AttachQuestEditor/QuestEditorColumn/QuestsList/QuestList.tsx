import React from "react";
import { connect } from "react-redux";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./QuestListStyles";
import PerfectScrollbar from "react-perfect-scrollbar";
import { setRouteCheckpoints, setMapCenter, setCheckpoint } from "../../../../../store/actions";
import QuestElement from "./QuestElement/QuestElement";

interface Props extends WithStyles<typeof styles> {
  routeQuests: any;
}

const QuestList: React.FC<Props> = ({ classes, routeQuests }): JSX.Element => {
  const { questList, srollbarContainer } = classes;
  return (
    <div className={questList}>
      <PerfectScrollbar className={srollbarContainer}>
        {routeQuests && routeQuests.map((quest: any, index: number) => <QuestElement key={quest.uuid} quest={quest} index={index} />)}
      </PerfectScrollbar>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    routeQuests: state.questsGM.storeQuests
  };
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    { setRouteCheckpoints, setMapCenter, setCheckpoint }
  )(QuestList)
);
