import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  questList: {
    textAlign: "justify",
    height: "78vh",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "0 0 4px 4px"
  },
  srollbarContainer: {
    paddingBottom: "90px"
  }
});
