import React from "react";
import { Icon } from "antd";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles, QuestContainer, QuestDropBox } from "./QuestElementStyles";
import { Draggable, Droppable } from "react-beautiful-dnd";
import useToggle from "../../../../../../hooks/useToggle";
import Timer from "@material-ui/icons/Timer";
import TimerOff from "@material-ui/icons/TimerOff";
import CheckBox from "@material-ui/icons/CheckBox";
import RadioButtonChecked from "@material-ui/icons/RadioButtonChecked";
import InfoPopper from "../../../../InfoPopper/InfoPopper";

interface Props extends WithStyles<typeof styles> {
  quest: any;
  index: number;
}
const QuestElement: React.FC<Props> = ({ classes, quest, index }) => {
  const { uuid, name } = quest;
  const { questContent, dropBoxIconContainer, questNameText, iconExtended } = classes;
  const [dropBoxEnter, toggleDropBoxEnter] = useToggle(false);

  return (
    <Droppable isDropDisabled droppableId={uuid}>
      {(provided, snapshot) => {
        const isDragging = snapshot.draggingFromThisWith === uuid;
        return (
          <QuestContainer dropBoxEnter={dropBoxEnter} isDragging={isDragging} {...provided.droppableProps} ref={provided.innerRef}>
            <Draggable key={uuid} draggableId={uuid} index={index}>
              {(provided, snapshot) => {
                const isDraggingOver = Boolean(snapshot.draggingOver);
                return (
                  <div>
                    <QuestDropBox
                      isDragging={isDragging}
                      isDraggingOver={isDraggingOver}
                      onMouseEnter={() => toggleDropBoxEnter()}
                      onMouseLeave={() => toggleDropBoxEnter()}
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <Icon style={{ fontSize: "1.9rem", top: "-5px" }} type="question" />
                    </QuestDropBox>
                  </div>
                );
              }}
            </Draggable>
            <div className={questContent}>
              <p className={questNameText}>{name}</p>
              <div>
                <InfoPopper quest={quest} className={iconExtended} isQuest={true} />
              </div>
              <div className={dropBoxIconContainer}>
                {quest.timer ? <Timer fontSize="inherit" /> : <TimerOff fontSize="inherit" />}
                {quest.questionType === "SINGLE_CHOICE_QUESTION" ? <RadioButtonChecked fontSize="inherit" /> : <CheckBox fontSize="inherit" />}
              </div>
              <span style={{ position: "absolute" }}>{provided.placeholder}</span>
            </div>
          </QuestContainer>
        );
      }}
    </Droppable>
  );
};
//   <Icon type="bulb" style={{ position: "absolute", top: "5px", right: "5px" }} />

export default withStyles(styles)(QuestElement);
