import { createStyles } from "@material-ui/core";
import styled from "styled-components";

export const styles = createStyles({
  questElement: {
    width: 95,
    height: 95,
    borderRadius: 4,

    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.3), 0px 1px 1px 0px rgba(0,0,0,0.19), 0px 2px 1px -1px rgba(0,0,0,0.15)"
  },
  questContent: {
    display: "grid",
    gridTemplateColumns: "2fr",
    gridTemplateRows: "2fr",
    marginLeft: 20
  },
  toolButton: {
    padding: 0,
    minWidth: "30px",
    height: "30px",
    fontSize: "1.1rem",
    color: "#757575"
  },
  dropBoxIconContainer: {
    position: "absolute",
    display: "flex",
    bottom: 5,
    right: 5,
    fontSize: 15
  },
  questNameText: {
    wordBreak: "break-all",
    "&::first-letter": {
      textTransform: "capitalize"
    }
  },
  iconExtended: {
    minWidth: "30px",
    height: "30px",
    fontSize: "1.4rem",
    color: "#757575"
  }
});

interface IsDragging {
  isDragging: boolean;
}
interface DraggingOver {
  isDraggingOver: boolean;
}

interface DropBoxEnter extends IsDragging {
  dropBoxEnter: boolean;
}

interface QuestDropBoxProps extends IsDragging, DraggingOver {}

export const QuestContainer = styled.div<DropBoxEnter>`
  color: #757575;
  position: relative;
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  padding: 5px;
  border: 1px solid transparent;
  border-radius: 4px;
  background-color: #fff;
  min-height: 112px;
  margin: 0 12px 10px 12px;
  font-size: 0.8125rem;
  outline: none;
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.3), 0px 1px 1px 0px rgba(0, 0, 0, 0.19), 0px 2px 1px -1px rgba(0, 0, 0, 0.15);
  transition: border-color 0.3s ease;
  &:hover {
    border: 1px solid ${props => (props.dropBoxEnter || props.isDragging ? "transparent" : "var(--main-gm-blue)")};
  }

  &:first-child {
    margin-top: 10px;
  }
`;
// &:after {
//   content: "";
//   position: absolute;
//   left: 50%;
//   top: 11px;
//   width: 1px;
//   height: 90px;
//   border: 0.1px solid #d9d9d9;
// }

export const QuestDropBox = styled.div<QuestDropBoxProps>`
  will-change: opacity;
  border: 1px solid ${props => (props.isDragging ? "var(--main-gm-blue)" : "#d9d9d9")};
  position: relative;
  background-color: ${props => (props.isDragging ? "#eaf4ff" : "#fafafa")};
  opacity: ${props => (props.isDraggingOver ? 0.6 : 1)};
  display: flex;
  word-break: brek-all;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 100px;
  padding: 5px;
  border-radius: 4px;
  transition: border-color 0.3s ease, color 0.3s ease, background-color 0.3s ease;
  &:hover {
    border: 1px solid var(--main-gm-blue);
    color: var(--main-gm-blue);
    background-color: #eaf4ff;
  }
`;
