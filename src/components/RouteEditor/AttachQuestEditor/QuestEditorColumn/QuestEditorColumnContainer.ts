import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  columnContainer: {
    borderRadius: "4px",
    margin: "5px 5px 10px 5px",
    boxShadow: " 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)",
    overflow: "hidden"
  },

  columnHeader: {
    backgroundColor: "var(--main-gm-blue)",
    fontSize: "1.1rem",
    color: "#fff",
    width: "60%",
    padding: "10px 0px 10px 0px"
  },
  columnHeaderContainer: {
    backgroundColor: "var(--main-gm-blue)",
    display: "flex",
    padding: "5px 16px 5px 16px"
  },
  srollbarContainer: {
    paddingBottom: "90px"
  }
});
