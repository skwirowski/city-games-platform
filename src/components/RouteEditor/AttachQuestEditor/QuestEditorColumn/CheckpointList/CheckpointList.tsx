import React from "react";
import { connect } from "react-redux";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles } from "./CheckpointListStyles";
import PerfectScrollbar from "react-perfect-scrollbar";
import { setRouteCheckpoints, setMapCenter, setCheckpoint, detachQuestFromCheckpoint } from "../../../../../store/actions";
import CheckpointElement from "./CheckpointElement/CheckpointElement";

interface Props extends WithStyles<typeof styles> {
  routeCheckpoints: any;
  setMapCenter: (coords: any, lngOffset: boolean, offsetValue: number, routeEditor: boolean) => void;
  setCheckpoint: (checkpoint: any) => void;
  selectedCheckpoint: any;
  detachQuestFromCheckpoint: (quest: any, checkpoint_id: string) => void;
}

const CheckpointList: React.FC<Props> = ({
  classes,
  routeCheckpoints,
  setMapCenter,
  setCheckpoint,
  selectedCheckpoint,
  detachQuestFromCheckpoint
}): JSX.Element => {
  const { checkpointList, srollbarContainer } = classes;

  const onCenterClick = (checkpoint: any) => {
    if (!checkpoint) return;
    const { longitude, latitude } = checkpoint;
    setMapCenter([latitude, longitude], true, 0.05, true);
    setCheckpoint(checkpoint);
  };

  return (
    <div className={checkpointList}>
      <PerfectScrollbar className={srollbarContainer}>
        {routeCheckpoints &&
          routeCheckpoints.map((checkpoint: any, index: number) => (
            <CheckpointElement
              key={checkpoint.checkpoint_id}
              checkpoint={checkpoint}
              selectedCheckpoint={selectedCheckpoint}
              index={index}
              onCenterClick={onCenterClick}
              detachQuestFromCheckpoint={detachQuestFromCheckpoint}
            />
          ))}
      </PerfectScrollbar>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    routeCheckpoints: state.routeCheckpoints,
    selectedCheckpoint: state.checkpoint
  };
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    { setRouteCheckpoints, setMapCenter, setCheckpoint, detachQuestFromCheckpoint }
  )(CheckpointList)
);
