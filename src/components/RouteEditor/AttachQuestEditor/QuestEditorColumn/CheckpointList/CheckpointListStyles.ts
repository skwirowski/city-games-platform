import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  checkpointList: {
    textAlign: "justify",
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: "0 0 4px 4px",
    height: "78vh"
  },
  srollbarContainer: {
    paddingBottom: "90px"
  }
});
