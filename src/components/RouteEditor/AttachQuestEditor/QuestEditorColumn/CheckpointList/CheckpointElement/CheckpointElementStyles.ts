import { createStyles, Popper } from "@material-ui/core";
import styled from "styled-components";

export const styles = createStyles({
  checkpointContent: {
    display: "grid",
    gridTemplateColumns: "2fr",
    gridTemplateRows: "2fr",
    marginLeft: 25
  },
  toolButton: {
    padding: 0,
    minWidth: "30px",
    height: "30px",
    fontSize: "1.1rem",
    color: "#757575"
  },
  selectedCheckpointElement: {
    backgroundColor: "#EAF4FF",
    border: "1px solid var(--main-gm-blue)"
  },
  questionIcon: {
    position: "absolute",
    top: "4px",
    right: "4px",
    fontSize: "0.9rem"
  },
  iconExtended: {
    minWidth: "30px",
    height: "30px",
    fontSize: "1.4rem",
    color: "#757575"
  }
});

interface DropBoxEnter {
  dropBoxEnter: boolean;
  isSelected: boolean;
}

export const ElementContainer = styled.div<DropBoxEnter>`
  color: #757575;
  position: relative;
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  padding: 5px;
  border: 1px solid ${props => (!props.isSelected ? "transparent" : "var(--main-gm-blue)")};
  border-radius: 4px;
  background-color: ${props => (!props.isSelected ? "#fff" : "#EAF4FF")};
  min-height: 112px;
  margin: 0 12px 10px 12px;
  font-size: 0.8125rem;
  outline: none;
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.3), 0px 1px 1px 0px rgba(0, 0, 0, 0.19), 0px 2px 1px -1px rgba(0, 0, 0, 0.15);
  transition: border-color 0.3s ease;
  &:hover {
    border: ${props => (props.dropBoxEnter && !props.isSelected ? "1px solid transparent" : "1px solid var(--main-gm-blue)")};
  }

  &:first-child {
    margin-top: 10px;
  }
`;
//position: relative;
// &:after {
//   content: "";
//   position: absolute;
//   left: 50%;
//   top: 11px;
//   width: 1px;
//   height: 90px;
//   border: 0.1px solid #d9d9d9;
// }

export const QuestDropBox = styled.div`
  border: 1px dashed #d9d9d9;
  background-color: #fafafa;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 100px;
  padding: 5px;
  border-radius: 4px;
  transition: border-color 0.3s ease, color 0.3s ease, background-color 0.3s ease;
  &:hover {
    border: 1px dashed var(--main-gm-blue);
    color: var(--main-gm-blue);
    background-color: #eaf4ff;
  }
`;

export const AttachedQuest = styled.div`
  border: 1px solid #d9d9d9;
  position: relative;
  background-color: #fafafa;
  display: flex;
  word-break: break-all;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 100px;
  padding: 5px;
  border-radius: 4px;
  transition: border-color 0.3s ease, color 0.3s ease, background-color 0.3s ease;
  &:hover {
    border: 1px dashed var(--main-gm-blue);
    color: var(--main-gm-blue);
    background-color: #eaf4ff;
  }
`;

export const DropBoxHeaderText = styled.p`
  font-size: 0.7rem;
  margin-bottom: 0px;
`;
export const DropBoxContentText = styled.p`
  font-size: 0.6rem;
  margin-bottom: 0px;
`;

export const StyledPopper = styled<any>(Popper)`
  z-index: 2400;
  marginbottom: 10px;
  minwidth: 150px;

  left: -5px;
`;
export const Anchor = styled.span`
  position: absolute;
  width: 3em;
  bottom: 0;
  height: 1em;
  margin-bottom: -0.9em;
  &::after {
    left: -5px;
    position: relative;
    width: 0;
    height: 0;
    margin: auto;
    content: "";
    display: block;
    border-style: solid;
    border-color: #fff #0000 #0000 #0000;
    border-width: 1em 1em 0 1em;
  }
`;
