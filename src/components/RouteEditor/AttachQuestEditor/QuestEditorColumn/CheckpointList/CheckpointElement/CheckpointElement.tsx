import React from "react";
import { withStyles, WithStyles } from "@material-ui/core";
import { styles, ElementContainer, QuestDropBox, DropBoxHeaderText, DropBoxContentText, AttachedQuest } from "./CheckpointElementStyles";
import useToggle from "../../../../../../hooks/useToggle";
import Location from "@material-ui/icons/MyLocation";
import Delete from "@material-ui/icons/Delete";
import Button from "@material-ui/core/Button";
import InfoPopper from "../../../../InfoPopper/InfoPopper";
import { Droppable } from "react-beautiful-dnd";
import classnames from "classnames";

interface Props extends WithStyles<typeof styles> {
  checkpoint: any;
  index: number;
  onCenterClick: (checkpoint: any) => void;
  selectedCheckpoint: any;
  detachQuestFromCheckpoint: (quest: any, checkpoint_id: string) => void;
}

const CheckpointElement: React.FC<Props> = ({ classes, checkpoint, index, onCenterClick, selectedCheckpoint, detachQuestFromCheckpoint }) => {
  const { checkpoint_id, name } = checkpoint;
  const [dropBoxEnter, toggleDropBoxEnter] = useToggle(false);
  const { checkpointContent, toolButton, iconExtended } = classes;
  const isSelected = selectedCheckpoint.checkpoint_id === checkpoint_id;
  return (
    <ElementContainer dropBoxEnter={dropBoxEnter} isSelected={isSelected}>
      <Droppable isCombineEnabled droppableId={checkpoint_id}>
        {(provided, snapshot) => {
          return !checkpoint.attachedQuest ? (
            <div>
              <QuestDropBox
                {...provided.droppableProps}
                ref={provided.innerRef}
                onMouseEnter={() => toggleDropBoxEnter()}
                onMouseLeave={() => toggleDropBoxEnter()}
              >
                <DropBoxHeaderText>DROP HERE</DropBoxHeaderText>
                <DropBoxContentText>to attach the quest</DropBoxContentText>
              </QuestDropBox>
              <span style={{ position: "absolute" }}>{provided.placeholder}</span>
            </div>
          ) : (
            <AttachedQuest
              {...provided.droppableProps}
              ref={provided.innerRef}
              onMouseEnter={() => toggleDropBoxEnter()}
              onMouseLeave={() => toggleDropBoxEnter()}
            >
              <div>
                <InfoPopper isQuest={true} quest={checkpoint.attachedQuest} />
                <Button className={toolButton} aria-label="Delete">
                  <Delete fontSize="inherit" aria-label="Delete" onClick={() => detachQuestFromCheckpoint(checkpoint.attachedQuest, checkpoint_id)} />
                </Button>
              </div>
              <span style={{ position: "absolute" }}>{provided.placeholder}</span>
            </AttachedQuest>
          );
        }}
      </Droppable>

      <div className={checkpointContent}>
        <p>{name}</p>
        <div>
          <Button className={classnames(toolButton, iconExtended)} aria-label="Location">
            <Location fontSize="inherit" aria-label="Location" onClick={() => onCenterClick(checkpoint)} />
          </Button>
          <InfoPopper checkpoint={checkpoint} className={iconExtended} />
        </div>
      </div>
    </ElementContainer>
  );
};

export default withStyles(styles)(CheckpointElement);
