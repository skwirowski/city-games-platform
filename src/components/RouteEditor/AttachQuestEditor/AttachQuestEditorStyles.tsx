import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  buttonsContainer: {
    gridArea: "2/2/2/2",
    display: "flex",
    justifyContent: "flex-end"
  },
  cancelButton: {
    margin: "0 5px"
  }
});
