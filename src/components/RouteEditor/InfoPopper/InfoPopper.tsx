import React, { useState } from "react";
import { Button, Fade, Paper, withStyles, WithStyles } from "@material-ui/core";
import Announcement from "@material-ui/icons/Announcement";
import { StyledPopper } from "./InfoPopperStyles";
import { styles } from "./InfoPopperStyles";
import CheckpointText from "./CheckpointText";
import QuestText from "./QuestText";
import classnames from "classnames";

interface Props extends WithStyles<typeof styles> {
  checkpoint?: any;
  quest?: any;
  isQuest?: any;
  className?: string;
}

const InfoPopper: React.FC<Props> = ({ checkpoint, classes, quest, isQuest, className }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { toolButton, paperContainer } = classes;

  const handleClick = (event: React.FocusEvent<HTMLElement> | React.MouseEvent<HTMLElement, MouseEvent>) => {
    setAnchorEl(prev => (prev ? null : event.currentTarget));
  };

  const open = Boolean(anchorEl);
  const id = open ? "no-transition-popper" : undefined;

  return (
    <>
      <Button
        className={className ? classnames(toolButton, className) : toolButton}
        aria-describedby={id}
        tabIndex={0}
        onClick={handleClick}
        onBlur={() => setAnchorEl(null)}
      >
        <Announcement fontSize="inherit" aria-label="Announcement" />
      </Button>
      <StyledPopper
        placement="top-start"
        id={id}
        open={open}
        anchorEl={anchorEl}
        transition
        modifiers={{
          preventOverflow: {
            enabled: false
          }
        }}
      >
        {({ TransitionProps }: { TransitionProps: any }) => (
          <Fade {...TransitionProps} timeout={150}>
            <>
              {!isQuest ? (
                <Paper className={paperContainer}>{checkpoint && <CheckpointText checkpoint={checkpoint} classes={classes} />}</Paper>
              ) : (
                <Paper className={paperContainer}>{quest && <QuestText quest={quest} classes={classes} />}</Paper>
              )}
            </>
          </Fade>
        )}
      </StyledPopper>
    </>
  );
};
//  <Anchor />

export default withStyles(styles)(InfoPopper);
