import { createStyles, Popper } from "@material-ui/core";
import styled from "styled-components";

export const styles = createStyles({
  toolButton: {
    padding: 0,
    minWidth: "30px",
    height: "30px",
    fontSize: "1.1rem",
    color: "#757575"
  },
  paperContainer: {
    position: "relative",
    left: "-8px"
  },
  contentContainer: {
    color: "#757575",
    padding: "13px",
    minWidth: "150px"
    //
  },
  nameText: {
    fontSize: "0.9rem"
  },
  popperText: {
    paddingBottom: 5,
    color: "#333",
    position: "relative",
    "&::first-letter": {
      textTransform: "capitalize"
    }
  },
  textBold: {
    fontWeight: 500
  },
  popperIcon: {
    position: "absolute",
    fontSize: 15,
    color: "#757575"
  },
  answerOptionsText: {},
  answerOptionsContainer: {
    marginLeft: 7
  },
  popperImage: {
    width: 90,
    height: "100%",
    marginBottom: 5
  }
});

export const StyledPopper = styled<any>(Popper)`
  z-index: 2400;
  marginbottom: 10px;
  minwidth: 150px;
  left: -5px;
`;
export const Anchor = styled.span`
  position: absolute;
  width: 3em;
  bottom: 0;
  height: 1em;
  margin-bottom: -0.9em;
  &::after {
    left: -5px;
    position: relative;
    width: 0;
    height: 0;
    margin: auto;
    content: "";
    display: block;
    border-style: solid;
    border-color: #fff #0000 #0000 #0000;
    border-width: 1em 1em 0 1em;
  }
`;
