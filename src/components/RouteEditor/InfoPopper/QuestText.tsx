import React from "react";
import { Typography } from "@material-ui/core";
import classnames from "classnames";
import Timer from "@material-ui/icons/Timer";
import TimerOff from "@material-ui/icons/TimerOff";
import Done from "@material-ui/icons/Done";
import Clear from "@material-ui/icons/Clear";

interface Props {
  quest: any;
  classes: any;
}

const SINGLE_CHOICE_QUESTION = "SINGLE_CHOICE_QUESTION";

const QuestText: React.FC<Props> = ({ quest, classes }) => {
  const { contentContainer, nameText, popperText, textBold, popperIcon, answerOptionsContainer, answerOptionsText, popperImage } = classes;
  const { name, content, questionType, maxPoints, timer, answerOptions, attachedImage } = quest;

  return (
    <div className={contentContainer}>
      {attachedImage && attachedImage.content && (
        <img alt="Quest target" className={popperImage} src={`data:image/png;base64,${attachedImage.content}`} />
      )}

      <Typography className={classnames(popperText, nameText)} variant="h6">
        {name}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>
          Content: <span dangerouslySetInnerHTML={{ __html: content }} />
        </span>
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Question type: </span>
        {questionType === SINGLE_CHOICE_QUESTION ? "Single choice" : "Multiple choice"}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Max points: </span>
        {maxPoints}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Timer: </span>
        {timer ? <Timer className={popperIcon} /> : <TimerOff className={popperIcon} />}
      </Typography>
      <Typography className={classnames(popperText, answerOptionsText)} variant="caption">
        <span className={textBold}>Answer options: </span>
      </Typography>
      <div className={answerOptionsContainer}>
        {quest &&
          answerOptions &&
          answerOptions.map((answer: any, index: number) => (
            <span key={answer.id}>
              <Typography className={popperText} variant="caption">
                <span className={textBold}>{index + 1}: </span>
              </Typography>
              <div className={answerOptionsContainer}>
                <Typography className={popperText} variant="caption">
                  <span className={textBold}>Content: </span>
                  {answer.content}
                </Typography>
              </div>
              <div className={answerOptionsContainer}>
                <Typography className={popperText} variant="caption">
                  <span className={textBold}>Correct: </span>
                  {answer.correct ? <Done className={popperIcon} /> : <Clear className={popperIcon} />}
                </Typography>
              </div>
            </span>
          ))}
      </div>
    </div>
  );
};
export default QuestText;
