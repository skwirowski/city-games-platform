import React from "react";
import { Typography } from "@material-ui/core";
import { convertDMS } from "../../../utils/coordinates";
import moment from "moment";
import classnames from "classnames";

interface Props {
  checkpoint: any;
  classes: any;
}

const CheckpointText: React.FC<Props> = ({ checkpoint, classes }) => {
  const { contentContainer, nameText, popperText, textBold } = classes;
  const { longitude, latitude } = checkpoint;
  const { lat, lng } = convertDMS(longitude, latitude);
  return (
    <div className={contentContainer}>
      <Typography className={classnames(popperText, nameText)} variant="h6">
        {checkpoint.name}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Address: </span>
        {checkpoint.address}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Object name: </span>
        {checkpoint.objectName}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Description on the way: </span>
        {checkpoint.OnTheWay}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Description after arrival: </span>
        {checkpoint.descriptionAfterArrival}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Latitude: </span>
        {lat}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Longitude: </span>
        {lng}
      </Typography>
      <Typography className={popperText} variant="caption">
        <span className={textBold}>Creation date: </span>
        {moment(checkpoint.creationDate).fromNow()}
      </Typography>
    </div>
  );
};
export default CheckpointText;
