import React from "react";
import QrCodeScanner from "../QrCodeScanner";
import { verifyGameDefinitionQrCode } from "../../services/verifyGameDefinitionQrCode";
import { createGameForTeam } from "../../services/createGameForTeam";
import { getRouteForTeamInGames } from "../../services/getRouteForTeamInGames";
import { getQuestForCheckpoints } from "../../services/getQuestForCheckpoints";
import { routes } from "../../static/routesUrl";

interface Props {
  history: {
    push: (arg: string) => {};
  };
}

const StartGameQrScanner: React.SFC<Props> = ({ history }) => {
  const handleScan = async (data: string) => {
    try {
      await verifyGameDefinitionQrCode(data);
      const { data: routeForTeamInGames } = await getRouteForTeamInGames();
      const { routeGuid } = routeForTeamInGames;
      const { data: checkpoints } = await getQuestForCheckpoints(routeGuid);
      await createGameForTeam(checkpoints);
      history.push(routes.nextCheckpoint);
    } catch (error) {
      history.push(routes.invalidQrCode);
    }
  };

  return <QrCodeScanner onScan={handleScan} />;
};

export default StartGameQrScanner;
