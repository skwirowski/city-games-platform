import { createStyles } from "@material-ui/core";
import { secondary } from "../../shared/colors";
import bg from "../../assets/images/bg_night_city.jpg";

export const styles = createStyles({
  bg: {
    width: "100%",
    height: "40vh",
    background: `url(${bg}) no-repeat top fixed`,
    backgroundSize: "cover",
    position: "relative",
    borderBottomLeftRadius: "50px",
    borderBottomRightRadius: "50px"
  },
  textContainer: {
    width: "100%",
    padding: "1em 2em"
  },
  topText: {
    color: secondary,
    fontWeight: "bold"
  },
  bottomText: {
    margin: "1em 0"
  }
});
