import React from "react";
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from "enzyme";
import StartGameScreen from "../StartGameScreen";

describe("StartGameScreen", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedStartGameScreen = unwrap(StartGameScreen);
const createSut = props =>
  shallow(<UnwrappedStartGameScreen classes={{}} {...props} />);
