import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { styles } from "./StartGameScreenStyles";
import { routes } from "../../static/routesUrl";
import NavBar from "../TeamNavBar";
import Link from "../../shared/LinkWithoutDefaultStyles";
import Button from "../../shared/Button";

interface Props extends WithStyles<typeof styles> {}

const StartGameScreen: React.SFC<Props> = ({ classes }) => {
  return (
    <Grid container justify="center">
      <Grid item xs={12}>
        <div className={classes.bg} />
        <NavBar />
      </Grid>
      <Grid item xs={12} className={classes.textContainer}>
        <Typography variant="subtitle1" className={classes.topText}>
          <FormattedMessage id="StartGameScreen.topText" />
        </Typography>
        <Typography variant="subtitle2" className={classes.bottomText}>
          <FormattedMessage id="StartGameScreen.bottomText" />
        </Typography>
      </Grid>
      <Link to={routes.startGameQrScanner}>
        <Button color="secondary">
          <FormattedMessage id="StartGameScreen.btnText" />
        </Button>
      </Link>
    </Grid>
  );
};

export default withStyles(styles)(StartGameScreen);
