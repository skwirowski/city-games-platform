const styledScroll = {

  '&::-webkit-scrollbar': {
    backgroundColor: '#EBECF0',
    width: '16px'

  },
  '&::-webkit-scrollbar-track': {
    backgroundColor: '#ffffff',
  },
  '&::-webkit-scrollbar-track:hover': {},
  '&::-webkit-scrollbar-thumb': {
    backgroundColor: '#babac0',
    borderRadius: '16px',
    border: '5px solid #ffffff'
  },
  '&::-webkit-scrollbar-thumb:hover': {
    backgroundColor: '#a0a0a5',
  },

}

export default styledScroll