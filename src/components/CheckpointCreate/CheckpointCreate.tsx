import React from "react";

import Map from "../Map/Map";
import { Button, Icon } from "antd";
import useToggle from "../../hooks/useToggle";
import CheckpointFormDrawer from "./CheckpointFormDrawer";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import CheckpointSearch from "./CheckpointSearch/CheckpointSearch";
const styles = createStyles({
  container: {
    width: "100%",
    height: "100%"
  },
  drawer: {
    width: "900px",
    flexShrink: 0
  },
  drawerHeader: {
    width: "15vw",
    display: "flex",
    alignItems: "center",
    padding: "0 8px",
    justifyContent: "flex-end"
  },
  paper: {
    top: "64px"
  },
  toggleButton: {
    position: "absolute",
    zIndex: 1999,
    right: 50,
    top: 100,
    marginBottom: "16px"
  }
});

interface Props extends WithStyles<typeof styles> {
  toggle: boolean;
  toggleDrawer: any;
  history: {
    push: (arg: string) => {};
  };
  match: {
    params: {
      guid: string;
    };
  };
}

const CheckpointCreate: React.FC<Props> = ({ classes, history, match }) => {
  const [toggle, toggleDrawer] = useToggle(false);

  return (
    <div className={classes.container}>
      {!toggle && (
        <Button className={classes.toggleButton} type="primary" size="large" onClick={toggleDrawer}>
          <Icon type="form" />
        </Button>
      )}
      <Map singleCheckpoint={true} toggle={toggle} toggleDrawer={toggleDrawer} match={match} />
      <CheckpointFormDrawer toggle={toggle} toggleDrawer={toggleDrawer} history={history} match={match} />
      <CheckpointSearch toggleProps={{ toggle, toggleDrawer }} />
    </div>
  );
};

export default withStyles(styles)(CheckpointCreate);
