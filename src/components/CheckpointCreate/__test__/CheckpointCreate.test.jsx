import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import CheckpointCreate from '../CheckpointCreate';

describe('CheckpointCreate', () => {
  let wrapper;

  beforeEach(() => {
    const Unwrapped = unwrap(<CheckpointCreate />);
    wrapper = shallow(Unwrapped);
  });
  it('should render without crash', () => {
    expect(wrapper).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
