import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import CheckpointFormDrawer from '../CheckpointFormDrawer';

describe('CheckpointFormDrawer', () => {
  let wrapper;

  beforeEach(() => {
    const Unwrapped = unwrap(<CheckpointFormDrawer />);
    wrapper = shallow(Unwrapped);
  });
  it('should render without crash', () => {
    expect(wrapper).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
