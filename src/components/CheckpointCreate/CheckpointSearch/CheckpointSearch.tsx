import React, { useState } from "react";
import { connect } from "react-redux";
import { Button } from "antd";
import { TextField, Paper, withStyles, Fab } from "@material-ui/core";
import { setCheckpoint, setCheckpointCords, setMapCenter } from "../../../store/actions";
import Close from "@material-ui/icons/Close";
import { convertDMS } from "../../../utils/coordinates";
import uuidv4 from "uuid/v4";
import { searchByAddress } from "../../../services/searchByAddress";
import { styles } from "./CheckpointSearchStyles";
import { CSSTransition } from "react-transition-group";
import "../../styles/dialog.css";
import useToggle from "../../../hooks/useToggle";

import LocationSearching from "@material-ui/icons/LocationSearching";

const CheckpointSearch = (props: any) => {
  const initSearchData = {
    DisplayPosition: { lat: 0, lng: 0 },
    Label: ""
  };
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(false);

  const [error, setError] = useState("");
  const [searchOpen, toggleSearchOpen] = useToggle(false);

  const [searchedData, setSearchedData] = useState(initSearchData);
  const {
    checkpoint,
    setCheckpoint,
    setMapCenter,
    mapMode,
    toggleProps,
    classes: { searchContainer, searchButton, searchContnent, errorMessage, addressLabel, closeIcon, openSearch, searchInput }
  } = props;

  const onInputChange = (e: any) => {
    setText(e.target.value);
  };

  const onSearchHandle = async () => {
    if (!text || text.length < 3) {
      return setError("Address must contain at least 3 characters.");
    }
    setError("");

    try {
      setLoading(true);
      const data = await searchByAddress(text);
      setLoading(false);

      if (!data.length) {
        return setError("No search results.");
      }

      const {
        DisplayPosition: { Latitude, Longitude },
        Address: { Label }
      } = data[0].Result[0].Location;

      const { lat, lng } = convertDMS(Latitude, Longitude);
      setCheckpoint({
        ...checkpoint,
        latitude: lat,
        longitude: lng,
        lat: Latitude,
        lng: Longitude,
        checkpoint_id: uuidv4(),
        address: Label
      });

      setMapCenter([Latitude, Longitude], true, mapMode.lngOffset, false);
      if (!toggleProps.toggle) {
        toggleProps.toggleDrawer();
      }
      const dataSearched = {
        DisplayPosition: { lat: Latitude, lng: Longitude },
        Label: Label
      };
      setSearchedData(dataSearched);
      if (!errorMessage.length) {
        setError("");
      }
    } catch (error) {
      console.log("TCL: onRequestSend -> error", error);
    }
  };

  return (
    <>
      <Fab className={openSearch} color="primary" onClick={() => toggleSearchOpen()} aria-label="Add" size="medium">
        <LocationSearching style={{ fontSize: "1.9rem" }} />
      </Fab>
      <CSSTransition in={searchOpen} timeout={400} classNames="dialog" unmountOnExit appear>
        <Paper className={searchContainer}>
          <Close className={closeIcon} onClick={toggleSearchOpen} />
          <div className={searchContnent}>
            <TextField placeholder="Address" className={searchInput} type="text" value={text} onChange={onInputChange} />
            <Button className={searchButton} loading={loading} onClick={onSearchHandle} icon="search" type="primary">
              Search
            </Button>
          </div>
          {error ? <span className={errorMessage}>{error}</span> : <span className={addressLabel}>{searchedData.Label}</span>}
        </Paper>
      </CSSTransition>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    checkpoint: state.checkpoint,
    mapMode: state.mapMode
  };
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    { setCheckpoint, setCheckpointCords, setMapCenter }
  )(CheckpointSearch)
);
