import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  searchContainer: {
    position: "absolute",
    zIndex: 2000,
    bottom: "50px",
    left: "250px",
    width: "325px",
    height: "100px",
    padding: "35px 15px 15px 15px"
  },
  searchButton: {
    marginLeft: "15px"
  },
  searchContnent: {
    display: "flex"
  },
  errorMessage: {
    fontSize: "0.7rem",
    color: "red"
  },
  addressLabel: {
    color: "var(--main-gm-blue)",
    fontSize: "0.7rem"
  },
  closeIcon: {
    position: "absolute",
    top: "5px",
    right: "5px",
    fontSize: "1.4rem",
    "&:hover": {
      color: "var(--main-gm-blue)"
    }
  },
  openSearch: {
    position: "absolute",
    bottom: 55,
    left: 255,
    zIndex: 1200,
    backgroundColor: "var(--main-gm-blue)",
    "&:hover": {
      backgroundColor: "var(--main-gm-blue--hover)"
    }
  },
  searchInput: {
    "&::before": {
      borderColor: "var(--main-gm-blue)"
    },
    "&::after": {
      borderColor: "var(--main-gm-blue)"
    },
    "&:hover:not(.disabled):not(.error):not(.focused):before": {
      borderBottomColor: "var(--main-gm-blue)"
    }
  }
});
