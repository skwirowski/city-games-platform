import React from "react";
import { WithStyles, withStyles, createStyles, Drawer, IconButton, Toolbar } from "@material-ui/core";
import ChevronRight from "@material-ui/icons/ChevronRight";
import CheckpointFormContainer from "../CheckpointFormContainer/CheckpointFormContainer";

const styles = createStyles({
  drawer: {
    flexShrink: 0
  },
  drawerHeader: {
    width: "15vw",
    display: "flex",
    alignItems: "center",
    padding: "0 8px",
    justifyContent: "flex-end"
  },
  paper: {
    top: "64px",
    width: "35%",
    minWidth: "430px"
  }
});

interface Props extends WithStyles<typeof styles> {
  toggle: boolean;
  toggleDrawer: () => void;
  history: {
    push: (arg: string) => {};
  };
  match: {
    params: {
      guid: string;
    };
  };
}

const CheckpointFormDrawer: React.SFC<Props> = ({ classes, toggle, toggleDrawer, history, match }) => {
  return (
    <Drawer
      classes={{ paper: classes.paper }}
      className={classes.drawer}
      variant="persistent"
      anchor="right"
      open={toggle}
    >
      <Toolbar>
        <IconButton onClick={toggleDrawer}>
          <ChevronRight />
        </IconButton>
      </Toolbar>

      <CheckpointFormContainer history={history} match={match} />
    </Drawer>
  );
};

export default withStyles(styles)(CheckpointFormDrawer);
