import React, { useState, useEffect } from "react";
import { WithStyles, withStyles, Typography } from "@material-ui/core";
import { LocationOnOutlined, Timer, StarBorder } from '@material-ui/icons';
import { FormattedMessage } from "react-intl";
import Button from '../../shared/Button';
import NavBar from "../TeamNavBar";
import styles from './StartQuestStyles';

interface StartQuestProps extends WithStyles<typeof styles> {}

const StartQuest: React.FC<StartQuestProps> = ({ classes }) => {
  const [locationName, setLocationName] = useState<string>("");
  const [isQuestAvailable, setQuestAvailability] = useState<boolean>(false);
  const [questDescription, setQuestDescription] = useState<string>("");
  const [timeCondition, setTimeCondition] = useState<string>("");
  const [maxPoints, setMaxPoints] = useState<string>("");

  useEffect(() => {
    setLocationName("Most Tumski");
    setQuestDescription("Wstępny opis zadania! Ex amet ut dolore qui aliqua occaecat. Velit anim fugiat consequat dolor consectetur adipisicing sunt consectetur veniam est proident enim fugiat. Labore adipisicing sint est commodo nisi. Deserunt commodo Lorem est laborum irure nisi qui ad non mollit eu tempor ipsum.");
    setTimeCondition("Zadanie na czas");
    setMaxPoints("10");
  });

  return (
    <div className={classes.container}>
      <div>
        <NavBar color="secondary" />
        <Typography variant="h5" className={classes.title}>
          <LocationOnOutlined className={classes.locationIcon} />
          {locationName}
        </Typography>
      </div>
      <div className={classes.descriptionWrapper}>
        <Typography variant="subtitle1" component="p" className={classes.description}>
          {questDescription}
        </Typography>
        <div className={classes.goalsWrapper}>
          <Typography variant="subtitle1" component="span" className={classes.time}>
            <Timer className={classes.timerIcon} />
            {timeCondition}
          </Typography>
          <Typography variant="subtitle1" component="span" className={classes.points}>
            <StarBorder className={classes.starIcon} />
            <FormattedMessage id="StartQuest.maxPointsNumber" />: {maxPoints}
          </Typography>
        </div>
        {
          isQuestAvailable ? (
            <div className={classes.buttonsContainer}>
              <Button color="primary"><FormattedMessage id="StartQuest.solvePuzzle" /></Button>
              <Button color="secondary" variant="outlined"><FormattedMessage id="StartQuest.skipPuzzle" /></Button>
            </div>
          ) : (
            <Button color="primary"><FormattedMessage id="StartQuest.moveOn" /></Button>
          )
        }
      </div>
    </div>
  );
};

export default withStyles(styles)(StartQuest);