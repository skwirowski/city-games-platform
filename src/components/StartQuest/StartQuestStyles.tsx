import { createStyles } from "@material-ui/core";
import { secondary } from "../../shared/colors";

const styles = createStyles({
  container: {
    backgroundColor: "rgb(238,249,255)",
    width: "100%",
    minHeight: "100vh",
    paddingBottom: 30,
    textAlign: "center",
    "& p": {
      fontSize: "1.2em",
      letterSpacing: "1px"
    }
  },
  title: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    backgroundColor: "white",
    padding: "0.5em",
    color: secondary
  },
  locationIcon: {
    marginRight: 15
  },
  descriptionWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "80%",
    margin: "auto"
  },
  description: {
    textAlign: "left",
    padding: "30px 0"
  },
  goalsWrapper: {
    textAlign: "left",
    backgroundColor: "#FFFFFF",
    width: 300,
    borderRadius: 10,
    margin: "0 auto 30px auto",
    padding: 20
  },
  time: {
    display: "flex",
    alignItems: "center",
    color: "rgb(128, 128, 128)",
    marginBottom: 10
  },
  timerIcon: {
    width: 35,
    height: 35,
    color: secondary,
    marginRight: 15
  },
  points: {
    display: "flex",
    alignItems: "center",
    color: "rgb(128, 128, 128)",
    fontWeight: 600
  },
  starIcon: {
    width: 35,
    height: 35,
    color: secondary,
    marginRight: 15
  },
  buttonsContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: 122
  }
});


export default styles;