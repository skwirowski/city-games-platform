import React from "react";
import { WithStyles, withStyles, Typography } from "@material-ui/core";
import { ExtensionOutlined, Timer, StarBorder } from "@material-ui/icons";
import { FormattedMessage } from "react-intl";
import NavBar from "../../TeamNavBar";
import styles from "../CheckpointQuestStyles";

interface Props extends WithStyles<typeof styles> {
  title: string;
  timeForQuest: string;
  maxPoints: string;
  questDescription: string;
  questQuestion: string;
}

const QuestDescription: React.FC<Props> = ({
  classes,
  title,
  timeForQuest,
  maxPoints,
  questDescription,
  questQuestion
}) => {
  return (
    <div className={classes.container}>
      <div>
        <NavBar color="secondary" />
        <Typography variant="h5" className={classes.title}>
          <ExtensionOutlined className={classes.extensionIcon} />
          {title}
        </Typography>
      </div>
      <div className={classes.goalsContainer}>
        <Typography className={classes.goalWrapper}>
          <Timer className={classes.timerIcon} />
          {timeForQuest}
        </Typography>
        <Typography className={classes.goalWrapper}>
          <StarBorder className={classes.starIcon} />
          {maxPoints}
          <FormattedMessage id="QuestInProgress.moveOn" />
        </Typography>
      </div>
      <div className={classes.descriptionWrapper}>
        <Typography
          variant="subtitle1"
          component="p"
          className={classes.description}
        >
          {questDescription}
        </Typography>
        <Typography
          variant="subtitle1"
          component="p"
          className={classes.question}
        >
          {questQuestion}
        </Typography>
      </div>
    </div>
  );
};

export default withStyles(styles)(QuestDescription);
