import React from "react";
import { WithStyles, withStyles, TextField, Grid } from "@material-ui/core";
import styles from "../CheckpointQuestStyles";

interface IProps extends WithStyles<typeof styles> {
  handleChange: any;
}

const QuestInProgress: React.FC<IProps> = ({ classes, handleChange }) => {
  return (
    <Grid>
      <TextField
        onChange={handleChange}
        multiline
        rows={10}
        className={classes.textField}
        margin="normal"
      />
    </Grid>
  );
};

export default withStyles(styles)(QuestInProgress);
