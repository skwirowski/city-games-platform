import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import Radio from "@material-ui/core/Radio";

const styles = createStyles({
  container: {
    width: "100%",
    paddingLeft: "30px"
  },
  content: {
    textAlign: "left"
  },
  radio: {
    width: "15%"
  },
  contentContainer: {
    width: "85%",
    justifyContent: "flex-start"
  }
});

interface Props extends WithStyles<typeof styles> {
  answersOptions: any;
  singleChoiceAnswer: string;
  handleChangeSingleChoice: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const SingleChoiceQuest: React.FC<Props> = ({
  classes,
  answersOptions,
  singleChoiceAnswer,
  handleChangeSingleChoice
}) => {
  return (
    <Grid container direction="column" className={classes.container}>
      {answersOptions.length > 0
        ? answersOptions.map((answerOption: any, index: any) => (
            <Grid container direction="row" key={index}>
              <Grid container direction="row">
                <Grid className={classes.radio}>
                  <Radio
                    checked={singleChoiceAnswer === answerOption.uuid}
                    onChange={e => handleChangeSingleChoice(e)}
                    value={answerOption.uuid}
                    name="radio-button-demo"
                    inputProps={{ "aria-label": "A" }}
                  />
                </Grid>
                <Grid
                  container
                  alignContent="center"
                  className={classes.contentContainer}
                >
                  <Typography className={classes.content}>
                    {answerOption.content}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          ))
        : ""}
      ;
    </Grid>
  );
};

export default withStyles(styles)(SingleChoiceQuest);
