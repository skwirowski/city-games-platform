import React from "react";
import { WithStyles, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import Button from "../../../shared/Button";
import styles from "../CheckpointQuestStyles";

interface QuestInProgressProps extends WithStyles<typeof styles> {
  submit: any;
  skipPuzzle: any;
}

const QuestInProgress: React.FC<QuestInProgressProps> = ({
  classes,
  submit,
  skipPuzzle
}) => {
  return (
    <div className={classes.buttonsContainer}>
      <Button color="primary" onClick={submit}>
        <FormattedMessage id="QuestInProgress.sendAnswerButton" />
      </Button>
      <Button color="secondary" variant="outlined" onClick={skipPuzzle}>
        <FormattedMessage id="QuestInProgress.skipPuzzle" />
      </Button>
    </div>
  );
};

export default withStyles(styles)(QuestInProgress);
