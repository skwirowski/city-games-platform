import React, { useState } from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  FormControl,
  FormGroup,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";

const styles = createStyles({
  container: {
    width: "100%",
    paddingLeft: "35px",
    paddingBottom: "40px"
  },
  content: {
    textAlign: "left"
  },
  radio: {
    width: "15%"
  },
  contentContainer: {
    width: "85%",
    justifyContent: "flex-start"
  }
});

interface Props extends WithStyles<typeof styles> {
  answersOptions: any;
  handleChangeMultipleChoice: any;
  returnChecked: any;
}

const MultiChoiceQuest: React.FC<Props> = ({
  classes,
  answersOptions,
  handleChangeMultipleChoice,
  returnChecked
}) => {
  return (
    <Grid container direction="column" className={classes.container}>
      {answersOptions.length > 0
        ? answersOptions.map((answer: any, index: any) => (
            <FormControl key={index}>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={answer.checked}
                      onChange={handleChangeMultipleChoice(returnChecked)}
                      value={answer.uuid}
                    />
                  }
                  label={answer.content}
                />
              </FormGroup>
            </FormControl>
          ))
        : ""}
    </Grid>
  );
};

export default withStyles(styles)(MultiChoiceQuest);
