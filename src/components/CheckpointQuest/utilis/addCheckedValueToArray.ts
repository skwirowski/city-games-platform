const addCheckedValueToArray = (arrayOfObjects: any) => {
  arrayOfObjects.map((value: any) =>
    Object.assign({}, value, { checked: false })
  );
};

export default addCheckedValueToArray;
