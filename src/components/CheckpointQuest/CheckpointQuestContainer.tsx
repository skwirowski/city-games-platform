import React, { useState, useEffect } from "react";
import { WithStyles, withStyles } from "@material-ui/core";
import styles from "./CheckpointQuestStyles";
import addCheckedValueToArray from "./utilis/addCheckedValueToArray";
import QuestDescription from "./components/QuestDescription";
import CheckpointButtons from "./components/CheckpointButtons";
import OpenQuestionQuest from "./components/OpenQuestionQuest";
import SingleChoiceQuest from "./components/SingleChoiceQuest";
import MultiChoiceQuest from "./components/MultiChoiceQuest";

interface QuestInProgressProps extends WithStyles<typeof styles> {}

const CheckpointQuestContainer: React.FC<QuestInProgressProps> = ({
  classes
}) => {
  const [title, setTitle] = useState<string>("");
  const [timeForQuest, setTimeForQuest] = useState<string>("");
  const [maxPoints, setMaxPoints] = useState<string>("");
  const [questDescription, setQuestDescription] = useState<string>("");
  const [questQuestion, setQuestQuestion] = useState<string>("");
  const [singleChoiceAnswer, setSingleChoiceAnswer] = useState<string>("");
  const [multipleChoiceAnswer, setMultipleChoiceAnswer] = useState<
    Array<string>
  >([]);
  const [openQuestionAnswer, setOpenQuestionAnswer] = useState<string>("");
  const [answersOptions, setAnswerOptions] = useState([
    { uuid: "", content: "", checked: false }
  ]);

  useEffect(() => {
    setTitle("Martin M.");
    setTimeForQuest("1:29");
    setMaxPoints("10");
    setQuestDescription(
      "Description. Esse laborum irure cillum exercitation irure et labore nisi ipsum dolor incididunt eu deserunt ad. Fugiat exercitation voluptate aliqua ea deserunt officia do magna excepteur cillum elit sit incididunt sint."
    );
    setQuestQuestion(
      "Question. Exercitation in in ullamco cupidatat id laboris duis aliquip nulla?"
    );
  }, []);

  const handleChangeOpenQuestion = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setOpenQuestionAnswer(event.target.value);
  };

  function handleChangeSingleChoice(
    event: React.ChangeEvent<HTMLInputElement>
  ) {
    setSingleChoiceAnswer(event.target.value);
  }

  const handleChangeMultipleChoice = (call: any) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    let items = answersOptions;
    if (items) {
      items.forEach(item => {
        if (item.uuid === event.target.value) {
          item.checked = event.target.checked;
        }
      });
    }
    call(items);
    setAnswerOptions(items);
  };

  const returnChecked = (array: any) => {
    const newArr = array.filter((item: any) => item.checked === true);
    let result = newArr.map((a: any) => a.uuid);
    setMultipleChoiceAnswer(result);
  };

  const submit = () => {};
  return (
    <div className={classes.container}>
      <QuestDescription
        title={title}
        timeForQuest={timeForQuest}
        maxPoints={maxPoints}
        questDescription={questDescription}
        questQuestion={questQuestion}
      />
      {/*}Here will be logic here for choice right component {*/}

      <MultiChoiceQuest
        answersOptions={answersOptions}
        handleChangeMultipleChoice={handleChangeMultipleChoice}
        returnChecked={returnChecked}
      />

      {/*}End{*/}

      <CheckpointButtons submit={submit} skipPuzzle={submit} />
    </div>
  );
};

export default withStyles(styles)(CheckpointQuestContainer);
