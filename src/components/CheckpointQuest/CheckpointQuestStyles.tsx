import { createStyles } from "@material-ui/core";
import { secondary } from "../../shared/colors";

const styles = createStyles({
  container: {
    backgroundColor: "rgb(238,249,255)",
    width: "100%",
    paddingBottom: "30px",
    textAlign: "center",
    "& p": {
      fontSize: "1.2em",
      letterSpacing: "1px"
    }
  },
  title: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    backgroundColor: "white",
    padding: "0.5em",
    color: secondary
  },
  extensionIcon: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  goalsContainer: {
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
    width: "80%",
    margin: "20px auto 0"
  },
  goalWrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "&:last-child": {
      fontWeight: 600
    }
  },
  timerIcon: {
    width: 35,
    height: 35,
    color: secondary,
    marginRight: 15
  },
  starIcon: {
    width: 35,
    height: 35,
    color: secondary,
    marginRight: 15
  },
  descriptionWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "80%",
    margin: "auto"
  },
  description: {
    textAlign: "left",
    padding: "30px 0"
  },
  question: {
    textAlign: "left",
    fontWeight: 600
  },
  textField: {
    width: "80%",
    backgroundColor: "#FFFFFF",
    paddingLeft: 7,
    paddingRight: 7,
    marginBottom: 30
  },
  buttonsContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    height: 122
  }
});

export default styles;
