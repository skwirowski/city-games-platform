import React from "react";
import { Popup } from "react-leaflet";
import { FormattedMessage } from "react-intl";
import { Button } from "@material-ui/core";
import Delete from "@material-ui/icons/Delete";

interface Props {
  latFormat?: string;
  lngFormat?: string;
  handleDeleteMarker: (position: any) => void;
  position: any;
  isRouteEditor: boolean;
  deleteRouteCheckpoint: (checkpoint_id: string) => void;
  checkpoint_id: string;
}

const MapPopup: React.FC<Props> = ({ latFormat, lngFormat, handleDeleteMarker, position, isRouteEditor, deleteRouteCheckpoint, checkpoint_id }) => {
  return !isRouteEditor ? (
    <Popup aria-label="marker-popup">
      <span>
        <FormattedMessage id="Map.latitudeLabel" /> {latFormat}
      </span>
      <br />
      <span>
        <FormattedMessage id="Map.longitudeLabel" /> {lngFormat}
      </span>
      <br />
      <Button
        style={{
          width: "35px",
          minWidth: "35px",
          height: "35px",
          fontSize: "1.6rem",
          color: "#757575",
          position: "relative",
          bottom: "-10px",
          left: "112px"
        }}
        onClick={() => handleDeleteMarker(position)}
      >
        <Delete fontSize="default" aria-label="Delete" color="inherit" />
      </Button>
    </Popup>
  ) : (
    <Popup aria-label="marker-popup">
      <span>
        <FormattedMessage id="Map.latitudeLabel" /> {latFormat}
      </span>
      <br />
      <span>
        <FormattedMessage id="Map.longitudeLabel" /> {lngFormat}
      </span>
      <br />
      <Button
        style={{
          width: "35px",
          minWidth: "35px",
          height: "35px",
          fontSize: "1.6rem",
          color: "#757575",
          position: "relative",
          bottom: "-10px",
          left: "112px"
        }}
        onClick={() => deleteRouteCheckpoint(checkpoint_id)}
      >
        <Delete fontSize="default" aria-label="Delete" color="inherit" />
      </Button>
    </Popup>
  );
};
export default MapPopup;
