import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import uuidv4 from "uuid/v4";
import { Map as MapComponent, Marker, TileLayer } from "react-leaflet";
import { WithStyles, withStyles } from "@material-ui/core";
import { convertDMS } from "../../utils/coordinates";
import ReactDOMServer from "react-dom/server";
import MapMarker from "./MapMarker/MapMarker";
import L from "leaflet";
import { styles } from "./MapStyles";
import "leaflet/dist/leaflet.css";
import MapPopup from "./MapPopup";
import {
  setCheckpointCords,
  SetCheckpointCords,
  Checkpoint,
  setCheckpoint,
  SetCheckpoint,
  initialCheckpointValue,
  deleteRouteCheckpoint,
  setMapZoom,
  setMapCenter
} from "../../store/actions";

interface Position {
  lat: number;
  lng: number;
  latitude?: string;
  longitude?: string;
  checkpoint_id: string;
}

type Markers = Array<Position>;

interface Props extends WithStyles<typeof styles> {
  singleCheckpoint?: boolean;
  toggle: boolean;
  toggleDrawer?: () => void;
  match: {
    params: {
      guid: string;
    };
  };
  setCheckpoint: SetCheckpoint;
  setCheckpointCords: SetCheckpointCords;
  selectedCheckpoint: any;
  blocked?: boolean;
  routeCheckpoints: any;
  deleteRouteCheckpoint: any;
  setMapZoom: (zoom: number) => void;
  setMapCenter: (cords: any, lngOffset: boolean, offsetValue: number, routeEditor: boolean) => void;
  mapMode: any;
}

const Map: React.SFC<Props> = ({
  classes,
  singleCheckpoint,
  toggle,
  toggleDrawer,
  setCheckpointCords,
  setCheckpoint,
  match,
  selectedCheckpoint,
  blocked,
  routeCheckpoints,
  deleteRouteCheckpoint,
  setMapZoom,
  setMapCenter,
  mapMode
}) => {
  const defaultZoom = 13;
  const maxZoom: number = 19;
  const { map } = classes;
  const [center] = useState<[number, number]>([51.109, 17.0337]);
  const [zoom] = useState<number>(defaultZoom);

  const setPositionProps = (position: Position) => {
    const { lat, lng } = position;
    const formatedLatLng = convertDMS(lat, lng);
    position.latitude = formatedLatLng.lat;
    position.longitude = formatedLatLng.lng;
    position.checkpoint_id = uuidv4();
    return position;
  };

  const handleAddMarker = ({ latlng }: { latlng: Position }) => {
    if (blocked || !singleCheckpoint) return;

    latlng = setPositionProps(latlng);
    setCheckpoint && setCheckpoint({ ...selectedCheckpoint, ...latlng });
    !toggle && setMapCenter([latlng.lat, latlng.lng], true, mapMode.lngOffset, false);
    !toggle && toggleDrawer && toggleDrawer();
  };

  const handleDeleteMarker = (position: Position) => {
    setCheckpoint && setCheckpoint({ ...selectedCheckpoint, latitude: "", longitude: "", address: "" });
  };

  useEffect(() => {
    setSelectedCheckpoint(initialCheckpointValue);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (singleCheckpoint && match.params.guid) {
      setMapCenter([center[0], center[1]], true, mapMode.lngOffset, false);
      toggleDrawer && toggleDrawer();
    }
    return () => setMapCenter([center[0], center[1]], false, 0, false);
    // eslint-disable-next-line
  }, []);

  const setMarkerIcon = (checkpoint_id: string, singleCheckpoint: boolean) => {
    if (!singleCheckpoint) {
      const selectedCheckpoint_id = selectedCheckpoint && selectedCheckpoint.checkpoint_id;
      const isFocused = checkpoint_id === selectedCheckpoint_id;

      return L.divIcon({
        className: isFocused ? classes.focusedIcon : "",
        html: ReactDOMServer.renderToString(
          <MapMarker classes={classes} isFocused={isFocused} singleCheckpoint={singleCheckpoint} />
        ),

        popupAnchor: [1, -21]
      });
    } else {
      return L.divIcon({
        className: classes.focusedIcon,
        html: ReactDOMServer.renderToString(
          <MapMarker classes={classes} isFocused={true} singleCheckpoint={singleCheckpoint} />
        ),
        popupAnchor: [1, -21]
      });
    }
  };

  const setSelectedCheckpoint = (checkpoint: Checkpoint) => {
    setCheckpoint && setCheckpoint(checkpoint);
  };

  const isCreateMode = () => {
    return !blocked && singleCheckpoint && selectedCheckpoint && selectedCheckpoint.latitude;
  };

  return (
    <MapComponent
      aria-label="map-component"
      className={map}
      center={mapMode.mapCenter}
      onClick={handleAddMarker}
      zoom={zoom}
      maxZoom={maxZoom}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png"
      />
      {isCreateMode()
        ? [selectedCheckpoint].map(
            (checkpoint: any): JSX.Element => {
              const { checkpoint_id, latitude, longitude } = checkpoint;
              return (
                <Marker
                  aria-label="marker-component"
                  icon={setMarkerIcon(checkpoint_id, true)}
                  key={checkpoint_id}
                  position={checkpoint}
                >
                  <MapPopup
                    position={checkpoint}
                    latFormat={latitude}
                    lngFormat={longitude}
                    handleDeleteMarker={handleDeleteMarker}
                    deleteRouteCheckpoint={deleteRouteCheckpoint}
                    isRouteEditor={false}
                    checkpoint_id={checkpoint_id}
                  />
                </Marker>
              );
            }
          ) //Below it is the version of the map for route editor. Markers on this one are based on route Checkpoints and checkpoints currently existed.
        : routeCheckpoints.map(
            (checkpoint: any): JSX.Element => {
              const { checkpoint_id, latitude, longitude } = checkpoint;
              const converted = convertDMS(latitude, longitude);
              const position = { position_id: checkpoint_id, lat: latitude, lng: longitude };
              return (
                <Marker
                  aria-label="marker-component"
                  icon={setMarkerIcon(checkpoint_id, false)}
                  key={checkpoint_id}
                  position={position}
                  onClick={() => setSelectedCheckpoint(checkpoint)}
                >
                  <MapPopup
                    position={position}
                    latFormat={converted.lat}
                    lngFormat={converted.lng}
                    handleDeleteMarker={handleDeleteMarker}
                    deleteRouteCheckpoint={deleteRouteCheckpoint}
                    isRouteEditor={true}
                    checkpoint_id={checkpoint_id}
                  />
                </Marker>
              );
            }
          )}
    </MapComponent>
  );
};

const mapStateToProps = (state: any) => {
  return {
    selectedCheckpoint: state.checkpoint,
    routeCheckpoints: state.routeCheckpoints,
    mapMode: state.mapMode
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    { setCheckpointCords, setCheckpoint, deleteRouteCheckpoint, setMapZoom, setMapCenter }
  )(Map)
);
