import React from "react";
import classnames from "classnames";
import { WithStyles } from "@material-ui/core";
import { styles } from "../MapStyles";
import { focusedMarkerPath, normalMarkerPath } from "./SVGPath";

interface Props extends WithStyles<typeof styles> {
  isFocused: boolean;
  singleCheckpoint: boolean;
}

const MapMarker: React.FC<Props> = ({ classes, isFocused, singleCheckpoint }) => {
  const {
    mapIcon,
    iconContainer,
    pulse,
    iconBouncing,
    focusedIcon,
    blueIconColor,
    redIconColor,
    pulseAfterBlue,
    pulseAfterRed
  } = classes;

  const setIconColor = () => {
    return isFocused && !singleCheckpoint ? redIconColor : blueIconColor;
  };
  const setPulse = () => {
    return isFocused && !singleCheckpoint ? classnames(pulse, pulseAfterRed) : classnames(pulse, pulseAfterBlue);
  };
  return (
    <div className={!isFocused ? iconContainer : classnames(iconContainer, focusedIcon)}>
      <svg
        className={!isFocused ? mapIcon : classnames(mapIcon, focusedIcon)}
        viewBox="0 0 486.3 486.3"
        data-lt-installed="true"
        width="27px"
        height="40px"
      >
        {isFocused ? (
          <g>
            <g className={iconBouncing}>
              <path className={setIconColor()} d={focusedMarkerPath} />
            </g>
          </g>
        ) : (
          <g>
            <g>
              <path style={{ fill: "#0082FF" }} d={normalMarkerPath} />
            </g>
          </g>
        )}
      </svg>
      <div className={!isFocused ? "" : setPulse()} />
    </div>
  );
};

export default MapMarker;
