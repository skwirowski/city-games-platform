import "leaflet/dist/leaflet.css";
import L from "leaflet";

export const MarkerIcon = L.icon({
  iconUrl: "/images/marker_blue.svg",
  iconSize: [25, 57],
  iconAnchor: [10, 40],
  popupAnchor: [2, -15]
});

export const MarkerIconFocused = L.icon({
  iconUrl: "/images/marker_red.svg",
  iconSize: [25, 57],
  iconAnchor: [10, 40],
  popupAnchor: [2, -15],
  className: "MarkerIcon"
});
