import { createStyles } from "@material-ui/core";

export const styles = createStyles({
  map: {
    height: "100%",
    width: "100%"
  },
  iconBouncing: {
    animation: "bounce 1s linear",
    animationIterationCount: "infinite",
    animationFillMode: "forwards"
  },
  mapIcon: {
    position: "absolute",
    top: "-26px",
    left: "-8px"
  },
  focusedIcon: {
    zIndex: "2000!important" as any
  },
  iconContainer: {
    position: "absolute"
  },

  pulse: {
    top: "-9px",
    left: "13px",
    width: "8px",
    height: "14px",
    margin: "11px 0px 0px -12px",
    zIndex: -2,
    position: "absolute",
    transform: "rotateX(55deg)",
    background: "#d6d4d4",
    borderRadius: "50%",
    "&::after": {
      content: "''",
      borderRadius: "50%",
      height: "40px",
      width: "40px",
      position: "absolute",
      margin: "-13px 0 0 -16px",
      animation: "pulsate 1s ease-out",
      animationIterationCount: "infinite",
      opacity: 0,
      animationDelay: "0.5s"
    }
  },

  pulseAfterBlue: {
    "&::after": {
      boxShadow: "0 0 6px 3px #0082FF"
    }
  },
  pulseAfterRed: {
    "&::after": {
      boxShadow: "0 0 6px 3px #ff3501"
    }
  },

  "@keyframes pulsate": {
    "0%": {
      transform: "scale(0.1, 0.1)",
      opacity: 0
    },

    "50%": {
      opacity: 1
    },

    "100%": {
      transform: "scale(1.2, 1.2)",
      opacity: 0
    }
  },
  "@keyframes bounce": {
    "0%": {
      transform: "translateY(-55px)"
    },

    "50%": {
      transform: "translateY(0px)"
    },

    "100%": {
      transform: "translateY(-55px)"
    }
  },

  blueIconColor: {
    fill: "#0082FF"
  },
  redIconColor: {
    fill: "#FF3501"
  }
});
