import React from "react";
import { shallow } from "enzyme";
import Map from "../Map";
import { unwrap } from "@material-ui/core/test-utils";
import { act } from "react-dom/test-utils";
import { initializeStore } from "../../../store";
const store = initializeStore();

const match = {
  params: {}
};

describe("<Map/>", () => {
  let wrapper;
  beforeEach(() => {
    const UnwrappedMap = unwrap(Map);
    wrapper = shallow(
      <UnwrappedMap
        store={store}
        toggleDrawer={() => {}}
        match={match}
        classes={{ map: { height: "100vh" } }}
        editedCheckpointCoords={{ lat: 0, lng: 0 }}
      />
    )
      .find("Map")
      .dive();
  });

  const markerEvent = {
    first: {
      latlng: {
        lat: 55,
        lng: 45
      }
    },
    second: {
      latlng: {
        lat: 40,
        lng: 2
      }
    }
  };

  xit("renders without crashing", () => {
    expect(wrapper.isEmptyRender()).toBe(false);
  });

  xit("renders one Marker component after user click", () => {
    act(() => {
      wrapper.find('[aria-label="map-component"]').prop("onClick")(markerEvent.first);
    });
    wrapper.update();

    expect(wrapper.find('[aria-label="marker-component"]').length).toEqual(1);

    wrapper.update();
  });

  xit("can render more than wone Marker component if a valid arguments has been passed", () => {
    act(() => {
      wrapper.find('[aria-label="map-component"]').prop("onClick")(markerEvent.first);
    });
    wrapper.update();

    act(() => {
      wrapper.find('[aria-label="map-component"]').prop("onClick")(markerEvent.second);
    });
    wrapper.update();

    expect(wrapper.find('[aria-label="marker-component"]').length).toEqual(2);
  });

  xit("removes Marker after a user clicks the delete button", () => {
    act(() => {
      wrapper.find('[aria-label="map-component"]').prop("onClick")(markerEvent.first);
    });
    wrapper.update();
    expect(wrapper.find('[aria-label="marker-component"]').length).toEqual(1);

    act(() => {
      wrapper
        .find("MapPopup")
        .dive()
        .find('[aria-label="delete-marker-button"]')
        .simulate("click");
    });
    wrapper.update();

    expect(wrapper.find('[aria-label="marker-component"]').exists()).toEqual(false);
  });
});
