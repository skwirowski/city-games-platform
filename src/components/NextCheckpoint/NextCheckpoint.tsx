import React, { useState, useEffect } from "react";
import { WithStyles, withStyles, Grid } from "@material-ui/core";
import { compose } from "redux";
import { connect } from "react-redux";
import { styles } from "./NextCheckpointStyles";
import { fetchCurrentGameState } from "../../store/actions/team";
import NavBar from "../TeamNavBar";
import CheckpointName from "./CheckpointName";
import Description from "./Description";
import Address from "./Address";
import Location from "./Location";
import QrScannerButton from "./QrScannerButton";

export interface CheckpointValues {
  name: string;
  creatorGuid: string;
  descriptionOnTheWay: string;
  isDescriptionOnTheWayVisible: boolean;
  descriptionAfterArrival: string;
  isDescriptionAfterArrivalVisible: boolean;
  isLocationVisible: boolean;
  address: string;
  isAddressVisible: boolean;
  objectName: string;
  isObjectNameVisible: boolean;
  latitude: number;
  longitude: number;
  isQrCodeGenerated: boolean;
  isGeolocationConfirmationSelected: boolean;
  isQrCodeConfirmationSelected: boolean;
  guid: string;
  creationDate: string;
  timestamp: string;
}

interface Props extends WithStyles<typeof styles> {
  checkpoint: CheckpointValues;
  fetchCurrentGameState: () => {};
}

const NextCheckpoint: React.SFC<Props> = ({
  classes,
  checkpoint,
  fetchCurrentGameState
}) => {
  const [name, setName] = useState("");
  const [objectName, setObjectName] = useState("");
  const [description, setDescription] = useState("");
  const [address, setAdress] = useState("");
  const [isLocationVisible, setIsLocationVisible] = useState(false);
  const [position, setPosition] = useState<[number, number]>([0, 0]);

  useEffect(() => {
    fetchCurrentGameState();
  }, []);

  useEffect(() => {
    setName(checkpoint.name);
    if (checkpoint.isObjectNameVisible) setObjectName(checkpoint.objectName);
    if (checkpoint.isDescriptionOnTheWayVisible)
      setDescription(checkpoint.descriptionOnTheWay);
    if (checkpoint.isAddressVisible) setAdress(checkpoint.address);
    if (checkpoint.isLocationVisible) {
      setPosition([checkpoint.latitude, checkpoint.longitude]);
      setIsLocationVisible(true);
    }
  }, [checkpoint]);

  return (
    <div className={classes.bg}>
      <Grid container justify="center">
        <NavBar color="secondary" />
        <CheckpointName name={name} />
        {description ? <Description description={description} /> : null}
        {address ? <Address address={address} objectName={objectName} /> : null}
        {isLocationVisible ? <Location position={position} /> : null}
        <QrScannerButton />
      </Grid>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  checkpoint: state.team.currentCheckpoint
});
const enhance = compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    { fetchCurrentGameState }
  )
);
export default enhance(NextCheckpoint);
