import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import NearMeIcon from "@material-ui/icons/NearMeOutlined";
import { Map, TileLayer, Marker } from "react-leaflet";
import leaflet from "leaflet";
import { convertDMS } from "../../utils/coordinates";
import { styles } from "./NextCheckpointStyles";

interface Props extends WithStyles<typeof styles> {
  position: [number, number];
}

const customMarkerIcon = leaflet.icon({
  iconUrl: "/images/marker_blue.svg",
  iconSize: [25, 57]
});

const Location: React.SFC<Props> = ({ classes, position }) => {
  const { lat, lng } = convertDMS(position[0], position[1]);
  return (
    <React.Fragment>
      <Grid item xs={12} className={classes.location}>
        <NearMeIcon className={classes.locationIcon} />
        <Typography className={classes.locationText}>
          {`${lat}, ${lng}`}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Map center={position} zoom={15} className={classes.map}>
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png"
          />
          <Marker icon={customMarkerIcon} position={position} />
        </Map>
      </Grid>
    </React.Fragment>
  );
};

export default withStyles(styles)(Location);
