import { createStyles } from "@material-ui/core";
import { secondary } from "../../shared/colors";

export const styles = createStyles({
  bg: {
    backgroundColor: "rgb(238,249,255)",
    width: "100%",
    minHeight: "100%",
    paddingBottom: "1em"
  },
  title: {
    backgroundColor: "white",
    padding: "0.5em",
    textAlign: "center",
    color: secondary,
    marginBottom: "0.5em"
  },
  description: {
    padding: "1em 2em 2em 2em"
  },
  location: {
    marginLeft: "2em",
    marginBottom: "0.5em",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  locationIcon: {
    color: secondary,
    fontSize: "2em",
    marginRight: "0.5em"
  },
  locationText: {
    color: "grey"
  },
  map: {
    width: "100%",
    height: "40vh"
  },
  info: {
    padding: "1em",
    textAlign: "center",
    fontWeight: "bold"
  }
});
