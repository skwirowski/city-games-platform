import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import PlaceIcon from "@material-ui/icons/PlaceOutlined";
import { styles } from "./NextCheckpointStyles";

interface Props extends WithStyles<typeof styles> {
  address: string;
  objectName: string;
}

const Address: React.SFC<Props> = ({ classes, address, objectName }) => {
  const text = address ? `${address}, ${objectName}` : objectName;
  return (
    <Grid item xs={12} className={classes.location}>
      <PlaceIcon className={classes.locationIcon} />
      <Typography className={classes.locationText}>{text}</Typography>
    </Grid>
  );
};

export default withStyles(styles)(Address);
