import React from "react";
import { connect } from "react-redux";
import { routes } from "../../static/routesUrl";
import { CheckpointValues } from "./NextCheckpoint";
import QrCodeScanner from "../QrCodeScanner";
import { verifyCheckpointQrCode } from "../../services/verifyCheckpointQrCode";

interface Props {
  checkpoint: CheckpointValues;
  history: {
    push: (arg: string) => {};
  };
}

const NextCheckpointQrScanner: React.SFC<Props> = ({ checkpoint, history }) => {
  const handleScan = async (data: string) => {
    try {
      await verifyCheckpointQrCode(checkpoint.guid, data);
      history.push(routes.startQuest);
    } catch {
      history.push(routes.invalidQrCode);
    }
  };

  return <QrCodeScanner onScan={handleScan} />;
};

const mapStateToProps = (state: any) => ({
  checkpoint: state.team.currentCheckpoint
});
export default connect(mapStateToProps)(NextCheckpointQrScanner);
