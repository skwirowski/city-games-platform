import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { routes } from "../../static/routesUrl";
import Link from "../../shared/LinkWithoutDefaultStyles";
import Button from "../../shared/Button";
import { styles } from "./NextCheckpointStyles";

interface Props extends WithStyles<typeof styles> {}

const QrScannerButton: React.SFC<Props> = ({ classes }) => {
  return (
    <React.Fragment>
      <Grid item xs={12}>
        <Typography className={classes.info}>
          <FormattedMessage id="NextCheckpoint.qrInfoText" />
        </Typography>
      </Grid>
      <Link to={routes.nextCheckpointQrScanner}>
        <Button color="primary">
          <FormattedMessage id="NextCheckpoint.btnText" />
        </Button>
      </Link>
    </React.Fragment>
  );
};

export default withStyles(styles)(QrScannerButton);
