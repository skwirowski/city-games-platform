import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import { styles } from "./NextCheckpointStyles";

interface Props extends WithStyles<typeof styles> {
  name: string;
}

const CheckpointName: React.SFC<Props> = ({ classes, name }) => {
  return (
    <Grid item xs={12}>
      <Typography variant="h5" className={classes.title}>
        {name}
      </Typography>
    </Grid>
  );
};

export default withStyles(styles)(CheckpointName);
