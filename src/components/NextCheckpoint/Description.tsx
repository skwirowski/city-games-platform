import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import { styles } from "./NextCheckpointStyles";

interface Props extends WithStyles<typeof styles> {
  description: string;
}

const Description: React.SFC<Props> = ({ classes, description }) => {
  return (
    <Grid item xs={12}>
      <Typography className={classes.description}>{description}</Typography>
    </Grid>
  );
};

export default withStyles(styles)(Description);
