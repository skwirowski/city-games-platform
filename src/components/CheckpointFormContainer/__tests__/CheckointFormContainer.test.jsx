import React from "react";
import { Provider } from "react-redux";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import CheckPointFormContainer from "../CheckpointFormContainer";
import { initializeStore } from "../../../store/";
const store = initializeStore();
describe("CheckPointFormContainer", () => {
  let wrapper;

  beforeEach(() => {
    const Unwrapped = unwrap(
      <Provider store={store}>
        <CheckPointFormContainer />
      </Provider>
    );
    wrapper = shallow(Unwrapped);
  });
  it("should render without crash", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should match snapshot", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
