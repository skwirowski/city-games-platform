import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { routes } from "../../static/routesUrl";
import notifications from "../../static/notifications";
import http from "../../services/routeServiceApi";
import CheckpointForm, { FormValues } from "../CheckpointForm/CheckpointForm";
import { parseDMS, convertDMS } from "../../utils/coordinates";
import { setCheckpoint, SetCheckpoint } from "../../store/actions";

interface Props {
  history: {
    push: (arg: string) => {};
  };
  match: {
    params: {
      guid: string;
    };
  };
  checkpoint: FormValues;
  setCheckpoint: SetCheckpoint;
}

const CheckpointFormContainer: React.FC<Props> = ({ history, match, checkpoint, setCheckpoint }) => {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (match.params.guid) {
      const fetchData = async () => {
        const { data } = await http.get(`/api/Checkpoints/${match.params.guid}`);
        const { latitude, longitude, guid } = data;
        const { lat, lng } = convertDMS(data.latitude, data.longitude);
        const newValues = {
          ...data,
          latitude: lat,
          longitude: lng,
          lat: latitude,
          lng: longitude,
          checkpoint_id: guid
        };
        setCheckpoint(newValues);
      };

      fetchData();
    }
    // eslint-disable-next-line
  }, [match.params.guid]);

  const handleSubmit = async (values: FormValues) => {
    setLoading(true);
    const newValues = { ...values, latitude: parseDMS(values.latitude), longitude: parseDMS(values.longitude) };

    if (match.params.guid) {
      try {
        // UPDATE CHECKPOINT
        const editedValues = { ...newValues, guid: match.params.guid };
        await http.put(`/api/Checkpoints/${match.params.guid}`, editedValues);
        notifications.checkpoint.checkpointUpdatedSuccess();
        setLoading(false);
        history.push(routes.checkpointList);
      } catch (error) {
        console.log("TCL: handleSubmit -> error", error);
        notifications.checkpoint.checkpointUpdatedError();
        setLoading(false);
      }
    } else {
      try {
        // CREATING NEW CHECKPOINT
        await http.post("/api/Checkpoints", newValues);
        notifications.checkpoint.checkpointCreatedSuccess();
        history.push(routes.checkpointList);
      } catch (error) {
        console.log("TCL: handleSubmit -> error", error);
        notifications.checkpoint.checkpointCreatedError();
        setLoading(false);
      }
    }

    return null;
  };

  return <CheckpointForm submit={handleSubmit} checkpoint={checkpoint} loading={loading} match={match} />;
};

const mapStateToProps = (state: any) => {
  return {
    checkpoint: state.checkpoint
  };
};

export default connect(
  mapStateToProps,
  { setCheckpoint }
)(CheckpointFormContainer);
