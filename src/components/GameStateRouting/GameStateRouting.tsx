import React, { useEffect, useState } from "react";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { fetchCurrentGameState } from "../../store/actions/team";
import { routes } from "../../static/routesUrl";

interface Props {
  fetchCurrentGameState: () => {};
}

const GameStateRouting: React.FC<Props> = props => {
  const getPath = async () => {
    await props.fetchCurrentGameState();
    //TODO: implement redirect logic
    return routes.nextCheckpoint;
  };
  const [path, setPath] = useState("");
  useEffect(() => {
    getPath().then(path => {
      setPath(path);
    });
  }, []);
  return path ? <Redirect to={path} /> : null;
};

export default connect(
  null,
  { fetchCurrentGameState }
)(GameStateRouting);
