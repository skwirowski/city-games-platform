import React, { useState, useEffect } from 'react';
import {
	WithStyles,
	withStyles,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	Paper,
	Button
} from '@material-ui/core';
import Description from '@material-ui/icons/Description';
import { FormattedMessage } from 'react-intl';
import http from '../../services/gameDefinitionApi';
import deleteGameDefinition from '../../services/deleteGameDefinition';
import { routes } from '../../static/routesUrl';
import { encryptString } from "../QrCodeForm/utils/hashingFunctions";
import { qrCodeHashKey } from "../QrCodeForm/utils/constants";
import notifications from "../../static/notifications";
import GameDefinitionsListElement from './GameDefinitionListElement/GameDefinitionsListElement';
import GameDefinitionModal from './GameDefinitionListElement/GameDefinitionModal';
import { styles } from './GameDefinitionsListStyles';

export interface GameDefinitionProps {
	guid: string;
	logo: string;
	name: string;
	startPlacePoint: string;
	description: string;
	termsOfUse: { content: string };
	startDate: string;
	finishDate: string;
	minimumTeamMemberCount: number;
	maximumTeamMemberCount: number;
	minimumTeamCount: number;
	maximumTeamCount: number;
	maximumRouteDuration: string;
	timeBetweenTeams: string;
	isQrCodeConfirmationSelected: boolean;
	isQrCodeGenerated: boolean;
	isActive: boolean;
}

interface GameDefinitionsListProps extends WithStyles<typeof styles> {
	history: {
		push: (arg1: string, arg2?: {}) => {}
	}
}

interface ModalProps {
	isModalOpen: boolean;
	modalTitle: string;
	modalContent: string;
}

const GameDefinitionsList: React.FC<GameDefinitionsListProps> = ({ classes, history }) => {
	const [gameDefinitions, setGameDefinition] = useState<Array<GameDefinitionProps>>([]);

	const [modalStateAndData, setModalStateAndData] = useState<ModalProps>({
		isModalOpen: false,
		modalTitle: '',
		modalContent: ''
	});

	const [toogleTableDetails, setTableDetails] = useState<boolean>(true);

	useEffect(() => {
		const fetchData = async() => {
			const response = await http.get("/api/GameDefinition");
			setGameDefinition(response.data);
		};

		fetchData();
	}, []);

	const onDeleteGameDefinitionClick = async (guid: string) => {
		try {
			await deleteGameDefinition(guid);
			notifications.gamedefinition.gameDeletedSuccess();
			const response = await http.get("/api/GameDefinition");
			setGameDefinition(response.data);
		} catch (error) {
			notifications.gamedefinition.gameDeletedError();
		}
	}

	const onPrintQrCodeClick = (guid: string) => {
		const hashedMessage = encryptString(guid, qrCodeHashKey)
		history.push(routes.gameDefinitionQrCode(guid), { value: hashedMessage });
	};

	const onActivateGameDefinition = async (guid: string) => {
		try {
			const game = gameDefinitions.find(definition => definition.guid === guid);
			await http.put(`/api/GameDefinition/${guid}`, {
				...game,
				isActive: true
			});
			notifications.gamedefinition.gameActivatedSuccess();
			const response = await http.get("/api/GameDefinition");
			setGameDefinition(response.data);
		} catch (error) {
			if (error.response.data.includes("Cannot update game definition.")) {
				notifications.gamedefinition.gameActivatedWarning();
			} else {
				notifications.gamedefinition.gameActivatedError();
			}
		}
	};

	const onDeactivateGameDefinition = async (guid: string) => {
		try {
			const game = gameDefinitions.find(definition => definition.guid === guid);
			await http.put(`/api/GameDefinition/${guid}`, {
				...game,
				isActive: false
			});
			notifications.gamedefinition.gameDeactivatedSuccess();
			const response = await http.get("/api/GameDefinition");
			setGameDefinition(response.data);
		} catch (error) {
			notifications.gamedefinition.gameDeactivatedError();
		}
	};

	const onGenerateQrCodeClick = (guid: string) => {
		return history.push(routes.gameDefinitionQrCodeForm(guid), {
			route: "/game-master/game",
			description: "GameDefinitionsList.createIdentifierDescription",
			label: "GameDefinitionsList.identifierInputLabel",
			helperText: "GameDefinitionsList.identifierInputHelperText"
		});
	};

	const handleOpenModal = (title: string, content: string) => {
		setModalStateAndData({
			isModalOpen: true,
			modalTitle: title,
			modalContent: content
		});
	}

	const handleCloseModal = () => {
		setModalStateAndData({
			isModalOpen: false,
			modalTitle: '',
			modalContent: ''
		});
	}

	const handleToggleTableDetailsClick = () => { setTableDetails(!toogleTableDetails) };

	const gameDefinitionsDetailedTable = () => (
		<TableRow>
			<TableCell align="center"><FormattedMessage id="GameDefinitionsList.tableHeadLogo" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadName" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadStartPlacePoint" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.sharedDescription" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.sharedTermsOfUse" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadStartDate" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadEndtDate" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadMinimumTeamCount" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadMaximumTeamCount" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadMinimumTeamMemberCount" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadMaximumTeamMemberCount" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadMaximumRouteDuration" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadTimeBetweenTeams" /></TableCell>
			<TableCell align="center"><FormattedMessage id="GameDefinitionsList.tableHeadActions" /></TableCell>
		</TableRow>
	);

	const gameDefinitionsSimpleTable = () => (
		<TableRow>
			<TableCell align="center"><FormattedMessage id="GameDefinitionsList.tableHeadLogo" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.tableHeadName" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.sharedDescription" /></TableCell>
			<TableCell align="left"><FormattedMessage id="GameDefinitionsList.sharedTermsOfUse" /></TableCell>
			<TableCell align="center"><FormattedMessage id="GameDefinitionsList.tableHeadActions" /></TableCell>
		</TableRow>
	);

	return (
		<Paper className={classes.root}>
		<Button
			className={classes.button}
			variant="contained"
			color="primary"
			onClick={() => handleToggleTableDetailsClick()}
		>
				{toogleTableDetails ? (
					<FormattedMessage id="GameDefinitionsList.moreDetailsTableButton" />
				) : (
					<FormattedMessage id="GameDefinitionsList.lessDetailsTableButton" />
				)}
			<Description />
		</Button>
			<Table className={classes.table}>
				<TableHead>
					{toogleTableDetails ? gameDefinitionsSimpleTable() : gameDefinitionsDetailedTable()}
				</TableHead>
				<TableBody>
					{gameDefinitions.length > 0 ? (
						gameDefinitions.map(gameDefinition => {
							return (
								<GameDefinitionsListElement
									key={gameDefinition.guid}
									gameDefinition={gameDefinition}
									deleteGameDefinition={onDeleteGameDefinitionClick}
									printQrCode={onPrintQrCodeClick}
									generateQrCode={onGenerateQrCodeClick}
									handleOpenModal={handleOpenModal}
									toggleTableDetails={toogleTableDetails}
									activateGameDefinition={onActivateGameDefinition}
									deactivateGameDefinition={onDeactivateGameDefinition}
								/>
							)
						})
					) : (
						<TableRow>
							<TableCell>
								<FormattedMessage id="shared.loading" />
							</TableCell>
						</TableRow>
					)}
				</TableBody>
			</Table>
			<GameDefinitionModal
				isOpen={modalStateAndData.isModalOpen}
				handleClose={handleCloseModal}
				gameDefintionModalTitle={modalStateAndData.modalTitle}
				gameDefinitionModalContent={modalStateAndData.modalContent}
		/>
		</Paper>
	);
}

export default withStyles(styles)(GameDefinitionsList);
