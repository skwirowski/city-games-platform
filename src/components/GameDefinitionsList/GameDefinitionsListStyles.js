import { createStyles } from '@material-ui/core';

export const styles = createStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
    gameLogo: {
    height: '50px'
  },
  button: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '15px auto 5px auto'
  }
});