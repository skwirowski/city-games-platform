import { createStyles } from '@material-ui/core';
import { primary } from "../../../shared/colors";

export const styles = createStyles({
  gameLogo: {
    height: 50
  },
  button: {
    whiteSpace: 'nowrap'
  },
  buttonActivate: {
    whiteSpace: 'nowrap',
    color: primary
  },
  buttonDeactivate: {
    whiteSpace: 'nowrap',
    color: 'orange'
  },
  activeRow: {
    backgroundColor: "rgba(0, 255, 0, 0.35)"
  },
  paper: {
    position: 'absolute',
    top: '15%',
    left: '40%',
    backgroundColor: '#FFFFFF',
    width: 400,
    maxHeight: 500,
    boxShadow: '10px 10px 9px 0px rgba(0,0,0,0.75)',
    padding: 30,
    outline: 'none',
    overflowY: 'scroll'
  },
  modalContent: {
    marginBottom: 30
  }
});