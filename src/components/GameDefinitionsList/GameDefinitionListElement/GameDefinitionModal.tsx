import React from 'react';
import {
  WithStyles,
  withStyles,
  Modal,
  Typography,
  Button
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { styles } from './GameDefinitionsListElementStyles';

interface GameDefinitionsListProps extends WithStyles<typeof styles> {
  isOpen: boolean;
  handleClose: () => void;
  gameDefintionModalTitle: string;
  gameDefinitionModalContent: string;
}

const GameDefinitionModal: React.FC<GameDefinitionsListProps> = ({ classes, isOpen, handleClose, gameDefintionModalTitle, gameDefinitionModalContent }) => {

  return (
    <Modal
      aria-labelledby="game-definition-modal"
      aria-describedby="game-definition-modal"
      open={isOpen}
      onClose={handleClose}
    >
      <div className={classes.paper}>
        <Typography variant="h6" id="modal-title">
          {gameDefintionModalTitle}
        </Typography>
        <Typography
          className={classes.modalContent}
          variant="subtitle1"
          id="simple-modal-description"
          dangerouslySetInnerHTML={{ __html: gameDefinitionModalContent }} />
        <Button
          variant="outlined"
          onClick={() => handleClose()}
        >
          <FormattedMessage id="shared.closeButton" />
        </Button>
      </div>
    </Modal>
  );
}

export default withStyles(styles)(GameDefinitionModal);
