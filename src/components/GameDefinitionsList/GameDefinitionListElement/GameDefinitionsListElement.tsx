import React, { Fragment } from 'react';
import {
  WithStyles,
  withStyles,
  TableCell,
  TableRow,
  Button
} from '@material-ui/core';
import { DeleteForever, Print, Settings, AddCircle, RemoveCircle } from '@material-ui/icons';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import Intl from "../../../utils/Intl";
import { GameDefinitionProps } from '../GameDefinitionsList';
import { styles } from './GameDefinitionsListElementStyles';
import NoLogoImg from './utils/no-logo.png';

interface GameDefinitionsListElementProps extends WithStyles<typeof styles> {
  gameDefinition: GameDefinitionProps;
  deleteGameDefinition: (guid: string) => {};
  printQrCode: (guid: string) => void;
  generateQrCode: (guid: string) => {};
  handleOpenModal: (arg1: string, arg2: string) => void;
  toggleTableDetails: boolean;
  activateGameDefinition: (guid: string) => void;
  deactivateGameDefinition: (guid: string) => void;
}

const GameDefinitionsListElement: React.FC<GameDefinitionsListElementProps> = (
  {
    classes,
    gameDefinition,
    deleteGameDefinition,
    printQrCode,
    generateQrCode,
    handleOpenModal,
    toggleTableDetails,
    activateGameDefinition,
    deactivateGameDefinition
  }) => {

  const formatSerializedDate = (date: string) => moment(date, moment.ISO_8601).format("MM-DD-YYYY");

  const gameDefinitionsDetailedTable = () => (
    <TableRow className={gameDefinition.isActive ? classes.activeRow : undefined}>
      <TableCell align="center">
        <img
          className={classes.gameLogo}
          src={(gameDefinition.logo === '') ? NoLogoImg : `data:image/png;base64, ${gameDefinition.logo}`}
          alt={`${gameDefinition.name} logo`}
        />
      </TableCell>
      <TableCell align="left">{gameDefinition.name}</TableCell>
      <TableCell align="left">{gameDefinition.startPlacePoint}</TableCell>
      <TableCell align="left">
        <Button
          className={classes.button}
          color="primary"
          size="small"
          onClick={() => handleOpenModal(Intl.messages["GameDefinitionsList.sharedDescription"], gameDefinition.description)}
        >
          <FormattedMessage id="GameDefinitionsList.showDescriptionButton" />
        </Button>
      </TableCell>
      <TableCell align="left">
        <Button
          className={classes.button}
          color="secondary"
          size="small"
          onClick={() => handleOpenModal(Intl.messages["GameDefinitionsList.sharedTermsOfUse"], gameDefinition.termsOfUse.content)}
        >
          <FormattedMessage id="GameDefinitionsList.showTermsOfUseButton" />
        </Button>
      </TableCell>
      <TableCell align="left">{formatSerializedDate(gameDefinition.startDate)}</TableCell>
      <TableCell align="left">{formatSerializedDate(gameDefinition.finishDate)}</TableCell>
      <TableCell align="left">{gameDefinition.minimumTeamMemberCount}</TableCell>
      <TableCell align="left">{gameDefinition.maximumTeamMemberCount}</TableCell>
      <TableCell align="left">{gameDefinition.minimumTeamCount}</TableCell>
      <TableCell align="left">{gameDefinition.maximumTeamCount}</TableCell>
      <TableCell align="left">{gameDefinition.maximumRouteDuration}</TableCell>
      <TableCell align="left">{gameDefinition.timeBetweenTeams}</TableCell>
      <TableCell align="center">
        <Button
          className={classes.button}
          color="secondary"
          size="small"
          onClick={() => deleteGameDefinition(gameDefinition.guid)}
        >
          <FormattedMessage id="shared.deleteButton" />
          <DeleteForever />
        </Button>
        {gameDefinition.isQrCodeGenerated ? (
          <Button
            className={classes.button}
            color="primary"
            size="small"
            onClick={() => printQrCode(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.printQrCodeButton" />
            <Print />
          </Button>
        ) : (
          <Button
            className={classes.button}
            color="primary"
            size="small"
            onClick={() => generateQrCode(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.qrCodeGenerateButton" />
            <Settings />
          </Button>
        )}

        {gameDefinition.isActive ? (
          <Button
            className={classes.buttonDeactivate}
            color="primary"
            size="small"
            onClick={() => deactivateGameDefinition(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.deactivateGame" />
            <RemoveCircle />
          </Button>
        ) : (
          <Button
            className={classes.buttonActivate}
            color="primary"
            size="small"
            onClick={() => activateGameDefinition(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.activateGame" />
            <AddCircle />
          </Button>
        )}
      </TableCell>
    </TableRow>
  );

  const gameDefinitionsSimpledTable = () => (
    <TableRow className={gameDefinition.isActive ? classes.activeRow : undefined}>
      <TableCell align="center">
        <img
          className={classes.gameLogo}
          src={(gameDefinition.logo === '') ? NoLogoImg : `data:image/png;base64, ${gameDefinition.logo}`}
          alt={`${gameDefinition.name} logo`}
        />
      </TableCell>
      <TableCell align="left">{gameDefinition.name}</TableCell>
      <TableCell align="left">
        <Button
          className={classes.button}
          color="primary"
          size="small"
          onClick={() => handleOpenModal(Intl.messages["GameDefinitionsList.sharedDescription"], gameDefinition.description)}
        >
          <FormattedMessage id="GameDefinitionsList.showDescriptionButton" />
        </Button>
      </TableCell>
      <TableCell align="left">
        <Button
          className={classes.button}
          color="secondary"
          size="small"
          onClick={() => handleOpenModal(Intl.messages["GameDefinitionsList.sharedTermsOfUse"], gameDefinition.termsOfUse.content)}
        >
          <FormattedMessage id="GameDefinitionsList.showTermsOfUseButton" />
        </Button>
      </TableCell>
      <TableCell align="center">
        <Button
          className={classes.button}
          color="secondary"
          size="small"
          onClick={() => deleteGameDefinition(gameDefinition.guid)}
        >
          <FormattedMessage id="shared.deleteButton" />
          <DeleteForever />
        </Button>
        {gameDefinition.isQrCodeGenerated ? (
          <Button
            className={classes.button}
            color="primary"
            size="small"
            onClick={() => printQrCode(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.printQrCodeButton" />
            <Print />
          </Button>
        ) : (
          <Button
            className={classes.button}
            color="primary"
            size="small"
            onClick={() => generateQrCode(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.qrCodeGenerateButton" />
            <Settings />
          </Button>
        )}

        {gameDefinition.isActive ? (
          <Button
            className={classes.buttonDeactivate}
            color="primary"
            size="small"
            onClick={() => deactivateGameDefinition(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.deactivateGame" />
            <RemoveCircle />
          </Button>
        ) : (
          <Button
            className={classes.buttonActivate}
            color="primary"
            size="small"
            onClick={() => activateGameDefinition(gameDefinition.guid)}
          >
            <FormattedMessage id="GameDefinitionsList.activateGame" />
              <AddCircle />
          </Button>
        )}
      </TableCell>
    </TableRow>
  )

  return (
    <Fragment>
      { toggleTableDetails ? gameDefinitionsSimpledTable() : gameDefinitionsDetailedTable() }
    </Fragment>
  );
}

export default withStyles(styles)(GameDefinitionsListElement);