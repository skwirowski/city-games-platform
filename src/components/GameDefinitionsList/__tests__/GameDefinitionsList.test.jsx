import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import GameDefinitionsList from '../GameDefinitionsList';

describe('GameDefinitionsList', () => {
  let wrapper;
  beforeEach(() => {
    const GameDefinitionsListUnwrapped = unwrap(GameDefinitionsList);
    wrapper = shallow(<GameDefinitionsListUnwrapped classes={{}} />)
  });

  it('should render without crash', () => {
    expect(wrapper).toBeTruthy();
  });
});