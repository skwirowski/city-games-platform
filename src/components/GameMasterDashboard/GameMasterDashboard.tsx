import React from "react";
import NavBarContainer from "../GameMasterNavBar/NavBarContainer";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import LogoutButton from "../../shared/LogoutButton";
import { getUserData } from "../../services/auth";


const GameMasterDashboard: React.SFC = () => {
  return (
    <Grid container>
      <Grid item xs={12}>
        <NavBarContainer />
      </Grid>
    </Grid>
  );
};

export default GameMasterDashboard;
