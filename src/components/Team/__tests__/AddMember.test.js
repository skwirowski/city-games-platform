import React from "react";
import AddMember from "../components/TeamForm/AddMember";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";

jest.spyOn(global.console, "warn");

const FormikBag = {
  values: {
    members: [{ firstName: "Kamil", lastName: "Kovalsky", editMode: false }]
  }
};
describe("TeamForm", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("Renders without crashing", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });
});

const Unwrapped = unwrap(AddMember);

const createSut = props =>
  shallow(
    <Unwrapped
      firstNameMemberProps={""}
      lastNameMemberProps={""}
      index={1}
      classes={{}}
      FormikBag={FormikBag}
      isSubmitted={false}
      arrayHelpers={{}}
      editMode={false}
      setEditMode={{}}
      {...props}
    />
  );
