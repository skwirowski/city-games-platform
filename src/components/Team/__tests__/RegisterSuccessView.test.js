import React from "react";
import SucceedRegistration from "../components/TeamForm/SucceedRegistration";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";

describe("TeamForm", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });
  it("Renders without crashing", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });
  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const Unwrapped = unwrap(SucceedRegistration);
const createSut = props => shallow(<Unwrapped classes={{}} {...props} />);
