import React from "react";
import TeamForm from "../components/TeamForm/TeamForm";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import SubmitButton from "../../../shared/SubmitButton";

describe("TeamForm", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("Renders without crashing", () => {
    expect(createSut().isEmptyRender()).toBe(false);
  });
  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });

  describe("Button", () => {
    fit("Should have right string name when team.Uuid is undefined", () => {
      // when
      const sut = createSut();
      // then
      expect(
        sut
          .find(SubmitButton)
          .children()
          .prop("id")
      ).toEqual("TeamForm.registerButton");
    });
  });
});

const UnwrappedTeamForm = unwrap(TeamForm);

const createSut = props =>
  shallow(
    <UnwrappedTeamForm
      classes={{}}
      team={{}}
      isSubmitted={false}
      FormikBag={{}}
      {...props}
    />
  );
