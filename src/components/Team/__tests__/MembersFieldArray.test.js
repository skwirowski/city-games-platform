import React from "react";
import MembersFieldArray from "../components/TeamForm/MembersFieldArray";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";

jest.spyOn(global.console, "warn");

const FormikBag = {
  values: {
    members: [{ firstName: "aa", lastName: "ss", editMode: false }]
  }
};
describe("TeamForm", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("Renders without crashing", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const Unwrapped = unwrap(MembersFieldArray);

const createSut = props =>
  shallow(
    <Unwrapped
      classes={{}}
      FormikBag={FormikBag}
      isSubmitted={false}
      {...props}
    />
  );
