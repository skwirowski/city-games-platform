import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Button
} from "@material-ui/core";
import deleteTeam from "../../../../services/deleteTeam";
import RemoveTeamModal from "./RemoveTeamModal";

const styles = createStyles({
  deleteButton: {
    width: "250px",
    backgroundColor: "#ff0055",
    color: "white",
    borderRadius: "25px",
    marginTop: "25px",
    height: "40px",
    boxShadow: "0px 3px 14px rgba(0, 143, 211, 0.7)",
    "&:hover": {
      backgroundColor: "#42a5f5"
    }
  }
});

interface Props extends WithStyles<typeof styles> {
  teamUuid: string;
  teamStatus: string;
}

const RemoveTeam: React.FC<Props> = ({ classes, teamUuid, teamStatus }) => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleDeleteTeam = async (guid: string) => {
    try {
      await deleteTeam(teamUuid);
      handleClose();
    } catch (error) {
      handleClose();
    }
  };

  return (
    <Grid container>
      <Button
        className={classes.deleteButton}
        onClick={handleClickOpen}
        aria-label="open-modal-button"
      >
        Usuń drużynę
      </Button>
      <RemoveTeamModal
        teamUuid={teamUuid}
        teamStatus={teamStatus}
        handleClose={handleClose}
        open={open}
        handleDeleteTeam={handleDeleteTeam}
      />
    </Grid>
  );
};

export default withStyles(styles)(RemoveTeam);
