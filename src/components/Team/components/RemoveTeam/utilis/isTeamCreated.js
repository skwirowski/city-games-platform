const isTeamCreated = teamStatus => teamStatus === "CREATED";

export default isTeamCreated;
