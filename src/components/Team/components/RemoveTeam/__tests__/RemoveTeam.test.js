import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import RemoveTeam from "../RemoveTeam";
import { act } from "react-dom/test-utils";

describe("RemoveTeam component", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });

  describe("Button", () => {
    it("Should render modal when user click button", () => {
      const sut = createSut();
      act(() => {
        sut.find('[aria-label="open-modal-button"]').prop("onClick")();
      });

      sut.update();
      expect(
        sut
          .find('[teamUuid="1234"]')
          .dive()
          .dive()
          .find('[aria-labelledby="alert-dialog-title"]')
          .prop("open")
      ).toBe(true);
    });
  });
});

const UnwrappedRemoveTeam = unwrap(RemoveTeam);
const createSut = props =>
  shallow(
    <UnwrappedRemoveTeam
      classes={{}}
      teamUuid="1234"
      teamStatus="CREATED"
      {...props}
    />
  );
