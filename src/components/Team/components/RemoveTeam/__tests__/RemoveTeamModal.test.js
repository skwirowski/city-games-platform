import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import RemoveTeamModal from "../RemoveTeamModal";

describe("RemoveTeam component", () => {
  it("should render without crash", () => {
    const sut = createSut("1234", "CREATED");
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut("1234", "CREATED");
    expect(sut.debug()).toMatchSnapshot();
  });

  describe("Modal dialog text", () => {
    it("Should render right text when component have id and status CREATED", () => {
      const sut = createSut("1234", "CREATED");
      expect(
        sut
          .find('[id="alert-dialog-description"]')
          .children()
          .text()
      ).toEqual(
        "Czy chcesz trwale usunąć konto drużyny z aplikacji? Po jego usunięciu ponowne zalogowanie się na to samo konto będzie niemożliwe."
      );
    });
    it("Should render right text when component have id and status IN_GAME", () => {
      const sut = createSut("1234", "IN_GAME");
      expect(
        sut
          .find('[id="alert-dialog-description"]')
          .children()
          .text()
      ).toEqual("Jesteś w trakcie gry, nie możesz teraz usunąć konta.");
    });
  });

  describe("Modal button", () => {
    it("Should call close modal function when click", () => {
      const mockedCloseModal = jest.fn();
      const sut = createSut("1234", "CREATED", mockedCloseModal);
      sut.find('[color="primary"]').simulate("click");
      expect(mockedCloseModal).toBeCalled();
    });

    it("Should call delete user function when click", () => {
      const mockedCloseModal = jest.fn();
      const mockedDelete = jest.fn();
      const sut = createSut("1234", "CREATED", mockedCloseModal, mockedDelete);
      sut.find('[color="secondary"]').simulate("click");
      expect(mockedDelete).toBeCalled();
    });
  });
});

const UnwrappedRemoveTeamModal = unwrap(RemoveTeamModal);
const createSut = (id, status, close, deleteUser) =>
  shallow(
    <UnwrappedRemoveTeamModal
      classes={{}}
      teamUuid={id}
      teamStatus={status}
      handleClose={close}
      handleDeleteTeam={deleteUser}
      open="true"
    />
  );
