import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button
} from "@material-ui/core";
import isTeamCreated from "./utilis/isTeamCreated";

const styles = createStyles({
  dialogButton: {
    width: "150px",
    height: "40px",
    backgroundColor: "rgb(0,143, 213)",
    color: "white",
    boxShadow: "0px 3px 14px rgba(0, 143, 211, 0.7)"
  },
  deleteTeamButton: {
    width: "150px",
    margin: "20px 0"
  }
});

interface Props extends WithStyles<typeof styles> {}
interface UserData {
  teamUuid: string;
  teamStatus: string;
  handleClose: any;
  open: boolean;
  handleDeleteTeam: any;
}
interface Props extends UserData {}

const RemoveTeamModal: React.FC<Props> = ({
  classes,
  teamUuid,
  teamStatus,
  handleClose,
  open,
  handleDeleteTeam
}) => {
  return (
    <Dialog
      id="modalDel"
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        {"Usuwanie konta drużyny"}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {isTeamCreated(teamStatus)
            ? textToDeleteAccount
            : textCanNotDeleteAccount}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Grid container justify="center" direction="column" alignItems="center">
          <Button
            id="closeModal"
            color="primary"
            onClick={handleClose}
            className={classes.dialogButton}
            variant="contained"
          >
            Rezygnuję
          </Button>
          <Button
            color="secondary"
            variant="contained"
            className={classes.deleteTeamButton}
            onClick={
              isTeamCreated(teamStatus)
                ? () => handleDeleteTeam(teamUuid)
                : () => handleClose()
            }
            disabled={!isTeamCreated(teamStatus)}
          >
            Usuń
          </Button>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

const textToDeleteAccount =
  "Czy chcesz trwale usunąć konto drużyny z aplikacji? Po jego usunięciu ponowne zalogowanie się na to samo konto będzie niemożliwe.";
const textCanNotDeleteAccount =
  "Jesteś w trakcie gry, nie możesz teraz usunąć konta.";

export default withStyles(styles)(RemoveTeamModal);
