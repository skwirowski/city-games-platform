import React from "react";
import {
  WithStyles,
  withStyles,
  Button,
  FormHelperText,
  Grid,
  createStyles
} from "@material-ui/core";
import DeleteForeverOutlinedIcon from "@material-ui/icons/Delete";
import PersonOutline from "@material-ui/icons/PersonOutline";
import Create from "@material-ui/icons/Create";
import { Field, ErrorMessage } from "formik";
import checkMembersExist from "../../utilis/checkMembersExist";
import Typography from "@material-ui/core/Typography";
import AddMember from "./AddMember";

const styles = createStyles({
  personOutline: {
    width: "34px",
    height: "34px",
    color: "rgb(0,143, 213)",
    marginTop: "5px",
    marginRight: "6px"
  },
  typograpfySubtitle: {
    marginTop: "10px",
    paddingLeft: "30px",
    color: "gray"
  },
  editMemberButton: {
    width: "25px",
    height: "35px",
    color: "rgb(0,143, 213)",
    padding: "8px",
    paddingLeft: "15px"
  },
  removeMemberButton: {
    minWidth: "30px",
    height: "35px",
    color: "black",
    padding: "8px"
  }
});

interface Props extends WithStyles<typeof styles> {
  FormikBag: any;
  isSubmitted: boolean;
  arrayHelpers: any;
  editMode: boolean;
  setEditMode: any;
}

const MembersList: React.FC<Props> = ({
  classes,
  FormikBag,
  isSubmitted,
  arrayHelpers,
  editMode,
  setEditMode
}) => {
  return (
    <Grid
      container
      direction="column"
      justify="center"
      style={{ paddingRight: "0" }}
    >
      {checkMembersExist(FormikBag) ? (
        FormikBag.values.members.map((member: any, index: any) => (
          <Grid key={index}>
            {member.editMode === false ? (
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
              >
                <Grid item xs={1}>
                  <PersonOutline className={classes.personOutline} />
                </Grid>
                <Grid item xs={7}>
                  <Typography
                    variant="subtitle1"
                    gutterBottom
                    className={classes.typograpfySubtitle}
                  >
                    {member.firstName + " "}
                    {member.lastName}
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Field
                    render={({ field, form }: any) => (
                      <Button
                        className={classes.editMemberButton}
                        disabled={isSubmitted || editMode === true}
                        name={`members.${index}.editMode`}
                        color="primary"
                        onClick={() => {
                          form.setFieldValue(`members.${index}.editMode`, true);
                          setEditMode(true);
                        }}
                      >
                        <Create />
                      </Button>
                    )}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Button
                    disabled={isSubmitted}
                    className={classes.removeMemberButton}
                    type="button"
                    color="primary"
                    onClick={() => arrayHelpers.remove(index)}
                  >
                    <DeleteForeverOutlinedIcon />
                  </Button>
                </Grid>
              </Grid>
            ) : (
              <AddMember
                FormikBag={FormikBag}
                isSubmitted={isSubmitted}
                arrayHelpers={arrayHelpers}
                firstNameMemberProps={`members.${index}.firstName`}
                lastNameMemberProps={`members.${index}.lastName`}
                index={index}
                editMode={editMode}
                setEditMode={setEditMode}
              />
            )}
          </Grid>
        ))
      ) : (
        <Grid>
          <FormHelperText
            error
            style={{ marginBottom: "20px", textAlign: "center" }}
          >
            <ErrorMessage name={`members`}>
              {msg => <span>{msg}</span>}
            </ErrorMessage>
          </FormHelperText>
        </Grid>
      )}
    </Grid>
  );
};

export default withStyles(styles)(MembersList);
