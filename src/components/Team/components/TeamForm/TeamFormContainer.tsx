import React, { useState, useEffect } from "react";
import {
  WithStyles,
  withStyles,
  Grid,
  Typography,
  createStyles
} from "@material-ui/core";
import { Formik, FormikActions, FormikProps } from "formik";
import * as Yup from "yup";
import { FormattedMessage } from "react-intl";
import TeamForm from "./TeamForm";
import createTeamValues from "../../utilis/createTeamValues";
import SucceedRegistration from "./SucceedRegistration";
import { ITeamForm } from "../../types";
import { getUserData } from "../../../../services/auth";
import fetchTeam from "../../../../services/fetchTeam";
import updateTeam from "../../../../services/updateTeam";
import createTeam from "../../../../services/createTeam";
import teamDtoFactory from "../../utilis/teamDtoFactory";
import arrowBackRedirect from "../../utilis/arrowBackRedirect";
import isRegeisteredSuccessfuly from "../../utilis/isRegeisteredSuccessfuly";
import ArrowBack from "../../../../shared/ArrowBack";
import Link from "../../../../shared/LinkWithoutDefaultStyles";
import Intl from "../../../../utils/Intl";

const styles = createStyles({
  primaryContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    margin: "0 auto",
    background: "rgb(238, 249, 255)",
    padding: "30px 20px 60px"
  },
  typographyPrimary: {
    color: "rgb(0,143, 213)",
    marginBottom: "10px"
  }
});

interface Props extends WithStyles<typeof styles> {
  match: any;
  history: any;
}

const TeamFormContainer: React.FC<Props> = ({ classes, match, history }) => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [team, setTeam] = useState({});
  const [error, setError] = useState({
    isError: false,
    errorMessage: ""
  });
  const userData = getUserData();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetchTeam(userData.uuid);
        setTeam(response.data);
      } catch (error) {
        setError({ isError: true, errorMessage: error.message });
      }
      fetchTeam(userData.uuid);
    };
    if (match.params.id) {
      fetchData();
    }
  }, [match.params.id, userData.uuid]);

  const sendCreateGroupRequest = async (values: any, actions: any) => {
    const data = teamDtoFactory(values, userData, match.params.id);
    try {
      await createTeam(data);
      actions.setSubmitting(false);
      setIsSubmitted(true);
    } catch (error) {
      setError({ isError: true, errorMessage: error.message });
    }
  };
  const sendUpdateGroupRequest = async (values: any, actions: any) => {
    const data = teamDtoFactory(values, userData, match.params.id);
    try {
      await updateTeam(data);
      actions.setSubmitting(false);
      setIsSubmitted(true);
    } catch (error) {
      setError({ isError: true, errorMessage: error.message });
    }
  };

  const submitForm = (
    values: any,
    actions: any,
    teamId: any,
    callbackCreate: any,
    callbackUpdate: any
  ) => {
    if (teamId !== undefined) {
      callbackUpdate(values, actions);
    } else {
      callbackCreate(values, actions);
    }
  };

  return (
    <Grid>
      {isRegeisteredSuccessfuly(isSubmitted, match.params.id, error.isError) ? (
        <Grid>
          <SucceedRegistration />
        </Grid>
      ) : (
        <Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="center"
            style={{ marginTop: "20px", marginBottom: "10px" }}
          >
            <Grid item xs={3}>
              <Link to={arrowBackRedirect(match.params.id)}>
                <ArrowBack />
              </Link>
            </Grid>
            <Grid item xs={9}>
              <Typography variant="h4" className={classes.typographyPrimary}>
                <FormattedMessage id="TeamForm.myTeam" />
              </Typography>
              {error.errorMessage}
            </Grid>
          </Grid>
          <Grid className={classes.primaryContainer}>
            <Formik
              initialValues={createTeamValues(team, userData)}
              enableReinitialize={true}
              validationSchema={formGroupSchema}
              onSubmit={(
                values: ITeamForm,
                actions: FormikActions<ITeamForm>
              ) =>
                submitForm(
                  values,
                  actions,
                  match.params.id,
                  sendCreateGroupRequest,
                  sendUpdateGroupRequest
                )
              }
              render={(FormikBag: FormikProps<ITeamForm>) => (
                <TeamForm
                  isSubmitted={isSubmitted}
                  team={team}
                  FormikBag={FormikBag}
                  uuid={userData.uuid}
                />
              )}
            />
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

const formGroupSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, Intl.messages["TeamForm.shortErrorMessage"])
    .max(150, Intl.messages["TeamForm.longErrorMessage"])
    .required(Intl.messages["TeamForm.requiredErrorMessage"]),
  email: Yup.string()
    .email(Intl.messages["TeamForm.emailErrorMessage"])
    .required(Intl.messages["TeamForm.requiredErrorMessage"]),
  phone: Yup.string()
    .min(9, Intl.messages["TeamForm.shortErrorMessage"])
    .max(15, Intl.messages["TeamForm.longErrorMessage"])
    .required(Intl.messages["TeamForm.requiredErrorMessage"]),
  terms: Yup.boolean().oneOf(
    [true],
    Intl.messages["TeamForm.termsErrorMessage"]
  ),
  members: Yup.array()
    .of(
      Yup.object().shape({
        firstName: Yup.string()
          .min(2, Intl.messages["TeamForm.shortErrorMessage"])
          .max(250, Intl.messages["TeamForm.longErrorMessage"])
          .required(Intl.messages["TeamForm.requiredErrorMessage"]),
        lastName: Yup.string()
          .min(2, Intl.messages["TeamForm.shortErrorMessage"])
          .max(250, Intl.messages["TeamForm.longErrorMessage"])
          .required(Intl.messages["TeamForm.requiredErrorMessage"])
      })
    )
    .min(1, Intl.messages["TeamForm.membersErrorMessage"])
});

export default withStyles(styles)(TeamFormContainer);
