import React, { useState } from "react";
import {
  WithStyles,
  withStyles,
  Grid,
  Typography,
  createStyles
} from "@material-ui/core";
import { FieldArray } from "formik";
import { FormattedMessage } from "react-intl";
import MembersList from "./MembersList";
import AddMember from "./AddMember";

const styles = createStyles({
  typographyPrimary: {
    color: "rgb(0,143, 213)",
    marginBottom: "10px"
  }
});

interface Props extends WithStyles<typeof styles> {
  FormikBag: any;
  isSubmitted: boolean;
}

const MembersFieldArray: React.FC<Props> = ({
  classes,
  FormikBag,
  isSubmitted
}) => {
  const [editMode, setEditMode] = useState(false);
  return (
    <FieldArray
      name="members"
      render={arrayHelpers => (
        <Grid container direction="column" justify="center" alignItems="center">
          <Typography variant="h4" className={classes.typographyPrimary}>
            <FormattedMessage id="TeamForm.TeamMembers" />
          </Typography>
          <MembersList
            isSubmitted={isSubmitted}
            FormikBag={FormikBag}
            arrayHelpers={arrayHelpers}
            editMode={editMode}
            setEditMode={setEditMode}
          />
          {editMode === false ? (
            <AddMember
              FormikBag={FormikBag}
              isSubmitted={isSubmitted}
              arrayHelpers={arrayHelpers}
              firstNameMemberProps={`firstName`}
              lastNameMemberProps={`lastName`}
              index={""}
              editMode={editMode}
              setEditMode={setEditMode}
            />
          ) : (
            ""
          )}
        </Grid>
      )}
    />
  );
};

export default withStyles(styles)(MembersFieldArray);
