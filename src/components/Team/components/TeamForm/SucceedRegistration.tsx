import React from "react";
import bg_blur_dark from "../../../../assets/images/bg_blur_dark.jpg";
import { WithStyles, withStyles, Grid, Typography, createStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import check_circle from "../../../../assets/images/check_circle.png";
import SubmitButton from "../../../../shared/SubmitButton";
import LinkWithoutDefaultStyles from "../../../../shared/LinkWithoutDefaultStyles";
const styles = createStyles({
  primaryContainerSuccess: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    padding: "35px",
    backgroundImage: `url(${bg_blur_dark})`,
    backgroundPosition: "top",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  checkCircle: {
    width: "70px",
    height: "70px",
    filter: "drop-shadow(0px 3px 8px rgba(80, 228, 144, 0.7))"
  },
  typographyWhite: {
    color: "white",
    textAlign: "center",
    marginTop: "15px"
  }
});
interface Props extends WithStyles<typeof styles> {}

const SucceedRegistration: React.FC<Props> = ({ classes }) => {
  return (
    <Grid container className={classes.primaryContainerSuccess}>
      <Grid style={{ marginTop: "40px" }}>
        <img src={check_circle} alt="Done" className={classes.checkCircle} />
      </Grid>
      <Grid>
        <Typography variant="h4" className={classes.typographyWhite}>
          <FormattedMessage id="TeamForm.congratulations" />
        </Typography>
        <Typography variant="subtitle1" className={classes.typographyWhite}>
          <FormattedMessage id="TeamForm.successfulRegister" />
        </Typography>
      </Grid>
      <Grid style={{ marginTop: "60px" }}>
        <LinkWithoutDefaultStyles to="/game">
          <SubmitButton disabled={false} type="button">
            <FormattedMessage id="TeamForm.letsStart" />
          </SubmitButton>
        </LinkWithoutDefaultStyles>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(SucceedRegistration);
