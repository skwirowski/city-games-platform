import React from "react";
import MembersFieldArray from "./MembersFieldArray";
import { ITeamForm } from "../../types";
import { Form, Field, FieldProps } from "formik";
import {
  WithStyles,
  withStyles,
  Checkbox,
  FormControl,
  FormHelperText,
  Grid,
  FormControlLabel,
  FormGroup,
  Typography,
  createStyles
} from "@material-ui/core";
import CheckBoxOutlineBlank from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import ErrorOutline from "@material-ui/icons/ErrorOutline";
import { FormattedMessage } from "react-intl";
import SubmitButton from "../../../../shared/SubmitButton";
import FTextFieldMobile from "../../../../shared/FormikComponents/FTextFieldMobile";
import RemoveTeam from "../RemoveTeam";
import Intl from "../../../../utils/Intl";
import { Link } from "react-router-dom";
import { routes } from "../../../../static/routesUrl";

const styles = createStyles({
  muiTextField: {
    backgroundColor: "white",
    borderBottom: "2px solid rgb(0,143, 213)"
  },
  checkboxForm: {
    margin: "0",
    marginTop: "5px"
  },
  checkbox: {
    width: "7px",
    marginRight: "10px",
    height: "20px",
    color: "rgb(0,143, 213)"
  },
  checkboxError: {
    marginBottom: "25px"
  },
  successText: {
    textAlign: "center",
    marginTop: "20px"
  },
  phoneInfoText: {
    fontSize: "12px",
    width: "76%",
    color: "rgb(0,143, 213)"
  }
});
interface Props extends WithStyles<typeof styles> {
  FormikBag: any;
  team: any;
  isSubmitted: boolean;
  uuid: any;
}

const TeamForm: React.FC<Props> = ({
  classes,
  FormikBag,
  team,
  isSubmitted,
  uuid
}) => {
  return (
    <Form>
      <Field
        name="name"
        render={({ field, form }: any) => (
          <FormControl fullWidth style={{ marginTop: "15px" }}>
            <FTextFieldMobile
              label={Intl.messages["TeamForm.teamNamePlaceholder"]}
              fieldProps={field}
              disabled={isSubmitted}
            />
            <FormHelperText error>
              {form.touched.name && form.errors.name && form.errors.name}
            </FormHelperText>
          </FormControl>
        )}
      />
      <Field
        name="phone"
        render={({ field, form }: any) => (
          <FormControl fullWidth style={{ marginTop: "15px" }}>
            <FTextFieldMobile
              label={Intl.messages["TeamForm.phonePlaceholder"]}
              fieldProps={field}
              disabled={team.email ? true : false}
            />
            <FormHelperText error>
              {form.touched.phone && form.errors.phone && form.errors.phone}
            </FormHelperText>
            <Grid container direction="row" style={{ marginTop: "0.4rem" }}>
              <ErrorOutline
                style={{ color: "rgb(0,143, 213)", marginRight: "0.6rem" }}
              />
              <Typography className={classes.phoneInfoText}>
                Numer telefonu, pod którym będziecie dostępni w czasie gry
              </Typography>
            </Grid>
          </FormControl>
        )}
      />
      <Field
        name="email"
        render={({ field, form }: any) => (
          <FormControl fullWidth style={{ marginTop: "15px" }}>
            <FTextFieldMobile
              label={Intl.messages["TeamForm.emailPlaceholder"]}
              fieldProps={field}
              disabled={true}
            />
            <FormHelperText error>
              {form.touched.email && form.errors.email && form.errors.email}
            </FormHelperText>
          </FormControl>
        )}
      />

      <Grid>
        <Field
          name="terms"
          type="checkbox"
          render={({ field, form }: FieldProps<ITeamForm>) => (
            <FormGroup>
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                <FormControlLabel
                  className={classes.checkboxForm}
                  control={
                    <Checkbox
                      color="primary"
                      icon={
                        <CheckBoxOutlineBlank
                          style={{
                            fontSize: 40
                          }}
                        />
                      }
                      checkedIcon={
                        <CheckBoxIcon
                          style={{ fontSize: 40, color: "rgb(0,143, 213)" }}
                        />
                      }
                      {...field}
                      disabled={isSubmitted}
                      className={classes.checkbox}
                    />
                  }
                  label={
                    <Typography
                      variant="subtitle2"
                      style={{ lineHeight: "1rem" }}
                    >
                      <FormattedMessage id="TeamForm.acceptTerms" />
                      <Link to={routes.termsAndConditions}>
                        <FormattedMessage id="TeamForm.acceptTermsLink" />
                      </Link>
                    </Typography>
                  }
                />
                <FormHelperText error className={classes.checkboxError}>
                  {form.touched.terms && form.errors.terms && form.errors.terms}
                </FormHelperText>
              </Grid>
            </FormGroup>
          )}
        />
      </Grid>
      <MembersFieldArray FormikBag={FormikBag} isSubmitted={isSubmitted} />
      <Grid container justify="center" style={{ marginTop: "30px" }}>
        {team.teamUuid === undefined ? (
          <SubmitButton disabled={isSubmitted} type="submit">
            <FormattedMessage id="TeamForm.registerButton" />
          </SubmitButton>
        ) : (
          <Grid>
            <SubmitButton disabled={isSubmitted} type="submit">
              <FormattedMessage id="TeamForm.readyButton" />
            </SubmitButton>
            <RemoveTeam teamUuid={uuid} teamStatus={team.teamStatus} />
          </Grid>
        )}
      </Grid>
      <Grid container justify="center" alignItems="center">
        {isSubmitted && team.teamUuid !== undefined ? (
          <Typography
            variant="h6"
            aria-label="success-update-account"
            className={classes.successText}
          >
            <FormattedMessage id="TeamForm.teamDataHaveBeenSaved" />
          </Typography>
        ) : (
          ""
        )}
      </Grid>
    </Form>
  );
};

export default withStyles(styles)(TeamForm);
