import React from "react";
import {
  WithStyles,
  withStyles,
  Button,
  FormHelperText,
  FormControl,
  Input,
  Grid,
  createStyles
} from "@material-ui/core";
import { Field, ErrorMessage, FieldProps } from "formik";
import { ITeamForm } from "../../types";
import PersonAddOutlined from "@material-ui/icons/PersonAddOutlined";
import addNewMember from "../../utilis/addNewMember";
import confirmFinishMemberEdit from "../../utilis/confirmFinishMemberEdit";
import validateMessage from "../../utilis/validateMessage";
import Intl from "../../../../utils/Intl";

const styles = createStyles({
  personOutline: {
    width: "34px",
    height: "34px",
    color: "rgb(0,143, 213)",
    marginTop: "5px",
    marginRight: "6px"
  },
  addMemberButton: {
    padding: "0",
    paddingBottom: "8px"
  },
  muiTextFieldLow: {
    paddingLeft: "15px",
    backgroundColor: "white",
    borderBottom: "2px solid rgb(0,143, 213)",
    height: "45px"
  }
});

interface Props extends WithStyles<typeof styles> {
  FormikBag: any;
  isSubmitted: boolean;
  arrayHelpers: any;
  firstNameMemberProps: any;
  lastNameMemberProps: any;
  index: any;
  editMode: boolean;
  setEditMode: any;
}

const AddMember: React.FC<Props> = ({
  classes,
  FormikBag,
  isSubmitted,
  arrayHelpers,
  firstNameMemberProps,
  lastNameMemberProps,
  index,
  editMode,
  setEditMode
}) => {
  return (
    <Grid
      container
      direction="row"
      style={{ marginTop: "5px" }}
      justify="flex-start"
    >
      <Grid item xs={2}>
        <Field
          render={({ field, form }: FieldProps<ITeamForm>) => {
            return (
              <Button
                name={firstNameMemberProps}
                className={classes.addMemberButton}
                disabled={isSubmitted}
                type="button"
                onClick={
                  index === ""
                    ? () => addNewMember(FormikBag, arrayHelpers, form)
                    : () => {
                        confirmFinishMemberEdit(
                          FormikBag,
                          form,
                          index,
                          setEditMode
                        );
                      }
                }
              >
                <PersonAddOutlined className={classes.personOutline} />
              </Button>
            );
          }}
        />
      </Grid>
      <Grid item xs={9}>
        <Field
          name={firstNameMemberProps}
          render={({ field, form }: FieldProps<ITeamForm>) => {
            return (
              <FormControl fullWidth>
                <Input
                  className={classes.muiTextFieldLow}
                  {...field}
                  disableUnderline={true}
                  placeholder={Intl.messages["TeamForm.firstNamePlaceholder"]}
                />
                {index !== "" ? (
                  <FormHelperText error>
                    <ErrorMessage name={firstNameMemberProps}>
                      {msg => <span>{msg}</span>}
                    </ErrorMessage>
                  </FormHelperText>
                ) : (
                  <Grid style={{ height: "20px" }}>
                    {validateMessage(FormikBag, "first")}
                  </Grid>
                )}
              </FormControl>
            );
          }}
        />
        <Field
          name={lastNameMemberProps}
          render={({ field, form }: FieldProps<ITeamForm>) => {
            return (
              <FormControl fullWidth>
                <Input
                  className={classes.muiTextFieldLow}
                  {...field}
                  disableUnderline={true}
                  placeholder={Intl.messages["TeamForm.lastNamePlaceholder"]}
                />
                {index !== "" ? (
                  <FormHelperText error>
                    <ErrorMessage name={lastNameMemberProps}>
                      {msg => <span>{msg}</span>}
                    </ErrorMessage>
                  </FormHelperText>
                ) : (
                  <Grid style={{ height: "20px" }}>
                    {validateMessage(FormikBag, "last")}
                  </Grid>
                )}
              </FormControl>
            );
          }}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(AddMember);
