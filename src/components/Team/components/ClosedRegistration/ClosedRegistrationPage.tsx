import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import AboutGameHeader from "../../../TeamGame/AboutGame/AboutGameHeader";
import Intl from "../../../../utils/Intl";

const styles = createStyles({
  textContainer: {
    padding: "45px 30px",
    textAlign: "center"
  }
});

interface Props extends WithStyles<typeof styles> {
  history: any;
}
const ClosedRegistrationPage: React.FC<Props> = ({ classes, history }) => {
  const handleGoBack = (e: any) => {
    e.preventDefault();
    history.goBack();
  };

  return (
    <Grid container direction="column">
      <AboutGameHeader handleGoBack={handleGoBack} />
      <Grid className={classes.textContainer}>
        <Typography variant="h5">
          {Intl.messages["TeamForm.registrationIsClosed"]}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(ClosedRegistrationPage);
