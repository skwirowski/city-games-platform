export interface ITeamForm {
  name: string;
  email: string;
  phone: string;
  terms: boolean;
  members: Array<{
    firstName: string;
    lastName: string;
    editMode: boolean;
  }>;
  firstName: string;
  lastName: string;
  editMode: boolean;
  teamId: any;
  gameUuid: any;
}
