import actualGameUuid from "../../../static/gameDefinitionUuid";
const teamDtoFactory = (values: any, userData: any, matchId: any) => {
  return {
    gameUuid: actualGameUuid,
    teamUuid: matchId ? values.teamUuid : userData.uuid,
    teamName: values.name,
    teamContactPhoneNumber: values.phone,
    teamContactEmail: values.email,
    teamMembers: values.members,
    teamId: matchId ? values.teamId : null
  };
};

export default teamDtoFactory;
