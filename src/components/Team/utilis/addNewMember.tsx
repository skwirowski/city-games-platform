const addNewMember = (FormikBag: any, arrayHelpers: any, form: any) => {
  if (
    FormikBag.values.firstName.length > 1 &&
    FormikBag.values.lastName.length > 1
  ) {
    arrayHelpers.push({
      firstName: FormikBag.values.firstName,
      lastName: FormikBag.values.lastName,
      editMode: false
    });
    form.setFieldValue("firstName", "");
    form.setFieldValue("lastName", "");
  }
};

export default addNewMember;
