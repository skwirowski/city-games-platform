const checkMembersExist = (FormikBag: any) => {
  if (FormikBag.values.members && FormikBag.values.members.length > 0) {
    return true;
  }
  return false;
};

export default checkMembersExist;
