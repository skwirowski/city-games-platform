const confirmFinishMemberEdit = (
  FormikBag: any,
  form: any,
  index: any,
  callback: any
) => {
  if (
    FormikBag.values.members[index].firstName.length > 1 &&
    FormikBag.values.members[index].lastName.length > 1
  ) {
    form.setFieldValue(`members.${index}.editMode`, false);
    callback(false);
  }
};

export default confirmFinishMemberEdit;
