import React from "react";
import { FormHelperText } from "@material-ui/core";

const validateMessage = (FormikBag: any, name: any) => {
  const firstName = FormikBag.values.firstName;
  const lastName = FormikBag.values.lastName;
  const Message = "Nazwa jest za krótka";
  if (name === "first" && firstName.length < 2 && firstName.length > 0) {
    return (
      <FormHelperText error>
        <span>{Message}</span>
      </FormHelperText>
    );
  } else if (name === "last" && lastName.length < 2 && lastName.length > 0) {
    return (
      <FormHelperText error>
        <span>{Message}</span>
      </FormHelperText>
    );
  }
};

export default validateMessage;
