const isRegistrationOpen = (
  finishDate: Date,
  actualDate: Date,
  matchParamsId: any
) => {
  return finishDate < actualDate && !matchParamsId;
};

export default isRegistrationOpen;
