const createTeamValues = (team: any, userData: any) => {
  return {
    teamUuid: team.teamUuid,
    name: team.teamUuid ? team.teamName : "",
    email: userData.email,
    phone: team.teamUuid ? team.teamContactPhoneNumber : "",
    terms: false,
    members: team.teamUuid ? team.teamMembers || [] : [],
    firstName: "",
    lastName: "",
    editMode: false,
    teamId: team.teamUuid ? team.teamId : "",
    gameUuid: team.teamUuid ? team.gameUuid : ""
  };
};

export default createTeamValues;
