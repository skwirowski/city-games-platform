const arrowBackRedirect = (matchParamsId: any) => {
  if (matchParamsId === undefined) {
    return "/";
  }
  return "/game";
};

export default arrowBackRedirect;
