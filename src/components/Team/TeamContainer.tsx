import React, { useState, useEffect } from "react";
import {
  WithStyles,
  withStyles,
  Grid,
  createStyles,
  Typography
} from "@material-ui/core";
import { RouteComponentProps } from "react-router-dom";
import TeamFormContainer from "./components/TeamForm";
import isRegistrationOpen from "./utilis/isRegistrationOpen";
import ClosedRegistrationPage from "./components/ClosedRegistration";
import fetchGameDefinition from "../../services/fetchGameDefinition";
import actualGameUuid from "../../static/gameDefinitionUuid";

const styles = createStyles({
  errorContainer: {
    paddingTop: "40px"
  }
});

interface Props extends WithStyles<typeof styles> {}
interface MatchParams {
  id: string;
  history: any;
}
interface Props extends RouteComponentProps<MatchParams> {}

const TeamContainer: React.FC<Props> = ({ classes, match, history }) => {
  const [finishRegistrationDate, setFinishRegistrationDate] = useState();
  const [actualDate, setActualDate] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    const fetchGameDefinitionData = async () => {
      try {
        const response = await fetchGameDefinition(actualGameUuid);
        const parsedDate = new Date(response.data.registrationFinishDate);
        const actualDate = new Date();
        setFinishRegistrationDate(parsedDate);
        setActualDate(actualDate);
      } catch (error) {
        setError(error.message);
      }
    };

    fetchGameDefinitionData();
  }, []);

  return (
    <Grid>
      {error ? (
        <Grid container justify="center" className={classes.errorContainer}>
          <Typography>{error}</Typography>
        </Grid>
      ) : (
        <Grid>
          {isRegistrationOpen(
            finishRegistrationDate,
            actualDate,
            match.params.id
          ) ? (
            <ClosedRegistrationPage history={history} />
          ) : (
            <TeamFormContainer match={match} history={history} />
          )}
        </Grid>
      )}
    </Grid>
  );
};

export default withStyles(styles)(TeamContainer);
