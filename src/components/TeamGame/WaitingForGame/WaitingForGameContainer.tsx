import React, { useState, useEffect, useRef } from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import BackgroundNightCity from "../../../assets/images/bg_night_city.jpg";
import PeopleOutline from "@material-ui/icons/PeopleOutline";
import ErrorOutline from "@material-ui/icons/ErrorOutline";
import TimeComponent from "./TimeComponent";
import Link from "../../../shared/LinkWithoutDefaultStyles";
import ErrorMessage from "../../../shared/ErrorMessage";
import Intl from "../../../utils/Intl";
import { getUserData } from "../../../services/auth";
import { routes } from "../../../static/routesUrl";
import actualGameUuid from "../../../static/gameDefinitionUuid";
import calcTimeLeft from "./utilis/calcTimeLeft";
import fetchRouteForTeamInGames from "../../../services/fetchRouteForTeamInGames";
import fetchGameDefinition from "../../../services/fetchGameDefinition";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Redirect } from "react-router";
import { TimeLeft } from "./types";
import waitingForStartDate from "./utilis/waitingForStartDate";
import isTimeForGame from "./utilis/isTimeForGame";
import leftOneDayToStart from "./utilis/leftOneDayToStart";

const styles = createStyles({
  primaryContainer: {
    height: "100vh",
    backgroundImage: `url(${BackgroundNightCity})`,
    backgroundPosition: "top",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    padding: "25px"
  },
  typographyHeader: {
    color: "white",
    fontSize: "2.5em",
    textShadow: "2px 2px black"
  },
  typoContainer: {
    marginTop: "80px"
  },
  muiIcon: {
    color: "white",
    fontSize: "35px"
  },
  typoIcon: {
    color: "white",
    fontSize: "20px",
    lineHeight: "2.2rem",
    paddingLeft: "20px"
  },
  timerContainer: {
    height: "50vh"
  },
  myTeamContainer: {
    marginTop: "1rem"
  },
  loader: {
    position: "absolute",
    top: "50%",
    left: "50%",
    color: "white"
  },
  errorText: {
    color: "White"
  }
});

interface Props extends WithStyles<typeof styles> {}

const WaitingForGameContainer: React.FC<Props> = ({ classes }) => {
  const [timeLeft, setTimeLeft] = useState<TimeLeft | undefined>(undefined);
  const isMounted = useRef(true);
  const [startDate, setStartDate] = useState();
  const [finishDate, setFinishDate] = useState();
  const [dateNow, setDateNow] = useState();
  const [errorMessage, setErrorMessage] = useState<string | undefined>(
    undefined
  );

  useEffect(() => {
    const fetchFinishDate = async () => {
      try {
        const response = await fetchGameDefinition(actualGameUuid);
        const parsedFinishDate = Date.parse(response.data.finishDate);
        setFinishDate(parsedFinishDate);
      } catch (error) {
        setErrorMessage(error.message);
      }
    };
    fetchFinishDate();
  }, []);

  useEffect(() => {
    const fetchRoute = async () => {
      try {
        const response = await fetchRouteForTeamInGames(
          getUserData().uuid,
          actualGameUuid
        );
        if (response.status === 200) {
          const parsedStartDate = Date.parse(
            response.data.gameStartTimeForTeam
          );
          setStartDate(parsedStartDate);
        } else {
          try {
            const response = await fetchGameDefinition(actualGameUuid);
            const parsedStartDate = Date.parse(response.data.startDate);
            setStartDate(parsedStartDate);
          } catch (error) {
            setErrorMessage(error.message);
          }
        }
      } catch (error) {
        setErrorMessage(error.message);
      }
    };
    fetchRoute();
  }, [startDate]);

  useEffect(() => {
    if (startDate) {
      setInterval(() => {
        if (isMounted.current) {
          const actualDate = new Date().getTime();
          setDateNow(actualDate);
          setTimeLeft(calcTimeLeft(startDate, actualDate));
        }
      }, 1000);
      return () => {
        isMounted.current = false;
      };
    }
  }, [startDate]);

  return (
    <Grid container direction="column" className={classes.primaryContainer}>
      <Grid container justify="center">
        {errorMessage ? <ErrorMessage message={errorMessage} /> : ""}
      </Grid>
      <Grid>
        {!timeLeft ? (
          <CircularProgress className={classes.loader} />
        ) : (
          <Grid>
            {waitingForStartDate(timeLeft.diff) ? (
              <Grid>
                <Grid className={classes.typoContainer}>
                  <Typography variant="h4" className={classes.typographyHeader}>
                    {Intl.messages["WaitingForGame.headerTextInfo"]}
                  </Typography>
                </Grid>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  className={classes.timerContainer}
                >
                  <TimeComponent
                    number={leftOneDayToStart(
                      timeLeft.days,
                      timeLeft.days,
                      timeLeft.hours
                    )}
                    text={leftOneDayToStart(
                      timeLeft.days,
                      Intl.messages["WaitingForGame.days"],
                      Intl.messages["WaitingForGame.hours"]
                    )}
                  />
                  <TimeComponent
                    number={leftOneDayToStart(
                      timeLeft.days,
                      timeLeft.hours,
                      timeLeft.minutes
                    )}
                    text={leftOneDayToStart(
                      timeLeft.days,
                      Intl.messages["WaitingForGame.hours"],
                      Intl.messages["WaitingForGame.minutes"]
                    )}
                  />
                  <TimeComponent
                    number={leftOneDayToStart(
                      timeLeft.days,
                      timeLeft.minutes,
                      timeLeft.seconds
                    )}
                    text={leftOneDayToStart(
                      timeLeft.days,
                      Intl.messages["WaitingForGame.minutes"],
                      Intl.messages["WaitingForGame.seconds"]
                    )}
                  />
                </Grid>
                <Grid>
                  <Link to={routes.aboutGame}>
                    <Grid container direction="row">
                      <ErrorOutline className={classes.muiIcon} />
                      <Typography className={classes.typoIcon}>
                        {Intl.messages["TeamNavBar.aboutGame"]}
                      </Typography>
                    </Grid>
                  </Link>
                  <Link to={`${routes.team}/${getUserData().uuid}`}>
                    <Grid
                      container
                      direction="row"
                      className={classes.myTeamContainer}
                    >
                      <PeopleOutline className={classes.muiIcon} />
                      <Typography className={classes.typoIcon}>
                        {Intl.messages["TeamNavBar.myTeam"]}
                      </Typography>
                    </Grid>
                  </Link>
                </Grid>
              </Grid>
            ) : (
              <Grid>
                {isTimeForGame(timeLeft.diff, dateNow, finishDate) ? (
                  <Grid>
                    <Redirect to={routes.startGame} />
                  </Grid>
                ) : (
                  <Grid>
                    {/*}
                    <Redirect to={routes.gameResults} />
                    {*/}
                    <Redirect to={routes.startGame} />
                  </Grid>
                )}
              </Grid>
            )}
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(WaitingForGameContainer);
