import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";

const styles = createStyles({
  container: {
    width: "33%",
    padding: "10px",
    flexDirection: "column",
    alignItems: "center",
    paddingTop: "60px"
  },
  numberContainer: {
    width: "70px",
    height: "70px",
    border: "1px solid white",
    borderRadius: "15px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    background: "rgba(0,0,0, 0.3)"
  },
  typoNumber: {
    color: "white",
    fontSize: "45px",
    margin: "0",
    textAlign: "center"
  },
  typoText: {
    color: "white",
    textAlign: "center",
    fontSize: "17px",
    textTransform: "uppercase",
    marginTop: "1rem"
  }
});

interface Props extends WithStyles<typeof styles> {
  number: any;
  text: any;
}

const TimeComponent: React.FC<Props> = ({ classes, number, text }) => {
  return (
    <Grid>
      <Grid container direction="column" className={classes.container}>
        <Grid className={classes.numberContainer}>
          <Typography className={classes.typoNumber}>{number}</Typography>
        </Grid>
        <Grid>
          <Grid className={classes.typoText}>{text}</Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(TimeComponent);
