const calcTimeLeft = (dateFuture: any, dateNow: any) => {
  const diff = dateFuture - dateNow;
  let seconds = Math.floor(diff / 1000);
  let minutes = Math.floor(seconds / 60);
  let hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);

  hours = hours - days * 24;
  minutes = minutes - days * 24 * 60 - hours * 60;
  seconds = seconds - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60;

  if (diff > 0) {
    return { days, hours, minutes, seconds, diff };
  } else {
    return { days: 0, hours: 0, minutes: 0, seconds: 0, diff };
  }
};

export default calcTimeLeft;
