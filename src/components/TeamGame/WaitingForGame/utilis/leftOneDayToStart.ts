const leftOneDayToStart = (days: number, valueOne: any, valueTwo: any) => {
  if (days > 0) {
    return valueOne;
  }
  return valueTwo;
};
export default leftOneDayToStart;
