const waitingForStartDate = (timeLeft: any) => {
  return timeLeft > 0;
};

export default waitingForStartDate;
