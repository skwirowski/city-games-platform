const isTimeForGame = (
  timeDiff: number,
  dateNow: number,
  finishDate: number
) => {
  return timeDiff < 0 && dateNow < finishDate;
};
export default isTimeForGame;
