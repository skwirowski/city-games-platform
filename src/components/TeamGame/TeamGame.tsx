import React from "react";
import { Grid } from "@material-ui/core";
import WaitingForGameContainer from "./WaitingForGame/WaitingForGameContainer";
import NavbarContainer from "../TeamNavBar/NavBarContainer";

const TeamGame: React.FC = () => {
  return (
    <Grid container direction="column">
      <NavbarContainer />
      <WaitingForGameContainer />
    </Grid>
  );
};

export default TeamGame;
