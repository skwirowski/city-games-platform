import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import { styles } from "./AboutGameHeaderStyles";
import { FormattedMessage } from "react-intl";
import ArrowBack from "../../../shared/ArrowBack";

interface Props extends WithStyles<typeof styles> {
  handleGoBack: any;
}

const AboutGameHeader: React.SFC<Props> = ({ classes, handleGoBack }) => {
  return (
    <Grid container className={classes.primaryContainer}>
      <Grid container direction="row" className={classes.arrowContainer}>
        <Grid onClick={e => handleGoBack(e)}>
          <ArrowBack />
        </Grid>
        <Typography className={classes.navigationText} variant="h6">
          O grze
        </Typography>
      </Grid>
      <Grid item xs={12} className={classes.textContainer}>
        <Typography className={classes.typoTitleText}>
          <FormattedMessage id="LandingPage.titleFirstLine" />
        </Typography>
        <Grid className={classes.underlineContainer}>
          <Typography className={classes.typoTitleText}>
            <FormattedMessage id="LandingPage.titleSecondLine" />
          </Typography>
          <Grid className={classes.underline} />
        </Grid>
        <Typography className={classes.locationCityText}>Wrocław</Typography>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(AboutGameHeader);
