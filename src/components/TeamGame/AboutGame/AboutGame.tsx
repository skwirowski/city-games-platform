import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import CalendarToday from "@material-ui/icons/CalendarTodayOutlined";
import Timer from "@material-ui/icons/Timer";
import LocationOn from "@material-ui/icons/LocationOnOutlined";
import * as colors from "../../../shared/colors";
import moment from "moment";

const styles = createStyles({
  primaryContainer: {
    padding: "35px"
  },
  iconContainer: {
    marginTop: "1.5em"
  },
  iconText: {
    color: "gray",
    fontSize: "18px",
    paddingLeft: "1.2em",
    lineHeight: "2em"
  },
  desc: {
    fontSize: "18px"
  },
  icon: {
    fontSize: "35px",
    color: colors.secondary
  }
});
interface Props extends WithStyles<typeof styles> {
  description: any;
  startDate: string;
  duration: string;
  startPlace: string;
}

const AboutGame: React.SFC<Props> = ({
  classes,
  description,
  startDate,
  duration,
  startPlace
}) => {
  const formatDate = "DD-MM-YYYY, HH:mm";
  return (
    <Grid container className={classes.primaryContainer}>
      <Grid>
        <Typography
          className={classes.desc}
          dangerouslySetInnerHTML={{ __html: description }}
        />
      </Grid>
      <Grid>
        <Grid container direction="row" className={classes.iconContainer}>
          <CalendarToday className={classes.icon} />
          <Typography className={classes.iconText}>
            {moment(startDate).format(formatDate)}
          </Typography>
        </Grid>
        <Grid container direction="row" className={classes.iconContainer}>
          <Timer className={classes.icon} />
          <Typography className={classes.iconText}>{duration}</Typography>
        </Grid>
        <Grid container direction="row" className={classes.iconContainer}>
          <LocationOn className={classes.icon} />
          <Typography className={classes.iconText}>{startPlace}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(AboutGame);
