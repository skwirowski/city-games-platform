import React, { useEffect, useState } from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Grid,
  Typography
} from "@material-ui/core";
import AboutGameHeader from "./AboutGameHeader";
import AboutGame from "./AboutGame";
import http from '../../../services/gameDefinitionApi';
import CircularProgress from "@material-ui/core/CircularProgress";
import * as colors from "../../../shared/colors";
import canRenderContent from "./utilis/canRenderContent";

const styles = createStyles({
  loader: {
    position: "absolute",
    top: "40%",
    left: "45%",
    color: colors.secondary
  },
  errorContainer: {
    marginTop: "40vh"
  }
});

interface Props extends WithStyles<typeof styles> {
  history: any;
}

export interface GameDefinitionProps {
  guid: string;
  logo: string;
  name: string;
  startPlacePoint: string;
  description: string;
  termsOfUse: { content: string };
  startDate: string;
  finishDate: string;
  minimumTeamMemberCount: number;
  maximumTeamMemberCount: number;
  minimumTeamCount: number;
  maximumTeamCount: number;
  maximumRouteDuration: string;
  timeBetweenTeams: string;
  isQrCodeConfirmationSelected: boolean;
  isQrCodeGenerated: boolean;
  isActive: boolean;
}

const AboutGameContainer: React.FC<Props> = ({ classes, history }) => {
  const [gameDefinition, setGameDefinition] = useState();
  const [error, setError] = useState();
  const handleGoBack = (e: any) => {
    e.preventDefault();
    history.goBack();
  };

  useEffect(() => {
    const fetchGameInformation = async () => {
      try {
        const response = await http.get("/api/GameDefinition");
        const game = response.data.find((definition: any) => definition.isActive === true);
        setGameDefinition(game);
      } catch {
        setError(error.message);
      }
    };
    fetchGameInformation();
  }, []);

  return (
    <Grid container direction="column">
      {canRenderContent(error, gameDefinition) ? (
        <Grid>
          <AboutGameHeader handleGoBack={handleGoBack} />
          <AboutGame
            description={gameDefinition.description}
            startDate={gameDefinition.startDate}
            duration={gameDefinition.maximumRouteDuration}
            startPlace={gameDefinition.startPlacePoint}
          />
        </Grid>
      ) : (
        <Grid>
          {!error ? (
            <CircularProgress className={classes.loader} />
          ) : (
            <Grid container justify="center" className={classes.errorContainer}>
              <Typography variant="h6">{error}</Typography>
            </Grid>
          )}
        </Grid>
      )}
      <Grid />
    </Grid>
  );
};

export default withStyles(styles)(AboutGameContainer);
