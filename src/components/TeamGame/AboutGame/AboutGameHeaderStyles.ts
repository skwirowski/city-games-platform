import { createStyles } from "@material-ui/core";
import BasicBackground from "../../../assets/images/bg.jpg";

export const styles = createStyles({
  primaryContainer: {
    width: "100%",
    height: "310px",
    background: `url(${BasicBackground}) no-repeat top fixed`,
    backgroundSize: "cover",
    position: "relative",
    borderBottomLeftRadius: "50px",
    borderBottomRightRadius: "50px"
  },
  navigationText: {
    color: "rgb(0, 143, 211)",
    fontWeight: "bold",
    fontSize: "2em",
    lineHeight: "1.3em",
    position: "absolute",
    right: "40%"
  },
  textContainer: {
    position: "absolute",
    top: "45%",
    left: "17%",
    transform: "translate(-15%, -25%)"
  },
  arrowContainer: {
    paddingTop: "20px"
  },
  typoTitleText: {
    fontFamily: "Maison Neue",
    fontWeight: "bold",
    color: "white",
    fontSize: "3em",
    lineHeight: "1em",
    letterSpacing: "1px"
  },
  underlineContainer: {
    width: "fit-content",
    position: "relative"
  },
  underline: {
    backgroundColor: "rgb(0, 143, 211)",
    height: "1em",
    zIndex: -1,
    position: "inherit",
    width: "106%",
    left: "-3%",
    bottom: "1.3em"
  },
  locationCityText: {
    color: "white",
    fontSize: "3.5em",
    fontWeight: "lighter"
  }
});
