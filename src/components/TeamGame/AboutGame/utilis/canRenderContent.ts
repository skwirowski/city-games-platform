const canRenderContent = (error: any, game: any) => {
  return !error && game;
};
export default canRenderContent;
