import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import TeamGame from "../TeamGame";

describe("TeamGame component", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedTeamGame = unwrap(TeamGame);
const createSut = () => shallow(<UnwrappedTeamGame />);
