import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import TimeComponent from "../../WaitingForGame/TimeComponent";
import { Typography } from "@material-ui/core/";

describe("TimeComponent component", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });

  describe("Render received props", () => {
    it("Should render received number props ", () => {
      const sut = createSut(5, "dni");
      expect(
        sut
          .find(Typography)
          .children()
          .text()
      ).toEqual("5");
    });
  });
});
const Unwrapped = unwrap(TimeComponent);
const createSut = (number, text) =>
  shallow(<Unwrapped classes={{}} number={number} text={text} />);
