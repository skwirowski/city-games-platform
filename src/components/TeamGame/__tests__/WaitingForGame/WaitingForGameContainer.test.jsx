import React from "react";
import { shallow } from "enzyme";
import WaitingForGameContainer from "../../WaitingForGame/WaitingForGameContainer";

describe("WaitingForGameContainer component", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });
});

const createSut = () => shallow(<WaitingForGameContainer classes={{}} />);
