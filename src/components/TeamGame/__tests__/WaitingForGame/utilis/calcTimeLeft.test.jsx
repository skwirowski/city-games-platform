import calcTimeLeft from "../../../WaitingForGame/utilis/calcTimeLeft";

describe("CalcTimeLeft function", () => {
  describe("Difference between start and end date", () => {
    it("Should return object with 0, 0, 0 values if given future date is older than alctual", () => {
      const actual = calcTimeLeft(71321442143, 81321442143);

      const expected = {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
        diff: -10000000000
      };

      expect(actual).toEqual(expected);
    });

    it("Should return object with 0 days if difference between dates if lower than 24 hours", () => {
      const givenFutureDate = new Date(2019, 6, 27, 12, 10, 10, 0).getTime();
      const givenActualDate = new Date(2019, 6, 26, 12, 15, 10, 0).getTime();
      const actual = calcTimeLeft(givenFutureDate, givenActualDate);

      const expected = {
        days: 0,
        hours: 23,
        minutes: 55,
        seconds: 0,
        diff: 86100000
      };

      expect(actual).toEqual(expected);
    });

    it("Should return object with 0 days and hours if difference between dates if lower than 60 minutes", () => {
      const givenFutureDate = new Date(2019, 6, 27, 12, 15, 0, 0).getTime();
      const givenActualDate = new Date(2019, 6, 27, 12, 10, 0, 0).getTime();
      const actual = calcTimeLeft(givenFutureDate, givenActualDate);

      const expected = {
        days: 0,
        hours: 0,
        minutes: 5,
        seconds: 0,
        diff: 300000
      };

      expect(actual).toEqual(expected);
    });

    it("Should return object with 0 days, 0 hours, 0 minutes, 0 seconds, 0 diff if given dates are the same", () => {
      const givenFutureDate = new Date(2019, 6, 27, 12, 15, 0, 0).getTime();
      const givenActualDate = new Date(2019, 6, 27, 12, 15, 0, 0).getTime();
      const actual = calcTimeLeft(givenFutureDate, givenActualDate);

      const expected = { days: 0, hours: 0, minutes: 0, seconds: 0, diff: 0 };

      expect(actual).toEqual(expected);
    });
  });
});
