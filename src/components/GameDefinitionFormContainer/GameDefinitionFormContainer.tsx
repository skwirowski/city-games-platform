import React, { useState } from "react";
import createGame from "../../services/createGame";
import GameDefinitionForm, { FormValues } from "./GameDefinitionForm/GameDefinitionForm";
import notifications from "../../static/notifications";
const GameDefinitionFormContainer = () => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (values: FormValues) => {
    try {
      setLoading(true);
      await createGame(values);
      notifications.gamedefinition.gameCreatedSuccess();
      setLoading(false);
    } catch (error) {
      notifications.gamedefinition.gameCreatedError();
      setLoading(false);
    }
    return null;
  };

  return <GameDefinitionForm submit={handleSubmit} loading={loading} />;
};

export default GameDefinitionFormContainer;
