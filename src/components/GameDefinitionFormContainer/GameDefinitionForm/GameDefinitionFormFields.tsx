import React from "react";
import { Field, FieldProps, FormikValues } from "formik";
import FLabeledInput from "../../../shared/FormikComponents/FLabeledInput";
import ErrorMessage from "../../../shared/FormikComponents/ErrorMessage";
import FDateTimePicker from "../../../shared/FormikComponents/FDateTimePicker";
import FFileInput from "../../../shared/FormikComponents/FFileInput/FFileInput";
import FTextEditor from "../../../shared/FormikComponents/FTextEditor";
import { init } from "./TextEditorInit";

const GameDefinitionFormFields: React.FC<FormikValues> = ({
  touched,
  errors,
  classes,
  intl
}) => {
  return (
    <>
      <div className={classes.textField}>
        <Field name="logo">
          {(props: FieldProps) => (
            <FFileInput
              name="logo"
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.logo"
              })}
            />
          )}
        </Field>
        <ErrorMessage error={errors.logo} touched={touched.logo} />
      </div>
      <div className={classes.textField}>
        <Field name="name">
          {(props: FieldProps) => (
            <FLabeledInput
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.name"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage error={errors.name} touched={touched.name} />
      </div>
      <div className={classes.textField}>
        <div className={classes.descriptionLabel}>
          {intl.formatMessage({ id: "GameDefinitionFormFields.description" })}
        </div>
        <Field name="description">
          {(props: FieldProps) => (
            <FTextEditor fieldProps={props} init={init} name="description" />
          )}
        </Field>
        <ErrorMessage
          error={errors.description}
          touched={touched.description}
        />
      </div>
      <div className={classes.textField}>
        <Field name="startPlacePoint">
          {(props: FieldProps) => (
            <FLabeledInput
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.startPlacePoint"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage
          error={errors.startPlacePoint}
          touched={touched.startPlacePoint}
        />
      </div>

      <div className={classes.textField}>
        <Field name="termsOfUse.content">
          {(props: FieldProps) => (
            <FLabeledInput
              fieldProps={props}
              multiline
              rows={2}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.termsOfUse"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage
          error={errors.termsOfUse ? errors.termsOfUse.content : ""}
          touched={touched.termsOfUse ? touched.termsOfUse.content : false}
        />
      </div>
      <div className={classes.textField}>
        <Field name="startDate">
          {(props: FieldProps) => (
            <FDateTimePicker
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.startDate"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage error={errors.startDate} touched={touched.startDate} />
      </div>
      <div className={classes.textField}>
        <Field name="finishDate">
          {(props: FieldProps) => (
            <FDateTimePicker
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.finishDate"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage error={errors.finishDate} touched={touched.finishDate} />
      </div>
      <div className={classes.textField}>
        <Field name="registrationFinishDate">
          {(props: FieldProps) => (
            <FDateTimePicker
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.teamRegistrationDeadline"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage
          error={errors.registrationFinishDate}
          touched={touched.registrationFinishDate}
        />
      </div>
      <div className={classes.coords}>
        <div className={classes.textField}>
          <Field name="minimumTeamCount">
            {(props: FieldProps) => (
              <FLabeledInput
                fieldProps={props}
                label={intl.formatMessage({
                  id: "GameDefinitionFormFields.minimumTeamCount"
                })}
                className={classes.formInput}
              />
            )}
          </Field>
          <ErrorMessage
            error={errors.minimumTeamCount}
            touched={touched.minimumTeamCount}
          />
        </div>
        <div className={classes.textField}>
          <Field name="maximumTeamCount">
            {(props: FieldProps) => (
              <FLabeledInput
                fieldProps={props}
                label={intl.formatMessage({
                  id: "GameDefinitionFormFields.maximumTeamCount"
                })}
                className={classes.formInput}
              />
            )}
          </Field>
          <ErrorMessage
            error={errors.maximumTeamCount}
            touched={touched.maximumTeamCount}
          />
        </div>
      </div>
      <div className={classes.coords}>
        <div className={classes.textField}>
          <Field name="minimumTeamMemberCount">
            {(props: FieldProps) => (
              <FLabeledInput
                fieldProps={props}
                label={intl.formatMessage({
                  id: "GameDefinitionFormFields.minimumTeamMemberCount"
                })}
                className={classes.formInput}
              />
            )}
          </Field>
          <ErrorMessage
            error={errors.minimumTeamMemberCount}
            touched={touched.minimumTeamMemberCount}
          />
        </div>
        <div className={classes.textField}>
          <Field name="maximumTeamMemberCount">
            {(props: FieldProps) => (
              <FLabeledInput
                fieldProps={props}
                label={intl.formatMessage({
                  id: "GameDefinitionFormFields.maximumTeamMemberCount"
                })}
                className={classes.formInput}
              />
            )}
          </Field>
          <ErrorMessage
            error={errors.maximumTeamMemberCount}
            touched={touched.maximumTeamMemberCount}
          />
        </div>
      </div>
      <div className={classes.textField}>
        <Field name="maximumRouteDuration">
          {(props: FieldProps) => (
            <FLabeledInput
              fieldProps={props}
              mask={true}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.maximumRouteDuration"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage
          error={errors.maximumRouteDuration}
          touched={touched.maximumRouteDuration}
        />
      </div>
      <div className={classes.textField}>
        <Field name="timeBetweenTeams">
          {(props: FieldProps) => (
            <FLabeledInput
              mask={true}
              fieldProps={props}
              label={intl.formatMessage({
                id: "GameDefinitionFormFields.timeBetweenTeams"
              })}
              className={classes.formInput}
            />
          )}
        </Field>
        <ErrorMessage
          error={errors.timeBetweenTeams}
          touched={touched.timeBetweenTeams}
        />
      </div>
    </>
  );
};

export default GameDefinitionFormFields;
