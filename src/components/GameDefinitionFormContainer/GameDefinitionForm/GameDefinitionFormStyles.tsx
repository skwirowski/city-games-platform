import { createStyles } from "@material-ui/core";
import styledScroll from "../../styles/styledScroll";
export const styles = createStyles({
  container: {
    margin: "1rem",
    padding: "2rem",
    width: "75%",
    overflowY: "scroll",
    "@media (max-width:700px)": {
      width: "85%"
    }
  },
  styledScroll: styledScroll,
  form: {
    display: "flex",
    flexDirection: "column"
  },
  formInput: {
    width: "100%",
    margin: "0.2rem"
  },
  textField: {
    width: "100%",
    margin: "0.2rem"
  },
  coords: {
    display: "flex"
  },
  errorMessage: {
    fontSize: "0.8rem",
    color: "red"
  },
  addButton: {
    alignSelf: "center",
    marginTop: "0.8rem",
    height: "2.5rem",
    backgroundColor: "#1890ff",
    "&:hover": {
      backgroundColor: "#40a9ff"
    }
  },
  confirmationOfArrivalLabel: {
    marginTop: "1rem"
  },
  descriptionLabel: {
    fontSize: "1rem",
    color: "#0000008a",
    lineHeight: 2,
    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
    marginTop: "16px"
  }
});
