import React from "react";
import { WithStyles, withStyles } from "@material-ui/core";
import { withFormik, Form, FormikErrors, FormikProps } from "formik";
import * as Yup from "yup";
import Paper from "@material-ui/core/Paper";
import SubmitButton from "../../../shared/SubmitButton";
import { styles } from "./GameDefinitionFormStyles";
import { getUserData } from "../../../services/auth";
import GameDefinitionFormFields from "./GameDefinitionFormFields";
import { injectIntl, InjectedIntlProps } from "react-intl";
import Intl from "../../../utils/Intl";
import classnames from "classnames";

interface Props extends WithStyles<typeof styles> {
  submit: (values: FormValues) => Promise<FormikErrors<FormValues> | null>;
  loading: boolean;
}

export interface FormValues {
  name: string;
  startPlacePoint: string;
  description: string;
  startDate: string | Date;
  finishDate: string | Date;
  registrationFinishDate: string | Date;
  creatorGuid: string;
  termsOfUse: { content: string };
  logo: string;
  minimumTeamCount: number;
  maximumTeamCount: number;
  minimumTeamMemberCount: number;
  maximumTeamMemberCount: number;
  maximumRouteDuration: string;
  timeBetweenTeams: string;
}
interface ILoading {
  loading: boolean;
}

const GameDefinitionForm: React.FC<
  FormikProps<FormValues> & Props & InjectedIntlProps
> = ({ touched, errors, classes, loading, intl }) => {
  return (
    <Paper className={classnames(classes.container, classes.styledScroll)}>
      <Form className={classes.form}>
        <GameDefinitionFormFields
          touched={touched}
          errors={errors}
          classes={classes}
          intl={intl}
        />
        <SubmitButton
          disabled={loading}
          className={classes.addButton}
          type="submit"
          children={Intl.messages["GameDefinitionForm.gameDefinitionBtnText"]}
        />
      </Form>
    </Paper>
  );
};

const timeRegEx = /^(?:(?:([0-9]?\d|2[0-9]):)?([0-5]?\d):)?([0-5]?\d)$/;

const startDate = new Date();
const finishDate = new Date();
const registrationFinishDate = new Date();
const hourDelayBetweenStartAndFinish = 4;
const hourDelayBetweenRegistrationFinishDate = 5;
finishDate.setHours(startDate.getHours() + hourDelayBetweenStartAndFinish);
registrationFinishDate.setHours(
  startDate.getHours() - hourDelayBetweenRegistrationFinishDate
);

const mapPropsToValues = () => ({
  name: "",
  startPlacePoint: "",
  description: "",
  startDate: startDate,
  finishDate: finishDate,
  registrationFinishDate: registrationFinishDate,
  creatorGuid: getUserData().uuid,
  termsOfUse: { content: "" },
  logo: "",
  minimumTeamCount: 1,
  maximumTeamCount: 1,
  minimumTeamMemberCount: 1,
  maximumTeamMemberCount: 1,
  maximumRouteDuration: "01:00:00",
  timeBetweenTeams: "00:05:00"
});

const validationSchema = () =>
  Yup.object().shape({
    logo: Yup.mixed(),
    name: Yup.string().required(
      Intl.messages["GameDefinitionForm.nameRequiredErrorMessage"]
    ),
    startPlacePoint: Yup.string().required(
      Intl.messages["GameDefinitionForm.startPlacePointRequiredErrorMessage"]
    ),
    description: Yup.string(),
    startDate: Yup.date()
      .required()
      .typeError(Intl.messages["GameDefinitionForm.invalidDateErrorMessage"]),
    finishDate: Yup.date()
      .required()
      .test(
        "startDate-match",
        Intl.messages["GameDefinitionForm.finishDateEqualOrLower"],
        function(value) {
          const date = this.parent.startDate;
          return date.getTime() < value.getTime();
        }
      )
      .typeError(Intl.messages["GameDefinitionForm.invalidDateErrorMessage"]),
    registrationFinishDate: Yup.date()
      .required()
      .test(
        "registrationFinishDate-match",
        Intl.messages["GameDefinitionForm.deadlineDateEqualOrHigher"],
        function(value) {
          const date = this.parent.startDate;
          return date.getTime() > value.getTime();
        }
      )
      .typeError(Intl.messages["GameDefinitionForm.invalidDateErrorMessage"]),
    termsOfUse: Yup.object().shape({
      content: Yup.string().required(
        Intl.messages["GameDefinitionForm.termsOfUseRequiredErrorMessage"]
      )
    }),
    minimumTeamCount: Yup.number()
      .min(1, Intl.messages["GameDefinitionForm.teamMinErrorMessage"])
      .required(Intl.messages["GameDefinitionForm.teamMinRequiredErrorMessage"])
      .typeError(Intl.messages["GameDefinitionForm.invalidNumberErrorMessage"]),
    maximumTeamCount: Yup.number()
      .required(Intl.messages["GameDefinitionForm.teamMaxRequiredErrorMessage"])
      .test(
        "minTeam-match",
        Intl.messages["GameDefinitionForm.MaxLowerThanMinErrorMessage"],
        function(value) {
          const minTeam = this.parent.minimumTeamCount;
          return minTeam <= value;
        }
      )
      .typeError(Intl.messages["GameDefinitionForm.invalidNumberErrorMessage"]),
    minimumTeamMemberCount: Yup.number()
      .min(1, Intl.messages["GameDefinitionForm.memberMinErrorMessage"])
      .required(Intl.messages["GameDefinitionForm.memberRequiredErrorMessage"])
      .typeError(Intl.messages["GameDefinitionForm.invalidNumberErrorMessage"]),
    maximumTeamMemberCount: Yup.number()
      .required(Intl.messages["GameDefinitionForm.memberRequiredErrorMessage"])
      .test(
        "minMember-match",
        Intl.messages["GameDefinitionForm.MaxLowerThanMinErrorMessage"],
        function(value) {
          const minMember = this.parent.minimumTeamMemberCount;
          return minMember <= value;
        }
      )
      .typeError(Intl.messages["GameDefinitionForm.invalidNumberErrorMessage"]),
    maximumRouteDuration: Yup.string()
      .required(
        Intl.messages[
          "GameDefinitionForm.maximumRouteDurationRequiredErrorMessage"
        ]
      )
      .matches(
        timeRegEx,
        Intl.messages["GameDefinitionForm.invalidTimeFormatErrorMessage"]
      ),
    timeBetweenTeams: Yup.string()
      .required(
        Intl.messages["GameDefinitionForm.timeBetweenTeamsRequiredErrorMessage"]
      )
      .matches(
        timeRegEx,
        Intl.messages["GameDefinitionForm.invalidTimeFormatErrorMessage"]
      )
  });

export default withStyles(styles)(
  withFormik<Props, FormValues>({
    mapPropsToValues: mapPropsToValues,
    validationSchema: validationSchema,
    handleSubmit: async (values, { props, setErrors }) => {
      const errors = await props.submit(values);
      if (errors) {
        setErrors(errors);
      }
    }
  })(injectIntl(GameDefinitionForm))
);
