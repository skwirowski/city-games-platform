export const init = {
  width: "100%",
  minHeight: "150px",
  menubar: false,
  resize: true,
  plugins: "link image code",
  toolbar:
    "undo redo | bold italic | alignleft aligncenter alignright | link | forecolor backcolor | styleselect | code"
};
