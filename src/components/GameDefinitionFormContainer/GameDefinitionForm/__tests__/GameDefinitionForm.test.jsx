import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import GameDefinitionForm from "../GameDefinitionForm";

describe("GameDefinitionForm", () => {
  let wrapper;
  beforeEach(() => {
    const Unwrapped = unwrap(<GameDefinitionForm submit={async () => null} />);
    wrapper = shallow(Unwrapped);
  });
  it("should render without crash", () => {
    expect(wrapper.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
