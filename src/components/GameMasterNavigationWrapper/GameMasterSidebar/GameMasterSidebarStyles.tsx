import { createStyles } from "@material-ui/core";
export const styles = createStyles({
  SidebarContainer: {
    height: "100vh"
  },
  RightColumn: {
    width: "100%",
    height: "100%",
    overflowY: "hidden",
    overflowX: "hidden"
  },
  ToggleButton: {
    position: "absolute",
    zIndex: 1999,
    right: -60,
    top: 15,
    marginBottom: "16px"
  },
  NormalLogo: {
    width: "100%",
    padding: "24px"
  },
  Logo: {
    height: "81px"
  },
  SmallLogo: {
    width: "100%",
    display: "flex",
    padding: "24px 0",
    justifyContent: "center"
  }
});
