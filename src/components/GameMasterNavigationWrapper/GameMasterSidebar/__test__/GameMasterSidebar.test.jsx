import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import GameMasterSidebar from '../GameMasterSidebar';

describe('GameMasterSidebar', () => {
  let wrapper;
  beforeEach(() => {
    const Unwrapped = unwrap(<GameMasterSidebar />);
    wrapper = shallow(Unwrapped);
  });
  it('should render without crash', () => {
    const Unwrapped = unwrap(<GameMasterSidebar />);
    wrapper = shallow(Unwrapped);
    expect(wrapper).toBeTruthy();
  });

  it('should match snapshot', () => {
    const Unwrapped = unwrap(<GameMasterSidebar />);
    wrapper = shallow(Unwrapped);
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
