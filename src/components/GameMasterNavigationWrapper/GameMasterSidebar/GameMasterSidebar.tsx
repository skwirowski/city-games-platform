import React from "react";
import { Layout, Menu, Icon, Button } from "antd";
import { Link, withRouter } from "react-router-dom";
import { WithStyles, withStyles } from "@material-ui/core";
import { ReactComponent as SmallLogoSVG } from "../../../assets/images/logoWhiteSmall.svg";
import { ReactComponent as NormalLogoSVG } from "../../../assets/images/logoWhiteNormal.svg";
import classnames from "classnames";
import { styles } from "./GameMasterSidebarStyles";
import { routes } from "../../../static/routesUrl";
import Intl from "../../../utils/Intl";

interface Props extends WithStyles<typeof styles> {
  collapsed: boolean;
  toggleCollapsed: () => void;
  history: any;
  location: any;
  match: any;
}

const GameMasterSidebar: React.FC<Props> = ({
  classes,
  collapsed,
  location,
  toggleCollapsed
}) => {
  const {
    SmallLogo,
    ToggleButton,
    NormalLogo,
    Logo,
    SidebarContainer
  } = classes;
  const {
    checkpointList,
    checkpointCreate,
    createGame,
    createRoute,
    createQuest,
    gameDefinitionsList,
    teamsList
  } = routes;

  const currentPath = location ? location.pathname : createGame;

  return (
    <Layout.Sider
      id="Sider"
      collapsible
      collapsed={collapsed}
      className={SidebarContainer}
    >
      <Button className={ToggleButton} type="primary" onClick={toggleCollapsed}>
        <Icon type={collapsed ? "menu-unfold" : "menu-fold"} />
      </Button>
      {collapsed ? (
        <div id="SmallLogo" className={classnames(SmallLogo, Logo)}>
          <SmallLogoSVG />
        </div>
      ) : (
        <div id="NormalLogo" className={classnames(NormalLogo, Logo)}>
          <NormalLogoSVG />
        </div>
      )}
      <Menu
        mode="inline"
        theme="dark"
        defaultSelectedKeys={[createGame]}
        selectedKeys={[currentPath]}
      >
        <Menu.Item key={createGame}>
          <Link to={createGame} />
          <Icon type="play-circle" />
          <span>{Intl.messages["GameMasterSidebar.createGame"]}</span>
        </Menu.Item>
        <Menu.Item key={gameDefinitionsList}>
          <Link to={gameDefinitionsList} />
          <Icon type="table" />
          <span>{Intl.messages["GameMasterSidebar.gamesList"]}</span>
        </Menu.Item>
        <Menu.Item key={checkpointCreate}>
          <Link to={checkpointCreate} />
          <Icon type="environment" />
          <span>{Intl.messages["GameMasterSidebar.createCheckpoint"]}</span>
        </Menu.Item>
        <Menu.Item key={checkpointList}>
          <Link to={checkpointList} />
          <Icon type="unordered-list" />
          <span>{Intl.messages["GameMasterSidebar.checkpointList"]}</span>
        </Menu.Item>
        <Menu.Item key={createRoute}>
          <Link to={createRoute} />
          <Icon type="flag" />
          <span>{Intl.messages["GameMasterSidebar.createRoute"]}</span>
        </Menu.Item>
        <Menu.Item key={createQuest}>
          <Link to={createQuest} />
          <Icon type="experiment" />
          <span>{Intl.messages["GameMasterSidebar.createQuest"]}</span>
        </Menu.Item>
        <Menu.Item key={teamsList}>
          <Link to={teamsList} />
          <Icon type="unordered-list" />
          <span>{Intl.messages["GameMasterSidebar.teamsList"]}</span>
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  );
};

export default withStyles(styles)(withRouter(GameMasterSidebar));
