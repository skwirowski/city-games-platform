import { createStyles } from "@material-ui/core";
export const styles = createStyles({
  header: {
    display: "flex"
  },
  Menu: {
    borderStyle: "none"
  },
  MenuItem: {
    borderStyle: "none"
  },
  Right: {
    display: "flex !important",
    alignItems: "center",
    marginLeft: "auto",
    justifyContent: "flex-end"
  },
  email: {
    marginLeft: "6px",
    color: "white"
  },
  antDropdown: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    marginRight: "24px"
  },
  languages: {
    display: "flex",
    padding: "0.5rem 1rem"
  },
  flag: {
    width: "2rem",
    marginLeft: "0.1rem",
    padding: "0",
    height: "1rem",
    cursor: "pointer"
  },
  logoutButton: {
    marginLeft: "20px"
  }
});
