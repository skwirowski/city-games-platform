import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import GameMasterHeader from '../GameMasterHeader';

describe('GameMasterHeader', () => {
  let wrapper;

  beforeEach(() => {
    const Unwrapped = unwrap(<GameMasterHeader />);
    wrapper = shallow(Unwrapped);
  });
  it('should render without crash', () => {
    expect(wrapper).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
