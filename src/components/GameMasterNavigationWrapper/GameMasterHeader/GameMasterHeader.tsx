import React from "react";
import { Icon, Layout, Button } from "antd";
import { logout } from "../../../services/auth";
import { WithStyles, withStyles } from "@material-ui/core";
import { styles } from "./GameMasterHeaderStyles";

interface Props extends WithStyles<typeof styles> {
  setLanguage: (language: string) => void;
}
const Header: React.SFC<Props> = ({ classes, setLanguage }) => {
  const { Right, flag, logoutButton, header } = classes;

  return (
    <Layout.Header className={header}>
      <div className={Right}>
        <img
          src="/images/pl.svg"
          alt="Switch to polish language"
          onClick={() => setLanguage("pl")}
          className={`${flag} plFlag`}
        />
        <img
          src="/images/gb.svg"
          alt="Switch to english language"
          onClick={() => setLanguage("en")}
          className={`${flag} enFlag`}
        />
        <Button className={logoutButton} onClick={logout} size="default" type="primary">
          Logout <Icon type="logout" />
        </Button>
      </div>
    </Layout.Header>
  );
};

export default withStyles(styles)(Header);
