import React from "react";
import { Layout } from "antd";
import useToggle from "../../hooks/useToggle";
import GameMasterSidebar from "./GameMasterSidebar/GameMasterSidebar";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import GameMasterHeader from "./GameMasterHeader/GameMasterHeader";
import "antd/dist/antd.css";

const styles = createStyles({
  Container: {
    width: "100vw",
    height: "100vh",
    overflow: "hidden"
  },
  RightColumn: {
    width: "100%",
    height: "100%",
    overflowY: "auto",
    overflowX: "hidden"
  },
  Content: {
    display: "flex",
    justifyContent: "space-around",
    height: "calc(100vh - 64px)"
  }
});
interface Props extends WithStyles<typeof styles> {
  setLanguage: (language: string) => void;
  children: JSX.Element | Array<JSX.Element>;
}

const GameMasterNavigationWrapper: React.SFC<Props> = ({ classes, children, setLanguage }) => {
  const [collapsed, toggleCollapsed] = useToggle(false);
  const { Container, Content, RightColumn } = classes;
  return (
    <Layout className={Container}>
      <GameMasterSidebar collapsed={collapsed} toggleCollapsed={toggleCollapsed} />
      <div className={RightColumn}>
        <GameMasterHeader setLanguage={setLanguage} />
        <div className={Content}>{children}</div>
      </div>
    </Layout>
  );
};

export default withStyles(styles)(GameMasterNavigationWrapper);
