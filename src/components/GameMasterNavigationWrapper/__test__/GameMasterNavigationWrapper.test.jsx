import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils';
import GameMasterNavigationWrapper from '../GameMasterNavigationWrapper';

describe('GameMasterNavigationWrapper', () => {
  let wrapper;

  beforeEach(() => {
    const Unwrapped = unwrap(<GameMasterNavigationWrapper />);
    wrapper = shallow(Unwrapped);
  });
  it('should render without crash', () => {
    expect(wrapper).toBeTruthy();
  });

  it('should match snapshot', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
