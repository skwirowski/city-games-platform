import { createStyles } from "@material-ui/core";
import bg from "../../assets/images/bg.jpg";

export const styles = createStyles({
  landingPage: {
    width: "100%",
    height: "100%",
    background: `url(${bg}) no-repeat top fixed`,
    backgroundSize: "cover",
    position: "relative"
  },
  logo: {
    position: "absolute",
    top: "1em",
    right: "1em",
    height: "2em"
  },
  textContainer: {
    position: "absolute",
    top: "25%",
    left: "15%",
    transform: "translate(-15%, -25%)"
  },
  titleText: {
    color: "white",
    fontSize: "3em",
    lineHeight: "1em",
    letterSpacing: "1px"
  },
  loactionText: {
    color: "white",
    fontSize: "3.5em",
    fontWeight: "lighter"
  },
  underlineContainer: {
    width: "fit-content",
    position: "relative"
  },
  underline: {
    backgroundColor: "rgb(0, 143, 211)",
    height: "1em",
    zIndex: -1,
    position: "inherit",
    width: "106%",
    left: "-3%",
    bottom: "1.1em"
  },
  buttonContainer: {
    position: "absolute",
    bottom: "4em",
    left: "50%",
    transform: "translate(-50%, 0)"
  }
});
