import React from "react";
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from "enzyme";
import LandingPage from "../LandingPage";

describe("LandingPage", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedLandingPage = unwrap(LandingPage);
const createSut = props =>
  shallow(<UnwrappedLandingPage classes={{}} {...props} />);
