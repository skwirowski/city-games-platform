import React from "react";
import { WithStyles, withStyles, Grid, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { routes } from "../../static/routesUrl";
import Link from "../../shared/LinkWithoutDefaultStyles";
import Button from "../../shared/Button";
import { styles } from "./LandingPageStyles";
import logo from "../../assets/images/logo.svg";

interface Props extends WithStyles<typeof styles> {}

const LandingPage: React.SFC<Props> = ({ classes }) => {
  return (
    <Grid container className={classes.landingPage}>
      <Grid item xs={12}>
        <img src={logo} alt="intive" className={classes.logo} />
      </Grid>
      <Grid item xs={12} className={classes.textContainer}>
        <Typography className={classes.titleText}>
          <FormattedMessage id="LandingPage.titleFirstLine" />
        </Typography>
        <div className={classes.underlineContainer}>
          <Typography className={classes.titleText}>
            <FormattedMessage id="LandingPage.titleSecondLine" />
          </Typography>
          <div className={classes.underline} />
        </div>
        <Typography className={classes.loactionText}>Wrocław</Typography>
      </Grid>
      <Grid item xs={12} className={classes.buttonContainer}>
        <Link to={routes.authenticated}>
          <Button color="primary">Start</Button>
        </Link>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(LandingPage);
