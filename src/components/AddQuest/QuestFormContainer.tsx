import React, { useEffect } from "react";
import { Formik } from "formik";
import AddQuest from "./AddQuest";
import * as Yup from "yup";
import api from "../../services/questServiceApi";
import notifications from "../../static/notifications";
let initialArray = {
  name: "",
  content: "",
  questionType: "OPEN_QUESTION",
  answerOptions: [],
  maxPoints: 0,
  timer: false,
  attachedImage: ""
};
const questNameMinErrorMessage = "To pole nie moze być puste";
const questNameMaxErrorMessage = "Maksymalna ilośc znaków dla tego pola to: 255";
const maxPointsRequiredErrorMessage = "To pole nie moze być puste";
const questDescriptionMaxErrorMessage = "Maksymalna ilośc znaków dla tego pola to: 50";
const questDescriptionMinErrorMessage = "Minimalna ilośc znaków dla tego pola to: 10";
const questDescriptionRequiredMessage = "To pole nie moze być puste";

const setValidationToQuestionType = (value: any) => {
  switch (value) {
    case "OPEN_QUESTION":
      return openQuestionValidation;
      break;
    case "SINGLE_CHOICE_QUESTION":
      return singleChoiceQuestionValidation;
      break;
    case "MULTIPLE_CHOICE_QUESTION":
      return multipleChoiceQuestionValidation;
      break;
    default:
      return openQuestionValidation;
  }
};

const openQuestionValidation = Yup.array()
  .of(
    Yup.object().shape({
      content: Yup.string()
        .required("Required")
        .min(5, "Za mało znaków")
    })
  )
  .required("Brak odpowiedzi")
  .min(1, "Za mało odpowiedzi");

const singleChoiceQuestionValidation = Yup.array()
  .of(
    Yup.object().shape({
      content: Yup.string()
        .required("Required")
        .min(5, "Za mało znaków")
    })
  )
  .required("Brak odpowiedzi")
  .min(2, "Za mało odpowiedzi");

const multipleChoiceQuestionValidation = Yup.array()
  .of(
    Yup.object().shape({
      content: Yup.string()
        .required("Required")
        .min(5, "Za mało znaków")
    })
  )
  .required("Brak odpowiedzi")
  .min(3, "Za mało odpowiedzi");

const submitHandler = (values: any, resetForm: any) => {
  let correctAnswers = values.answerOptions.filter((el: any) => el.correct).length;
  if (values.questionType === "SINGLE_CHOICE_QUESTION") {
    if (correctAnswers !== 1) {
      return false;
    }
  }

  if (values.questionType === "MULTIPLE_CHOICE_QUESTION") {
    if (correctAnswers === values.answerOptions.length || correctAnswers === 0 || correctAnswers === 1) {
      return false;
    }
  }
  values.attachedImage = {
    content: values.attachedImage
  };
  sendQuestion(values, resetForm);
};
const sendQuestion = async (values: any, resetForm: any) => {
  if (!values.attachedImage.content) {
    values.attachedImage = null;
  }
  try {
    let data = await api.post(`/api/v1/questions`, values);
    notifications.quest.questCreatedSuccess();
  } catch (error) {
    notifications.quest.questCreatedError();
  }
};

const QuestFormContainer = () => {
  useEffect(() => {
    updateValidation(initialArray.questionType);
  }, []);
  const [validationFormula, setValidationFormula] = React.useState();
  const updateValidation = (value: any) => {
    setValidationFormula(
      Yup.object().shape({
        name: Yup.string().required(questNameMinErrorMessage),
        content: Yup.string()
          .min(15, questDescriptionMinErrorMessage)
          .max(255, questDescriptionMaxErrorMessage)
          .required(questDescriptionRequiredMessage),
        maxPoints: Yup.number()
          .typeError("Pole musi zawierać liczbę!")
          .min(1, "Minimalna wartośc punktów to: 1")
          .required("PlaceHolder")
          .integer("Pole musi zawierać liczbę całkowitą!"),
        answerOptions: setValidationToQuestionType(value)
      })
    );
  };
  return (
    <div>
      <Formik
        initialValues={initialArray}
        validationSchema={validationFormula}
        onSubmit={(values, { resetForm }) => {
          submitHandler(values, resetForm);
        }}
        render={props => (
          <form onSubmit={props.handleSubmit}>
            <AddQuest
              errors={props.errors}
              touched={props.touched}
              handleChange={props.handleChange}
              values={props.values}
              setFieldValue={props.setFieldValue}
              props={props}
              typeChangeHandler={updateValidation}
            />
          </form>
        )}
      />
    </div>
  );
};

export default QuestFormContainer;
