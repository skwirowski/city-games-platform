import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import AddQuest from "../AddQuest";

describe("CheckpointForm", () => {
  let wrapper;
  const handleSubmit = jest.fn();
  const formValues = {
    questName: "Przykładowa nazwa zadania",
    questDescription: "Przykładowa treść zadania",
    questAnswersList: [{ answer: "Przykładowa odpowiedź", correct: true }],
    questMaxPoints: 10,
    questTime: true
  };

  beforeEach(() => {
    const Unwrapped = unwrap(
      <AddQuest submit={handleSubmit} initialValues={formValues} />
    );
    wrapper = shallow(Unwrapped);
  });
  it("should render without crash", () => {
    expect(wrapper).toBeTruthy();
  });
});
