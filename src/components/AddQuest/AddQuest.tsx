import React from "react";
import { useState } from "react";
import { Field } from "formik";
import { withStyles, createStyles, Tabs, Tab } from "@material-ui/core";
import FFileInput from "../../shared/FormikComponents/FFileInput/FFileInput";
import FArray from "./QuestAnswers";
import FTextEditor from "../../shared/FormikComponents/FTextEditor";
import { init } from "./TextEditorInit";
import SubmitButton from "../../shared/SubmitButton";
import FInput from "../../shared/FormikComponents/FInput";
import ErrorMessage from "../../shared/FormikComponents/ErrorMessage";
import FCheckbox from "../../shared/FormikComponents/FCheckbox";

const styles = createStyles({
  container: {
    height: "90vh",
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center"
  },
  label: { margin: "15px 0" },
  input: { margin: "10px 0", width: "60vw" },
  points: { width: "59%" },
  button: { marginTop: "30px" },
  submitButton: {
    backgroundColor: "#1890ff",
    "&:hover": {
      backgroundColor: "#40a9ff"
    }
  }
});

const questNamePlaceholder = "Nazwa zadania";
const questMaxPointsPlaceholder = "Ilość punktów";
const questTimePlaceholder = "Czy czas odpowiedzi ma znaczenie?";
const attachImagePlaceholder = "Wstaw zdjęcie";

export interface FormValues {
  questionName: string;
  questDescription: string;
  answerOptions: Array<Object>;
  singleChoiceList: Array<Object>;
  maxPoints: number;
  timer: boolean;
}

const AddQuest: React.FC<any> = ({ touched, errors, classes, handleChange, values, setFieldValue, typeChangeHandler }) => {
  const [isSubmitted] = useState(false);
  const [tabValue, setTabValue] = React.useState(values.questionType);

  let tabHandleChange = (event: any, newValue: any) => {
    typeChangeHandler(newValue);
    setFieldValue("answerOptions", []);
    setFieldValue("questionType", newValue);
    setTabValue(newValue);
  };
  return (
    <div className={classes.container}>
      <div className={classes.label}>
        <Field name="name" render={(props: any) => <FInput fieldProps={props} placeholder={questNamePlaceholder} className={classes.input} />} />
        <br />
        <ErrorMessage error={errors.name} touched={touched.name} />
      </div>
      <div className="editor">
        <Field name="description">{(props: any) => <FTextEditor fieldProps={props} init={init} name="content" />}</Field>
        <ErrorMessage error={errors.content} touched={touched.content} />
      </div>
      <Tabs value={tabValue} onChange={tabHandleChange}>
        <Tab value="OPEN_QUESTION" label="Zadanie otwarte" />
        <Tab value="SINGLE_CHOICE_QUESTION" label="Zadanie zamknięte z jedną odpowiedzią" />
        <Tab value="MULTIPLE_CHOICE_QUESTION" label="Zadanie zamknięte z kilkoma odpowiedziami" />
      </Tabs>
      {tabValue === "OPEN_QUESTION" && (
        <FArray
          name="answerOptions"
          change={handleChange}
          initialArray={values.answerOptions}
          questionType="answerOptions"
          error={errors.answerOptions}
          touched={touched.answerOptions}
        />
      )}
      {(tabValue === "SINGLE_CHOICE_QUESTION" || tabValue === "MULTIPLE_CHOICE_QUESTION") && (
        <FArray
          name="answerOptions"
          change={handleChange}
          initialArray={values.answerOptions}
          questionType="closeQuestion"
          error={errors.answerOptions}
          touched={touched.answerOptions}
        />
      )}
      {typeof errors.answerOptions === "string" ? <div>{errors.answerOptions}</div> : null}
      <div className={classes.label}>
        <label className={classes.label}>Ilość punktów: </label>
        <Field
          name="maxPoints"
          render={(props: any) => <FInput fieldProps={props} placeholder={questMaxPointsPlaceholder} className={classes.points} />}
        />
        <br />
        <ErrorMessage error={errors.maxPoints} touched={touched.maxPoints} />
      </div>
      <Field name="timer" render={(props: any) => <FCheckbox fieldProps={props} className="" label={questTimePlaceholder} />} />

      <Field name="attachedImage" render={(props: any) => <FFileInput fieldProps={props} label={attachImagePlaceholder} name="attachedImage" />} />

      <SubmitButton disabled={isSubmitted} children="Wyślij" type="submit" className={classes.submitButton} />
    </div>
  );
};

export default withStyles(styles)(AddQuest);
