export const init = {
  width: "60vw",
  height: "30vh",
  menubar: false,
  plugins: "link image code",
  toolbar: "undo redo | bold italic | alignleft aligncenter alignright | code"
};
