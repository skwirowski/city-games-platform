import React from "react";
import { FieldArray, Field } from "formik";
import {
  WithStyles,
  withStyles,
  createStyles,
  TextField,
  Button
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import FCheckbox from "../../shared/FormikComponents/FCheckbox";

export interface Props extends WithStyles<typeof styles> {
  initialArray: Array<Object>;
  name: string;
  change: any;
  questionType: string;
  error: any;
  touched: any;
}
const styles = createStyles({
  removeButton: { marginLeft: "1rem" },
  arrayContainer: { margin: "1rem" },
  arrayElement: { marginBottom: "2rem" }
});

let addQuestionField = (arr: any, type: string) => {
  arr.push({
    content: "",
    correct: type !== "closeQuestion"
  });
};

const QuestAnswers: React.FC<Props> = ({
  initialArray,
  name,
  classes,
  change,
  questionType,
  error
}) => {
  return (
    <FieldArray
      name={`${name}`}
      render={arrayHelpers => (
        <div className={classes.arrayContainer}>
          {initialArray.map((k, index) => (
            <div key={index}>
              <TextField
                className={classes.arrayElement}
                type="text"
                name={`${name}[${index}].content`}
                placeholder="Podaj odpowiedź"
                onChange={change}
              />
              {questionType === "closeQuestion" ? (
                <Field
                  name={`${name}[${index}].correct`}
                  render={(props: any) => (
                    <FCheckbox
                      fieldProps={props}
                      className={classes.removeButton}
                      label={"Poprawna odpowiedź?"}
                    />
                  )}
                />
              ) : null}
              <Button
                className={classes.removeButton}
                onClick={() => arrayHelpers.remove(index)}
                variant="contained"
                color="secondary"
              >
                <DeleteIcon />
              </Button>
              {error && error[index] ? <div>{error[index].content}</div> : null}
            </div>
          ))}
          <Button
            onClick={() => addQuestionField(arrayHelpers, questionType)}
            variant="contained"
            color="primary"
          >
            {initialArray.length === 0
              ? "Dodaj odpowiedź"
              : "Dodaj kolejną odpowiedź"}
          </Button>
        </div>
      )}
    />
  );
};

export default withStyles(styles)(QuestAnswers);
