import React from "react";
import { Grid } from "@material-ui/core";
import NavBar from "./NavBar";
import MuiDrawer from "./MuiDrawer";

interface Props {
  color?: string;
}

const NavBarContainer: React.SFC<Props> = ({ color }) => {
  const [open, setOpen] = React.useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <Grid>
      <NavBar
        drawerState={open}
        drawerOpenHandler={handleDrawerOpen}
        color={color}
      />
      <MuiDrawer drawerState={open} drawerClose={handleDrawerClose} />
    </Grid>
  );
};

export default NavBarContainer;
