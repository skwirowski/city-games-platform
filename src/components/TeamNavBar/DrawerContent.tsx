import React from "react";
import {
  ListItem,
  WithStyles,
  withStyles,
  createStyles,
  Typography
} from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Link from "../../shared/LinkWithoutDefaultStyles";

const styles = createStyles({
  itemIcon: {
    color: "rgba(7, 88 ,141)",
    fontSize: "35px"
  },
  typographyText: {
    color: "white",
    fontSize: "20px"
  }
});

interface Props extends WithStyles<typeof styles> {
  Icon: any;
  to: string;
  children: string;
}

const DrawerContet: React.SFC<Props> = ({ classes, Icon, to, children }) => {
  return (
    <ListItem>
      <ListItemIcon>
        <Icon className={classes.itemIcon} />
      </ListItemIcon>
      <Link to={to}>
        <Typography className={classes.typographyText}>{children}</Typography>
      </Link>
    </ListItem>
  );
};

export default withStyles(styles)(DrawerContet);
