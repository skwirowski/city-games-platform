import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import DrawerContent from "../DrawerContent";

describe("DrawerContent ", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedDrawerContent = unwrap(DrawerContent);
const createSut = props =>
  shallow(
    <UnwrappedDrawerContent
      classes={{}}
      Icon={"icon"}
      to="string"
      children=""
    />
  );
