import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import NavBar from "../NavBar";
import { IconButton } from "@material-ui/core";

describe("NavBar component", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });

  describe("Open drawer", () => {
    it("Should call open drawer function when click", () => {
      const mockedOpenDrawer = jest.fn();
      const sut = createSut(mockedOpenDrawer);
      sut.find(IconButton).simulate("click");
      expect(mockedOpenDrawer).toBeCalled();
    });
  });
});

const UnwrappedNavBar = unwrap(NavBar);
const createSut = (openHandler, props) =>
  shallow(
    <UnwrappedNavBar
      classes={{}}
      drawerState={false}
      drawerOpenHandler={openHandler}
    />
  );
