import React from "react";
import { shallow } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import NavBarContainer from "../NavBarContainer";

describe("NavBarContainer component", () => {
  it("should render without crash", () => {
    const sut = createSut();
    expect(sut).toBeTruthy();
  });

  it("should match snapshot", () => {
    const sut = createSut();
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedNavBarContainer = unwrap(NavBarContainer);
const createSut = props => shallow(<UnwrappedNavBarContainer {...props} />);
