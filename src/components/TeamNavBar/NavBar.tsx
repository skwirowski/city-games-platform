import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  IconButton,
  Grid
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";
import * as colors from "../../shared/colors";

const styles = createStyles({
  burgerMenuContainer: {
    position: "absolute",
    left: "1.2em",
    top: "1em",
    "& button": {
      padding: "0"
    }
  },
  menuButton: {
    color: "white"
  },
  menuButtonPrimary: {
    color: colors.primary
  },
  menuButtonSecondary: {
    color: colors.secondary
  },
  menuIcon: {
    height: "1.2em",
    width: "1.2em"
  },
  hide: {
    visibility: "hidden"
  }
});

interface Props extends WithStyles<typeof styles> {
  drawerOpenHandler: any;
  drawerState: boolean;
  color?: string;
}

const Navbar: React.SFC<Props> = ({
  classes,
  drawerState,
  drawerOpenHandler,
  color
}) => {
  const getMenuButtonClass = () => {
    switch (color) {
      case "primary":
        return classes.menuButtonPrimary;
      case "secondary":
        return classes.menuButtonSecondary;
      default:
        return classes.menuButton;
    }
  };

  return (
    <Grid className={classes.burgerMenuContainer}>
      <IconButton
        color="inherit"
        aria-label="Open drawer"
        onClick={drawerOpenHandler}
        className={clsx(classes.menuButton, drawerState && classes.hide)}
      >
        <MenuIcon className={getMenuButtonClass()} />
      </IconButton>
    </Grid>
  );
};

export default withStyles(styles)(Navbar);
