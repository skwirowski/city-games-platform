import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Drawer,
  IconButton,
  List,
  Grid
} from "@material-ui/core";
import DrawerContent from "./DrawerContent";
import GroupOutlined from "@material-ui/icons/GroupOutlined";
import VideogameAssetOutlined from "@material-ui/icons/VideogameAssetOutlined";
import MapOutlined from "@material-ui/icons/MapOutlined";
import FormatListNumberedOutlined from "@material-ui/icons/FormatListNumberedOutlined";
import ErrorOutline from "@material-ui/icons/ErrorOutline";
import Pool from "@material-ui/icons/Pool";
import AssignmentOutlined from "@material-ui/icons/AssignmentOutlined";
import ExitToAppOutlined from "@material-ui/icons/ExitToAppOutlined";
import MenuIcon from "@material-ui/icons/Menu";
import { logout } from "../../services/auth";
import { getUserData } from "../../services/auth";
import { routes } from "../../static/routesUrl";
import Intl from "../../utils/Intl";

const styles = createStyles({
  drawer: {
    flexShrink: 0
  },
  paper: {
    backgroundColor: "rgb(0, 143, 211)",
    boxShadow: "0 0 20px 15px rgba(0,0,0,0.75)"
  },
  drawerHeader: {
    width: "15vw",
    display: "flex",
    justifyContent: "flex-start",
    padding: "15px 0 0 15px"
  },
  burgerIconButton: {
    padding: "5px"
  },
  menuIcon: {
    color: "white",
    height: "1.2em",
    width: "1.2em"
  },
  drawerListContainer: {
    width: "70vw",
    paddingLeft: "1em"
  },
  firstIconGroup: {
    marginBottom: "70px"
  }
});
interface Props extends WithStyles<typeof styles> {
  drawerState: boolean;
  drawerClose: any;
}

const MuiDrawer: React.SFC<Props> = ({ classes, drawerState, drawerClose }) => {
  return (
    <Drawer
      classes={{ paper: classes.paper }}
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={drawerState}
    >
      <Grid className={classes.drawerHeader}>
        <IconButton onClick={drawerClose} className={classes.burgerIconButton}>
          <MenuIcon className={classes.menuIcon} />
        </IconButton>
      </Grid>
      <List className={classes.drawerListContainer}>
        <Grid className={classes.firstIconGroup}>
          <DrawerContent
            Icon={GroupOutlined}
            to={`${routes.team}/${getUserData().uuid}`}
            children={Intl.messages["TeamNavBar.myTeam"]}
          />
          <DrawerContent
            Icon={VideogameAssetOutlined}
            to="#"
            children={Intl.messages["TeamNavBar.game"]}
          />
          <DrawerContent
            Icon={MapOutlined}
            to="#"
            children={Intl.messages["TeamNavBar.route"]}
          />
          <DrawerContent
            Icon={FormatListNumberedOutlined}
            to="#"
            children={Intl.messages["TeamNavBar.results"]}
          />
        </Grid>
        <Grid>
          <DrawerContent
            Icon={ErrorOutline}
            to={routes.aboutGame}
            children={Intl.messages["TeamNavBar.aboutGame"]}
          />
          <DrawerContent
            Icon={Pool}
            to="#"
            children={Intl.messages["TeamNavBar.gamemaster"]}
          />
          <DrawerContent
            Icon={AssignmentOutlined}
            to={routes.termsAndConditions}
            children={Intl.messages["TeamNavBar.rules"]}
          />
          <Grid onClick={logout}>
            <DrawerContent
              Icon={ExitToAppOutlined}
              to="#"
              children={Intl.messages["TeamNavBar.logout"]}
            />
          </Grid>
        </Grid>
      </List>
    </Drawer>
  );
};

export default withStyles(styles)(MuiDrawer);
