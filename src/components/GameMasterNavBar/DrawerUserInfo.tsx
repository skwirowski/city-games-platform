import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  ListItem,
  Typography
} from "@material-ui/core";
import { getUserData } from "../../services/auth";

const styles = createStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    width: "100px",
    height: "100px",
    marginBottom: "1em"
  }
});
interface Props extends WithStyles<typeof styles> {}

const DrawerUserInfo: React.SFC<Props> = ({ classes }) => {
  const { email } = getUserData();
  return (
    <ListItem className={classes.container}>
      <img
        className={classes.avatar}
        src="http://chittagongit.com/download/53081"
        alt=""
      />
      <Typography align="left" variant="subtitle1">
        Game Master
        <br />
        {email}
      </Typography>
    </ListItem>
  );
};

export default withStyles(styles)(DrawerUserInfo);
