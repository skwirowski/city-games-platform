import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  AppBar,
  Toolbar,
  IconButton
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import LogoutButton from "../../shared/LogoutButton";
import logo from "../../assets/images/logo.svg";
import clsx from "clsx";

const styles = createStyles({
  appBar: { backgroundColor: "#272727" },
  menuButton: {
    marginRight: "1em"
  },
  hide: {
    visibility: "hidden"
  },
  logo: { marginLeft: "40vw" }
});
interface Props extends WithStyles<typeof styles> {
  drawerOpenHandler: any;
  drawerState: boolean;
}

const Navbar: React.SFC<Props> = ({
  classes,
  drawerState,
  drawerOpenHandler
}) => {
  return (
    <AppBar className={classes.appBar} position="static">
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="Open drawer"
          onClick={drawerOpenHandler}
          className={clsx(classes.menuButton, drawerState && classes.hide)}
        >
          <MenuIcon />
        </IconButton>
        <a href="/">
          <img className={classes.logo} src={logo} alt="Intive Logo" />
        </a>
      </Toolbar>
      <LogoutButton />
    </AppBar>
  );
};

export default withStyles(styles)(Navbar);
