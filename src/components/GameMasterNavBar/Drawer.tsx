import React from "react";
import {
  WithStyles,
  withStyles,
  createStyles,
  Drawer,
  IconButton,
  List
} from "@material-ui/core";
import DrawerContent from "./DrawerContent";
import DrawerUserInfo from "./DrawerUserInfo";
import ListIcon from "@material-ui/icons/List";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import SettingsIcon from "@material-ui/icons/Settings";
import HomeIcon from "@material-ui/icons/Home";
import AddBoxIcon from "@material-ui/icons/AddBox";

const styles = createStyles({
  drawer: {
    drawerWidth: "900px",
    flexShrink: 0
  },
  drawerHeader: {
    width: "15vw",
    display: "flex",
    alignItems: "center",
    padding: "0 8px",
    justifyContent: "flex-end"
  }
});
interface Props extends WithStyles<typeof styles> {
  drawerState: boolean;
  drawerClose: any;
}

const MUIDrawer: React.SFC<Props> = ({ classes, drawerState, drawerClose }) => {
  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={drawerState}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={drawerClose}>
          <ChevronLeftIcon />
        </IconButton>
      </div>
      <List>
        <DrawerUserInfo />
        <DrawerContent Icon={HomeIcon} to="/" children="Home" />
        <DrawerContent Icon={ListIcon} to="#" children="Przeglądaj druzyny" />
        <DrawerContent
          Icon={AddBoxIcon}
          to="/quest-add"
          children="Dodaj zadanie"
        />
        <DrawerContent Icon={SettingsIcon} to="#" children="Ustawienia" />
      </List>
    </Drawer>
  );
};

export default withStyles(styles)(MUIDrawer);
