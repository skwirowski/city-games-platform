import React from "react";
import { ListItem } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Link from "../../shared/LinkWithoutDefaultStyles";

interface Props {
  Icon: any;
  to: string;
  children: string;
}

const DrawerContet: React.SFC<Props> = ({ Icon, to, children }) => {
  return (
    <ListItem>
      <ListItemIcon>
        <Icon />
      </ListItemIcon>
      <Link to={to} children={children} />
    </ListItem>
  );
};

export default DrawerContet;
