import React from "react";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import NavBar from "./NavBar";
import Drawer from "./Drawer";

const styles = createStyles({});
interface Props extends WithStyles<typeof styles> {}

const NavBarContainer: React.SFC<Props> = ({ classes }) => {
  const [open, setOpen] = React.useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <NavBar drawerState={open} drawerOpenHandler={handleDrawerOpen} />
      <Drawer drawerState={open} drawerClose={handleDrawerClose} />
    </div>
  );
};

export default withStyles(styles)(NavBarContainer);
