import React from "react";
import ReactDOM from "react-dom";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import pl from "react-intl/locale-data/pl";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { initAzure } from "./services/auth";
import { IntlProvider } from "react-intl";
import Intl from "./utils/Intl";

initAzure();
addLocaleData([...en, ...pl]);
function render() {
  ReactDOM.render(
    <IntlProvider locale={Intl.locale} messages={Intl.messages}>
      <App setLanguage={lang => Intl.load(lang).then(render)} />
    </IntlProvider>,
    document.getElementById("root")
  );
}

Intl.load("pl").then(render);
serviceWorker.register();
