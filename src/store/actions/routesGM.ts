import { GET_ROUTES, DELETE_ROUTE, GET_ROUTE, UPDATE_ROUTE, SET_ROUTE, CREATE_ROUTE } from "../constants";
import { ThunkDispatch } from "redux-thunk";
import routeApi from "../../services/routeServiceApi";
export interface RouteAction {
  type: string;
  payload: any;
}

export const getRoute = (guid: string) => async (dispatch: ThunkDispatch<{}, {}, RouteAction>) => {
  try {
    const { data } = await routeApi.get(`/api/Route/${guid}`);
    dispatch({
      type: GET_ROUTE,
      payload: data
    });
  } catch (error) {
    console.log("TCL: getRoute -> error", error);
  }
};

export const getRoutes = () => async (dispatch: ThunkDispatch<{}, {}, RouteAction>) => {
  try {
    const { data } = await routeApi.get("/api/Route");

    dispatch({
      type: GET_ROUTES,
      payload: data
    });
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};

export const createRoute = (route: any, routeName: string) => async (dispatch: ThunkDispatch<{}, {}, RouteAction>) => {
  try {
    const { data }: any = await routeApi.post(`/api/Route?routeName=${routeName}`, route);

    dispatch({
      type: CREATE_ROUTE,
      payload: data
    });
  } catch (error) {
    console.log("TCL: createRoute -> error", JSON.stringify(error, null, 2));
  }
};

export const updateRoute = (guid: string, updatedRoute: any) => async (dispatch: ThunkDispatch<{}, {}, RouteAction>) => {
  try {
    const { data }: any = await routeApi.put(`/api/Route/${guid}`, updatedRoute);

    dispatch({
      type: UPDATE_ROUTE,
      payload: {
        data: data,
        guid: guid
      }
    });
  } catch (error) {
    console.log("TCL: error", error);
  }
};

export const deleteRoute = (guid: string) => async (dispatch: ThunkDispatch<{}, {}, RouteAction>) => {
  try {
    await routeApi.delete(`/api/Route/${guid}`);
    dispatch({
      type: DELETE_ROUTE,
      payload: guid
    });
  } catch (error) {
    console.log("TCL: deleteRoute -> error", error);
  }
};

export const setRoute = (route: any) => async (dispatch: ThunkDispatch<{}, {}, RouteAction>) => {
  dispatch({
    type: SET_ROUTE,
    payload: route
  });
};
