import { SET_CHECKPOINT, SET_CHECKPOINT_CORDS } from "../constants";
import { ThunkDispatch } from "redux-thunk";
import { FormValues } from "../../components/CheckpointForm/CheckpointForm";

export type Checkpoint = CheckpointExt;
interface CheckpointExt extends FormValues {
  guid?: string;
  checkpoint_id?: string;
  lat?: string;
  lng?: string;
}

export interface CheckpointAction {
  type: string;
  payload: any;
}

export const initialCheckpointValue = {
  name: "",
  descriptionOnTheWay: "",
  descriptionAfterArrival: "",
  address: "",
  objectName: "",
  latitude: "",
  longitude: "",
  isQrCodeConfirmationSelected: false,
  isGeolocationConfirmationSelected: false,
  isLocationVisible: true,
  isAddressVisible: false,
  isObjectNameVisible: false,
  isDescriptionOnTheWayVisible: false,
  isDescriptionAfterArrivalVisible: false
};

export interface SetCheckpointProps {
  setCheckpoint: (checkpoint: CheckpointExt) => Promise<void>;
}

export interface CheckpointProps {
  checkpoint: CheckpointExt;
}

export type SetCheckpoint = (checkpoint: CheckpointExt | null) => Promise<void>;
export type SetCheckpointCords = (cords: any) => Promise<void>;

export const setCheckpoint = (checkpoint: CheckpointExt | null) => async (
  dispatch: ThunkDispatch<{}, {}, CheckpointAction>
) => {
  dispatch({
    type: SET_CHECKPOINT,
    payload: checkpoint
  });
};

export const setCheckpointCords = (cords: any) => async (dispatch: ThunkDispatch<{}, {}, CheckpointAction>) => {
  dispatch({
    type: SET_CHECKPOINT_CORDS,
    payload: cords
  });
};
