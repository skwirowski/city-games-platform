import { SET_CHECKPOINTS, FETCH_CHECKPOINTS } from "../constants";
import { ThunkDispatch } from "redux-thunk";
import routeApi from "../../services/routeServiceApi";

export interface CheckpointsAction {
  type: string;
  payload: Checkpoints;
}
export interface CheckpointValues {
  name: string;
  descriptionOnTheWay: string;
  descriptionAfterArrival: string;
  address: string;
  objectName: string;
  latitude: string;
  longitude: string;
  isQrCodeConfirmationSelected: boolean;
  isGeolocationConfirmationSelected: boolean;
  isLocationVisible: boolean;
  isAddressVisible: boolean;
  isObjectNameVisible: boolean;
  isDescriptionOnTheWayVisible: boolean;
  isDescriptionAfterArrivalVisible: boolean;
}

export interface SetCheckpointsProps {
  setCheckpoints: (position: CheckpointValues | Array<CheckpointValues>) => CheckpointsAction;
}

export type Checkpoints = CheckpointValues | Array<CheckpointValues>;

export const setCheckpoints = (checkpoints: Checkpoints) => async (dispatch: ThunkDispatch<{}, {}, CheckpointsAction>) => {
  dispatch({
    type: SET_CHECKPOINTS,
    payload: checkpoints
  });
};

export const fetchCheckpoints = () => async (dispatch: ThunkDispatch<{}, {}, CheckpointsAction>) => {
  const { data } = await routeApi.get("/api/Checkpoints");
  dispatch({
    type: FETCH_CHECKPOINTS,
    payload: data
  });
};
