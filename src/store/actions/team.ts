import {
  SET_CURRENT_CHECKPOINT,
  SET_CURRENT_QUEST,
  SET_CURRENT_CHECKPOINT_STATUS,
  SET_CURRENT_GAME_PROGRESS
} from "../constants";
import { ThunkDispatch } from "redux-thunk";
import { getGameGuidForTeam } from "../../services/getGameGuidForTeam";
import { getNextCheckpoint } from "../../services/getNextCheckpoint";
import routeApi from "../../services/routeServiceApi";
import questApi from "../../services/questServiceApi";

export interface CurrentCheckpointAction {
  type: string;
  payload: any;
}

export const fetchCurrentGameState = () => async (
  dispatch: ThunkDispatch<{}, {}, CurrentCheckpointAction>
) => {
  try {
    const { data: game } = await getGameGuidForTeam();
    const { data: currentCheckpoint } = await getNextCheckpoint(game.guid);
    dispatch(setCheckpointDetails(currentCheckpoint.checkpointGuid));
    dispatch(setQuestDetails(currentCheckpoint.questGuid));
    dispatch(setCurrentCheckpointStatus(currentCheckpoint.checkpointStatus));
    dispatch(setCurrentGameProgress(currentCheckpoint.progress));
  } catch (error) {
    console.log(error);
  }
};

const setCheckpointDetails = (guid: string) => async (
  dispatch: ThunkDispatch<{}, {}, CurrentCheckpointAction>
) => {
  try {
    const { data } = await routeApi.get(`/api/Checkpoints/${guid}`);
    dispatch({
      type: SET_CURRENT_CHECKPOINT,
      payload: data
    });
  } catch (error) {
    console.log(error);
  }
};

const setQuestDetails = (guid: string) => async (
  dispatch: ThunkDispatch<{}, {}, CurrentCheckpointAction>
) => {
  try {
    const { data } = await questApi.get(`/api/v1/questions/${guid}`);
    dispatch({
      type: SET_CURRENT_QUEST,
      payload: data
    });
  } catch (error) {
    console.log(error);
  }
};

const setCurrentCheckpointStatus = (status: number) => ({
  type: SET_CURRENT_CHECKPOINT_STATUS,
  payload: status
});

const setCurrentGameProgress = (progress: boolean) => ({
  type: SET_CURRENT_GAME_PROGRESS,
  payload: progress
});
