import {
  SET_ROUTE_CHECKPOINTS,
  DELETE_ROUTE_CHECKPOINT,
  FETCH_AND_SET_ROUTE_CHECKPOINTS,
  SET_ROUTE,
  ATTACH_QUESTS_TO_ROUTE_CHECKPOINTS,
  SET_QUESTS_FOR_CHECKPOINTS,
  ATTACH_QUEST_TO_ROUTE_CHECKPOINT,
  REMOVE_QUEST_FROM_STORE,
  DETACH_QUEST_FROM_CHECKPOINT,
  ADD_QUEST_TO_STORE,
  SET_STORE_QUESTS,
  SET_QUESTS,
  SET_CHECKPOINTS
} from "../constants";
import { ThunkDispatch } from "redux-thunk";
import routeApi from "../../services/routeServiceApi";
import questApi from "../../services/questServiceApi";

import { mapGuidsToRouteCheckpoints, mapGuidsToRouteCheckpointsQuest, mapQuestsToRouteQuests } from "../../utils/mapGuidsToRouteCheckpoints";

export interface RouteCheckpointAction {
  type: string;
  payload: any;
}

export const setRouteCheckpoints = (checkpoints: any) => async (dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>) => {
  try {
    dispatch({
      type: SET_ROUTE_CHECKPOINTS,
      payload: checkpoints
    });
  } catch (error) {
    console.log("TCL: fetchRoutes -> error", error);
  }
};

export const deleteRouteCheckpoint = (checkpoint_id: string) => async (dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>) => {
  try {
    dispatch({
      type: DELETE_ROUTE_CHECKPOINT,
      payload: checkpoint_id
    });
  } catch (error) {
    console.log("TCL: fetchRoutes -> error", error);
  }
};

export const fetchAndSetRouteCheckpoints = (guid: string, checkpoints: any) => async (dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>) => {
  try {
    const { data: route } = await routeApi.get(`/api/Route/${guid}`);

    let checkpointsData = checkpoints;
    if (!checkpoints.length) {
      const { data } = await routeApi.get("/api/Checkpoints");
      checkpointsData = data;
    }

    const t0 = performance.now();

    const routeCheckpoints = mapGuidsToRouteCheckpoints(route.checkpoints, checkpointsData);
    const t1 = performance.now();
    console.log("Call took " + (t1 - t0) + " milliseconds.");
    dispatch({
      type: SET_ROUTE,
      payload: route
    });
    dispatch({
      type: FETCH_AND_SET_ROUTE_CHECKPOINTS,
      payload: routeCheckpoints
    });
  } catch (error) {
    console.log("TCL: fetchRoutes ff-> error", error);
  }
};

export const attachQuestToRouteCheckpoint = (quest: any, checkpoint_id: string) => async (dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>) => {
  try {
    if (!quest && !checkpoint_id) {
      throw Error("Empty Data");
    }
    const questToCheckpoint = {
      quest,
      checkpoint_id
    };

    dispatch({
      type: REMOVE_QUEST_FROM_STORE,
      payload: quest.uuid
    });
    dispatch({
      type: ATTACH_QUEST_TO_ROUTE_CHECKPOINT,
      payload: questToCheckpoint
    });
  } catch (error) {
    console.log("TCL: fetchRoutes ff-> error", error);
  }
};

export const detachQuestFromCheckpoint = (quest: any, checkpoint_id: string) => async (dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>) => {
  try {
    if (!quest && !checkpoint_id) {
      throw Error("Empty Data");
    }

    dispatch({
      type: DETACH_QUEST_FROM_CHECKPOINT,
      payload: checkpoint_id
    });
    dispatch({
      type: ADD_QUEST_TO_STORE,
      payload: quest
    });
  } catch (error) {
    console.log("TCL: fetchRoutes ff-> error", error);
  }
};
export const fetchAndSetRouteCheckpointsAndQuest = (guid: string, checkpoints: any = [], quests: any = []) => async (
  dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>
) => {
  try {
    const t0 = performance.now();
    const { data: route } = await routeApi.get(`/api/Route/${guid}`);
    let checkpointsData = checkpoints;

    if (!checkpoints.length) {
      const { data } = await routeApi.get("/api/Checkpoints");
      checkpointsData = data;
      dispatch({
        type: SET_CHECKPOINTS,
        payload: checkpointsData
      });
    }

    let questsData = quests;
    if (!quests.length) {
      const { data } = await questApi.get("/api/v1/questions");
      questsData = data;

      dispatch({
        type: SET_QUESTS,
        payload: quests
      });
    }

    let questsForCheckpoints = [];
    try {
      const { data } = await routeApi.get(`/api/QuestForCheckpoints/${guid}`);
      questsForCheckpoints = data;
    } catch (error) {
      questsForCheckpoints = [];
    }

    const { questsToSet, routeQuests } = mapQuestsToRouteQuests(questsData, questsForCheckpoints);

    const routeCheckpoints = mapGuidsToRouteCheckpointsQuest(questsForCheckpoints, routeQuests, checkpointsData, route.checkpoints);

    dispatch({
      type: SET_QUESTS_FOR_CHECKPOINTS,
      payload: questsForCheckpoints
    });
    dispatch({
      type: SET_ROUTE,
      payload: route
    });
    dispatch({
      type: SET_STORE_QUESTS,
      payload: questsToSet
    });

    dispatch({
      type: FETCH_AND_SET_ROUTE_CHECKPOINTS,
      payload: routeCheckpoints
    });
    const t1 = performance.now();
    console.log("Call took " + (t1 - t0) + " milliseconds.");
  } catch (error) {
    console.log("TCL: fetchAndSetRouteCheckpointsAndQuest -> error", error);
  }
};

export const attachQuestsToCheckpoints = (questsToAdd: any, questsToUpdate: any, questsToDelete: any) => async (
  dispatch: ThunkDispatch<{}, {}, RouteCheckpointAction>
) => {
  try {
    const isAdd = Boolean(questsToAdd.length);
    const isUpdate = Boolean(questsToUpdate.length);
    const isDelete = Boolean(questsToDelete.length);

    const attachQuestsCallArray = [
      isAdd ? routeApi.post("/api/QuestForCheckpoints/add-many", questsToAdd) : Promise.resolve(0),
      isUpdate ? routeApi.put("/api/QuestForCheckpoints/update-many", questsToUpdate) : Promise.resolve(0),
      isDelete ? routeApi.delete("/api/QuestForCheckpoint/delete-many", { data: questsToDelete }) : Promise.resolve(0)
    ];

    if (isAdd || isUpdate || isDelete) {
      await Promise.all([attachQuestsCallArray]).catch(function(err) {
        console.log(err.message);
        throw err;
      });
    }
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};
