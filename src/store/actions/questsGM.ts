import {
  GET_QUESTS,
  REMOVE_QUEST_FROM_STORE,
  ADD_QUEST_TO_STORE,
  SET_QUESTS,
  SET_QUESTS_FOR_CHECKPOINTS,
  GET_QUESTS_FOR_CHECKPOINTS
} from "../constants";
import { ThunkDispatch } from "redux-thunk";
import questApi from "../../services/questServiceApi";
import routeApi from "../../services/routeServiceApi";

export interface QuestAction {
  type: string;
  payload: any;
}

export const getQuests = () => async (dispatch: ThunkDispatch<{}, {}, QuestAction>) => {
  try {
    const { data } = await questApi.get("/api/v1/questions");

    dispatch({
      type: GET_QUESTS,
      payload: data
    });
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};
export const removeQuestFromStore = (quest_uuid: string) => async (dispatch: ThunkDispatch<{}, {}, QuestAction>) => {
  try {
    dispatch({
      type: REMOVE_QUEST_FROM_STORE,
      payload: quest_uuid
    });
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};
export const addQuestToStore = (quest: any) => async (dispatch: ThunkDispatch<{}, {}, QuestAction>) => {
  try {
    dispatch({
      type: ADD_QUEST_TO_STORE,
      payload: quest
    });
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};
export const setQuests = (quests: any) => async (dispatch: ThunkDispatch<{}, {}, QuestAction>) => {
  try {
    dispatch({
      type: SET_QUESTS,
      payload: quests
    });
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};
export const setQuestsForCheckpoints = (questForCheckpoints: any) => async (dispatch: ThunkDispatch<{}, {}, QuestAction>) => {
  try {
    dispatch({
      type: SET_QUESTS_FOR_CHECKPOINTS,
      payload: questForCheckpoints
    });
  } catch (error) {
    console.log("TCL: getRoutes -> error", error);
  }
};
