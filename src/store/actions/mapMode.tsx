import { SET_MAP_CENTER, SET_MAP_ZOOM } from "../constants";
import { ThunkDispatch } from "redux-thunk";

export interface MapModeAction {
  type: string;
  payload: any;
}

export const setMapCenter = (coords: any, lngOffset: boolean, offsetValue: number, routeEditor: boolean) => async (
  dispatch: ThunkDispatch<{}, {}, MapModeAction>
) => {
  try {
    if (lngOffset) {
      if (routeEditor) {
        dispatch({
          type: SET_MAP_CENTER,
          payload: [coords[0], coords[1] - offsetValue]
        });
      } else {
        dispatch({
          type: SET_MAP_CENTER,
          payload: [coords[0], coords[1] + offsetValue]
        });
      }
    } else {
      dispatch({
        type: SET_MAP_CENTER,
        payload: coords
      });
    }
  } catch (error) {
    console.log("TCL: fetchRoutes -> error", error);
  }
};

export const setMapZoom = (zoom: number) => async (dispatch: ThunkDispatch<{}, {}, MapModeAction>) => {
  try {
    dispatch({
      type: SET_MAP_ZOOM,
      payload: zoom
    });
  } catch (error) {
    console.log("TCL: error", error);
  }
};
