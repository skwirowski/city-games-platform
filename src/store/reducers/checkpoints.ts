import { SET_CHECKPOINTS, FETCH_CHECKPOINTS } from "../constants";
import { CheckpointsAction, Checkpoints } from "../actions";

export default (state: Checkpoints | null = null, action: CheckpointsAction) => {
  switch (action.type) {
    case SET_CHECKPOINTS:
      return action.payload;
    case FETCH_CHECKPOINTS:
      return action.payload;
    default:
      return state;
  }
};
