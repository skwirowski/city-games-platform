import {
  SET_ROUTE_CHECKPOINTS,
  DELETE_ROUTE_CHECKPOINT,
  FETCH_AND_SET_ROUTE_CHECKPOINTS,
  ATTACH_QUEST_TO_ROUTE_CHECKPOINT,
  DETACH_QUEST_FROM_CHECKPOINT
} from "../constants";
import { RouteAction } from "../actions";

export default (state: any = [], action: RouteAction) => {
  switch (action.type) {
    case SET_ROUTE_CHECKPOINTS:
      return action.payload;
    case DELETE_ROUTE_CHECKPOINT:
      return state.filter((checkpoint: any) => checkpoint.checkpoint_id !== action.payload);
    case FETCH_AND_SET_ROUTE_CHECKPOINTS:
      return action.payload;
    case ATTACH_QUEST_TO_ROUTE_CHECKPOINT:
      const { quest, checkpoint_id } = action.payload;
      return state.map((checkpoint: any) => {
        if (checkpoint.checkpoint_id !== checkpoint_id) return checkpoint;
        checkpoint.attachedQuest = quest;
        return checkpoint;
      });
    case DETACH_QUEST_FROM_CHECKPOINT:
      return state.map((checkpoint: any) => {
        if (checkpoint.checkpoint_id !== action.payload) return checkpoint;
        checkpoint.attachedQuest = null;
        return checkpoint;
      });

    default:
      return state;
  }
};
