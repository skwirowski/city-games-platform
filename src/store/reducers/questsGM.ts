import { GET_QUESTS, REMOVE_QUEST_FROM_STORE, ADD_QUEST_TO_STORE, SET_QUESTS, SET_STORE_QUESTS, SET_QUESTS_FOR_CHECKPOINTS } from "../constants";
import { QuestAction } from "../actions";

const initState = {
  allQuests: [],
  storeQuests: [],
  questsForCheckpoints: []
};

export default (state: any = initState, action: QuestAction) => {
  switch (action.type) {
    case GET_QUESTS:
      return {
        allQuests: action.payload,
        storeQuests: state.storeQuests,
        questsForCheckpoints: state.questsForCheckpoints
      };
    case SET_QUESTS:
      return {
        allQuests: action.payload,
        storeQuests: state.storeQuests,
        questsForCheckpoints: state.questsForCheckpoints
      };
    case SET_STORE_QUESTS:
      return {
        allQuests: state.allQuests,
        storeQuests: action.payload,
        questsForCheckpoints: state.questsForCheckpoints
      };
    case REMOVE_QUEST_FROM_STORE:
      return {
        allQuests: state.allQuests,
        storeQuests: state.storeQuests.filter((quest: any) => quest.uuid !== action.payload),
        questsForCheckpoints: state.questsForCheckpoints
      };
    case ADD_QUEST_TO_STORE:
      return {
        allQuests: state.allQuests,
        storeQuests: [...state.storeQuests, action.payload],
        questsForCheckpoints: state.questsForCheckpoints
      };
    case SET_QUESTS_FOR_CHECKPOINTS:
      return {
        allQuests: state.allQuests,
        storeQuests: state.storeQuests,
        questsForCheckpoints: action.payload
      };

    default:
      return state;
  }
};
