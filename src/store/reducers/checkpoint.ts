import { SET_CHECKPOINT, SET_CHECKPOINT_CORDS } from "../constants";
import { CheckpointAction, Checkpoint, initialCheckpointValue } from "../actions";

export default (state: Checkpoint | null = initialCheckpointValue, action: CheckpointAction) => {
  switch (action.type) {
    case SET_CHECKPOINT:
      return action.payload;
    case SET_CHECKPOINT_CORDS:
      const { latitude, longitude } = action.payload;
      return { ...state, latitude: latitude, longitude: longitude };

    default:
      return state;
  }
};
