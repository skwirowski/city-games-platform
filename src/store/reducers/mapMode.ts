import { SET_MAP_CENTER, SET_MAP_ZOOM } from "../constants";
import { MapModeAction } from "../actions";

const lngOffset = 0.05;
const mapCenterCoords = [51.109, 17.0337];
const mapZoom = 13;

const initialState = {
  lngOffset: lngOffset,
  mapCenter: mapCenterCoords,
  mapZoom: mapZoom
};

export default (state: any = initialState, action: MapModeAction) => {
  switch (action.type) {
    case SET_MAP_CENTER:
      return { ...state, mapCenter: action.payload };
    case SET_MAP_ZOOM:
      return { ...state, mapZoom: action.payload };
    default:
      return state;
  }
};
