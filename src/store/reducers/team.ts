import {
  SET_CURRENT_CHECKPOINT,
  SET_CURRENT_QUEST,
  SET_CURRENT_CHECKPOINT_STATUS,
  SET_CURRENT_GAME_PROGRESS
} from "../constants";
import { CurrentCheckpointAction } from "../actions/team";

const initialState = {
  currentCheckpoint: {},
  currentQuest: {},
  currentCheckpointStatus: 0,
  progress: 0
};

export default (state: any = initialState, action: CurrentCheckpointAction) => {
  switch (action.type) {
    case SET_CURRENT_CHECKPOINT:
      return {
        ...state,
        currentCheckpoint: action.payload
      };
    case SET_CURRENT_QUEST:
      return {
        ...state,
        currentQuest: action.payload
      };
    case SET_CURRENT_CHECKPOINT_STATUS:
      return {
        ...state,
        currentCheckpointStatus: action.payload
      };
    case SET_CURRENT_GAME_PROGRESS:
      return {
        ...state,
        progress: action.payload
      };
    default:
      return state;
  }
};
