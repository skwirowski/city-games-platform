import { GET_ROUTES, UPDATE_ROUTE, DELETE_ROUTE, GET_ROUTE, SET_ROUTE, CREATE_ROUTE } from "../constants";
import { RouteAction } from "../actions";

const initialState = {
  routes: [],
  route: null
};

export default (state: any = initialState, action: RouteAction) => {
  switch (action.type) {
    case GET_ROUTE:
      return {
        routes: state.routes,
        route: action.payload
      };
    case GET_ROUTES:
      return {
        routes: action.payload,
        route: state.route
      };
    case SET_ROUTE:
      return {
        routes: state.routes,
        route: action.payload
      };
    case CREATE_ROUTE:
      return {
        routes: [...state.routes, action.payload],
        route: action.payload
      };
    case UPDATE_ROUTE:
      const { data, guid } = action.payload;

      const updatedRoutes = state.routes.map((route: any) => {
        if (route.guid !== guid) return route;
        return data;
      });
      return {
        routes: updatedRoutes,
        route: data
      };
    case DELETE_ROUTE:
      return {
        routes: state.routes.filter((route: any) => route.guid !== action.payload),
        route: state.route
      };

    default:
      return state;
  }
};
