export const SET_POSITIONS = "SET_POSITIONS";
export const SET_CHECKPOINTS = "SET_CHECKPOINTS";
export const FETCH_CHECKPOINTS = "FETCH_CHECKPOINTS";
export const SET_CHECKPOINT = "SET_CHECKPOINT";
export const SET_CHECKPOINT_CORDS = "SET_CHECKPOINT_CORDS";

export const GET_ROUTE = "GET_ROUTE";
export const SET_ROUTE = "SET_ROUTE";
export const GET_ROUTES = "GET_ROUTES";
export const CREATE_ROUTE = "CREATE_ROUTE";
export const UPDATE_ROUTE = "UPDATE_ROUTE";
export const DELETE_ROUTE = "DELETE_ROUTE";



export const SET_ROUTE_CHECKPOINTS = 'SET_ROUTE_CHECKPOINTS'
export const FETCH_AND_SET_ROUTE_CHECKPOINTS = 'FETCH_AND_SET_ROUTE_CHECKPOINTS'
export const DELETE_ROUTE_CHECKPOINT = 'DELETE_ROUTE_CHECKPOINT'
export const DETACH_QUEST_FROM_CHECKPOINT = "DETACH_QUEST_FROM_CHECKPOINT"
export const ATTACH_QUEST_TO_ROUTE_CHECKPOINT = "ATTACH_QUEST_TO_ROUTE_CHECKPOINT";
export const ATTACH_QUESTS_TO_ROUTE_CHECKPOINTS = "ATTACH_QUESTS_TO_ROUTE_CHECKPOINTS";


export const SET_EDIT_MODE = "SET_EDIT_MODE";
export const SET_CREATE_MODE = "SET_CREATE_MODE";
export const SET_MAP_CENTER = "SET_CREATE_MODE";
export const SET_MAP_ZOOM = "SET_MAP_ZOOM";


export const GET_QUESTS = "GET_QUESTS";
export const SET_QUESTS = "SET_QUESTS";
export const GET_QUESTS_FOR_CHECKPOINTS = "GET_QUESTS_FOR_CHECKPOINTS";
export const SET_QUESTS_FOR_CHECKPOINTS = "SET_QUESTS_FOR_CHECKPOINTS";
export const SET_STORE_QUESTS = "SET_STORE_QUESTS";
export const ADD_QUEST_TO_STORE = "ADD_QUEST_TO_STORE";
export const REMOVE_QUEST_FROM_STORE = "REMOVE_QUEST_FROM_STORE";
export const SET_CURRENT_CHECKPOINT = "SET_CURRENT_CHECKPOINT";
export const SET_CURRENT_QUEST = "SET_CURRENT_QUEST";
export const SET_CURRENT_CHECKPOINT_STATUS = "SET_CURRENT_CHECKPOINT_STATUS";
export const SET_CURRENT_GAME_PROGRESS = "SET_CURRENT_GAME_PROGRESS";