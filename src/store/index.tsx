import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";

import routeCheckpoints from "./reducers/routeCheckpoints";
import checkpoints from "./reducers/checkpoints";
import checkpoint from "./reducers/checkpoint";
import routesGM from "./reducers/routesGM";
import questsGM from "./reducers/questsGM";
import mapMode from "./reducers/mapMode";
import team from "./reducers/team";

export const reducer = combineReducers({
  routeCheckpoints,
  checkpoints,
  checkpoint,
  routesGM,
  questsGM,
  mapMode,
  team
});

export function initializeStore() {
  return createStore(reducer, {}, composeWithDevTools(applyMiddleware(thunkMiddleware)));
}
