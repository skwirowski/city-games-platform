import en_language from "./en.json";
import pl_language from "./pl.json";

interface IMessages {
    [key: string]: {
        [key: string]: string
    },
}

const messages:IMessages =  {
    "en": en_language,
    "pl": pl_language
}

export default messages;