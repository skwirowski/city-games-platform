import { toDegreesMinutesAndSeconds, convertDMS, convertDMSToDD, parseDMS } from "../coordinates";

describe("toDegreesMinutesAndSeconds", () => {
  it("should return appropriate values", () => {
    const decimalCoords = 70.123;
    expect(toDegreesMinutesAndSeconds(decimalCoords)).toBe(`70°7'22.800"`);
  });
});

describe("convertDMS", () => {
  it("should return appropriate values", () => {
    const latitude = 70.123;
    const longitude = 23.11;

    expect(convertDMS(latitude, longitude).lat).toBe(`70°7'22.800"N`);
    expect(convertDMS(latitude, longitude).lng).toBe(`23°6'36.000"E`);
  });
});

describe("convertDMSToDD", () => {
  it("should return appropriate values", () => {
    const degrees = 70;
    const minutes = 7;
    const seconds = 22;
    const direction = "N";
    expect(convertDMSToDD(degrees, minutes, seconds, direction)).toBe(70.12277777777777);
  });
});

describe("parseDMS", () => {
  it("should return appropriate values", () => {
    const dmsString = `70°7'22"N`;

    expect(parseDMS(dmsString)).toBe(70.12277777777777);
  });
});
