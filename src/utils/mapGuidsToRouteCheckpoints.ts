import uuidv4 from "uuid/v4";

export function mapToKeyValue(collection: Array<any>, key: string) {
  return collection.reduce((accumulator: any, currentElement: any) => {
    accumulator[currentElement[key]] = currentElement;
    return accumulator;
  }, {});
}

function mapGuidToCheckpoint(keyValueCheckpoints: any, routeCheckpointsGuid: Array<string>) {
  return routeCheckpointsGuid.map((checkpointGuid: string) => {
    const guid = checkpointGuid.toLocaleLowerCase();
    if (keyValueCheckpoints[guid]) {
      const tmpCheckpoint = { ...keyValueCheckpoints[guid] };
      tmpCheckpoint.checkpoint_id = uuidv4();
      return tmpCheckpoint;
    } else {
      return;
    }
  });
}

function mapGuidToCheckpointQuest(keyValueCheckpoints: any, routeCheckpointsGuid: Array<string>, questsForCheckpoints: Array<any>, routeQuests: any) {
  return routeCheckpointsGuid.map((checkpointGuid: string) => {
    const guid = checkpointGuid.toLocaleLowerCase();
    if (keyValueCheckpoints[guid]) {
      const tmpCheckpoint = { ...keyValueCheckpoints[guid] };
      tmpCheckpoint.checkpoint_id = uuidv4();
      questsForCheckpoints.length &&
        questsForCheckpoints.forEach((questCheckpoint: any) => {
          if (questCheckpoint.checkpointGuid === guid) {
            tmpCheckpoint.attachedQuest = routeQuests[questCheckpoint.questGuid];
            return tmpCheckpoint;
          }
        });
      return tmpCheckpoint;
    } else {
      return;
    }
  });
}

export function mapGuidsToRouteCheckpoints(routeCheckpointsGuid: Array<string>, checkpoints: any) {
  if (!routeCheckpointsGuid.length && !checkpoints.length) return [];
  const keyValueCheckpoints = mapToKeyValue(checkpoints, "guid");
  return mapGuidToCheckpoint(keyValueCheckpoints, routeCheckpointsGuid);
}

export function mapQuestsToRouteQuests(quests: any, questsForCheckpoints: any) {
  if (!questsForCheckpoints.length) {
    return {
      routeQuests: [],
      questsToSet: quests
    };
  }

  const keyValueQuestCheckpoint = mapToKeyValue(questsForCheckpoints, "questGuid");

  let routeQuests: any = {};
  const questsToSet: any = [];
  quests.forEach((quest: any) => {
    const { uuid } = quest;
    if (keyValueQuestCheckpoint[uuid]) {
      routeQuests = { ...routeQuests, [uuid]: quest };
    } else {
      questsToSet.push(quest);
    }
  });
  return {
    routeQuests,
    questsToSet
  };
}

// export function mapGuidsToRouteCheckpointsQuest(
//   questsForCheckpoints: Array<any>,
//   routeQuests: any,
//   checkpoints: any,
//   routeCheckpointsGuid: Array<string>
// ) {

//   const routeCheckpoints = checkpoints.filter((checkpoint: any) => {
//     if (routeCheckpointsGuid.length && routeCheckpointsGuid.includes(checkpoint.guid.toLocaleUpperCase())) {
//       checkpoint.checkpoint_id = uuidv4();
//       questsForCheckpoints.length &&
//         questsForCheckpoints.forEach((questCheckpoint: any) => {
//           if (questCheckpoint.checkpointGuid === checkpoint.guid) {
//             checkpoint.attachedQuest = routeQuests[questCheckpoint.questGuid];
//             return checkpoint;
//           }
//         });
//       return checkpoint;
//     }
//   });
//   return routeCheckpoints;
// }

export function mapGuidsToRouteCheckpointsQuest(
  questsForCheckpoints: Array<any>,
  routeQuests: any,
  checkpoints: any,
  routeCheckpointsGuid: Array<string>
) {
  if (!routeCheckpointsGuid.length && !checkpoints.length) return [];
  const keyValueCheckpoints = mapToKeyValue(checkpoints, "guid");
  return mapGuidToCheckpointQuest(keyValueCheckpoints, routeCheckpointsGuid, questsForCheckpoints, routeQuests);
}
