import { intlShape } from "react-intl";

var intl: any;

interface IProps {
  contextTypes: any;
}

export const IntlCapture: IProps = (props: any, context: any) => {
  intl = context.intl;
  return props.children;
};

IntlCapture.contextTypes = {
  intl: intlShape.isRequired
};

interface IMessages {
  [key: string]: string;
}

class _Intl {
  t(message: any, values: any) {
    return intl.formatMessage(message, values);
  }
  locale = "pl";
  messages: IMessages = {};
  async load(locale: any) {
    this.messages = await import(`../translations/${locale}.json`);
  }
}
const Intl = new _Intl();
export default Intl;
