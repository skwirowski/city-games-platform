import { notification } from "antd";

type Placement = "topLeft" | "topRight" | "bottomLeft" | "bottomRight";
type Variant = "success" | "info" | "error" | "warning";

export const openNotificationWithIcon = (
  type: Variant,
  message: string,
  description: string,
  placement: Placement = "bottomLeft",
  duration: number = 4
) => {
  if (!message) return;
  notification.config({
    placement,
    duration //time in seconds
  });

  notification[type]({
    message,
    description,
    style: {
      boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.3), 0px 1px 1px 0px rgba(0,0,0,0.19), 0px 2px 1px -1px rgba(0,0,0,0.15)"
    }
  });
};
