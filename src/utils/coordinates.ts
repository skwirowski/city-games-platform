export const toDegreesMinutesAndSeconds = (decimalCoord: number) => {
  const absolute = Math.abs(decimalCoord);
  const degrees = Math.floor(absolute);
  const minutesNotTruncated = (absolute - degrees) * 60;
  const minutes = Math.floor(minutesNotTruncated);
  const seconds = ((minutesNotTruncated - minutes) * 60).toFixed(3);

  return `${degrees}°${minutes}'${seconds}"`;
};

export const convertDMS = (lat: number, lng: number) => {
  const latitude = toDegreesMinutesAndSeconds(lat);
  const latitudeCardinal = lat >= 0 ? "N" : "S";

  const longitude = toDegreesMinutesAndSeconds(lng);
  const longitudeCardinal = lng >= 0 ? "E" : "W";

  return { lat: latitude + latitudeCardinal, lng: longitude + longitudeCardinal };
};

export const convertDMSToDD = (degrees: number, minutes: number, seconds: number, direction: string): number => {
  let dd = degrees + minutes / 60 + seconds / (60 * 60);

  if (direction === "S" || direction === "W") {
    dd = dd * -1;
  }

  return (dd * 1000) / 1000;
};

export const parseDMS = (dmsString: string): number => {
  const degrees = dmsString.split("°")[0];
  const minutes = dmsString.split("°")[1].split(`'`)[0];
  const seconds = dmsString
    .split("°")[1]
    .split(`'`)[1]
    .split('"')[0];
  const direction = dmsString
    .split("°")[1]
    .split(`'`)[1]
    .split('"')[1];

  const result = convertDMSToDD(+degrees, +minutes, +seconds, direction);

  return result;
};
