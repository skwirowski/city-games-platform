import React from "react";
import { FieldProps } from "formik";
import FormControl from "@material-ui/core/FormControl";
import { DateTimePicker, MuiPickersUtilsProvider } from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";

interface IProps {
  fieldProps: FieldProps;
  className?: string;
  label?: string;
  initialFocusedDate?: number;
  value?: number;
}

const FLabeledInput: React.FC<IProps> = ({ fieldProps, className, label, initialFocusedDate }) => {
  const {
    field: { value, name },
    form: { setFieldValue }
  } = fieldProps;
  return (
    <FormControl style={{ display: "flex" }}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <DateTimePicker ampm={false} label={label} value={value} onChange={e => setFieldValue(name, e, true)} initialFocusedDate={initialFocusedDate} className={className}/>
      </MuiPickersUtilsProvider>
    </FormControl>
  );
};

export default FLabeledInput;
