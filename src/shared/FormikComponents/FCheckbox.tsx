import React from "react";
import { Checkbox, FormControlLabel } from "@material-ui/core";
import { FieldProps } from "formik";

interface Props {
  fieldProps: FieldProps;
  className?: string;
  label: string;
  handleChange?: () => {};
  checked?: boolean;
  disabled?: boolean;
}

const FChekcbox: React.FC<Props> = ({
  fieldProps,
  className,
  label,
  handleChange,
  checked,
  disabled
}) => {
  return (
    <FormControlLabel
      {...fieldProps.field}
      value={fieldProps.field.name}
      control={
        <Checkbox
          className={className}
          onChange={handleChange}
          checked={fieldProps.field.value}
          disabled={disabled}
        />
      }
      label={label}
    />
  );
};

export default FChekcbox;
