import React from "react";
import { Input } from "@material-ui/core";
import { FieldProps } from "formik";

interface Props {
  fieldProps: FieldProps;
  className: string;
  placeholder: string;
  multiline?: boolean;
  rows?: number;
  position?: any;
  name?: string;
}

const FInput: React.FC<Props> = ({ fieldProps, className, placeholder, multiline, rows, position, name }) => {
  return (
    <Input
      {...fieldProps.field}
      placeholder={placeholder}
      className={className}
      multiline={multiline || false}
      rows={rows || 1}
    />
  );
};

export default FInput;
