import React from "react";
import { Input } from "@material-ui/core";
import { FieldProps } from "formik";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import FMaskedInput from "./FMaskedInput";

interface IProps {
  fieldProps: FieldProps;
  className: string;
  multiline?: boolean;
  rows?: number;
  label?: string;
  mask?: boolean;
}

const FLabeledInput: React.FC<IProps> = ({ fieldProps, className, multiline, rows, label, mask }) => {
  return (
    <FormControl style={{ display: "flex" }}>
      <InputLabel htmlFor={label}>{label}</InputLabel>
      <Input
        {...fieldProps.field}
        id={label}
        className={className}
        placeholder={""}
        multiline={multiline || false}
        rows={rows || 1}
        inputComponent={mask ? FMaskedInput : "input"}
      />
    </FormControl>
  );
};

export default FLabeledInput;
