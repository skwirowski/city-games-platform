import React from 'react';
import { shallow } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils'
import ErrorMessage from '../ErrorMessage';

describe('ErrorMessage', () => {
    it('should render without crash', () => {
        const UnwrappedErrorMessage = unwrap(ErrorMessage);
        const wrapper = shallow(<UnwrappedErrorMessage  error={'error'} touched={true} classes={{}}/>)
        expect(wrapper).toBeTruthy();
    });

    it('should display error message if it gets an error and is touched', () => {
        const UnwrappedErrorMessage = unwrap(ErrorMessage);
        const wrapper = shallow(<UnwrappedErrorMessage  error={'error'} touched={true} classes={{}}/>)
        const message = wrapper.find('[aria-label="error-message"]')
        expect(message.text()).toEqual('error');
    });
    it('should not display error message if it gets an error and is not touched', () => {
        const UnwrappedErrorMessage = unwrap(ErrorMessage);
        const wrapper = shallow(<UnwrappedErrorMessage  error={'error'} touched={false} classes={{}}/>);
        expect(wrapper.text()).toEqual('');
    })
    it('should not display error message if it does not get an error and is touched', () => {
        const UnwrappedErrorMessage = unwrap(ErrorMessage);
        const wrapper = shallow(<UnwrappedErrorMessage  error={null} touched={true} classes={{}}/>);
        expect(wrapper.text()).toEqual('');
    })
})