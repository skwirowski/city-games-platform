import React from "react";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";

export const styles = createStyles({
  errorMessage: {
    fontSize: "12px",
    color: "red"
  }
});

interface Props extends WithStyles<typeof styles> {
  error: string | undefined;
  touched: boolean | undefined;
}

const ErrorMessage: React.FC<Props> = ({ error, touched, classes }) => {
  return (
    <>
      {error && touched && (
        <span className={classes.errorMessage} aria-label="error-message">
          {error}
        </span>
      )}
    </>
  );
};

export default withStyles(styles)(ErrorMessage);
