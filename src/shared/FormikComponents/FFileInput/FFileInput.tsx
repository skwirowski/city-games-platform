import React, { useState } from "react";
import useToggle from "../../../hooks/useToggle";
import { FieldProps } from "formik";
import "antd/dist/antd.css";
import { Upload, Icon, Modal, message } from "antd";
import { RcFile } from "antd/lib/upload/interface";
import { getBase64 } from "./utils/getBase64";

type UploadFileStatus = "error" | "success" | "done" | "uploading" | "removed";
interface UploadFile {
  uid: string;
  size: number;
  name: string;
  fileName?: string;
  lastModified?: number;
  lastModifiedDate?: Date;
  url?: string;
  status?: UploadFileStatus;
  percent?: number;
  thumbUrl?: string;
  originFileObj?: any;
  response?: any;
  error?: any;
  linkProps?: any;
  type: string;
}
interface IProps {
  fieldProps: FieldProps;
  label?: string;
  name: string;
}

interface IFile extends UploadFile {
  preview?: object;
}

type IFiles = Array<IFile>;

const dummyRequest = ({
  file,
  onSuccess
}: {
  file: IFile;
  onSuccess: (res: string) => void;
}) => {
  setTimeout(() => {
    onSuccess("ok");
  }, 0);
};

const FFileInput: React.FC<IProps> = ({ fieldProps, label, name }) => {
  const [previewVisible, togglePreviewVisible] = useToggle(false);
  const [previewImage, setPreviewImage] = useState<string>("");
  const [fileList, setFileList] = useState<IFiles>([]);

  const isFileTypeValid = (fileType: string): boolean => {
    const acceptedFiles = ["image/jpeg", "image/png"];
    return acceptedFiles.includes(fileType);
  };
  const isFileSizeValid = (fileSize: number): boolean => {
    return fileSize / 1024 / 1024 < 6;
  };

  const beforeUpload = (file: RcFile | IFile) => {
    const isValidFormat = isFileTypeValid(file.type);
    if (!isValidFormat) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isValidSize = isFileSizeValid(file.size);
    if (!isValidSize) {
      message.error("Image must be smaller than 6MB!");
    }
    const isValid = isValidFormat && isValidSize;
    return isValid;
  };

  const handlePreview = async (file: IFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(String(file.url || file.preview));
    togglePreviewVisible();
  };

  const handleChange = async ({ fileList }: { fileList: IFiles }) => {
    if (!fileList.length) {
      setFileList([]);
      fieldProps.form.setFieldValue(name, "", true);
      return;
    }
    const newFiles = fileList.slice(-1);
    const newFile = newFiles[0];
    const { size, type, originFileObj } = newFile;

    if (!isFileTypeValid(type) || !isFileSizeValid(size)) return;

    setFileList(newFiles);

    const imageURL = await getBase64(originFileObj);
    const base64string = imageURL.split(",")[1];
    fieldProps.form.setFieldValue(name, base64string, true);
  };

  return (
    <div className="clearfix">
      <Upload
        beforeUpload={beforeUpload}
        customRequest={dummyRequest}
        listType="picture-card"
        fileList={fileList}
        onPreview={handlePreview}
        onChange={handleChange}
        accept="image/jpeg,image/png"
      >
        <div>
          <Icon type="plus" />
          <div className="ant-upload-text">{label}</div>
        </div>
      </Upload>
      <Modal
        visible={previewVisible}
        footer={null}
        onCancel={togglePreviewVisible}
      >
        <img alt="example" style={{ width: "100%" }} src={previewImage} />
      </Modal>
    </div>
  );
};

export default FFileInput;
