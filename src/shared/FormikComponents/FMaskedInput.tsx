import React from "react";
import MaskedInput from "react-text-mask";

interface IProps {
  inputRef?: (ref: HTMLElement | null) => void;
}
const FMaskedInput: React.FC<IProps> = props => {
  const { inputRef, ...rest } = props;
  const mask: (string | RegExp)[] = [/[0-9]/, /\d/, ":", /[0-5]/, /[0-9]/, ":", /[0-5]/, /[0-9]/];

  return inputRef ? (
    <MaskedInput
      {...rest}
      ref={ref => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={mask}
    />
  ) : null;
};

export default FMaskedInput;
