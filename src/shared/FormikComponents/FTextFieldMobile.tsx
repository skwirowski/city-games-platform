import React from "react";
import { TextField, WithStyles, withStyles, createStyles } from "@material-ui/core";
import { FieldProps } from "formik";

const styles = createStyles({
  muiTextField: {
    backgroundColor: "white",
    borderBottom: "2px solid rgb(0,143, 213)"
  }
});

interface Props extends WithStyles<typeof styles> {
  fieldProps: FieldProps;
  label: string;
  disabled: boolean;
}

const FTextFieldMobile: React.FC<Props> = ({ classes, fieldProps, label, disabled }) => {
  return (
    <TextField
      label={label}
      {...fieldProps}
      disabled={disabled}
      variant="filled"
      InputProps={{
        disableUnderline: true,
        classes: {
          input: classes.muiTextField
        }
      }}
    />
  );
};

export default withStyles(styles)(FTextFieldMobile);
