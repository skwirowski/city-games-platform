import React from "react";
import { Editor } from "@tinymce/tinymce-react";
import { FieldProps } from "formik";

interface Props {
  fieldProps: FieldProps;
  name: string;
  init: object;
}

const FTextEditor: React.FC<Props> = ({ fieldProps, init, name }) => {
  const {
    field: { value }
  } = fieldProps;
  const apiKey = "d0rjomuoh42himkmggilvwyms700hkq3octue2kqr1l6kk5z";

  const handleEditorChange = (content: string) => {
    fieldProps.form.setFieldValue(name, content);
  };

  return <Editor value={value} apiKey={apiKey} initialValue={""} init={init} onEditorChange={handleEditorChange} />;
};

export default FTextEditor;
