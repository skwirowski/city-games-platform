import React from "react";
import { Link } from "react-router-dom";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";

const styles = createStyles({
  link: { textDecoration: "none", color: "inherit" }
});

interface Props extends WithStyles<typeof styles> {
  to: string;
}

const LinkWithoutDefaultStyles: React.FC<Props> = ({
  classes,
  to,
  children
}) => {
  return (
    <Link to={to} className={classes.link}>
      {children}
    </Link>
  );
};

export default withStyles(styles)(LinkWithoutDefaultStyles);
