import React from "react";
import classnames from "classnames";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import { Button } from "@material-ui/core";

const styles = createStyles({
  submitButton: {
    width: "250px",
    backgroundColor: "rgb(0,143, 213)",
    color: "white",
    borderRadius: "25px",
    boxShadow: "0px 3px 14px rgba(0, 143, 211, 0.7)",
    "&:hover": {
      backgroundColor: "#42a5f5"
    }
  }
});
interface IProps extends WithStyles<typeof styles> {
  disabled: boolean;
  type: "button" | "reset" | "submit" | undefined;
  children: string | JSX.Element;
  className?: string | undefined;
}

const SubmitButton: React.SFC<IProps> = ({ classes: { submitButton }, disabled, type, children, className }) => {
  return (
    <Button type={type} variant="contained" size="large" disabled={disabled} className={classnames(submitButton, className)}>
      {children}
    </Button>
  );
};

export default withStyles(styles)(SubmitButton);
