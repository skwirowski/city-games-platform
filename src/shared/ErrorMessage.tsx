import React from "react";
import classnames from "classnames";
import {
  WithStyles,
  withStyles,
  createStyles,
  Typography
} from "@material-ui/core";

const styles = createStyles({
  errorText: {
    color: "red"
  }
});

interface IProps extends WithStyles<typeof styles> {
  message: string | undefined;
  className?: string | undefined;
}

const ErrorMessage: React.FC<IProps> = ({
  classes: { errorText },
  message,
  className
}) => {
  return (
    <Typography
      variant="subtitle2"
      className={classnames(errorText, className)}
    >
      {message}
    </Typography>
  );
};

export default withStyles(styles)(ErrorMessage);
