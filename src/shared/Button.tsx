import React from "react";
import { WithStyles, withStyles, createStyles, Omit } from "@material-ui/core";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { Button as MuiButton } from "@material-ui/core";
import { ButtonProps } from "@material-ui/core/Button";
import * as colors from "./colors";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.primary,
      contrastText: "white"
    },
    secondary: {
      main: colors.secondary,
      contrastText: "white"
    }
  },
  typography: {
    useNextVariants: true
  }
});

const styles = createStyles({
  button: {
    width: "250px",
    borderRadius: "25px",
    borderWidth: "3px",
    boxShadow: "0px 3px 14px rgba(0, 0, 0, 0.3)"
  }
});

interface Props
  extends WithStyles<typeof styles>,
    Omit<ButtonProps, "classes"> {}

const Button: React.SFC<Props> = ({
  classes,
  children,
  variant,
  size,
  ...rest
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <MuiButton
        variant={variant || "contained"}
        size={size || "large"}
        className={classes.button}
        {...rest}
      >
        {children}
      </MuiButton>
    </MuiThemeProvider>
  );
};

export default withStyles(styles)(Button);
