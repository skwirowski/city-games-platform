import React from "react";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import KeyboardBackspace from "@material-ui/icons/KeyboardBackspace";

const styles = createStyles({
  arrow: {
    width: "60px",
    fontSize: "40px",
    paddingBottom: "3px",
    marginLeft: "20px",
    color: "rgb(0,143, 213)",
    "&:hover": {
      cursor: "pointer"
    }
  }
});
interface Props extends WithStyles<typeof styles> {}

const ArrowBack: React.SFC<Props> = ({ classes }) => {
  return <KeyboardBackspace className={classes.arrow} />;
};

export default withStyles(styles)(ArrowBack);
