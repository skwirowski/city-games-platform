import React from "react";
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from "enzyme";
import Button from "../Button";

describe("Button component", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedButton = unwrap(Button);
const createSut = props => shallow(<UnwrappedButton classes={{}} {...props} />);
