import React from "react";
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from "enzyme";
import Link from "../LinkWithoutDefaultStyles";

describe("Link component", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should render children", () => {
    expect(sut.find("div.test-div")).toBeDefined();
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });
});

const UnwrappedLink = unwrap(Link);
const children = <div className="test-div">test div</div>;
const createSut = props =>
  shallow(
    <UnwrappedLink classes={{}} to="/" {...props}>
      {children}
    </UnwrappedLink>
  );
