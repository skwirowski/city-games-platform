import React from "react";
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from "enzyme";
import LogoutButton from "../LogoutButton";
import * as auth from "../../services/auth";

jest.mock("../../services/auth");

describe("LogoutButton", () => {
  let sut;
  beforeEach(() => {
    sut = createSut();
  });

  it("should render without crash", () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it("should match snapshot", () => {
    expect(sut.debug()).toMatchSnapshot();
  });

  it("should call logout on click", () => {
    sut.simulate("click");
    expect(auth.logout).toHaveBeenCalledTimes(1);
  });
});

const UnwrappedLogoutButton = unwrap(LogoutButton);
const createSut = props => shallow(<UnwrappedLogoutButton classes={{}} {...props} />);
