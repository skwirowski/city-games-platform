import React from "react";
import { WithStyles, withStyles, createStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { Button } from "@material-ui/core";
import { logout } from "../services/auth";

const styles = createStyles({
  button: {
    position: "absolute",
    top: "1em",
    right: "1em"
  }
});
interface Props extends WithStyles<typeof styles> {}

const LogoutButton: React.SFC<Props> = ({ classes }) => {
  return (
    <Button variant="contained" color="primary" className={classes.button} onClick={logout}>
      <FormattedMessage id="shared.logoutButton" />
    </Button>
  );
};

export default withStyles(styles)(LogoutButton);
