import React, { SyntheticEvent } from 'react';
import clsx from 'clsx';
import { createStyles, Snackbar, SnackbarContent, withStyles, WithStyles } from "@material-ui/core";
import { green } from '@material-ui/core/colors';
import { FormattedMessage } from "react-intl";
import { AUTO_HIDE_DURATION } from '../../static/snackbar';

const styles = createStyles({
  online: {
    backgroundColor: green[600],
  }
});

interface Props extends WithStyles<typeof styles> {
  isOnline: boolean;
  isOpen: boolean;
  onClose: (event: SyntheticEvent<any, Event>, reason: string) => void;
}

const translationKey = (isOnline: boolean) => isOnline ? 'online' : 'offline';

const ConnectionStatus: React.SFC<Props> = ({ classes, isOnline, onClose, isOpen }) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center"
      }}
      open={isOpen}
      autoHideDuration={AUTO_HIDE_DURATION}
      onClose={onClose}
    >
      <SnackbarContent
        className={clsx({ [classes.online]: isOnline })}
        message={
          <FormattedMessage id={`ConnectionStatus.text.${translationKey(isOnline)}`} />
        }
      />
    </Snackbar>
  )
};

export default withStyles(styles)(ConnectionStatus);