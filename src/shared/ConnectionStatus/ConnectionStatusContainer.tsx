import React, { useEffect, useState, SyntheticEvent } from 'react';
import ConnectionStatus from './ConnectionStatus';


const ConnectionStatusContainer: React.SFC<{}> = () => {
  const [isOnline, setIsOnline] = useState(true);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    window.addEventListener("online", updateStatus);
    window.addEventListener("offline", updateStatus);
    return () => {
      window.removeEventListener("online", updateStatus);
      window.removeEventListener("offline", updateStatus);
    };
  }, []);

  const updateStatus = () => {
    setIsOnline(!!navigator.onLine);
    setIsOpen(true)
  }

  const handleClose = (event: SyntheticEvent<any, Event>, reason: string) => {
    if (reason === 'clickaway') return;
    setIsOpen(false);
  }

  return (
    <ConnectionStatus
      isOnline={isOnline}
      isOpen={isOpen}
      onClose={handleClose}
    />
  )
};

export default ConnectionStatusContainer;