import React from 'react';
import { unwrap } from "@material-ui/core/test-utils";
import { shallow } from 'enzyme';
import ConnectionStatus from '../ConnectionStatus';
import { SnackbarContent } from '@material-ui/core';

const UnwrappedConnectionStatus = unwrap(ConnectionStatus);
const createSut = props => shallow(<UnwrappedConnectionStatus classes={{}} {...props} />);

describe('ConnectionStatus', () => {
  it('should render without crash', () => {
    expect(createSut().isEmptyRender()).toBe(false);
  });

  it('should render online statement when is online', () => {
    const isOnline = true;

    const sut = createSut({ isOnline });

    expect(
      sut
        .find(SnackbarContent)
        .props()
        .message
        .props
        .id
    ).toBe('ConnectionStatus.text.online');
  });

  it('should render online statement when is offline', () => {
    const isOnline = false;

    const sut = createSut({ isOnline });

    expect(
      sut
        .find(SnackbarContent)
        .props()
        .message
        .props
        .id
    ).toBe('ConnectionStatus.text.offline');
  });

  it('should call onClose function', () => {
    const onCloseMock = jest.fn();
    const sut = createSut({ onClose: onCloseMock });

    sut.props().onClose()

    expect(onCloseMock).toHaveBeenCalled();
  });
});
