import React from 'react';
import ConnectionStatusContainer from '../ConnectionStatusContainer';
import { shallow } from 'enzyme';

const createSut = props => shallow(<ConnectionStatusContainer {...props} />);

describe('ConnectionStatus', () => {
  it('should render without crash', () => {
    expect(createSut().isEmptyRender()).toBe(false);
  });
});
