import api from "./teamServiceApi";

const deleteTeamByAdmin = (uuid: any) => {
  return api.delete(`/api/v1/teams/delete/admin/${uuid}`);
};

export default deleteTeamByAdmin;
