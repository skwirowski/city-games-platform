import { http } from "./http";
import { AxiosRequestConfig } from "axios";
export const baseUrl =
  "https://cors-anywhere.herokuapp.com/city-games-platform-team-service.azurewebsites.net";

export default {
  get: (url: string, config?: AxiosRequestConfig) =>
    http.get(baseUrl + url, config),
  post: (url: string, data?: any, config?: AxiosRequestConfig) =>
    http.post(baseUrl + url, data, config),
  put: (url: string, data?: any, config?: AxiosRequestConfig) =>
    http.put(baseUrl + url, data, config),
  delete: (url: string, config?: AxiosRequestConfig) =>
    http.delete(baseUrl + url, config)
};
