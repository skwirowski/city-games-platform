import api from "./gameDefinitionApi";
import gameDefinitionUuid from "../static/gameDefinitionUuid";
import { getUserData } from "./auth";

export const getRouteForTeamInGames = () => {
  const user = getUserData();
  return api.get(`/api/RouteForTeamInGames/${user.uuid},${gameDefinitionUuid}`);
};
