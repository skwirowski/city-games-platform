import routeServiceApi from "../services/routeServiceApi";

const deleteRoute = async (guid: string) => {
  const response = await routeServiceApi.delete(`/api/Route/${guid}`);
  return response;
};

export default deleteRoute;
