import api from "./gameDefinitionApi";

interface RouteForTeamInGamesValues {
  teamGuid: string,
  routeGuid: string,
  gameStartTimeForTeam: Date,
  gameDefinitionGuid: string
}

const createRouteForTeamInGame = (routeForTeamInGamesValues: RouteForTeamInGamesValues) => {
    return api.post("/api/RouteForTeamInGames", routeForTeamInGamesValues);
  };
  
  export default createRouteForTeamInGame;
  