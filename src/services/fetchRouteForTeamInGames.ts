import api from "./gameDefinitionApi";

const fetchRouteForTeamInGames = (teamUuid: string, gameUuid: string) => {
  return api.get(`/api/RouteForTeamInGames/${teamUuid},${gameUuid}`);
};

export default fetchRouteForTeamInGames;
