import api from "./gameDefinitionApi";
import gameDefinitionUuid from "../static/gameDefinitionUuid";

export const verifyGameDefinitionQrCode = (qrCodeContent: string) => {
  return api.get(
    `/api/GameDefinition/${gameDefinitionUuid}/qrcode/Verify?qrCodeContent=${encodeURIComponent(
      qrCodeContent
    )}`
  );
};
