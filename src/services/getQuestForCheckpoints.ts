import api from "./routeServiceApi";

export const getQuestForCheckpoints = (routeGuid?: string) => {
  if (routeGuid) return api.get(`/api/QuestForCheckpoints/${routeGuid}`);
  return api.get("/api/QuestForCheckpoints");
};
