import api from "./gameApi";
import { getUserData } from "./auth";
import gameDefinitionUuid from "../static/gameDefinitionUuid";

export const getGameGuidForTeam = () => {
  const user = getUserData();
  return api.get(`/api/Game/${gameDefinitionUuid},${user.uuid}`);
};
