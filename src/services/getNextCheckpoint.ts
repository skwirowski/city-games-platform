import api from "./gameApi";

export const getNextCheckpoint = (gameGuid: string) => {
  return api.get(`/api/Checkpoint/${gameGuid}/next`);
};
