import gameDefinitionApi from "../services/gameDefinitionApi";
import { FormValues } from "../components/GameDefinitionFormContainer/GameDefinitionForm/GameDefinitionForm";

const createGame = async (gameDefinitionValues: FormValues) => {
  try {
    const response = await gameDefinitionApi.post("/api/GameDefinition", gameDefinitionValues);
    return response;
  } catch (error) {
    console.log("TCL: error", JSON.stringify(error, null, 2));
  }
};
export default createGame;
