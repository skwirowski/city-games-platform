import api from "./gameDefinitionApi";

interface RouteForTeamInGamesValues {
  teamGuid: string,
  routeGuid: string,
  gameStartTimeForTeam: Date,
  gameDefinitionGuid: string
}

const updateRouteForTeamInGame = (routeForTeamInGamesValues: RouteForTeamInGamesValues) => {
    return api.put("/api/RouteForTeamInGames", routeForTeamInGamesValues);
  };
  
  export default updateRouteForTeamInGame;
  