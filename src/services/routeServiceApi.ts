import { http } from "./http";
import { AxiosRequestConfig } from "axios";
export const baseUrl = "https://cors-anywhere.herokuapp.com/city-games-platform-route-service-test.azurewebsites.net";

const apiKey = {
  headers: { common: { "X-API-KEY": "8a88891b-4782-4ab4-8dd4-169f2a4d9b44" } }
};

export default {
  get: (url: string, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.get(baseUrl + url, config);
  },
  post: (url: string, data?: any, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.post(baseUrl + url, data, config);
  },
  put: (url: string, data?: any, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.put(baseUrl + url, data, config);
  },
  delete: (url: string, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.delete(baseUrl + url, config);
  }
};
