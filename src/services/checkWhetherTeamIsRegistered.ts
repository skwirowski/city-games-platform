import api from "./teamServiceApi";

export const checkWhetherTeamIsRegistered = async (uuid: string) => {
  try {
    return await api.get(`/api/v1/teams/${uuid}`);
  } catch (error) {
    return false;
  }
};
