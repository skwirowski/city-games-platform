import api from "./gameDefinitionApi";

const fetchRouteForTeamInGames = async () => {
    const {data} = await api.get("/api/RouteForTeamInGames");
    return data;
  };
  
  export default fetchRouteForTeamInGames;
