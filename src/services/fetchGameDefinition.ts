import api from "./gameDefinitionApi";

const fetchGameDefinition = (gameUuid: string) => {
  return api.get(`/api/GameDefinition/${gameUuid}`);
};

export default fetchGameDefinition;
