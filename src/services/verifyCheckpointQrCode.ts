import api from "./routeServiceApi";

export const verifyCheckpointQrCode = (
  checkpointGuid: string,
  qrCodeContent: string
) => {
  return api.post(
    `/api/Checkpoints/${checkpointGuid}/qrcode/verify?qrCodeContent=${encodeURIComponent(
      qrCodeContent
    )}`
  );
};
