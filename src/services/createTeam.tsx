import api from "./teamServiceApi";

const createTeam = (data: any) => {
  return api.post(`/api/v1/teams`, data);
};

export default createTeam;
