import routeServiceApi from "../services/routeServiceApi";

const fetchCheckpoints = async ()=>{
    const {data } = await routeServiceApi.get("/api/Checkpoints");
    return data
}

export default fetchCheckpoints