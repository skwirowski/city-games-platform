import { http } from "../http";
import api, { baseUrl } from "../gameDefinitionApi";

http.get = jest.fn();
http.post = jest.fn();
http.put = jest.fn();
http.delete = jest.fn();
const url = "/myUrl";
const apiKey = {
  headers: { common: { 'X-API-KEY': 'ddc8bab0-b031-4ced-9fad-48f59c49083d' } }
};

describe("gameDefinitionApi", () => {
  it("should wrap http get to pass baseUrl", () => {
    const config = { ...apiKey, undefined };
    api.get(url);
    expect(http.get).toHaveBeenCalledWith(baseUrl + url, config);
  });
  it("should wrap http get to pass baseUrl and config", () => {
    const configAPI = { ...apiKey, undefined };
    const configHeaders = { headers: "header1" };
    const config = { ...configHeaders, ...configAPI }
    api.get(url, config);
    expect(http.get).toHaveBeenCalledWith(baseUrl + url, config);
  });
  it("should wrap http post to pass baseUrl", () => {
    const configAPI = { ...apiKey, undefined };
    const configHeaders = { headers: "header1" };
    const config = { ...configHeaders, ...configAPI }
    api.post(url);
    expect(http.post).toHaveBeenCalledWith(baseUrl + url, undefined, config);
  });
  it("should wrap http post to pass baseUrl, data and config", () => {
    const data = { item: "item1" };
    const configAPI = { ...apiKey, undefined };
    const configHeaders = { headers: "header1" };
    const config = { ...configHeaders, ...configAPI }
    api.post(url, data, config);
    expect(http.post).toHaveBeenCalledWith(baseUrl + url, data, config);
  });
  it("should wrap http put to pass baseUrl", () => {
    const configAPI = { ...apiKey, undefined };
    const configHeaders = { headers: "header1" };
    const config = { ...configHeaders, ...configAPI }
    api.put(url);
    expect(http.put).toHaveBeenCalledWith(baseUrl + url, undefined, config);
  });
  it("should wrap http put to pass baseUrl, data and config", () => {
    const data = { item: "item1" };
    const configAPI = { ...apiKey, undefined };
    const configHeaders = { headers: "header1" };
    const config = { ...configHeaders, ...configAPI }
    api.put(url, data, config);
    expect(http.put).toHaveBeenCalledWith(baseUrl + url, data, config);
  });
  it("should wrap http delete to pass baseUrl", () => {
    const config = { ...apiKey, undefined };
    api.delete(url);
    expect(http.delete).toHaveBeenCalledWith(baseUrl + url, config);
  });
  it("should wrap http delete to pass baseUrl and config", () => {
    const configAPI = { ...apiKey, undefined };
    const configHeaders = { headers: "header1" };
    const config = { ...configHeaders, ...configAPI }
    api.delete(url, config);
    expect(http.delete).toHaveBeenCalledWith(baseUrl + url, config);
  });
});
