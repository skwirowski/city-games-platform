import { http } from "../http";
import api, { baseUrl } from "../teamServiceApi";

http.get = jest.fn();
http.post = jest.fn();
http.put = jest.fn();
http.delete = jest.fn();
const url = "/myUrl";

describe("teamServiceApi", () => {
  it("should wrap http get to pass baseUrl", () => {
    api.get(url);
    expect(http.get).toHaveBeenCalledWith(baseUrl + url, undefined);
  });
  it("should wrap http get to pass baseUrl and config", () => {
    const config = { headers: "header1" };
    api.get(url, config);
    expect(http.get).toHaveBeenCalledWith(baseUrl + url, config);
  });
  it("should wrap http post to pass baseUrl", () => {
    api.post(url);
    expect(http.post).toHaveBeenCalledWith(baseUrl + url, undefined, undefined);
  });
  it("should wrap http post to pass baseUrl, data and config", () => {
    const data = { item: "item1" };
    const config = { headers: "header1" };
    api.post(url, data, config);
    expect(http.post).toHaveBeenCalledWith(baseUrl + url, data, config);
  });
  it("should wrap http put to pass baseUrl", () => {
    api.put(url);
    expect(http.put).toHaveBeenCalledWith(baseUrl + url, undefined, undefined);
  });
  it("should wrap http put to pass baseUrl, data and config", () => {
    const data = { item: "item1" };
    const config = { headers: "header1" };
    api.put(url, data, config);
    expect(http.put).toHaveBeenCalledWith(baseUrl + url, data, config);
  });
  it("should wrap http delete to pass baseUrl", () => {
    api.delete(url);
    expect(http.delete).toHaveBeenCalledWith(baseUrl + url, undefined);
  });
  it("should wrap http delete to pass baseUrl and config", () => {
    const config = { headers: "header1" };
    api.delete(url, config);
    expect(http.delete).toHaveBeenCalledWith(baseUrl + url, config);
  });
});
