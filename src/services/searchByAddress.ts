import axios from "axios";

const APP_ID = "DSoHqCkTkNVlfdp9c6Nk";
const APP_CODE = "IiyFSO2M5VvFswp1rb_Y1g";

export async function searchByAddress(address: string) {
  const { data }: any = await axios.get(
    `https://geocoder.api.here.com/6.2/geocode.json?app_id=${APP_ID}&app_code=${APP_CODE}&searchtext=${address}`
  );

  return data.Response.View;
}
