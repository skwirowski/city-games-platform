import { http } from "./http";
import { AxiosRequestConfig } from "axios";
export const baseUrl =
  "https://city-games-platform-game-definition-serv-test.azurewebsites.net";

const apiKey = {
  headers: { common: { "X-API-KEY": "ddc8bab0-b031-4ced-9fad-48f59c49083d" } }
};

export default {
  get: (url: string, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.get(baseUrl + url, config);
  },
  post: (url: string, data?: any, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.post(baseUrl + url, data, config);
  },
  put: (url: string, data?: any, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.put(baseUrl + url, data, config);
  },
  delete: (url: string, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.delete(baseUrl + url, config);
  }
};
