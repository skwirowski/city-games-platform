import azureB2C from "react-azure-adb2c";
import jwtDecode from "jwt-decode";

export const isLoggedIn = () => {
  return !!azureB2C.getAccessToken();
};

export const logout = () => {
  azureB2C.signOut();
};

export const getUserData = () => {
  const decoded = jwtDecode(azureB2C.getAccessToken());
  return {
    name: (decoded as any).name,
    email: (decoded as any).emails[0],
    uuid: (decoded as any).oid
  };
};

export const getToken = () => azureB2C.getAccessToken();

const getUserFlow = () => {
  const urlHash: string = sessionStorage.getItem("msal.urlHash") || "";
  if (urlHash.includes("error_description=AADB2C90118")) return "B2C_1_reset";
  return "B2C_1_login";
};

export const initAzure = () => {
  const userFlow = getUserFlow();
  azureB2C.initialize({
    instance: "https://login.microsoftonline.com/tfp/",
    tenant: "citygamesplatform.onmicrosoft.com",
    signInPolicy: userFlow,
    applicationId: "cfe9c0ee-f382-4539-a0f7-94f0fd359290",
    cacheLocation: "sessionStorage",
    scopes: [
      "https://citygamesplatform.onmicrosoft.com/team-service/user_impersonation"
    ],
    redirectUri: process.env.REACT_APP_AZURE_REDIRECT_URL,
    postLogoutRedirectUri: window.location.origin
  });
};
