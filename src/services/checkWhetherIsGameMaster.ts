import api from "./teamServiceApi";

export const checkWhetherIsGameMaster = async (uuid: string) => {
  try {
    return await api.get(`/api/v1/gameMasters/${uuid}`);
  } catch (error) {
    return false;
  }
};
