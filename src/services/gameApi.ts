import { http } from "./http";
import { AxiosRequestConfig } from "axios";
export const baseUrl =
  "https://city-games-platform-game-service-test.azurewebsites.net";

const apiKey = {
  headers: { common: { "X-API-KEY": "12b3ec0b-a615-47e4-900c-f59b9a10798d" } }
};

export default {
  get: (url: string, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.get(baseUrl + url, config);
  },
  post: (url: string, data?: any, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.post(baseUrl + url, data, config);
  },
  put: (url: string, data?: any, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.put(baseUrl + url, data, config);
  },
  delete: (url: string, config?: AxiosRequestConfig) => {
    config = { ...apiKey, ...config };
    return http.delete(baseUrl + url, config);
  }
};
