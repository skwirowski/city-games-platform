import routeServiceApi from "../services/routeServiceApi";

const createRoute = async (guidList: Array<string>) => {
  const response = await routeServiceApi.post("/api/Route", guidList);
  return response;
};

export default createRoute;
