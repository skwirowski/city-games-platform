import api from "./teamServiceApi";

const updateTeam = (data: any) => {
  return api.put(`/api/v1/teams`, data);
};

export default updateTeam;
