import api from "./gameApi";
import gameDefinitionUuid from "../static/gameDefinitionUuid";
import { getUserData } from "./auth";

export const createGameForTeam = (checkpoints: Array<Object>) => {
  const user = getUserData();
  const body = {
    teamGuid: user.uuid,
    gameDefinitionGuid: gameDefinitionUuid,
    checkpoints
  };
  return api.post(`/api/Game`, body);
};
