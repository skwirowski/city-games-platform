import api from "./teamServiceApi";

const fetchTeam = (uuid: any) => {
  return api.get(`/api/v1/teams/${uuid}`);
};

export default fetchTeam;
