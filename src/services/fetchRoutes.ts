import routeServiceApi from "./routeServiceApi";

const fetchRoutes = async () => {
  const response = await routeServiceApi.get("/api/Route");
  return response;
};

export default fetchRoutes;
