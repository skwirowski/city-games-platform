import axios, { AxiosRequestConfig } from "axios";
import { getToken } from "./auth";

let instance = axios.create({});

instance.interceptors.request.use((config: AxiosRequestConfig) => {
  const token = getToken();
  config.headers.authorization = `Bearer ${token}`;
  return config;
});

export const http = instance;
