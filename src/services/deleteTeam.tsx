import api from "./teamServiceApi";

const deleteTeam = (uuid: any) => {
  return api.delete(`/api/v1/teams/${uuid}`);
};

export default deleteTeam;
