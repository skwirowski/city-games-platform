import http from './gameDefinitionApi';

const deleteGameDefinition = (guid: string) => {
  return http.delete(`/api/GameDefinition/${guid}`);
};

export default deleteGameDefinition;