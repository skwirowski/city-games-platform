import api from "./teamServiceApi";

const fetchTeamsInGame = async (uuid: any) => {
  const {data} =  await api.get(`/api/v1/teams/game/${uuid}`);
  return data;
};

export default fetchTeamsInGame;
