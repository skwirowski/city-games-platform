import { message } from "antd";
import Intl from "../utils/Intl";

message.config({
  duration: 2,
  maxCount: 1
});

const messages = {
  route: {
    routeCreatedLengthError: () => message.error(Intl.messages["RouteCreator.routeCreatedLengthError"]),
    routeCreatedNameEmptyError: () => message.error(Intl.messages["RouteCreator.routeCreatedNameEmptyError"]),
    routeCreatedNameLengthError: () => message.error(Intl.messages["RouteCreator.routeCreatedNameLengthError"])
  }
};

export default messages;
