import { openNotificationWithIcon } from "../utils/openNotificationWithIcon";
import Intl from "../utils/Intl";

const notifications = {
  gamedefinition: {
    gameCreatedSuccess: () => openNotificationWithIcon("success", Intl.messages["GameDefinitionForm.gameCreatedSuccess"], ""),
    gameCreatedError: () => openNotificationWithIcon("error", Intl.messages["GameDefinitionForm.gameCreatedError"], ""),
    gameDeletedSuccess: () => openNotificationWithIcon("warning", Intl.messages["GameDefinitionsList.gameDeleteWarning"], ""),
    gameDeletedError: () => openNotificationWithIcon("error", Intl.messages["GameDefinitionsList.gameDeleteError"], ""),
    gameActivatedSuccess: () => openNotificationWithIcon("success", Intl.messages["GameDefinitionsList.activateGameSuccess"], ""),
    gameActivatedWarning: () => openNotificationWithIcon("warning", Intl.messages["GameDefinitionsList.activateGameWarning"], ""),
    gameActivatedError: () => openNotificationWithIcon("error", Intl.messages["GameDefinitionsList.activateGameError"], ""),
    gameDeactivatedSuccess: () => openNotificationWithIcon("success", Intl.messages["GameDefinitionsList.deactivateGameSuccess"], ""),
    gameDeactivatedError: () => openNotificationWithIcon("error", Intl.messages["GameDefinitionsList.deactivateGameError"], "")
  },
  checkpoint: {
    checkpointCreatedSuccess: () => openNotificationWithIcon("success", Intl.messages["CheckpointForm.checkpointCreatedSuccess"], ""),
    checkpointCreatedError: () => openNotificationWithIcon("error", Intl.messages["CheckpointForm.checkpointCreatedError"], ""),
    checkpointUpdatedSuccess: () => openNotificationWithIcon("success", Intl.messages["CheckpointForm.checkpointUpdatedSuccess"], ""),
    checkpointUpdatedError: () => openNotificationWithIcon("error", Intl.messages["CheckpointForm.checkpointUpdatedError"], ""),
    checkpointDeletedSuccess: () => openNotificationWithIcon("success", Intl.messages["CheckpointsList.deletedCheckpointSuccess"], ""),
    deletedCheckpointError: () => openNotificationWithIcon("error", Intl.messages["CheckpointsList.deletedCheckpointError"], "")
  },
  route: {
    routeCreatedSuccess: () => openNotificationWithIcon("success", Intl.messages["RouteCreator.routeCreatedSuccess"], ""),
    routeCreatedError: () => openNotificationWithIcon("error", Intl.messages["RouteCreator.routeCreatedError"], ""),
    routeDeletedSuccess: () => openNotificationWithIcon("success", Intl.messages["RouteCreator.routeDeletedSuccess"], ""),
    routeDeletedError: () => openNotificationWithIcon("error", Intl.messages["RouteCreator.routeDeletedError"], ""),
    routeUpdatedSuccess: () => openNotificationWithIcon("success", Intl.messages["RouteCreator.routeUpdatedSuccess"], ""),
    routeUpdatedError: () => openNotificationWithIcon("error", Intl.messages["RouteCreator.routeUpdatedError"], "")
  },
  quest: {
    questCreatedSuccess: () => openNotificationWithIcon("success", Intl.messages["AddQuest.success"], ""),
    questCreatedError: () => openNotificationWithIcon("error", Intl.messages["AddQuest.error"], "")
  }
};

export default notifications;
